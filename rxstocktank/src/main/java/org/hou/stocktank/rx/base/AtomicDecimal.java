package org.hou.stocktank.rx.base;


import org.ta4j.core.Decimal;

import java.math.BigInteger;
import java.util.Comparator;
import java.util.concurrent.atomic.AtomicReference;

public class AtomicDecimal extends Number implements Comparable<Object>{
    AtomicReference<Decimal> value;

    // Constructors
    public AtomicDecimal() {
        this(0.0);
    }

    public AtomicDecimal(double initVal) {
        this(Decimal.valueOf(initVal));
    }

    public AtomicDecimal(Decimal initVal) {
        value = new AtomicReference<Decimal>(initVal);
    }

    public AtomicDecimal(Object initVal) {
        this(objectToDecimal(initVal));
    }

    // Atomic methods
    public Decimal get() {
        return value.get();
    }

    public void set(Decimal newVal) {
        value.set(newVal);
    }

    public void set(Object newVal) {
        set(objectToDecimal(newVal));
    }

    public void set(double newVal) {
        set(Decimal.valueOf(newVal));
    }

    public void lazySet(Decimal newVal) {
        set(newVal);
    }

    public void lazySet(Object newVal) {
        set(newVal);
    }

    public void lazySet(double newVal) {
        set(newVal);
    }

    public boolean compareAndSet(Decimal expect, Decimal update) {
        while (true) {
            Decimal origVal = get();

            if (origVal.compareTo(expect) == 0) {
                if (value.compareAndSet(origVal, update))
                    return true;
            } else {
                return false;
            }
        }
    }

    public boolean compareAndSet(double expect, double update) {
        return compareAndSet(Decimal.valueOf(expect), Decimal.valueOf(update));
    }

    public boolean compareAndSet(Object expect, Object update) {
        return compareAndSet(objectToDecimal(expect),
                objectToDecimal(update));
    }

    public boolean weakCompareAndSet(Decimal expect, Decimal update) {
        return compareAndSet(expect, update);
    }

    public boolean weakCompareAndSet(double expect, double update) {
        return compareAndSet(expect, update);
    }

    public boolean weakCompareAndSet(Object expect, Object update) {
        return compareAndSet(expect, update);
    }

    public Decimal getAndSet(Decimal setVal) {
        while (true) {
            Decimal origVal = get();

            if (compareAndSet(origVal, setVal)) return origVal;
        }
    }

    public double getAndSet(double setVal) {
        return getAndSet(Decimal.valueOf(setVal)).toDouble();
    }

    public Decimal getAndSet(Object setVal) {
        return getAndSet(objectToDecimal(setVal));
    }

    public Decimal getAndAdd(Decimal delta) {
        while (true) {
            Decimal origVal = get();
            Decimal newVal = origVal.plus(delta);
            if (compareAndSet(origVal, newVal)) return origVal;
        }
    }

    public Decimal addAndGet(Decimal delta) {
        while (true) {
            Decimal origVal = get();
            Decimal newVal = origVal.plus(delta);
            if (compareAndSet(origVal, newVal)) return newVal;
        }
    }

    public double getAndAdd(double delta) {
        return getAndAdd(Decimal.valueOf(delta)).toDouble();
    }

    public double addAndGet(double delta) {
        return addAndGet(Decimal.valueOf(delta)).toDouble();
    }

    public Decimal getAndAdd(Object delta) {
        return getAndAdd(objectToDecimal(delta));
    }

    public Decimal addAndGet(Object delta) {
        return addAndGet(objectToDecimal(delta));
    }

    public Decimal getAndIncrement() {
        return getAndAdd(Decimal.ONE);
    }

    public Decimal getAndDecrement() {
        return getAndAdd(Decimal.valueOf(-1));
    }

    public Decimal incrementAndGet() {
        return addAndGet(Decimal.ONE);
    }

    public Decimal decrementAndGet() {
        return addAndGet(Decimal.valueOf(-1));
    }

    public double altGetAndIncrement() {
        return getAndIncrement().toDouble();
    }

    public double altGetAndDecrement() {
        return getAndDecrement().toDouble();
    }

    public double altIncrementAndGet() {
        return incrementAndGet().toDouble();
    }

    public double altDecrementAndGet() {
        return decrementAndGet().toDouble();
    }

    public Decimal getAndMultiply(Decimal multiplicand) {
        while (true) {
            Decimal origVal = get();
            Decimal newVal = origVal.multipliedBy(multiplicand);
            if (compareAndSet(origVal, newVal)) return origVal;
        }
    }

    public Decimal multiplyAndGet(Decimal multiplicand) {
        while (true) {
            Decimal origVal = get();
            Decimal newVal = origVal.multipliedBy(multiplicand);
            if (compareAndSet(origVal, newVal)) return newVal;
        }
    }
    public Decimal getAndDivide(Decimal divisor) {
        while (true) {
            Decimal origVal = get();
            Decimal newVal = origVal.dividedBy(divisor);
            if (compareAndSet(origVal, newVal)) return origVal;
        }
    }

    public Decimal divideAndGet(Decimal divisor) {
        while (true) {
            Decimal origVal = get();
            Decimal newVal = origVal.dividedBy(divisor);
            if (compareAndSet(origVal, newVal)) return newVal;
        }
    }

    public Decimal getAndMultiply(Object multiplicand) {
        return getAndMultiply(objectToDecimal(multiplicand));
    }

    public Decimal multiplyAndGet(Object multiplicand) {
        return multiplyAndGet(objectToDecimal(multiplicand));
    }

    public Decimal getAndDivide(Object divisor) {
        return getAndDivide(objectToDecimal(divisor));
    }

    public Decimal divideAndGet(Object divisor) {
        return divideAndGet(objectToDecimal(divisor));
    }

    // Methods of the Number class
    @Override
    public int intValue() {
        return (int)getDecimalValue().toDouble();
    }

    @Override
    public long longValue() {
        return (long)getDecimalValue().toDouble();
    }

    @Override
    public float floatValue() {
        return (float)getDecimalValue().toDouble();
    }

    @Override
    public double doubleValue() {
        return getDecimalValue().toDouble();
    }

    @Override
    public byte byteValue() {
        return (byte)intValue();
    }

    @Override
    public short shortValue() {
        return (short)intValue();
    }

    public char charValue() {
        return (char)intValue();
    }

    public Decimal getDecimalValue() {
        return get();
    }

    public Double getDoubleValue() {
        return Double.valueOf(doubleValue());
    }

    public boolean isNaN() {
        return getDoubleValue().isNaN();
    }

    public boolean isInfinite() {
        return getDoubleValue().isInfinite();
    }

    // Methods of the Decimal Class
    public Decimal abs() {
        while (true) {
            Decimal origVal = get();
            Decimal newVal = origVal.abs();
            if (compareAndSet(origVal, newVal)) return newVal;
        }
    }

    public Decimal max(Decimal val) {
        while (true) {
            Decimal origVal = get();
            Decimal newVal = origVal.max(val);
            if (compareAndSet(origVal, newVal)) return newVal;
        }
    }

    public Decimal min(Decimal val) {
        while (true) {
            Decimal origVal = get();
            Decimal newVal = origVal.min(val);
            if (compareAndSet(origVal, newVal)) return newVal;
        }
    }

    public Decimal negate() {
        while (true) {
            Decimal origVal = get();
            Decimal newVal = origVal.multipliedBy(Decimal.valueOf(-1));
            if (compareAndSet(origVal, newVal)) return newVal;
        }
    }

    public Decimal pow(int n) {
        while (true) {
            Decimal origVal = get();
            Decimal newVal = origVal.pow(n);
            if (compareAndSet(origVal, newVal)) return newVal;
        }
    }

    // Support methods for hashing and comparing
    @Override
    public String toString() {
        return get().toString();
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) return false;
        try {
            return (compareTo(obj) == 0);
        } catch (Exception e) {
            return false;
        }
    }

    @Override
    public int hashCode() {
        return get().hashCode();
    }

    public int compareTo(AtomicDecimal aValue) {
        return get().compareTo(aValue.get());
    }

    public int compareTo(Object aValue) {
        return get().compareTo(objectToDecimal(aValue));
    }

    public int compareTo(double aValue) {
        return get().compareTo(Decimal.valueOf(aValue));
    }

    public static AtomicDoubleComparator comparator =
            new AtomicDoubleComparator();
    public static class AtomicDoubleComparator
            implements Comparator<Object> {
        public int compare(AtomicDecimal d1, AtomicDecimal d2) {
            return d1.compareTo(d2);
        }

        public int compare(Object d1, Object d2) {
            return objectToDecimal(d1).compareTo(objectToDecimal(d2));
        }

        public int compareReverse(AtomicDecimal d1, AtomicDecimal d2) {
            return d2.compareTo(d1);
        }

        public int compareReverse(Object d1, Object d2) {
            return objectToDecimal(d2).compareTo(objectToDecimal(d1));
        }
    }

    // Support Routines and constants
    private static Decimal objectToDecimal(Object obj) {
        if (obj instanceof AtomicDecimal) {
            return ((AtomicDecimal) obj).get();
        } if (obj instanceof Decimal) {
            return (Decimal) obj;
        } else if (obj instanceof BigInteger) {
            return Decimal.valueOf((int)obj); // FIXME BigInteger to int cast
        } else if (obj instanceof String) {
            return Decimal.valueOf((String) obj);
        } else if (obj instanceof Number) {
            return Decimal.valueOf(((Number)obj).doubleValue());
        } else {
            return Decimal.valueOf(obj.toString());
        }
    }
}
