package org.hou.stocktank.rx.base;


import io.reactivex.Flowable;
import org.ta4j.core.Tick;

public class StockSource {
    private final String stockName;
    private final Flowable<Tick> data;

    public StockSource(String stockName, Flowable<Tick> data) {
        this.stockName = stockName;
        this.data = data;
    }

    public String getStockName() {
        return stockName;
    }

    public Flowable<Tick> getData() {
        return data;
    }
}
