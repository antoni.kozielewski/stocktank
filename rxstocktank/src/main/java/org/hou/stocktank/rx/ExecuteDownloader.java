package org.hou.stocktank.rx;

import org.hou.stocktank.rx.providers.bossa.BossaSingleFileDownloader;

import java.io.IOException;

/**
 * Created by Ant on 2018-06-18.
 */
public class ExecuteDownloader {
    private static final String SOURCE_FILENAME = "data/mstall.zip";

    public static void main(String[] args) throws IOException {
        System.out.println(" DOWNLOAD Bossa File ....");
        BossaSingleFileDownloader downloader = new BossaSingleFileDownloader();
        downloader.setDataPath(SOURCE_FILENAME);
        downloader.run();
        System.out.println(" FILE IS READY !!!!!!!!!!!");
    }
}
