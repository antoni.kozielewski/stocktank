package org.hou.stocktank.rx.indicators;

import io.reactivex.Flowable;
import org.ta4j.core.Decimal;
import org.ta4j.core.Tick;

public class RxStochasticOscillatorDIndicator implements RxIndicator<Tick,Decimal> {
    private final int dTimeFrame;
    private RxStochasticOscillatorKIndicator kIndicator;
    private RxSMAIndicator smaIndicator;

    public RxStochasticOscillatorDIndicator(RxStochasticOscillatorKIndicator kIndicator) {
        this(kIndicator, 3);
    }

    public RxStochasticOscillatorDIndicator(RxStochasticOscillatorKIndicator kIndicator, int dTimeFrame) {
        this.kIndicator = kIndicator;
        this.dTimeFrame = dTimeFrame;
        smaIndicator = new RxSMAIndicator(dTimeFrame);
    }

    @Override
    public Flowable<Decimal> calculate(Flowable<Tick> inputData) {
        return smaIndicator.calculate(kIndicator.calculate(inputData));
    }

}
