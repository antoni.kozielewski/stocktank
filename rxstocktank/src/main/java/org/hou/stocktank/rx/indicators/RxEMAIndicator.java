package org.hou.stocktank.rx.indicators;

import io.reactivex.Flowable;
import org.hou.stocktank.rx.base.AtomicDecimal;
import org.hou.stocktank.rx.base.RingBuffer;
import org.ta4j.core.Decimal;

import java.util.concurrent.atomic.AtomicInteger;

public class RxEMAIndicator implements RxIndicator<Decimal, Decimal> {
    private final int timeframe;
    private final Decimal multiplier;

    public RxEMAIndicator(int timeframe) {
        this.timeframe = timeframe;
        this.multiplier = Decimal.TWO.dividedBy(Decimal.valueOf(timeframe + 1));
    }

    @Override
    public Flowable<Decimal> calculate(Flowable<Decimal> inputData) {
        RingBuffer<Decimal> buffer = new RingBuffer<>(timeframe);
        AtomicDecimal emaPrev = new AtomicDecimal(0);
        AtomicInteger counter = new AtomicInteger(0);
        return inputData
                .map(value -> {
                    if (counter.getAndIncrement() < timeframe-1) {
                        buffer.put(value);
                        emaPrev.set( buffer
                                .stream()
                                .reduce(Decimal.valueOf(0), (a, b) -> a.plus(b))
                                .dividedBy(Decimal.valueOf(buffer.getSize())));
                        return emaPrev.get();
                    }
                    counter.incrementAndGet();
                    Decimal result = value.minus(emaPrev.get()).multipliedBy(multiplier).plus(emaPrev.get());
                    emaPrev.set(result);
                    return result;
                });
    }

}
