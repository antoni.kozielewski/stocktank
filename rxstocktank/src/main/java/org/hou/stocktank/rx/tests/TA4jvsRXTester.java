package org.hou.stocktank.rx.tests;


import io.reactivex.Flowable;
import io.reactivex.schedulers.Schedulers;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hou.stocktank.rx.indicators.RxIndicator;
import org.hou.stocktank.rx.providers.bossa.RxBossaSingleFileDataProvider;
import org.hou.stocktank.services.BossaSingleFileDataProviderService;
import org.ta4j.core.Indicator;
import org.ta4j.core.Tick;
import org.ta4j.core.TimeSeries;

import java.util.Arrays;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Function;

public class TA4jvsRXTester {
    private static Log LOGGER = LogFactory.getLog(TA4jvsRXTester.class);
    private static final String SOURCE_FILENAME = "data/mstall.zip";
    private static final List<String> stockNames = Arrays.asList("JUJUBEE", "CDPROJEKT", "KGHM", "WIG20", "AMICA", "KRUK", "CEZ", "ATAL", "PGE", "TAURONPE", "ENEA", "VIVID", "MBANK", "FARM51", "NEUCA", "WIELTON", "11BIT");
    BossaSingleFileDataProviderService dataProviderService = new BossaSingleFileDataProviderService();
    RxBossaSingleFileDataProvider rxDataProvider = new RxBossaSingleFileDataProvider(SOURCE_FILENAME);

    public TA4jvsRXTester() {
        dataProviderService = new BossaSingleFileDataProviderService();
        dataProviderService.setDataPath(SOURCE_FILENAME);
    }

    private TimeSeries getTimeSeries(String stockName) {
        return dataProviderService.load(stockName);
    }

    private Flowable<Tick> getTickStream(String stockName) {
        return rxDataProvider.getStream(stockName);
    }

    private <I, T> Boolean test(Indicator<T> ta4jIndicator, Function<Tick, I> rxStreamPrepareFunction, RxIndicator<I, T> rxIndicator) {
       rxDataProvider.getStocks(stockNames)
               .parallel()
               .runOn(Schedulers.computation())
               .map(stockSource -> {
                   AtomicInteger index = new AtomicInteger(0);
                   rxIndicator.calculate(stockSource.getData().map(rxStreamPrepareFunction::apply))
                           .map(rxValue -> {
                                T ta4jValue = ta4jIndicator.getValue(index.getAndIncrement());
                                if(!rxValue.equals(ta4jValue)) {
                                    StringBuilder sb = new StringBuilder();
                                    sb.append("ERROR !!! ");
                                    sb.append(ta4jIndicator.getClass().getName() + " {" + ta4jValue + "} ");
                                    sb.append(" VS ");
                                    sb.append(rxIndicator.getClass().getName() + " {" + rxValue + "} | ");
                                    sb.append(" stock=" + stockSource.getStockName() + " | ");
                                    sb.append(" tick=" + index);
                                    throw new RuntimeException(sb.toString());
                                }
                                return Boolean.TRUE;
                           });
                   return stockSource.getStockName() + " | " + rxIndicator.getClass().getName() + " :: OK " ;
               })
               .sequential()
               .subscribe(System.out::println);
       return true;
    }


    public <I, T> Boolean test(Function<TimeSeries, Indicator<T>> indicatorCreationFunction, Function<Tick, I> rxStreamPrepareFunction, RxIndicator<I, T> rxIndicator) {
        Flowable.fromIterable(stockNames)
                .parallel(10)
                .map(stockName -> test(indicatorCreationFunction.apply(getTimeSeries(stockName)), rxStreamPrepareFunction, rxIndicator))
                .sequential()
                .subscribe(b -> System.out.print(""));
        return true;
    }

}
