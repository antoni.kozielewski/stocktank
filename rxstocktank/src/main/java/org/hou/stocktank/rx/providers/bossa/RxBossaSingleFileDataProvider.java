package org.hou.stocktank.rx.providers.bossa;

import io.reactivex.Flowable;
import org.hou.stocktank.rx.base.IterableEnumeration;
import org.hou.stocktank.rx.base.StockSource;
import org.hou.stocktank.rx.providers.DataProvider;
import org.slf4j.LoggerFactory;
import org.ta4j.core.BaseTick;
import org.ta4j.core.Decimal;
import org.ta4j.core.Tick;

import java.io.*;
import java.time.Duration;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Enumeration;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;


public class RxBossaSingleFileDataProvider implements DataProvider {
    private static final org.slf4j.Logger logger = LoggerFactory.getLogger(BossaSingleFileDownloader.class);
    private String dataPath;
    private String dataFileExtension = ".mst";
    private final Character ESCAPE_CHAR = '\\';
    private final Character QUOTE_CHAR = '"';
    private final Character SEPARATOR_CHAR = ',';


    public RxBossaSingleFileDataProvider() {
        super();
    }

    public RxBossaSingleFileDataProvider(String filePath) {
        super();
        this.dataPath = filePath;
    }

    @Override
    public Flowable<StockSource> getStocks() {
        return getStocksNames().map(stockName -> new StockSource(stockName, getStream(stockName)));
    }

    @Override
    public Flowable<StockSource> getStocks(String... stocks) {
        return getStocks(Arrays.asList(stocks));
    }

    @Override
    public Flowable<StockSource> getStocks(List<String> stocks) {
        return getStocksNames().filter(stockName -> stocks.contains(stockName)).map(stockName -> new StockSource(stockName, getStream(stockName)));
    }

    public Flowable<Tick> getStream(String stockName) {
        try {
            InputStream inputStream = getInputStream(stockName);

            return Flowable.using(
                    () -> new BufferedReader(new FileReader(this.dataPath)),
                    reader -> Flowable.fromIterable(() -> reader.lines().iterator()),
                    reader -> reader.close())
                    .map(line -> parseLine(line));

        } catch (IOException e) {
            throw new RuntimeException(e); // TODO ?? or exception wrapper ?
        }
    }

    public Flowable<String> getStocksNames() {
        try {
            File f = new File(dataPath);
            ZipFile zipFile = new ZipFile(f);
            Enumeration<? extends ZipEntry> entries = zipFile.entries();
            return Flowable.fromIterable(new IterableEnumeration<>(entries))
                    .filter(entry -> !entry.isDirectory())
                    .map(entry -> entry.getName())
                    .map(name -> name.substring(0, name.indexOf(".")));
        } catch (IOException e) {
            throw new RuntimeException(e); // TODO maybe there should be exception wrapper
        }
    }

    public String getDataPath() {
        return dataPath;
    }

    public String getDataFileExtension() {
        return dataFileExtension;
    }

    public void setDataPath(String dataPath) {
        this.dataPath = dataPath;
    }

    public void setDataFileExtension(String dataFileExtension) {
        this.dataFileExtension = dataFileExtension;
    }

    private InputStream getInputStream(String stockName) throws IOException {
        File f = new File(dataPath);
        ZipFile zipFile = new ZipFile(f);
        Enumeration<? extends ZipEntry> entries = zipFile.entries();
        while (entries.hasMoreElements()) {
            ZipEntry entry = entries.nextElement();
            if (!entry.isDirectory()) {
                if (entry.getName().toLowerCase().equals(stockName.toLowerCase() + dataFileExtension)) {
                    logger.debug("found : {}", stockName.toLowerCase() + dataFileExtension);
                    return zipFile.getInputStream(entry);
                }
            }
        }
        logger.error("file not found: {}/{}", dataPath, stockName.toLowerCase() + dataFileExtension);
        return null;
    }

    // format:
    //DTYYYYMMDD
    private ZonedDateTime parseDateTime(String dateField) {
        Integer year = Integer.valueOf(dateField.substring(0, 4));
        Integer month = Integer.valueOf(dateField.substring(4, 6));
        Integer day = Integer.valueOf(dateField.substring(6, 8));
        return ZonedDateTime.of(year, month, day, 0, 0, 0, 0, ZoneId.systemDefault());
    }

    // format:
    //<TICKER>,<DTYYYYMMDD>,<OPEN>,<HIGH>,<LOW>,<CLOSE>,<VOL>
    private Tick parseLine(String[] line) {
        ZonedDateTime date = parseDateTime(line[1]);
        Decimal open = Decimal.valueOf(line[2]);
        Decimal high = Decimal.valueOf(line[3]);
        Decimal low = Decimal.valueOf(line[4]);
        Decimal close = Decimal.valueOf(line[5]);
        Decimal volume = Decimal.valueOf(line[6]);
        return new BaseTick(Duration.ofHours(8), date.withHour(17), open, high, low, close, volume);
    }

    private Tick parseLine(String line) {
        return parseLine(splitLine(line));
    }

    private boolean isEscapable(String nextLine, boolean inQuotes, int i) {
        return inQuotes && nextLine.length() > i + 1 && (nextLine.charAt(i + 1) == QUOTE_CHAR || nextLine.charAt(i + 1) == ESCAPE_CHAR);
    }

    private boolean isEscapedQuote(String nextLine, boolean inQuotes, int i) {
        return inQuotes && nextLine.length() > i + 1 && nextLine.charAt(i + 1) == QUOTE_CHAR;
    }

    private String[] splitLine(String line) {
        if (line == null) {
            return null;
        } else {
            ArrayList tokensOnThisLine = new ArrayList();
            StringBuilder sb = new StringBuilder(64);
            boolean inQuotes = false;

            do {
                if (inQuotes) {
                    return new String[]{};
                }

                for (int i = 0; i < line.length(); ++i) {
                    char c = line.charAt(i);
                    if (c == ESCAPE_CHAR) {
                        if (this.isEscapable(line, inQuotes, i)) {
                            sb.append(line.charAt(i + 1));
                            ++i;
                        } else {
                            ++i;
                        }
                    } else if (c == QUOTE_CHAR) {
                        if (this.isEscapedQuote(line, inQuotes, i)) {
                            sb.append(line.charAt(i + 1));
                            ++i;
                        } else {
                            inQuotes = !inQuotes;
                            if (i > 2 && line.charAt(i - 1) != SEPARATOR_CHAR && line.length() > i + 1 && line.charAt(i + 1) != SEPARATOR_CHAR) {
                                sb.append(c);
                            }
                        }
                    } else if (c == SEPARATOR_CHAR && !inQuotes) {
                        tokensOnThisLine.add(sb.toString());
                        sb = new StringBuilder(64);
                    } else {
                        sb.append(c);
                    }
                }
            } while (inQuotes);

            tokensOnThisLine.add(sb.toString());
            return (String[]) tokensOnThisLine.toArray(new String[0]);
        }
    }

}
