package org.hou.stocktank.rx.providers;


import io.reactivex.Flowable;
import org.hou.stocktank.rx.base.StockSource;

import java.util.List;

/**
 * provide StockSources
 */
public interface DataProvider {
    Flowable<StockSource> getStocks();

    Flowable<StockSource> getStocks(String... stocks);

    Flowable<StockSource> getStocks(List<String> stocks);
}
