package org.hou.stocktank.rx.indicators;

import io.reactivex.Flowable;
import org.ta4j.core.Decimal;

import java.util.concurrent.atomic.AtomicInteger;

public class RxAverageLossIndicator implements RxIndicator<Decimal,Decimal> {
    private int timeFrame;

    public RxAverageLossIndicator(int timeFrame) {
        this.timeFrame = timeFrame;
    }

    @Override
    public Flowable<Decimal> calculate(Flowable<Decimal> inputData) {
        AtomicInteger counter = new AtomicInteger(0);
        return new RxCumulatedLossesIndicator(timeFrame)
                .calculate(inputData)
                .map(loss -> {
                    if (counter.incrementAndGet()<timeFrame) {
                        return loss.dividedBy(Decimal.valueOf(counter.get()));
                    } else {
                        return loss.dividedBy(Decimal.valueOf(timeFrame));
                    }
                });
    }
}
