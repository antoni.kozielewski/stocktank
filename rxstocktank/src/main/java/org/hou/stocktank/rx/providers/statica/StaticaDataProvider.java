package org.hou.stocktank.rx.providers.statica;

import io.reactivex.Flowable;
import org.hou.stocktank.rx.base.StockSource;
import org.hou.stocktank.rx.providers.DataProvider;

import java.util.List;

public class StaticaDataProvider implements DataProvider {

    @Override
    public Flowable<StockSource> getStocks() {
        return null;
    }

    @Override
    public Flowable<StockSource> getStocks(String... stocks) {
        return null;
    }

    @Override
    public Flowable<StockSource> getStocks(List<String> stocks) {
        return null;
    }
}
