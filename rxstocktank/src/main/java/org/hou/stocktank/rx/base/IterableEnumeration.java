package org.hou.stocktank.rx.base;

import java.util.Enumeration;
import java.util.Iterator;


public class IterableEnumeration<E> implements Iterable<E> {
    private Enumeration<? extends E> enumeration;

    public IterableEnumeration(Enumeration<? extends E> enumeration) {
        this.enumeration = enumeration;
    }

    @Override
    public Iterator<E> iterator() {
        return new Iterator<E>() {

            @Override
            public boolean hasNext() {
                return enumeration.hasMoreElements();
            }

            @Override
            public E next() {
                return enumeration.nextElement();
            }
        };

    }
}
