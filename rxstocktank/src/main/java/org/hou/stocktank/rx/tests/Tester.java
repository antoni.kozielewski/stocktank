package org.hou.stocktank.rx.tests;


import org.hou.stocktank.rx.indicators.*;
import org.ta4j.core.Tick;
import org.ta4j.core.indicators.*;
import org.ta4j.core.indicators.helpers.*;

public class Tester {
    public static void main(String[] args) {
        System.out.println("---------- START -------------");
        TA4jvsRXTester tester = new TA4jvsRXTester();

        tester.test(
                t -> new StochasticOscillatorDIndicator(new StochasticOscillatorKIndicator(t, 20)),
                tick -> tick,
                new RxStochasticOscillatorDIndicator(new RxStochasticOscillatorKIndicator(20))
        );

        tester.test(
                t -> new StochasticOscillatorKIndicator(t, 20),
                tick -> tick,
                new RxStochasticOscillatorKIndicator(20)
        );

        tester.test(
                t -> new LowestValueIndicator(new ClosePriceIndicator(t), 20),
                Tick::getClosePrice,
                new RxLowestValueIndicator(20)
        );

        tester.test(
                t -> new HighestValueIndicator(new ClosePriceIndicator(t), 20),
                Tick::getClosePrice,
                new RxHighestValueIndicator(20)
        );

        tester.test(
                t -> new RSIIndicator(new ClosePriceIndicator(t), 20),
                Tick::getClosePrice,
                new RxRSIIndicator(20)
        );

        tester.test(
                t -> new AverageGainIndicator(new ClosePriceIndicator(t), 20),
                Tick::getClosePrice,
                new RxAverageGainIndicator(20)
        );

        tester.test(
                t -> new AverageLossIndicator(new ClosePriceIndicator(t), 20),
                Tick::getClosePrice,
                new RxAverageLossIndicator(20)
        );

        tester.test(
                t -> new CumulatedGainsIndicator(new ClosePriceIndicator(t), 20),
                Tick::getClosePrice,
                new RxCumulatedGainsIndicator(20)
        );

        tester.test(
                t -> new CumulatedLossesIndicator(new ClosePriceIndicator(t), 20),
                Tick::getClosePrice,
                new RxCumulatedLossesIndicator(20)
        );

        tester.test(
                t -> new MACDIndicator(new ClosePriceIndicator(t), 10, 20),
                Tick::getClosePrice,
                new RxMACDIndicator(10, 20));

        tester.test(
                t -> new SMAIndicator(new ClosePriceIndicator(t), 20),
                Tick::getClosePrice,
                new RxSMAIndicator(20));

        tester.test(
                t -> new EMAIndicator(new ClosePriceIndicator(t), 20),
                Tick::getClosePrice,
                new RxEMAIndicator(20));

    }
}
