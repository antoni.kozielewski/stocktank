package org.hou.stocktank.rx.indicators;


import io.reactivex.Flowable;
import org.hou.stocktank.rx.base.RingBuffer;
import org.ta4j.core.Decimal;

public class RxSMAIndicator implements RxIndicator<Decimal, Decimal> {
    private int timeframe;

    public RxSMAIndicator(int timeframe) {
        this.timeframe = timeframe;
    }

    public Flowable<Decimal> calculate(Flowable<Decimal> inputData) {
        RingBuffer<Decimal> buffer = new RingBuffer<>(timeframe);
        return inputData
                .serialize()
                .map(value -> {
                    buffer.put(value);
                    return buffer
                            .stream()
                            .reduce(Decimal.valueOf(0), (a, b) -> a.plus(b))
                            .dividedBy(Decimal.valueOf(buffer.getSize()));
                });
    }

    public int getTimeframe() {
        return timeframe;
    }
}
