package org.hou.stocktank.rx.indicators;

import io.reactivex.Flowable;
import org.hou.stocktank.rx.base.RingBuffer;
import org.ta4j.core.Decimal;

public class RxLowestValueIndicator implements RxIndicator<Decimal, Decimal> {
    private final int timeFrame;

    public RxLowestValueIndicator(int timeFrame) {
        this.timeFrame = timeFrame;
    }

    @Override
    public Flowable<Decimal> calculate(Flowable<Decimal> inputData) {
        RingBuffer<Decimal> buffer = new RingBuffer(timeFrame);
        return inputData.map(value -> {
            buffer.put(value);
            return buffer.stream().reduce(Decimal.valueOf(Integer.MAX_VALUE), (a, b) -> a.isGreaterThan(b) ? b : a);
        });
    }

}
