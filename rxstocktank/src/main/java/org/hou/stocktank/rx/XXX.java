package org.hou.stocktank.rx;

import com.pretty_tools.dde.DDEException;
import com.pretty_tools.dde.client.DDEClientConversation;
import io.reactivex.Flowable;
import org.junit.rules.Stopwatch;

import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Observable;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;

/**
 * Created by Ant on 2018-06-18.
 */
public class XXX {
    private static final String SOURCE_FILENAME = "data/mstall.zip";
    private static final List<String> stockNames = Arrays.asList("JUJUBEE", "CDPROJEKT");

    private static final String SERVICE_NAME = "Statica";

    public static void main(String[] args) throws DDEException {

        DDEClientConversation conversation = new DDEClientConversation();

        // Establish conversation with opened and active workbook
        conversation.connect(SERVICE_NAME, "CDR");
        AtomicInteger counter = new AtomicInteger(0);
        AtomicLong atomicLong = new AtomicLong(0);
        Flowable.interval(1000, 100, TimeUnit.MILLISECONDS)
                .blockingNext()
                .forEach(x -> {
                    if(counter.getAndIncrement() % 1000 == 0) {
                            System.out.println("---------------------------" + atomicLong.get());
                    }
                    try {
                        long time = new Date().getTime();
                        conversation.request("kurs");
                        conversation.request("czas");
                        time = time - new Date().getTime();
                        atomicLong.addAndGet(time);
                    } catch (DDEException e) {
                        e.printStackTrace();
                    }
                });

        conversation.disconnect();

    }
}
