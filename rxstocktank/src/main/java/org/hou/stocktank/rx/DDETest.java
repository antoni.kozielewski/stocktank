package org.hou.stocktank.rx;

import com.pretty_tools.dde.client.DDEClientConversation;
import io.reactivex.Flowable;
import io.reactivex.Scheduler;
import io.reactivex.schedulers.Schedulers;
import javafx.util.Pair;
import org.hou.stocktank.rx.utils.RetryWithDelay;

import java.util.concurrent.TimeUnit;

/**
 * Created by Ant on 2018-06-18.
 */
public class DDETest {
    private static final String SERVICE_NAME = "Statica";

    private class Triple{
        private String ticker;
        private String value;
        private String time;

        public Triple(String ticker, String value, String time) {
            this.ticker = ticker;
            this.value = value;
            this.time = time;
        }

        public String getTicker() {
            return ticker;
        }

        public String getValue() {
            return value;
        }

        public String getTime() {
            return time;
        }
    }

    private void run() {
        Flowable.fromArray("CDR","PGE","JJB","F51","KRU","PKO","11BIT","ENA","CCC","OPL","TPE","CIE","MBK")
                .parallel()
                .runOn(Schedulers.computation())
                .map(ticker -> {
                    DDEClientConversation conversation = new DDEClientConversation();
                    conversation.connect(SERVICE_NAME, ticker);
                    String value = conversation.request("kurs");
                    String time = conversation.request("czas");
                    Triple result = new Triple(ticker, value, time);
                    conversation.disconnect();
                    return result;
                })
                .sequential()
                .retry(x -> true)
                //.retryWhen(new RetryWithDelay(3,100))
                .blockingNext()
                .forEach(pair -> System.out.println(pair.getTicker() +" :: " + pair.getValue() + " | " + pair.getTime()));
        System.out.println("done.");
    }

    public static void main(String[] args) {
        DDETest test = new DDETest();
        test.run();
    }
}
