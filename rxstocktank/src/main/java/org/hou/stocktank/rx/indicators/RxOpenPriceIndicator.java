package org.hou.stocktank.rx.indicators;

import io.reactivex.Flowable;
import org.ta4j.core.Decimal;
import org.ta4j.core.Tick;


public class RxOpenPriceIndicator implements RxIndicator<Tick, Decimal> {

    @Override
    public Flowable<Decimal> calculate(Flowable<Tick> inputData) {
        return inputData.map(tick -> tick.getOpenPrice());
    }

}
