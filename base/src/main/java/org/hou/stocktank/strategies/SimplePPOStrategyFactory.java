package org.hou.stocktank.strategies;

import org.ta4j.core.*;
import org.ta4j.core.indicators.PPOIndicator;
import org.ta4j.core.indicators.helpers.ClosePriceIndicator;
import org.ta4j.core.indicators.RSIIndicator;
import org.ta4j.core.trading.rules.CrossedDownIndicatorRule;
import org.ta4j.core.trading.rules.CrossedUpIndicatorRule;
import org.hou.stocktank.base.*;

/**
 * bazuje tylko na wartosciach PPO
 */
public class SimplePPOStrategyFactory extends AbstractStrategyFactory {
    private static final int BUY_LEVEL = -10;
    private static final int SELL_LEVEL = 10;

    public SimplePPOStrategyFactory() {
        IndicatorDefinition ppoDef = new IndicatorDefinition(IndicatorName.PPO, new IndicatorDefinition(IndicatorName.CLOSE_PRICE, null))
                .withParam(Param.SHORT_TIME_FRAME, 10)
                .withParam(Param.LONG_TIME_FRAME, 20)
                .withLevel(Level.BUY_LEVEL, -10)
                .withLevel(Level.SELL_LEVEL, 10);
        indicatorDefinitions.put(IndicatorName.PPO,ppoDef);
    }

    public SimplePPOStrategyFactory(SimplePPOStrategyFactory source) {
        super(source);
    }

    @Override
    public String getName() {
        return "PPO ("
                + getParam(IndicatorName.PPO, Param.SHORT_TIME_FRAME) + ", "
                + getParam(IndicatorName.PPO, Param.LONG_TIME_FRAME) + ") ["
                + getLevel(IndicatorName.PPO, Level.BUY_LEVEL) + ", "
                + getLevel(IndicatorName.PPO, Level.SELL_LEVEL) + "]";
    }

    @Override
    public Strategy buildStrategy(TimeSeries series) {
        if (series == null) {
            throw new IllegalArgumentException("Series cannot be null");
        }

        Indicator ppo = new PPOIndicator(new ClosePriceIndicator(series), getParam(IndicatorName.PPO,Param.SHORT_TIME_FRAME), getParam(IndicatorName.PPO,Param.LONG_TIME_FRAME));

        Rule entryRule = new CrossedUpIndicatorRule(ppo, Decimal.valueOf(getLevel(IndicatorName.PPO, Level.BUY_LEVEL)));
        Rule exitRule = new CrossedDownIndicatorRule(ppo, Decimal.valueOf(getLevel(IndicatorName.PPO, Level.SELL_LEVEL)));

        Strategy strategy = new BaseStrategy(entryRule, exitRule);
        strategy.setUnstablePeriod(unstablePeriod);
        return strategy;
    }

    @Override
    public Boolean validateConfiguration() {
        return getLevel(IndicatorName.PPO, Level.BUY_LEVEL) > getLevel(IndicatorName.PPO, Level.SELL_LEVEL) &&
                getParam(IndicatorName.PPO, Param.SHORT_TIME_FRAME) < getParam(IndicatorName.PPO, Param.LONG_TIME_FRAME);
    }

    @Override
    public StrategyFactory createClone() {
        return new SimplePPOStrategyFactory(this);
    }

    @Override
    public StrategyFactory createRandom() {
        SimplePPOStrategyFactory sf = new SimplePPOStrategyFactory(this);
        sf.setLevel(IndicatorName.PPO, Level.BUY_LEVEL, randomBetween(-100, 0));
        sf.setLevel(IndicatorName.PPO, Level.SELL_LEVEL, randomBetween(0, 100));
        sf.setParam(IndicatorName.PPO, Param.SHORT_TIME_FRAME, randomBetween(3,100));
        sf.setParam(IndicatorName.PPO, Param.LONG_TIME_FRAME, randomBetween(9,100));
        return sf;
    }
}
