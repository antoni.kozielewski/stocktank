package org.hou.stocktank.strategies;


import org.hou.stocktank.analysis.AnalysisResult;
import org.hou.stocktank.analysis.MarketState;
import org.hou.stocktank.analysis.analyzers.IchimokuAnalyzerIndicator;
import org.hou.stocktank.base.AbstractStrategyFactory;
import org.hou.stocktank.base.StrategyFactory;
import org.ta4j.core.*;
import org.ta4j.core.indicators.helpers.ClosePriceIndicator;
import org.ta4j.core.trading.rules.AbstractRule;
import org.ta4j.core.trading.rules.StopLossRule;

import java.util.Arrays;
import java.util.List;

public class IchimokuStrategyFactory extends AbstractStrategyFactory {

    public class AnalysisResultBasedRule extends AbstractRule {
        private Indicator<AnalysisResult> indicator;
        private List<MarketState> marketConditions;
        private double minValueCondition;

        public AnalysisResultBasedRule(Indicator<AnalysisResult> indicator, List<MarketState> marketConditions, double minValueCondition) {
            this.indicator = indicator;
            this.marketConditions = marketConditions;
            this.minValueCondition = minValueCondition;
        }

        @Override
        public boolean isSatisfied(int i, TradingRecord tradingRecord) {
            AnalysisResult res = indicator.getValue(i);
            return (marketConditions.contains(res.getMarketState()))&&(res.getValue() >= minValueCondition);
        }

    }

    @Override
    public String getName() {
        return "Ichimoku Strategy";
    }

    @Override
    public Strategy buildStrategy(TimeSeries series) {
        IchimokuAnalyzerIndicator indicator = new IchimokuAnalyzerIndicator(series);
        Rule entryRule = new AnalysisResultBasedRule(indicator, Arrays.asList(MarketState.WEAK_BUY), 1);
        //Rule entryRule = new AnalysisResultBasedRule(indicator, Arrays.asList(MarketState.NEUTRAL_BUY, MarketState.STRONG_BUY), 1);
        //Rule exitRule = new AnalysisResultBasedRule(indicator, Arrays.asList(MarketState.WEAK_SELL, MarketState.NEUTRAL_SELL, MarketState.STRONG_SELL), 0);
        Rule exitRule = new AnalysisResultBasedRule(indicator, Arrays.asList( MarketState.NEUTRAL_SELL, MarketState.STRONG_SELL), 0)
                .or(new StopLossRule(new ClosePriceIndicator(series), Decimal.valueOf(4)));
        //Rule exitRule = new AnalysisResultBasedRule(indicator, Arrays.asList(MarketState.STRONG_SELL), 0);
        Strategy strategy = new BaseStrategy(entryRule, exitRule);
        return strategy;
    }

    @Override
    public Boolean validateConfiguration() {
        return true;
    }

    @Override
    public StrategyFactory createClone() {
        return new IchimokuStrategyFactory();
    }

    @Override
    public StrategyFactory createRandom() {
        return new IchimokuStrategyFactory();
    }
}
