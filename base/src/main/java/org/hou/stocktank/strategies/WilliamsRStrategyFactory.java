package org.hou.stocktank.strategies;

import org.hou.stocktank.base.AbstractStrategyFactory;
import org.hou.stocktank.base.IndicatorDefinition;
import org.hou.stocktank.base.IndicatorName;
import org.hou.stocktank.base.Level;
import org.hou.stocktank.base.Param;
import org.hou.stocktank.base.StrategyFactory;

import org.ta4j.core.*;
import org.ta4j.core.indicators.WilliamsRIndicator;
import org.ta4j.core.indicators.helpers.ClosePriceIndicator;
import org.ta4j.core.indicators.helpers.MaxPriceIndicator;
import org.ta4j.core.indicators.helpers.MinPriceIndicator;
import org.ta4j.core.trading.rules.OverIndicatorRule;
import org.ta4j.core.trading.rules.UnderIndicatorRule;

public class WilliamsRStrategyFactory extends AbstractStrategyFactory {
    private static final int MIN_WILLIAMS_TIME_FRAME = 3;
    private static final int MAX_WILLIAMS_TIME_FRAME = 100;
    private static final int MIN_SELL_LEVEL = -50;
    private static final int MAX_SELL_LEVEL = 0;
    private static final int MIN_BUY_LEVEL = -120;
    private static final int MAX_BUY_LEVEL = -51;

    public WilliamsRStrategyFactory(StrategyFactory source) {
        super(source);
    }

    public WilliamsRStrategyFactory() {
        super();
        IndicatorDefinition def =
                new IndicatorDefinition(IndicatorName.WILLIAMS_R, new IndicatorDefinition(IndicatorName.CLOSE_PRICE, null))
                        .withParam(Param.TIME_FRAME, 10)
                        .withLevel(Level.BUY_LEVEL, -80)
                        .withLevel(Level.SELL_LEVEL, -20);
        setIndicatorDefinition(IndicatorName.WILLIAMS_R, def);
        indicatorDefinitions.put(IndicatorName.WILLIAMS_R, def);
    }

    @Override
    public String getName() {
        return "Williams %R (" +
                getParam(IndicatorName.WILLIAMS_R, Param.TIME_FRAME) + ") ["
                + getLevel(IndicatorName.WILLIAMS_R, Level.BUY_LEVEL) + ", "
                + getLevel(IndicatorName.WILLIAMS_R, Level.SELL_LEVEL) + "]";
    }

    @Override
    public Strategy buildStrategy(TimeSeries series) {
        if (series == null) {
            throw new IllegalArgumentException("Series cannot be null");
        }

        MaxPriceIndicator maxPriceIndicator = new MaxPriceIndicator(series);
        MinPriceIndicator minPriceIndicator = new MinPriceIndicator(series);
        Indicator wiliams = new WilliamsRIndicator(new ClosePriceIndicator(series), getParam(IndicatorName.WILLIAMS_R, Param.TIME_FRAME), maxPriceIndicator, minPriceIndicator);
        //Indicator wiliams = new WilliamsRIndicator(series, getParam(IndicatorName.WILLIAMS_R, Param.TIME_FRAME));

        Rule entryRule = new UnderIndicatorRule(wiliams, Decimal.valueOf(getLevel(IndicatorName.WILLIAMS_R, Level.BUY_LEVEL)));
        Rule exitRule = new OverIndicatorRule(wiliams, Decimal.valueOf(getLevel(IndicatorName.WILLIAMS_R, Level.SELL_LEVEL)));
        Strategy strategy = new BaseStrategy(entryRule, exitRule);
        return strategy;
    }

    @Override
    public Boolean validateConfiguration() {
        return getLevel(IndicatorName.WILLIAMS_R, Level.BUY_LEVEL) < getLevel(IndicatorName.WILLIAMS_R, Level.SELL_LEVEL);
    }

    @Override
    public StrategyFactory createClone() {
        return new WilliamsRStrategyFactory(this);
    }

    @Override
    public StrategyFactory createRandom() {
        StrategyFactory sf = new WilliamsRStrategyFactory(this)
                .withParam(IndicatorName.WILLIAMS_R, Param.TIME_FRAME, randomBetween(MIN_WILLIAMS_TIME_FRAME, MAX_WILLIAMS_TIME_FRAME));
        sf.setLevel(IndicatorName.WILLIAMS_R, Level.SELL_LEVEL, randomBetween(MIN_SELL_LEVEL, MAX_SELL_LEVEL));
        sf.setLevel(IndicatorName.WILLIAMS_R, Level.BUY_LEVEL, randomBetween(MIN_BUY_LEVEL, MAX_BUY_LEVEL));
        return sf;
    }
}
