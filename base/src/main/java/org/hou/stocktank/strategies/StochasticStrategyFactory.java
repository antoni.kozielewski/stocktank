package org.hou.stocktank.strategies;


import org.hou.stocktank.base.AbstractStrategyFactory;
import org.hou.stocktank.base.IndicatorDefinition;
import org.hou.stocktank.base.IndicatorName;
import org.hou.stocktank.base.Level;
import org.hou.stocktank.base.Param;
import org.hou.stocktank.base.StrategyFactory;

import org.ta4j.core.*;

import org.ta4j.core.indicators.SMAIndicator;
import org.ta4j.core.indicators.StochasticOscillatorKIndicator;
import org.ta4j.core.indicators.helpers.ClosePriceIndicator;
import org.ta4j.core.indicators.helpers.MaxPriceIndicator;
import org.ta4j.core.indicators.helpers.MinPriceIndicator;
import org.ta4j.core.trading.rules.CrossedDownIndicatorRule;
import org.ta4j.core.trading.rules.CrossedUpIndicatorRule;
import org.ta4j.core.trading.rules.OverIndicatorRule;
import org.ta4j.core.trading.rules.UnderIndicatorRule;

public class StochasticStrategyFactory extends AbstractStrategyFactory {
    private static final int minTimeFrame = 3;
    private static final int maxTimeFrame = 200;
    private static final int minBuyLevel = 0;
    private static final int maxBuyLevel = 50;
    private static final int minSellLevel = 51;
    private static final int maxSellLevel = 200;


    public StochasticStrategyFactory(StochasticStrategyFactory source) {
        super(source);
    }

    public StochasticStrategyFactory() {
        IndicatorDefinition kDefinition =
                new IndicatorDefinition(IndicatorName.STOCH_K, new IndicatorDefinition(IndicatorName.CLOSE_PRICE, null))
                        .withParam(Param.TIME_FRAME, 15)
                        .withLevel(Level.BUY_LEVEL, 20)
                        .withLevel(Level.SELL_LEVEL, 80);
        IndicatorDefinition dDefinition = new IndicatorDefinition(IndicatorName.STOCH_D, kDefinition);
        IndicatorDefinition slowDefinition = new IndicatorDefinition(IndicatorName.SMA, dDefinition)
                        .withParam(Param.TIME_FRAME, 3);
        indicatorDefinitions.put(IndicatorName.STOCH_K, kDefinition);
        indicatorDefinitions.put(IndicatorName.STOCH_D, dDefinition);
        indicatorDefinitions.put(IndicatorName.SMA, slowDefinition);
    }

    public String getName() {
        return "Stochastic %K %D ("
                + getParam(IndicatorName.STOCH_K, Param.TIME_FRAME) + ", "
                + "3, " +
                + getParam(IndicatorName.SMA, Param.TIME_FRAME) + ") ["
                + getLevel(IndicatorName.STOCH_K, Level.BUY_LEVEL) + ", "
                + getLevel(IndicatorName.STOCH_D, Level.SELL_LEVEL) + "]";
    }

    @Override
    public Strategy buildStrategy(TimeSeries series) {

        MaxPriceIndicator maxPriceIndicator = new MaxPriceIndicator(series);
        MinPriceIndicator minPriceIndicator = new MinPriceIndicator(series);
        StochasticOscillatorKIndicator kbase = new StochasticOscillatorKIndicator(new ClosePriceIndicator(series), getParam(IndicatorName.STOCH_K, Param.TIME_FRAME), maxPriceIndicator, minPriceIndicator);
        SMAIndicator k = new SMAIndicator(kbase,3);
        SMAIndicator d = new SMAIndicator(k, 3);

        Rule entryRule = new CrossedUpIndicatorRule(d, k)
                .and(new UnderIndicatorRule(d, Decimal.valueOf(getLevel(IndicatorName.STOCH_K, Level.BUY_LEVEL))))
                .and(new UnderIndicatorRule(k, Decimal.valueOf(getLevel(IndicatorName.STOCH_K, Level.BUY_LEVEL))));
        Rule exitRule = new CrossedDownIndicatorRule(d, k)
                .and(new OverIndicatorRule(d, Decimal.valueOf(getLevel(IndicatorName.STOCH_K, Level.SELL_LEVEL))))
                .and(new OverIndicatorRule(k, Decimal.valueOf(getLevel(IndicatorName.STOCH_K, Level.SELL_LEVEL))));

        Strategy strategy = new BaseStrategy(entryRule, exitRule);
        strategy.setUnstablePeriod(getUnstablePeriod());
        return strategy;
    }

    @Override
    public Boolean validateConfiguration() {
        return (getLevel(IndicatorName.STOCH_K, Level.BUY_LEVEL) < getLevel(IndicatorName.STOCH_K, Level.SELL_LEVEL))
                &&
                (getLevel(IndicatorName.STOCH_D, Level.BUY_LEVEL) < getLevel(IndicatorName.STOCH_D, Level.SELL_LEVEL));
    }

    @Override
    public StrategyFactory createClone() {
        return new StochasticStrategyFactory(this);
    }

    @Override
    public StrategyFactory createRandom() {
        StochasticStrategyFactory sf = new StochasticStrategyFactory(this);
        sf.setParam(IndicatorName.STOCH_K, Param.TIME_FRAME, randomBetween(minTimeFrame, maxTimeFrame));
        sf.setLevel(IndicatorName.STOCH_K, Level.BUY_LEVEL, randomBetween(minBuyLevel, maxBuyLevel));
        sf.setLevel(IndicatorName.STOCH_K, Level.SELL_LEVEL, randomBetween(minSellLevel, maxSellLevel));
        sf.setLevel(IndicatorName.STOCH_D, Level.BUY_LEVEL, randomBetween(minBuyLevel, maxBuyLevel));
        sf.setLevel(IndicatorName.STOCH_D, Level.SELL_LEVEL, randomBetween(minSellLevel, maxSellLevel));
        return sf;
    }
}
