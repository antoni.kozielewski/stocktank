package org.hou.stocktank.strategies;

import org.hou.stocktank.base.AbstractStrategyFactory;
import org.hou.stocktank.base.IndicatorDefinition;
import org.hou.stocktank.base.IndicatorName;
import org.hou.stocktank.base.Level;
import org.hou.stocktank.base.Param;

import org.ta4j.core.*;
import org.ta4j.core.indicators.CMOIndicator;
import org.ta4j.core.indicators.helpers.ClosePriceIndicator;
import org.ta4j.core.trading.rules.CrossedDownIndicatorRule;
import org.ta4j.core.trading.rules.CrossedUpIndicatorRule;

public class CMOStrategyFactory extends AbstractStrategyFactory {
    private static final int MIN_BUY_LEVEL = 0;
    private static final int MAX_BUY_LEVEL = 50;
    private static final int MIN_SELL_LEVEL = 51;
    private static final int MAX_SELL_LEVEL = 100;

    public CMOStrategyFactory() {
        super();
        IndicatorDefinition def =
                new IndicatorDefinition(IndicatorName.CMO, new IndicatorDefinition(IndicatorName.CLOSE_PRICE, null))
                        .withParam(Param.TIME_FRAME, 14)
                        .withLevel(Level.BUY_LEVEL, 20)
                        .withLevel(Level.SELL_LEVEL, 80);
        setIndicatorDefinition(IndicatorName.CMO, def);
    }

    public CMOStrategyFactory(CMOStrategyFactory source) {
        super(source);
    }

    public String getName() {
        return "CMO (" +
                getParam(IndicatorName.CMO,Param.TIME_FRAME)+") ["+
                getLevel(IndicatorName.CMO,Level.BUY_LEVEL) + ", " +
                getLevel(IndicatorName.CMO,Level.SELL_LEVEL) + "]";
    }

    @Override
    public Strategy buildStrategy(TimeSeries series) {
        if (series == null) {
            throw new IllegalArgumentException("Series cannot be null");
        }

        Indicator cmo = new CMOIndicator(new ClosePriceIndicator(series), getParam(IndicatorName.CMO, Param.TIME_FRAME));

        Rule entryRule = new CrossedUpIndicatorRule(cmo, Decimal.valueOf(getLevel(IndicatorName.CMO, Level.BUY_LEVEL)));
        Rule exitRule = new CrossedDownIndicatorRule(cmo, Decimal.valueOf(getLevel(IndicatorName.CMO, Level.SELL_LEVEL)));

        Strategy strategy = new BaseStrategy(entryRule, exitRule);
        strategy.setUnstablePeriod(unstablePeriod);
        return strategy;

    }

    @Override
    public Boolean validateConfiguration() {
        return getLevel(IndicatorName.CMO, Level.BUY_LEVEL) < getLevel(IndicatorName.CMO, Level.SELL_LEVEL);
    }

    @Override
    public CMOStrategyFactory createClone() {
        return new CMOStrategyFactory(this);
    }

    @Override
    public CMOStrategyFactory createRandom() {
        Integer buyLevel = MIN_BUY_LEVEL + (int) (Math.random() * (MAX_BUY_LEVEL - MIN_BUY_LEVEL));
        Integer sellLevel = MIN_SELL_LEVEL + (int) (Math.random() * (MAX_SELL_LEVEL - MIN_SELL_LEVEL));
        CMOStrategyFactory sf = new CMOStrategyFactory(this);
        sf.setLevel(IndicatorName.CMO, Level.BUY_LEVEL, buyLevel);
        sf.setLevel(IndicatorName.CMO, Level.SELL_LEVEL, sellLevel);
        return sf;
    }
}
