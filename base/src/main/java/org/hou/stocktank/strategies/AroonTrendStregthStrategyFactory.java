package org.hou.stocktank.strategies;

import org.hou.stocktank.base.AbstractStrategyFactory;
import org.hou.stocktank.base.IndicatorDefinition;
import org.hou.stocktank.base.IndicatorName;
import org.hou.stocktank.base.Level;
import org.hou.stocktank.base.Param;
import org.hou.stocktank.base.StrategyFactory;

import org.ta4j.core.*;
import org.ta4j.core.indicators.AroonUpIndicator;
import org.ta4j.core.trading.rules.CrossedDownIndicatorRule;
import org.ta4j.core.trading.rules.CrossedUpIndicatorRule;

public class AroonTrendStregthStrategyFactory extends AbstractStrategyFactory {
    private static final int MIN_TRIGGER_LEVEL = -99;
    private static final int MAX_TRIGGER_LEVEL = 99;
    private static final int MIN_TIME_FRAME = 3;
    private static final int MAX_TIME_FRAME = 60;

    public AroonTrendStregthStrategyFactory(StrategyFactory source) {
        super(source);
    }

    public AroonTrendStregthStrategyFactory() {
        IndicatorDefinition def = new IndicatorDefinition(IndicatorName.AROON_UP, new IndicatorDefinition(IndicatorName.CLOSE_PRICE, null))
                .withParam(Param.TIME_FRAME, 9)
                .withLevel(Level.TRIGGER_LEVEL, 50);
        indicatorDefinitions.put(IndicatorName.AROON_UP, def);
    }

    @Override
    public String getName() {
        return "AroonTrendStrength";
    }

    @Override
    public Strategy buildStrategy(TimeSeries series) {
        if (series == null) {
            throw new IllegalArgumentException("Series cannot be null");
        }

        Indicator aroon = new AroonUpIndicator(series, getParam(IndicatorName.AROON_UP, Param.TIME_FRAME));

        Rule entryRule = new CrossedUpIndicatorRule(aroon, Decimal.valueOf(getLevel(IndicatorName.AROON_UP, Level.TRIGGER_LEVEL)));
        Rule exitRule = new CrossedDownIndicatorRule(aroon, Decimal.valueOf(getLevel(IndicatorName.AROON_UP, Level.TRIGGER_LEVEL)));

        Strategy strategy = new BaseStrategy(entryRule, exitRule);
        strategy.setUnstablePeriod(getUnstablePeriod());
        return strategy;
    }

    @Override
    public Boolean validateConfiguration() {
        return (99 > getLevel(IndicatorName.AROON_UP, Level.TRIGGER_LEVEL)) && (getLevel(IndicatorName.AROON_UP, Level.TRIGGER_LEVEL) > 0);
    }

    @Override
    public StrategyFactory createClone() {
        return new AroonTrendStregthStrategyFactory(this);
    }

    @Override
    public StrategyFactory createRandom() {
        StrategyFactory sf = new AroonTrendStregthStrategyFactory(this)
                .withParam(IndicatorName.AROON_UP, Param.TIME_FRAME, randomBetween(MIN_TIME_FRAME, MAX_TIME_FRAME))
                .withLevel(IndicatorName.AROON_UP, Level.TRIGGER_LEVEL, randomBetween(MIN_TRIGGER_LEVEL, MAX_TRIGGER_LEVEL));
        return sf;
    }

}
