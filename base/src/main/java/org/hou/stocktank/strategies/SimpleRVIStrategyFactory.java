package org.hou.stocktank.strategies;

import org.hou.stocktank.base.AbstractStrategyFactory;
import org.hou.stocktank.base.IndicatorDefinition;
import org.hou.stocktank.base.IndicatorName;
import org.hou.stocktank.base.Param;
import org.hou.stocktank.base.StrategyFactory;
import org.hou.stocktank.indicators.RelativeVigorIndexIndicator;
import org.hou.stocktank.indicators.VolumeWeightedMovingAverage;

import org.ta4j.core.*;
import org.ta4j.core.indicators.SMAIndicator;
import org.ta4j.core.indicators.helpers.VolumeIndicator;
import org.ta4j.core.trading.rules.CrossedDownIndicatorRule;
import org.ta4j.core.trading.rules.CrossedUpIndicatorRule;

public class SimpleRVIStrategyFactory extends AbstractStrategyFactory {
    private static final int RVI_MIN_TIMEFRAME = 4;
    private static final int RVI_MAX_TIMEFRAME = 50;
    private static final int VOLUME_MIN_TIMEFRAME = 4;
    private static final int VOLUME_MAX_TIMEFRAME = 50;

    public SimpleRVIStrategyFactory() {
        IndicatorDefinition rviSMA = new IndicatorDefinition(IndicatorName.SMA, new IndicatorDefinition(IndicatorName.RVI, null))
                .withParam(Param.TIME_FRAME, 10);
        IndicatorDefinition volumeWeightedMA = new IndicatorDefinition(IndicatorName.VWMA, new IndicatorDefinition(IndicatorName.RVI, null))
                .withParam(Param.TIME_FRAME, 4);
        indicatorDefinitions.put(IndicatorName.SMA, rviSMA);
        indicatorDefinitions.put(IndicatorName.VWMA, volumeWeightedMA);
    }

    public SimpleRVIStrategyFactory(SimpleRVIStrategyFactory source) {
        super(source);
    }

    @Override
    public String getName() {
        return "RVI ("
                + getParam(IndicatorName.SMA, Param.TIME_FRAME) + ", "
                + getParam(IndicatorName.VWMA, Param.TIME_FRAME) + ")";
    }

    @Override
    public Strategy buildStrategy(TimeSeries series) {
        if (series == null) {
            throw new IllegalArgumentException("Series cannot be null");
        }
        Indicator rvi = new SMAIndicator(new RelativeVigorIndexIndicator(series), getParam(IndicatorName.SMA, Param.TIME_FRAME));
        Indicator signal = new VolumeWeightedMovingAverage(new RelativeVigorIndexIndicator(series), new VolumeIndicator(series), getParam(IndicatorName.VWMA, Param.TIME_FRAME));

        Rule entryRule = new CrossedDownIndicatorRule(rvi, signal);
        Rule exitRule = new CrossedUpIndicatorRule(rvi, signal);

        Strategy strategy = new BaseStrategy(entryRule, exitRule);
        strategy.setUnstablePeriod(getUnstablePeriod());
        return strategy;
    }

    @Override
    public Boolean validateConfiguration() {
        return
                (this.getIndicatorsDefinitions().get(IndicatorName.SMA).getParam(Param.TIME_FRAME) > 0) &&
                        (this.getIndicatorsDefinitions().get(IndicatorName.VWMA).getParam(Param.TIME_FRAME) > 0);
    }

    @Override
    public StrategyFactory createClone() {
        return new SimpleRVIStrategyFactory(this);
    }

    @Override
    public StrategyFactory createRandom() {
        return new SimpleRVIStrategyFactory(this)
                .withParam(IndicatorName.SMA, Param.TIME_FRAME, randomBetween(RVI_MIN_TIMEFRAME, RVI_MAX_TIMEFRAME))
                .withParam(IndicatorName.VWMA, Param.TIME_FRAME, randomBetween(VOLUME_MIN_TIMEFRAME, VOLUME_MAX_TIMEFRAME));
    }
}
