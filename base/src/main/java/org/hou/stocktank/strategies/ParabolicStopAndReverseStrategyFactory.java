package org.hou.stocktank.strategies;


import org.ta4j.core.*;
import org.ta4j.core.indicators.helpers.ClosePriceIndicator;
import org.ta4j.core.indicators.EMAIndicator;
import org.ta4j.core.trading.rules.CrossedDownIndicatorRule;
import org.ta4j.core.trading.rules.CrossedUpIndicatorRule;
import org.hou.stocktank.base.*;
import org.hou.stocktank.indicators.ParabolicSARIndicator;

public class ParabolicStopAndReverseStrategyFactory extends AbstractStrategyFactory {

    private final static int MAX_TIME_FRAME = 100;
    private final static int MIN_TIME_FRAME = 5;

    public ParabolicStopAndReverseStrategyFactory(StrategyFactory source) {
        super(source);
    }

    public ParabolicStopAndReverseStrategyFactory() {
        super();
        IndicatorDefinition psarDef = new IndicatorDefinition(IndicatorName.PSAR,null);
        IndicatorDefinition def = new IndicatorDefinition(IndicatorName.EMA, new IndicatorDefinition(IndicatorName.CLOSE_PRICE, null))
                .withParam(Param.TIME_FRAME, 4);
        setIndicatorDefinition(IndicatorName.PSAR, psarDef);
        setIndicatorDefinition(IndicatorName.EMA, def);
    }

    @Override
    public String getName() {
        return "Parabolic Stop And Reverse";
    }

    @Override
    public Strategy buildStrategy(TimeSeries series) {
        if (series == null) {
            throw new IllegalArgumentException("Series cannot be null");
        }

        Indicator psar = new ParabolicSARIndicator(series);
        Indicator ema = new EMAIndicator(new ClosePriceIndicator(series), getParam(IndicatorName.EMA, Param.TIME_FRAME));
        Rule entryRule = new CrossedDownIndicatorRule(psar, ema);
        Rule exitRule = new CrossedUpIndicatorRule(psar, ema);

        Strategy strategy = new BaseStrategy(entryRule, exitRule);
        strategy.setUnstablePeriod(unstablePeriod);
        return strategy;
    }

    @Override
    public Boolean validateConfiguration() {
        return true;
    }

    @Override
    public StrategyFactory createClone() {
        return new ParabolicStopAndReverseStrategyFactory(this);
    }

    @Override
    public StrategyFactory createRandom() {
        StrategyFactory sf = new ParabolicStopAndReverseStrategyFactory(this);
        sf.setParam(IndicatorName.PSAR, Param.TIME_FRAME, randomBetween(MIN_TIME_FRAME, MAX_TIME_FRAME));
        return sf;
    }
}
