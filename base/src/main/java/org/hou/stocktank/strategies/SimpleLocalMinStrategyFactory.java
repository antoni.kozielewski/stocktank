package org.hou.stocktank.strategies;

import org.hou.stocktank.base.*;
import org.hou.stocktank.indicators.LocalMinMaxPointIndicator;
import org.hou.stocktank.rules.MinTradeDistanceRule;
import org.ta4j.core.*;
import org.ta4j.core.indicators.EMAIndicator;
import org.ta4j.core.indicators.MACDIndicator;
import org.ta4j.core.indicators.helpers.ClosePriceIndicator;
import org.ta4j.core.trading.rules.OverIndicatorRule;
import org.ta4j.core.trading.rules.StopLossRule;
import org.ta4j.core.trading.rules.UnderIndicatorRule;

public class SimpleLocalMinStrategyFactory extends AbstractStrategyFactory {

    public SimpleLocalMinStrategyFactory() {
        IndicatorDefinition macdDef = new IndicatorDefinition(IndicatorName.MACD, new IndicatorDefinition(IndicatorName.CLOSE_PRICE, null))
                .withParam(Param.SHORT_TIME_FRAME, 12)
                .withParam(Param.LONG_TIME_FRAME, 26);
        indicatorDefinitions.put(IndicatorName.MACD, macdDef);
    }

    public SimpleLocalMinStrategyFactory(StrategyFactory source) {
        super(source);
    }

    @Override
    public String getName() {
        return "SimpleLocalMinStrategyFactory:MACD";
    }

    @Override
    public Strategy buildStrategy(TimeSeries series) {
        if (series == null) {
            throw new IllegalArgumentException("Series cannot be null");
        }

        Indicator macd = new MACDIndicator(new ClosePriceIndicator(series), getParam(IndicatorName.MACD, Param.SHORT_TIME_FRAME), getParam(IndicatorName.MACD, Param.LONG_TIME_FRAME));
        Indicator<Decimal> ema = new EMAIndicator(macd,9);
        Indicator trendChangeIndicator = new LocalMinMaxPointIndicator(series, macd, 1, 1);

        Rule entryRule = new OverIndicatorRule(trendChangeIndicator, Decimal.ZERO)
                .and(new MinTradeDistanceRule(2))
                ;
        Rule exitRule = new UnderIndicatorRule(trendChangeIndicator, Decimal.ZERO)
                .and(new MinTradeDistanceRule(2))
                .or(new StopLossRule(new ClosePriceIndicator(series),Decimal.valueOf(3.8)))
                ;

        Strategy strategy = new BaseStrategy(entryRule, exitRule);
        strategy.setUnstablePeriod(getUnstablePeriod());
        return strategy;
    }

    @Override
    public Boolean validateConfiguration() {
        return true;
    }

    @Override
    public StrategyFactory createClone() {
        return new SimpleLocalMinStrategyFactory(this);
    }

    @Override
    public StrategyFactory createRandom() {
        return new SimpleLocalMinStrategyFactory(this);
    }
}
