package org.hou.stocktank.strategies;

import org.hou.stocktank.base.AbstractStrategyFactory;
import org.hou.stocktank.base.IndicatorDefinition;
import org.hou.stocktank.base.IndicatorName;
import org.hou.stocktank.base.Level;
import org.hou.stocktank.base.Param;

import org.ta4j.core.*;
import org.ta4j.core.indicators.helpers.ClosePriceIndicator;
import org.ta4j.core.indicators.RSIIndicator;
import org.ta4j.core.trading.rules.CrossedDownIndicatorRule;
import org.ta4j.core.trading.rules.CrossedUpIndicatorRule;

public class RSIStrategyFactory extends AbstractStrategyFactory {
    private static final int MIN_TIME_FRAME = 3;
    private static final int MAX_TIME_FRAME = 50;
    private static final int MIN_BUY_LEVEL = 0;
    private static final int MAX_BUY_LEVEL = 50;
    private static final int MIN_SELL_LEVEL = 51;
    private static final int MAX_SELL_LEVEL = 100;

    public RSIStrategyFactory() {
        super();
        IndicatorDefinition def =
                new IndicatorDefinition(IndicatorName.RSI, new IndicatorDefinition(IndicatorName.CLOSE_PRICE, null))
                        .withParam(Param.TIME_FRAME, 14)
                        .withLevel(Level.BUY_LEVEL, 20)
                        .withLevel(Level.SELL_LEVEL, 70);
        setIndicatorDefinition(IndicatorName.RSI, def);
    }

    public RSIStrategyFactory(RSIStrategyFactory source) {
        super(source);
    }

    public String getName() {
        return "RSI ("
                + getParam(IndicatorName.RSI, Param.TIME_FRAME) + ") ["
                + getLevel(IndicatorName.RSI,Level.BUY_LEVEL) +", "
                + getLevel(IndicatorName.RSI, Level.SELL_LEVEL) + "]";
    }

    @Override
    public Strategy buildStrategy(TimeSeries series) {
        if (series == null) {
            throw new IllegalArgumentException("Series cannot be null");
        }

        Indicator rsi = new RSIIndicator(new ClosePriceIndicator(series), getParam(IndicatorName.RSI, Param.TIME_FRAME));

        Rule entryRule = new CrossedUpIndicatorRule(rsi, Decimal.valueOf(getLevel(IndicatorName.RSI, Level.BUY_LEVEL)));
        Rule exitRule = new CrossedDownIndicatorRule(rsi, Decimal.valueOf(getLevel(IndicatorName.RSI, Level.SELL_LEVEL)));

        Strategy strategy = new BaseStrategy(entryRule, exitRule);
        strategy.setUnstablePeriod(unstablePeriod);
        return strategy;
    }

    @Override
    public Boolean validateConfiguration() {
        return getLevel(IndicatorName.RSI, Level.BUY_LEVEL) < getLevel(IndicatorName.RSI, Level.SELL_LEVEL);
    }

    @Override
    public RSIStrategyFactory createClone() {
        return new RSIStrategyFactory(this);
    }

    @Override
    public RSIStrategyFactory createRandom() {
        Integer buyLevel = MIN_BUY_LEVEL + (int) (Math.random() * (MAX_BUY_LEVEL - MIN_BUY_LEVEL));
        Integer sellLevel = MIN_SELL_LEVEL + (int) (Math.random() * (MAX_SELL_LEVEL - MIN_SELL_LEVEL));
        RSIStrategyFactory sf = new RSIStrategyFactory(this);
        sf.setParam(IndicatorName.RSI, Param.TIME_FRAME, randomBetween(MIN_TIME_FRAME, MAX_TIME_FRAME));
        sf.setLevel(IndicatorName.RSI, Level.BUY_LEVEL, buyLevel);
        sf.setLevel(IndicatorName.RSI, Level.SELL_LEVEL, sellLevel);
        return sf;
    }
}
