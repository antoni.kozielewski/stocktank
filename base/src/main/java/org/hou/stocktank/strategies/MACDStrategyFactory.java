package org.hou.stocktank.strategies;

import org.hou.stocktank.base.AbstractStrategyFactory;
import org.hou.stocktank.base.IndicatorDefinition;
import org.hou.stocktank.base.IndicatorName;
import org.hou.stocktank.base.Param;
import org.ta4j.core.*;
import org.ta4j.core.indicators.EMAIndicator;
import org.ta4j.core.indicators.MACDIndicator;
import org.ta4j.core.indicators.helpers.ClosePriceIndicator;
import org.ta4j.core.trading.rules.CrossedDownIndicatorRule;
import org.ta4j.core.trading.rules.CrossedUpIndicatorRule;

import java.util.Random;

public class MACDStrategyFactory extends AbstractStrategyFactory {
    private static final Integer minShortFrame = 10;
    private static final Integer maxShortFrame = 100;
    private static final Integer minLongFrame = 10;
    private static final Integer maxLongFrame = 200;
    private static final Integer minSignalFrame = 4;
    private static final Integer maxSignalFrame = 40;

    public MACDStrategyFactory() {
        IndicatorDefinition macdDef = new IndicatorDefinition(IndicatorName.MACD, new IndicatorDefinition(IndicatorName.CLOSE_PRICE, null))
                .withParam(Param.SHORT_TIME_FRAME, 12)
                .withParam(Param.LONG_TIME_FRAME, 26);
        IndicatorDefinition signalDef = new IndicatorDefinition(IndicatorName.EMA, macdDef)
                .withParam(Param.TIME_FRAME, 9);
        indicatorDefinitions.put(IndicatorName.MACD, macdDef);
        indicatorDefinitions.put(IndicatorName.EMA, signalDef);
    }

    public MACDStrategyFactory(MACDStrategyFactory source) {
        super(source);
    }

    public String getName() {
        return "MACD ("
                + getParam(IndicatorName.MACD, Param.SHORT_TIME_FRAME) + ", "
                + getParam(IndicatorName.MACD, Param.LONG_TIME_FRAME) + ", ("
                + getParam(IndicatorName.EMA, Param.TIME_FRAME) +"))";
    }

    @Override
    public Strategy buildStrategy(TimeSeries series) {
        if (series == null) {
            throw new IllegalArgumentException("Series cannot be null");
        }

        Indicator macd = new MACDIndicator(new ClosePriceIndicator(series), getParam(IndicatorName.MACD, Param.SHORT_TIME_FRAME), getParam(IndicatorName.MACD, Param.LONG_TIME_FRAME));
        Indicator signal = new EMAIndicator(macd, getParam(IndicatorName.EMA, Param.TIME_FRAME));

        Rule entryRule = new CrossedUpIndicatorRule(macd, signal);
        Rule exitRule = new CrossedDownIndicatorRule(macd, signal);

        Strategy strategy = new BaseStrategy(entryRule, exitRule);
        strategy.setUnstablePeriod(getUnstablePeriod());
        return strategy;
    }

    @Override
    public Boolean validateConfiguration() {
        Integer shortV = this.getIndicatorsDefinitions().get(IndicatorName.MACD).getParam(Param.SHORT_TIME_FRAME);
        Integer longV = this.getIndicatorsDefinitions().get(IndicatorName.MACD).getParam(Param.LONG_TIME_FRAME);
        Integer emaMACDShort = this.getIndicatorsDefinitions().get(IndicatorName.EMA).getBaseIndicator().getParam(Param.SHORT_TIME_FRAME);
        Integer emaMACDLong = this.getIndicatorsDefinitions().get(IndicatorName.EMA).getBaseIndicator().getParam(Param.LONG_TIME_FRAME);
        return ((shortV < longV) && (emaMACDShort < emaMACDLong));
    }

    @Override
    public MACDStrategyFactory createClone() {
        return new MACDStrategyFactory(this);
    }

    public MACDStrategyFactory createRandom() {
        Random r = new Random();
        Integer shortFrame = minShortFrame + (int) (Math.random() * maxShortFrame);
        Integer longFrame = ((minLongFrame < shortFrame) ? shortFrame : minLongFrame) + (int) (Math.random() * maxLongFrame);
        if (shortFrame > longFrame) {
            throw new RuntimeException("!!!!!!!!!!!! short > long !!!!!!!!!!!!!!!");
        }
        MACDStrategyFactory sf = new MACDStrategyFactory(this);
        sf.setParam(IndicatorName.MACD, Param.SHORT_TIME_FRAME, shortFrame);
        sf.setParam(IndicatorName.MACD, Param.LONG_TIME_FRAME, longFrame);
        sf.setParam(IndicatorName.EMA, Param.TIME_FRAME, minSignalFrame + (int) (Math.random() * maxSignalFrame));
        return sf;
    }
}
