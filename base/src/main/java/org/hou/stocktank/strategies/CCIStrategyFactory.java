package org.hou.stocktank.strategies;

import java.util.Random;

import org.hou.stocktank.base.AbstractStrategyFactory;
import org.hou.stocktank.base.IndicatorDefinition;
import org.hou.stocktank.base.IndicatorName;
import org.hou.stocktank.base.Level;
import org.hou.stocktank.base.Param;
import org.hou.stocktank.base.StrategyFactory;

import org.ta4j.core.*;
import org.ta4j.core.indicators.CCIIndicator;
import org.ta4j.core.trading.rules.OverIndicatorRule;
import org.ta4j.core.trading.rules.UnderIndicatorRule;

public class CCIStrategyFactory extends AbstractStrategyFactory {
    private static final int MIN_BUY_LEVEL = 51;
    private static final int MAX_BUY_LEVEL = 120;
    private static final int MIN_SELL_LEVEL = -120;
    private static final int MAX_SELL_LEVEL = 50;
    private static final int MIN_SHORT_FRAME = 5;
    private static final int MAX_SHORT_FRAME = 100;
    private static final int MIN_LONG_FRAME = 20;
    private static final int MAX_LONG_FRAME = 120;


    public CCIStrategyFactory(CCIStrategyFactory strategyFactory) {
        super(strategyFactory);
    }

    public CCIStrategyFactory() {
        super();
        IndicatorDefinition indicatorDefinition = new IndicatorDefinition(IndicatorName.CCI, new IndicatorDefinition(IndicatorName.CLOSE_PRICE, null))
                .withParam(Param.LONG_TIME_FRAME, 200)
                .withParam(Param.SHORT_TIME_FRAME, 6)
                .withLevel(Level.SELL_LEVEL, -100)
                .withLevel(Level.BUY_LEVEL, 100);
        indicatorDefinitions.put(IndicatorName.CCI, indicatorDefinition);
    }

    @Override
    public String getName() {
        return "CCI(" + getParam(IndicatorName.CCI, Param.SHORT_TIME_FRAME) + ", "
                + getParam(IndicatorName.CCI, Param.LONG_TIME_FRAME) + ") ["
                + getLevel(IndicatorName.CCI, Level.SELL_LEVEL) + ", " +
                + getLevel(IndicatorName.CCI, Level.BUY_LEVEL) + "]";
    }

    @Override
    public Strategy buildStrategy(TimeSeries series) {
        if (series == null) {
            throw new IllegalArgumentException("Series cannot be null");
        }
        Indicator longCCI = new CCIIndicator(series, getParam(IndicatorName.CCI, Param.LONG_TIME_FRAME));
        Indicator shortCCI = new CCIIndicator(series, getParam(IndicatorName.CCI, Param.SHORT_TIME_FRAME));

        Rule entryRule = new OverIndicatorRule(longCCI, Decimal.valueOf(getLevel(IndicatorName.CCI, Level.BUY_LEVEL))) // Bull trend
                .and(new UnderIndicatorRule(shortCCI, Decimal.valueOf(getLevel(IndicatorName.CCI, Level.SELL_LEVEL)))); // Signal

        Rule exitRule = new UnderIndicatorRule(longCCI, Decimal.valueOf(getLevel(IndicatorName.CCI, Level.SELL_LEVEL))) // Bear trend
                .and(new OverIndicatorRule(shortCCI, Decimal.valueOf(MAX_BUY_LEVEL))); // Signal
        Strategy strategy = new BaseStrategy(entryRule, exitRule);
        strategy.setUnstablePeriod(unstablePeriod);
        return strategy;
    }

    @Override
    public Boolean validateConfiguration() {
        return (getParam(IndicatorName.CCI, Param.LONG_TIME_FRAME) > getParam(IndicatorName.CCI, Param.SHORT_TIME_FRAME))
                && (getLevel(IndicatorName.CCI, Level.BUY_LEVEL) > getLevel(IndicatorName.CCI, Level.SELL_LEVEL));
    }

    @Override
    public StrategyFactory createClone() {
        return new CCIStrategyFactory(this);
    }

    @Override
    public StrategyFactory createRandom() {
        Random r = new Random();
        Integer buyLevel = MIN_BUY_LEVEL + (int) (Math.random() * (MAX_BUY_LEVEL - MIN_BUY_LEVEL));
        Integer sellLevel = MIN_SELL_LEVEL + (int) (Math.random() * (MAX_SELL_LEVEL - MIN_SELL_LEVEL));
        Integer shortFrame = MIN_SHORT_FRAME + (int) (Math.random() * (MAX_SHORT_FRAME - MIN_SHORT_FRAME));
        Integer longFrame = MIN_LONG_FRAME + (int) (Math.random() * (MAX_LONG_FRAME - MIN_LONG_FRAME));
        CCIStrategyFactory sf = new CCIStrategyFactory(this);
        sf.setParam(IndicatorName.CCI, Param.SHORT_TIME_FRAME, shortFrame);
        sf.setParam(IndicatorName.CCI, Param.LONG_TIME_FRAME, longFrame);
        sf.setLevel(IndicatorName.CCI, Level.BUY_LEVEL, buyLevel);
        sf.setLevel(IndicatorName.CCI, Level.SELL_LEVEL, sellLevel);
        return sf;
    }
}
