package org.hou.stocktank.strategies.advanced;

import org.ta4j.core.*;
import org.ta4j.core.indicators.EMAIndicator;
import org.ta4j.core.indicators.MACDIndicator;
import org.ta4j.core.indicators.SMAIndicator;
import org.ta4j.core.indicators.StochasticOscillatorKIndicator;
import org.ta4j.core.indicators.helpers.ClosePriceIndicator;
import org.ta4j.core.indicators.helpers.MaxPriceIndicator;
import org.ta4j.core.indicators.helpers.MinPriceIndicator;
import org.ta4j.core.trading.rules.OverIndicatorRule;
import org.ta4j.core.trading.rules.UnderIndicatorRule;
import org.hou.stocktank.analysis.analyzers.AnalyzerToDecimalIndicatorWrapper;
import org.hou.stocktank.analysis.analyzers.advanced.BPF01Analyzer;
import org.hou.stocktank.base.AbstractStrategyFactory;
import org.hou.stocktank.base.StrategyFactory;
import org.hou.stocktank.indicators.LineAngleIndicator;
import org.hou.stocktank.rules.IsLastSessionRule;


public class BPF01StrategyFactory extends AbstractStrategyFactory {


    @Override
    public String getName() {
        return "BPF01StrategyFactory";
    }

    @Override
    public Strategy buildStrategy(TimeSeries series) {
        BPF01Analyzer analyzer = new BPF01Analyzer(series);
        AnalyzerToDecimalIndicatorWrapper bpf01 = new AnalyzerToDecimalIndicatorWrapper(analyzer);

        MaxPriceIndicator maxPriceIndicator = new MaxPriceIndicator(series);
        MinPriceIndicator minPriceIndicator = new MinPriceIndicator(series);
        StochasticOscillatorKIndicator kbase = new StochasticOscillatorKIndicator(new ClosePriceIndicator(series), 14, maxPriceIndicator, minPriceIndicator);
        SMAIndicator stochK = new SMAIndicator(kbase, 3);
        SMAIndicator stochD = new SMAIndicator(stochK, 3);


        Indicator macd = new MACDIndicator(new ClosePriceIndicator(series), 30, 70);
        Indicator macdSignal = new EMAIndicator(macd, 25);

        Indicator emaFast = new SMAIndicator(new ClosePriceIndicator(series), 23);
        Indicator emaSlow = new SMAIndicator(emaFast, 27);
        Indicator angle = new SMAIndicator(new LineAngleIndicator(emaFast, 2), 5);

        Rule entryRule =
                new OverIndicatorRule(bpf01, Decimal.valueOf(4.0))
//                .and(new OverIndicatorRule(angle, Decimal.valueOf(4)))
                ;

        Rule exitRule = new UnderIndicatorRule(bpf01, Decimal.valueOf(-4.0))
//                        .or(new FollowingStopLossRule(series, Decimal.valueOf(10)))
//                        .or(new CrossedDownIndicatorRule(macd, macdSignal))
//                        .or(new UnderIndicatorRule(angle, Decimal.valueOf(4)))
                        .or(new IsLastSessionRule(series));


        Strategy strategy = new BaseStrategy(entryRule, exitRule);
        strategy.setUnstablePeriod(unstablePeriod);
        return strategy;
    }

    @Override
    public Boolean validateConfiguration() {
        return null;
    }

    @Override
    public StrategyFactory createClone() {
        return null;
    }

    @Override
    public StrategyFactory createRandom() {
        return null;
    }
}
