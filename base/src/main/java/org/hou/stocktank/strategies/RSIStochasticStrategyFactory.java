package org.hou.stocktank.strategies;

import org.ta4j.core.*;
import org.ta4j.core.indicators.StochasticOscillatorDIndicator;
import org.ta4j.core.indicators.StochasticOscillatorKIndicator;
import org.ta4j.core.indicators.helpers.ClosePriceIndicator;
import org.ta4j.core.indicators.helpers.MaxPriceIndicator;
import org.ta4j.core.indicators.helpers.MinPriceIndicator;
import org.ta4j.core.indicators.RSIIndicator;
import org.ta4j.core.trading.rules.*;
import org.hou.stocktank.base.*;

import java.util.Random;

public class RSIStochasticStrategyFactory extends AbstractStrategyFactory {
    private static final int MIN_BUY_LEVEL = 0;
    private static final int MAX_BUY_LEVEL = 50;
    private static final int MIN_SELL_LEVEL = 51;
    private static final int MAX_SELL_LEVEL = 100;
    private static final int MIN_RSI_TIME_FRAME = 3;
    private static final int MAX_RSI_TIME_FRAME = 50;
    private static final int MIN_STOCH_TIME_FRAME = 5;
    private static final int MAX_STOCH_TIME_FRAME = 100;


    public RSIStochasticStrategyFactory(RSIStochasticStrategyFactory source) {
        super(source);
    }

    public RSIStochasticStrategyFactory() {
        super();
        IndicatorDefinition def =
                new IndicatorDefinition(IndicatorName.RSI, new IndicatorDefinition(IndicatorName.CLOSE_PRICE, null))
                        .withParam(Param.TIME_FRAME, 14)
                        .withLevel(Level.BUY_LEVEL, 40)
                        .withLevel(Level.SELL_LEVEL, 65);
        IndicatorDefinition kDefinition =
                new IndicatorDefinition(IndicatorName.STOCH_K, new IndicatorDefinition(IndicatorName.CLOSE_PRICE, null))
                        .withParam(Param.TIME_FRAME, 9); // default values = 5, 9, 21
        IndicatorDefinition dDefinition =
                new IndicatorDefinition(IndicatorName.STOCH_D, kDefinition);
        setIndicatorDefinition(IndicatorName.RSI, def);
        indicatorDefinitions.put(IndicatorName.STOCH_K, kDefinition);
        indicatorDefinitions.put(IndicatorName.STOCH_D, dDefinition);
    }

    @Override
    public String getName() {
        return "RSI + Stoch %K %D ("
                + getParam(IndicatorName.RSI, Param.TIME_FRAME) + ")("
                + getParam(IndicatorName.STOCH_K, Param.TIME_FRAME) + ") ["
                + getLevel(IndicatorName.RSI, Level.BUY_LEVEL) + ", "
                + getLevel(IndicatorName.RSI, Level.SELL_LEVEL) + "]";
    }

    @Override
    public Strategy buildStrategy(TimeSeries series) {
        if (series == null) {
            throw new IllegalArgumentException("Series cannot be null");
        }

        Indicator rsi = new RSIIndicator(new ClosePriceIndicator(series), getParam(IndicatorName.RSI, Param.TIME_FRAME));
        MaxPriceIndicator maxPriceIndicator = new MaxPriceIndicator(series);
        MinPriceIndicator minPriceIndicator = new MinPriceIndicator(series);
        StochasticOscillatorKIndicator k = new StochasticOscillatorKIndicator(new ClosePriceIndicator(series), getParam(IndicatorName.STOCH_K, Param.TIME_FRAME), maxPriceIndicator, minPriceIndicator);
        StochasticOscillatorDIndicator d = new StochasticOscillatorDIndicator(k);


        Rule entryRule = new UnderIndicatorRule(rsi, Decimal.valueOf(getLevel(IndicatorName.RSI, Level.BUY_LEVEL)))
                .and(new UnderIndicatorRule(k, Decimal.valueOf(getLevel(IndicatorName.RSI, Level.BUY_LEVEL))))
                .and(new UnderIndicatorRule(d, Decimal.valueOf(getLevel(IndicatorName.RSI, Level.BUY_LEVEL))))
                .and(new CrossedUpIndicatorRule(k, d));

        Rule exitRule = new OverIndicatorRule(rsi, Decimal.valueOf(getLevel(IndicatorName.RSI, Level.SELL_LEVEL)))
                .and(new OverIndicatorRule(k, Decimal.valueOf(getLevel(IndicatorName.RSI, Level.SELL_LEVEL))))
                .and(new OverIndicatorRule(d, Decimal.valueOf(getLevel(IndicatorName.RSI, Level.SELL_LEVEL))))
                .and(new CrossedDownIndicatorRule(k, d))
                .or(new StopLossRule(new ClosePriceIndicator(series), Decimal.valueOf(5.1)))
                ;

        Strategy strategy = new BaseStrategy(entryRule, exitRule);
        strategy.setUnstablePeriod(unstablePeriod);
        return strategy;
    }

    @Override
    public Boolean validateConfiguration() {
        return (getLevel(IndicatorName.STOCH_K, Level.BUY_LEVEL) < getLevel(IndicatorName.STOCH_K, Level.SELL_LEVEL))
                &&
                (getLevel(IndicatorName.STOCH_D, Level.BUY_LEVEL) < getLevel(IndicatorName.STOCH_D, Level.SELL_LEVEL))
                &&
                (getLevel(IndicatorName.RSI, Level.BUY_LEVEL) < getLevel(IndicatorName.RSI, Level.SELL_LEVEL));
    }

    @Override
    public StrategyFactory createClone() {
        return new RSIStochasticStrategyFactory(this);
    }

    @Override
    public StrategyFactory createRandom() {
        StrategyFactory sf = createClone();
        sf.setParam(IndicatorName.RSI, Param.TIME_FRAME, randomBetween(MIN_RSI_TIME_FRAME, MAX_RSI_TIME_FRAME));
        sf.setLevel(IndicatorName.RSI, Level.BUY_LEVEL, randomBetween(MIN_BUY_LEVEL, MAX_BUY_LEVEL));
        sf.setLevel(IndicatorName.RSI, Level.SELL_LEVEL, randomBetween(MIN_SELL_LEVEL, MAX_SELL_LEVEL));
        Integer timeFrame = randomBetween(MIN_STOCH_TIME_FRAME, MAX_STOCH_TIME_FRAME);
        sf.setParam(IndicatorName.STOCH_K, Param.TIME_FRAME, timeFrame);
        return sf;
    }
}
