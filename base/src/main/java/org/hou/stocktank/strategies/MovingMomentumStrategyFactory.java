package org.hou.stocktank.strategies;

import org.hou.stocktank.base.AbstractStrategyFactory;
import org.hou.stocktank.base.IndicatorDefinition;
import org.hou.stocktank.base.IndicatorName;
import org.hou.stocktank.base.Level;
import org.hou.stocktank.base.Param;
import org.hou.stocktank.base.StrategyFactory;

import org.ta4j.core.*;
import org.ta4j.core.indicators.StochasticOscillatorKIndicator;
import org.ta4j.core.indicators.helpers.ClosePriceIndicator;
import org.ta4j.core.indicators.EMAIndicator;
import org.ta4j.core.indicators.MACDIndicator;
import org.ta4j.core.trading.rules.CrossedDownIndicatorRule;
import org.ta4j.core.trading.rules.CrossedUpIndicatorRule;
import org.ta4j.core.trading.rules.OverIndicatorRule;
import org.ta4j.core.trading.rules.UnderIndicatorRule;

public class MovingMomentumStrategyFactory extends AbstractStrategyFactory {
    private static final int MIN_SHORT_EMA = 4;
    private static final int MAX_SHORT_EMA = 50;
    private static final int MIN_LONG_EMA = 12;
    private static final int MAX_LONG_EMA = 120;
    private static final int MIN_STOCH_K = 5;
    private static final int MAX_STOCH_K = 50;
    private static final int MIN_MACD_SHORT = 5;
    private static final int MAX_MACD_SHORT = 80;
    private static final int MIN_MACD_LONG = 10;
    private static final int MAX_MACD_LONG = 180;
    private static final int MIN_EMA_MACD = 5;
    private static final int MAX_EMA_MACD = 50;
    private static final int MIN_BUY_LEVEL = 0;
    private static final int MAX_BUY_LEVEL = 49;
    private static final int MIN_SELL_LEVEL = 50;
    private static final int MAX_SELL_LEVEL = 100;


    public MovingMomentumStrategyFactory(MovingMomentumStrategyFactory source) {
        super(source);
    }

    public MovingMomentumStrategyFactory() {
        super();
        IndicatorDefinition shortEmaDef = new IndicatorDefinition(IndicatorName.SHORT_EMA, new IndicatorDefinition(IndicatorName.CLOSE_PRICE, null))
                .withParam(Param.TIME_FRAME, 9);
        IndicatorDefinition longEmaDef = new IndicatorDefinition(IndicatorName.LONG_EMA, new IndicatorDefinition(IndicatorName.CLOSE_PRICE, null))
                .withParam(Param.TIME_FRAME, 26);
        IndicatorDefinition sotchK = new IndicatorDefinition(IndicatorName.STOCH_K, null)
                .withParam(Param.TIME_FRAME, 14)
                .withLevel(Level.BUY_LEVEL, 20)
                .withLevel(Level.SELL_LEVEL, 80);
        IndicatorDefinition macdDef = new IndicatorDefinition(IndicatorName.MACD, new IndicatorDefinition(IndicatorName.CLOSE_PRICE, null))
                .withParam(Param.SHORT_TIME_FRAME, 9)
                .withParam(Param.LONG_TIME_FRAME, 26);
        IndicatorDefinition emaMacd = new IndicatorDefinition(IndicatorName.EMA_MACD, macdDef)
                .withParam(Param.TIME_FRAME, 18);
        indicatorDefinitions.put(IndicatorName.SHORT_EMA, shortEmaDef);
        indicatorDefinitions.put(IndicatorName.LONG_EMA, longEmaDef);
        indicatorDefinitions.put(IndicatorName.STOCH_K, sotchK);
        indicatorDefinitions.put(IndicatorName.MACD, macdDef);
        indicatorDefinitions.put(IndicatorName.EMA_MACD, emaMacd);
    }


    @Override
    public String getName() {
        return "MovingMomentum";
    }

    @Override
    public Strategy buildStrategy(TimeSeries series) {
        if (series == null) {
            throw new IllegalArgumentException("Series cannot be null");
        }

        ClosePriceIndicator closePrice = new ClosePriceIndicator(series);

        // The bias is bullish when the shorter-moving average moves above the longer moving average.
        // The bias is bearish when the shorter-moving average moves below the longer moving average.
        EMAIndicator shortEma = new EMAIndicator(closePrice, getParam(IndicatorName.SHORT_EMA, Param.TIME_FRAME));
        EMAIndicator longEma = new EMAIndicator(closePrice, getParam(IndicatorName.LONG_EMA, Param.TIME_FRAME));

        //StochasticOscillatorKIndicator stochasticOscillK = new StochasticOscillatorKIndicator(series, getParam(IndicatorName.STOCH_K, Param.TIME_FRAME));
        StochasticOscillatorKIndicator stochasticOscillK = new StochasticOscillatorKIndicator(series, getParam(IndicatorName.STOCH_K, Param.TIME_FRAME));

        MACDIndicator macd = new MACDIndicator(closePrice, getParam(IndicatorName.MACD, Param.SHORT_TIME_FRAME), getParam(IndicatorName.MACD, Param.LONG_TIME_FRAME));
        EMAIndicator emaMacd = new EMAIndicator(macd, getParam(IndicatorName.EMA_MACD, Param.TIME_FRAME));

        // Entry rule
        Rule entryRule = new OverIndicatorRule(shortEma, longEma) // Trend
                .and(new CrossedDownIndicatorRule(stochasticOscillK, Decimal.valueOf(getLevel(IndicatorName.STOCH_K, Level.BUY_LEVEL)))) // Signal 1
                .and(new OverIndicatorRule(macd, emaMacd)); // Signal 2

        // Exit rule
        Rule exitRule = new UnderIndicatorRule(shortEma, longEma) // Trend
                .and(new CrossedUpIndicatorRule(stochasticOscillK, Decimal.valueOf(getLevel(IndicatorName.STOCH_K, Level.SELL_LEVEL)))) // Signal 1
                .and(new UnderIndicatorRule(macd, emaMacd)); // Signal 2

        return new BaseStrategy(entryRule, exitRule);
    }

    @Override
    public Boolean validateConfiguration() {
        return (getParam(IndicatorName.SHORT_EMA, Param.TIME_FRAME) < getParam(IndicatorName.LONG_EMA, Param.TIME_FRAME))
                && (getParam(IndicatorName.MACD, Param.SHORT_TIME_FRAME) < getParam(IndicatorName.MACD, Param.LONG_TIME_FRAME))
                && (getLevel(IndicatorName.STOCH_K, Level.SELL_LEVEL) > getLevel(IndicatorName.STOCH_K, Level.BUY_LEVEL));
    }

    @Override
    public StrategyFactory createClone() {
        return new MovingMomentumStrategyFactory(this);
    }

    @Override
    public StrategyFactory createRandom() {
        StrategyFactory sf = new MovingMomentumStrategyFactory(this);
        sf.setParam(IndicatorName.SHORT_EMA, Param.TIME_FRAME, randomBetween(MIN_SHORT_EMA, MAX_SHORT_EMA));
        sf.setParam(IndicatorName.LONG_EMA, Param.TIME_FRAME, randomBetween(MIN_LONG_EMA, MAX_LONG_EMA));
        sf.setParam(IndicatorName.STOCH_K, Param.TIME_FRAME, randomBetween(MIN_STOCH_K, MAX_STOCH_K));
        sf.setParam(IndicatorName.MACD, Param.SHORT_TIME_FRAME, randomBetween(MIN_MACD_SHORT, MAX_MACD_SHORT));
        sf.setParam(IndicatorName.MACD, Param.LONG_TIME_FRAME, randomBetween(MIN_MACD_LONG, MAX_MACD_LONG));
        sf.setParam(IndicatorName.EMA_MACD, Param.TIME_FRAME, randomBetween(MIN_EMA_MACD, MAX_EMA_MACD));
        sf.setLevel(IndicatorName.STOCH_K, Level.BUY_LEVEL, randomBetween(MIN_BUY_LEVEL, MAX_BUY_LEVEL));
        sf.setLevel(IndicatorName.STOCH_K, Level.SELL_LEVEL, randomBetween(MIN_SELL_LEVEL, MAX_SELL_LEVEL));
        return sf;
    }
}
