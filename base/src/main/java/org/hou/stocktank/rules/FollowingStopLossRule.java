package org.hou.stocktank.rules;


import org.ta4j.core.Decimal;
import org.ta4j.core.TimeSeries;
import org.ta4j.core.TradingRecord;
import org.ta4j.core.trading.rules.AbstractRule;

public class FollowingStopLossRule extends AbstractRule {
    private TimeSeries series;
    private Decimal stopLossPercent;

    public FollowingStopLossRule(TimeSeries series, Decimal stopLossPercent) {
        this.series = series;
        this.stopLossPercent = stopLossPercent;
    }

    @Override
    public boolean isSatisfied(int index, TradingRecord tradingRecord) {
        if ((tradingRecord.getLastOrder() != null) && (tradingRecord.getLastOrder().isBuy())) {
            Decimal maxValue = Decimal.ZERO;
            for (int i = tradingRecord.getLastOrder().getIndex() + 1; i < index; i++) {
                maxValue = (maxValue.isLessThan(series.getTick(i).getClosePrice())) ? series.getTick(i).getClosePrice() : maxValue;
            }
            if ((maxValue.isGreaterThan(series.getTick(index).getClosePrice())) && (maxValue.toDouble() * stopLossPercent.toDouble() / 100 < (maxValue.toDouble() - series.getTick(index).getClosePrice().toDouble()))) {
                return true;
            }
        }
        return false;
    }

}
