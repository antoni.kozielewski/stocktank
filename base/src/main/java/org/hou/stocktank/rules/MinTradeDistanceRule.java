package org.hou.stocktank.rules;

import org.ta4j.core.Order;
import org.ta4j.core.Trade;
import org.ta4j.core.TradingRecord;
import org.ta4j.core.trading.rules.AbstractRule;

public class MinTradeDistanceRule extends AbstractRule{
    private int distance;

    public MinTradeDistanceRule(int minDistance) {
        super();
        this.distance = minDistance;
    }

    @Override
    public boolean isSatisfied(int i, TradingRecord tradingRecord) {
        Order order = tradingRecord.getLastOrder();
        if (order != null) {
            return i >= order.getIndex() + distance;
        }
        return true;
    }

}
