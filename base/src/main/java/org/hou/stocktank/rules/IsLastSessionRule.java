package org.hou.stocktank.rules;

import org.ta4j.core.TimeSeries;
import org.ta4j.core.TradingRecord;
import org.ta4j.core.trading.rules.AbstractRule;


public class IsLastSessionRule extends AbstractRule {
    private int sessionsCount;

    public IsLastSessionRule(TimeSeries series){
        this.sessionsCount = series.getTickCount();
    }

    @Override
    public boolean isSatisfied(int i, TradingRecord tradingRecord) {
        return i >= sessionsCount-1;
    }

}
