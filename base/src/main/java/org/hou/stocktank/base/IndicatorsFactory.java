package org.hou.stocktank.base;

import java.util.HashMap;
import java.util.Map;

import org.hou.stocktank.indicators.ParabolicSARIndicator;
import org.hou.stocktank.indicators.RelativeVigorIndexIndicator;
import org.hou.stocktank.indicators.VolumeWeightedMovingAverage;

import org.ta4j.core.Indicator;
import org.ta4j.core.TimeSeries;
import org.ta4j.core.indicators.*;
import org.ta4j.core.indicators.helpers.ClosePriceIndicator;
import org.ta4j.core.indicators.helpers.MaxPriceIndicator;
import org.ta4j.core.indicators.helpers.MinPriceIndicator;
import org.ta4j.core.indicators.helpers.VolumeIndicator;


public class IndicatorsFactory {
    private Map<IndicatorName, Indicator> indicators = new HashMap<>();

    public Map<IndicatorName, Indicator> buildIndicators(Map<IndicatorName, IndicatorDefinition> indicatorDefinitions, TimeSeries series) {
        for (IndicatorName in : indicatorDefinitions.keySet()) {
            buildIndicator(indicatorDefinitions.get(in), series);
        }
        return indicators;
    }

    private Indicator buildIndicator(IndicatorDefinition def, TimeSeries series) {
        if (indicators.containsKey(def.getIndicatorName())) return indicators.get(def.getIndicatorName());
        // nima to trzeba zrobic
        try {
            switch (def.getIndicatorName()) {
                case CLOSE_PRICE:
                    indicators.put(def.getIndicatorName(), new ClosePriceIndicator(series));
                    return indicators.get(def.getIndicatorName());
                case MAX_PRICE:
                    indicators.put(def.getIndicatorName(), new MaxPriceIndicator(series));
                    return indicators.get(def.getIndicatorName());
                case MIN_PRICE:
                    indicators.put(def.getIndicatorName(), new MinPriceIndicator(series));
                    return indicators.get(def.getIndicatorName());
                case MACD:
                    indicators.put(def.getIndicatorName(),
                            new MACDIndicator(buildIndicator(def.getBaseIndicator(), series),
                                    def.getParam(Param.SHORT_TIME_FRAME),
                                    def.getParam(Param.LONG_TIME_FRAME)));
                    return indicators.get(def.getIndicatorName());
                case RSI:
                    indicators.put(def.getIndicatorName(),
                            new RSIIndicator(buildIndicator(def.getBaseIndicator(), series),
                                    def.getParam(Param.TIME_FRAME)));
                    return indicators.get(def.getIndicatorName());
                case EMA:
                case SHORT_EMA:
                case LONG_EMA:
                case EMA_MACD:
                    indicators.put(def.getIndicatorName(),
                            new EMAIndicator(buildIndicator(def.getBaseIndicator(), series),
                                    def.getParam(Param.TIME_FRAME)));
                    return indicators.get(def.getIndicatorName());
                case STOCH_K:
                    if (def.getBaseIndicator() == null) {
                        indicators.put(def.getIndicatorName(), new StochasticOscillatorKIndicator(series, def.getParam(Param.TIME_FRAME)));
                    } else {
                        indicators.put(def.getIndicatorName(), new StochasticOscillatorKIndicator(buildIndicator(def.getBaseIndicator(), series), def.getParam(Param.TIME_FRAME), new MaxPriceIndicator(series), new MinPriceIndicator
                                (series)));
                    }
                    return indicators.get(def.getIndicatorName());
                case STOCH_D:
                    indicators.put(def.getIndicatorName(), new StochasticOscillatorDIndicator(buildIndicator(def.getBaseIndicator(), series)));
                    return indicators.get(def.getIndicatorName());
                case WILLIAMS_R:
                    indicators.put(def.getIndicatorName(), new WilliamsRIndicator(series, def.getParam(Param.TIME_FRAME)));
                    return indicators.get(def.getIndicatorName());
                case AROON_UP:
                    indicators.put(def.getIndicatorName(), new AroonUpIndicator(series, def.getParam(Param.TIME_FRAME)));
                    return indicators.get(def.getIndicatorName());
                case CMO:
                    indicators.put(def.getIndicatorName(), new CMOIndicator(new ClosePriceIndicator(series), def.getParam(Param.TIME_FRAME)));
                    return indicators.get(def.getIndicatorName());
                case VOLUME:
                    indicators.put(def.getIndicatorName(), new VolumeIndicator(series));
                    return indicators.get(def.getIndicatorName());
                case RVI:
                    indicators.put(def.getIndicatorName(), new RelativeVigorIndexIndicator(series));
                    return indicators.get(def.getIndicatorName());
                case VWMA:
                    indicators.put(def.getIndicatorName(), new VolumeWeightedMovingAverage(buildIndicator(def.getBaseIndicator(),series), new VolumeIndicator(series), def.getParam(Param.TIME_FRAME)));
                    return indicators.get(def.getIndicatorName());
                case SMA:
                    indicators.put(def.getIndicatorName(), new SMAIndicator(buildIndicator(def.getBaseIndicator(), series), def.getParam(Param.TIME_FRAME)));
                    return indicators.get(def.getIndicatorName());
                case PSAR:
                    indicators.put(def.getIndicatorName(), new ParabolicSARIndicator(series));
                    return indicators.get(def.getIndicatorName());
                default:
                    throw new RuntimeException("Indicator definitions not found in IndicatorsFactory.buildIndicator : " + def.getIndicatorName());
            }
        } catch (IllegalArgumentException e) {
            System.out.println("Illegal argument: " + def.toString());
            throw e;
        }
    }

}
