package org.hou.stocktank.base;

public enum Level {
    TRIGGER_LEVEL("trigger"),
    BUY_LEVEL("buy level"),
    SELL_LEVEL("sell level");

    private String value;

    private Level(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    public String toString() {
        return value;
    }
}
