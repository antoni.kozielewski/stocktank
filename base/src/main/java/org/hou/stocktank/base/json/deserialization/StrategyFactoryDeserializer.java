package org.hou.stocktank.base.json.deserialization;

import java.lang.reflect.Type;
import java.util.Map;

import org.hou.stocktank.base.IndicatorDefinition;
import org.hou.stocktank.base.IndicatorName;
import org.hou.stocktank.base.StrategyFactory;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;

public class StrategyFactoryDeserializer implements JsonDeserializer<StrategyFactory> {
    @Override
    public StrategyFactory deserialize(JsonElement jsonElement, Type type, JsonDeserializationContext jsonDeserializationContext) throws JsonParseException {
        IndicatorDefinitionDeserializer indicatorDefinitionDeserializer = new IndicatorDefinitionDeserializer();
        JsonObject jsonObject = (JsonObject) jsonElement;
        String strategyFactoryClassName = jsonObject.get("strategyFactoryClassName").getAsString();
        Class c = null;
        try {
            // sama strategia jest brana po refleksji
            c = Class.forName(strategyFactoryClassName);
            StrategyFactory sf = (StrategyFactory) c.newInstance();
            // deserializujemy wszystkie definicje indykatorow
            for (Map.Entry<String, JsonElement> entry : jsonObject.get("indicatorDefinitions").getAsJsonObject().entrySet()) {
                IndicatorDefinition indicatorDefinition = indicatorDefinitionDeserializer.deserialize(entry.getValue(), IndicatorDefinition.class, jsonDeserializationContext);
                sf.setIndicatorDefinition(IndicatorName.valueOf(entry.getKey()), indicatorDefinition);
            }
            return sf;
        } catch (ClassNotFoundException e) {
            throw new RuntimeException(e);
        } catch (InstantiationException e) {
            throw new RuntimeException(e);
        } catch (IllegalAccessException e) {
            throw new RuntimeException(e);
        }
    }
}
