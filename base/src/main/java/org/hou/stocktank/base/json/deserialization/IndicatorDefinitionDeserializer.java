package org.hou.stocktank.base.json.deserialization;


import java.lang.reflect.Type;
import java.util.Map;

import org.hou.stocktank.base.Configurable;
import org.hou.stocktank.base.Configuration;
import org.hou.stocktank.base.IndicatorDefinition;
import org.hou.stocktank.base.IndicatorName;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;

public class IndicatorDefinitionDeserializer implements JsonDeserializer<IndicatorDefinition> {

    public Configurable deserializeConfiguraiton(JsonObject jsonObject) {
        Configurable result = new Configuration();
        for (Map.Entry<String, JsonElement> entry : jsonObject.getAsJsonObject("configuration").entrySet()) {
            result.setParam(entry.getKey(), entry.getValue().getAsInt());
        }
        return result;
    }

    @Override
    public IndicatorDefinition deserialize(JsonElement jsonElement, Type type, JsonDeserializationContext jsonDeserializationContext) throws JsonParseException {
        JsonObject jobject = (JsonObject) jsonElement;
        IndicatorDefinition baseIndicator = null;
        if (jobject.get("baseIndicator") != null) {
            baseIndicator = deserialize(jobject.get("baseIndicator"), IndicatorDefinition.class, jsonDeserializationContext);
        }
        Configurable config = deserializeConfiguraiton(jobject.getAsJsonObject("config"));
        Configurable levels = deserializeConfiguraiton(jobject.getAsJsonObject("levels"));
        IndicatorDefinition result = new IndicatorDefinition(IndicatorName.valueOf(jobject.get("indicatorName").getAsString()), baseIndicator)
                .withParams(config)
                .withLevels(levels);
        return result;
    }
}
