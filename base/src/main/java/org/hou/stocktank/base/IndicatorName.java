package org.hou.stocktank.base;


public enum IndicatorName {

    MAX_PRICE,
    MIN_PRICE,
    CLOSE_PRICE,
    VOLUME,

    SMA,
    EMA,

    MACD,
    RSI,

    AROON_UP,
    CMO,
    EMA_MACD,
    SHORT_EMA,
    LONG_EMA,
    CCI,
    STOCH_K,
    STOCH_D,
    WILLIAMS_R,
    RVI,
    VWMA, // volume weighted moving average
    PPO,
    PSAR; // Parabolic Stop and Reverse
}
