package org.hou.stocktank.base;

import java.util.Map;

public class IndicatorDefinition implements Configurable {
    private IndicatorDefinition baseIndicator;
    private Configurable config = new Configuration();
    private Configurable levels = new Configuration();
    private String name = "unnamed";
    private IndicatorName indicatorName;

    public IndicatorDefinition(IndicatorName indicatorName, IndicatorDefinition baseIndicator) {
        this.indicatorName = indicatorName;
        this.baseIndicator = baseIndicator;
    }

    public IndicatorName getIndicatorName() {
        return indicatorName;
    }

    public IndicatorDefinition withLevels(Configurable levels) {
        this.levels = levels;
        return this;
    }

    public Configurable getLevels() {
        return levels;
    }

    public void setLevels(Configurable levels) {
        this.levels = levels;
    }

    public Integer getLevel(Level level) {
        return levels.getParam(level.getValue());
    }

    public Integer getLevel(String level) {
        return levels.getParam(level);
    }

    public void setLevel(Level level, Integer value) {
        levels.setParam(level.getValue(), value);
    }

    public void setLevel(String level, Integer value) {
        levels.setParam(level, value);
    }

    public IndicatorDefinition withLevel(Level level, Integer value) {
        levels.setParam(level.getValue(), value);
        return this;
    }

    public IndicatorDefinition withParams(Configurable config) {
        this.config = config;
        return this;
    }

    public IndicatorDefinition withConfig(Configurable config) {
        return withParams(config);
    }

    public Map<String, Integer> getParams() {
        return config.getParams();
    }

    public void setParams(Configurable config) {
        this.config = config;
    }

    public IndicatorDefinition withParam(Param param, Integer value) {
        setParam(param.getValue(), value);
        return this;
    }

    public Configurable getConfig() {
        return this.config;
    }

    public void setParam(String key, Integer value) {
        config.setParam(key, value);
    }

    public Integer getParam(Param param) {
        return config.getParam(param.getValue());
    }

    public Integer getParam(String key) {
        return config.getParam(key);
    }


    public IndicatorDefinition getBaseIndicator() {
        return baseIndicator;
    }

    public void copy(Configurable source) {
        config.copy(source);
    }

    public void copy(IndicatorDefinition source) {
        this.baseIndicator = source.getBaseIndicator();
        copy(source.getConfig());
        this.name = source.name;
        this.indicatorName = indicatorName;
    }

    public String toString() {
        String result = indicatorName.toString();
        result = result + " : " + config.toString();
        result = result + " {" + levels.toString() + "} ";
        return result;
    }
}
