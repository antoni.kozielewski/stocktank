package org.hou.stocktank.base;

import java.util.Map;

public interface Configurable {
    Map<String, Integer> getParams();

    void setParam(String key, Integer value);

    Integer getParam(String key);

    void copy(Configurable source);
}
