package org.hou.stocktank.base;


import org.ta4j.core.Strategy;
import org.ta4j.core.TimeSeries;

import java.util.Map;

public interface StrategyFactory {

    String getName();

    Strategy buildStrategy(TimeSeries series);

    void setIndicatorDefinition(IndicatorName key, IndicatorDefinition definition);

    Map<IndicatorName, IndicatorDefinition> getIndicatorsDefinitions();

    StrategyFactory setParam(IndicatorName indicator, Param param, Integer value);

    StrategyFactory withParam(IndicatorName indicator, Param param, Integer value);

    StrategyFactory setParam(IndicatorName indicator, String param, Integer value);

    StrategyFactory withLevel(IndicatorName indicatorName, String level, Integer value);

    StrategyFactory withLevel(IndicatorName indicatorName, Level level, Integer value);

    StrategyFactory setLevel(IndicatorName indicatorName, Level level, Integer value);

    Integer getLevel(IndicatorName indicatorName, Level level);

    Integer getParam(IndicatorName indicatorName, Param param);

    Boolean validateConfiguration();

    int getUnstablePeriod();

    void setUnstablePeriod(int unstablePeriod);

    void copy(StrategyFactory sf);

    StrategyFactory createClone();

    StrategyFactory createRandom();

    String getDescription();

    String getStrategyFactoryClassName();

    String getLifeDescription();

    void setLifeDescription(String desc);

}
