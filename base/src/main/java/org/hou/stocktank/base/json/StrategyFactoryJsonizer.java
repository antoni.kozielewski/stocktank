package org.hou.stocktank.base.json;

import org.hou.stocktank.base.StrategyFactory;
import org.hou.stocktank.base.json.deserialization.StrategyFactoryDeserializer;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class StrategyFactoryJsonizer {

    public static String serialize(StrategyFactory strategyFactory) {
        GsonBuilder builder = new GsonBuilder();
        builder.registerTypeAdapter(StrategyFactory.class, new StrategyFactoryDeserializer());
        Gson gson = builder.create();
        return gson.toJson(strategyFactory);
    }

    public static StrategyFactory deserialize(String json) {
        GsonBuilder builder = new GsonBuilder();
        builder.registerTypeAdapter(StrategyFactory.class, new StrategyFactoryDeserializer());
        Gson gson = builder.create();
        return gson.fromJson(json, StrategyFactory.class);
    }

}
