package org.hou.stocktank.base;

import java.util.HashMap;
import java.util.Map;

public abstract class AbstractStrategyFactory implements StrategyFactory {
    protected String strategyFactoryClassName = this.getClass().getName();
    protected Map<IndicatorName, IndicatorDefinition> indicatorDefinitions = new HashMap<>();
    protected String lifeDescription;
    protected String description;
    protected int unstablePeriod = 0;

    public AbstractStrategyFactory() {
    }

    public AbstractStrategyFactory(StrategyFactory source) {
        this.copy(source);
    }

    @Override
    public void setIndicatorDefinition(IndicatorName key, IndicatorDefinition definition) {
        indicatorDefinitions.put(key, definition);
    }

    @Override
    public StrategyFactory setParam(IndicatorName key, Param param, Integer value) {
        this.setParam(key, param.getValue(), value);
        return this;
    }

    @Override
    public StrategyFactory withParam(IndicatorName key, Param param, Integer value) {
        return setParam(key, param, value);
    }

    @Override
    public StrategyFactory setParam(IndicatorName key, String param, Integer value) {
        indicatorDefinitions.get(key).setParam(param, value);
        return this;
    }

    @Override
    public StrategyFactory withLevel(IndicatorName indicatorName, Level level, Integer value) {
        return setLevel(indicatorName, level, value);
    }

    public StrategyFactory withLevel(IndicatorName indicatorName, String level, Integer value) {
        IndicatorDefinition def = this.getIndicatorsDefinitions().get(indicatorName);
        def.setLevel(level, value);
        this.indicatorDefinitions.put(indicatorName, def);
        return this;
    }

    @Override
    public StrategyFactory setLevel(IndicatorName indicatorName, Level level, Integer value) {
        IndicatorDefinition def = this.getIndicatorsDefinitions().get(indicatorName);
        def.setLevel(level, value);
        this.indicatorDefinitions.put(indicatorName, def);
        return this;
    }

    @Override
    public Integer getLevel(IndicatorName indicatorName, Level level) {
        return this.getIndicatorsDefinitions().get(indicatorName).getLevel(level);
    }

    @Override
    public Integer getParam(IndicatorName indicatorName, Param param) {
        return this.getIndicatorsDefinitions().get(indicatorName).getParam(param);
    }

    @Override
    public Map<IndicatorName, IndicatorDefinition> getIndicatorsDefinitions() {
        return indicatorDefinitions;
    }

    @Override
    public int getUnstablePeriod() {
        return unstablePeriod;
    }

    @Override
    public void setUnstablePeriod(int unstablePeriod) {
        this.unstablePeriod = unstablePeriod;
    }

    @Override
    public void copy(StrategyFactory source) {
        for (IndicatorName indicatorName : source.getIndicatorsDefinitions().keySet()) {
            IndicatorDefinition sourceId = source.getIndicatorsDefinitions().get(indicatorName);
            IndicatorDefinition newId = new IndicatorDefinition(indicatorName, sourceId.getBaseIndicator());
            newId.copy(sourceId);
            this.indicatorDefinitions.put(indicatorName, newId);
        }
        this.unstablePeriod = source.getUnstablePeriod();
    }

    @Override
    public String toString() {
        String result = this.getClass().getCanonicalName();
        for (IndicatorName name : indicatorDefinitions.keySet()) {
            result = result + ", [" + indicatorDefinitions.get(name).toString() + "]";
        }
        return result + " :: " + description;
    }

    @Override
    public String getDescription() {
        StringBuilder sb = new StringBuilder();
        sb.append(getName() + "[");
        for(IndicatorName indicatorName : indicatorDefinitions.keySet()) {
            sb.append(indicatorDefinitions.get(indicatorName).toString());
        }
        sb.append("]");
        return sb.toString();
    }

    protected int randomBetween(int min, int max) {
        return min + (int) (Math.random() * (max - min));
    }

    @Override
    public String getStrategyFactoryClassName() {
        return strategyFactoryClassName;
    }

    @Override
    public String getLifeDescription() {
        return lifeDescription;
    }

    public void setLifeDescription(String desc) {
        this.lifeDescription = desc;
    }
}
