package org.hou.stocktank.base;

import java.util.HashMap;
import java.util.Map;

public class Configuration implements Configurable {
    private Map<String, Integer> configuration = new HashMap<>();

    @Override
    public Map<String, Integer> getParams() {
        return configuration;
    }

    @Override
    public void setParam(String key, Integer value) {
        configuration.put(key, value);
    }

    public Integer getParam(String key) {
        return configuration.get(key);
    }

    public String toString() {
        String s = "(";
        String separator = "";
        for (String key : configuration.keySet()) {
            s = s + separator + key + "=" + configuration.get(key);
            separator = ", ";
        }
        s = s + ")";
        return s;
    }

    public void copy(Configurable source) {
        for (String key : source.getParams().keySet()) {
            this.setParam(key, source.getParam(key));
        }
    }

}
