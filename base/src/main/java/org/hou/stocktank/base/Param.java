package org.hou.stocktank.base;


public enum Param {
    TIME_FRAME("time_frame"),
    SHORT_TIME_FRAME("short_time_frame"),
    LONG_TIME_FRAME("long_time_frame"),
    ACCELERATION("acceleration"),
    INIT_ACCELERATION("init_acceleration"),
    MAX_ACCELERATION("max_acceleration");


    private String value;

    private Param(String value) {
        this.value = value;
    }

    public String getValue() {
        return this.value;
    }

    public String toString() {
        return value;
    }
}
