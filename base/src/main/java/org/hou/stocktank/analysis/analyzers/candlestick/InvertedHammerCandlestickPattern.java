package org.hou.stocktank.analysis.analyzers.candlestick;

import org.hou.stocktank.analysis.AnalysisResult;
import org.hou.stocktank.analysis.MarketState;
import org.ta4j.core.TimeSeries;

public class InvertedHammerCandlestickPattern extends BaseCandlestickAnalyzer {


    public InvertedHammerCandlestickPattern(TimeSeries series) {
        super(series);
    }

    private String getName() {
        return "/\\ - Inverted Hammer pattern";
    }

    private String getId() {
        return "InvertedHarami";
    }

    @Override
    public AnalysisResult getValue(int i) {
        if ((i > 3)
                && isDownstairs(i - 3) && isDownstairs(i - 2) && isDownstairs(i - 1)
                && isValueLowerThanBody(getPrevious(i), getTimeSeries().getTick(i).getOpenPrice())
                && hasLowerShadow(i)
                && isFalling(i)
                && !hasUpperShadow(i)
                ) {
            return new AnalysisResult(MarketState.TURN_UP, 0, "...", getName(), getId());
        } else {
            return new AnalysisResult(MarketState.UNKNOWN, 0, "...", getName(), getId());
        }
    }
}
