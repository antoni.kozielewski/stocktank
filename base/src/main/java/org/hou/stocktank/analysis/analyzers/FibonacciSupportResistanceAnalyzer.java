package org.hou.stocktank.analysis.analyzers;

import org.hou.stocktank.analysis.AnalysisResult;
import org.hou.stocktank.analysis.MarketState;
import org.hou.stocktank.indicators.pivotpoints.FibonacciSupportResistanceIndicator;
import org.hou.stocktank.indicators.pivotpoints.PivotPoint;
import org.hou.stocktank.indicators.pivotpoints.PivotPointIndicator;
import org.hou.stocktank.indicators.pivotpoints.SimplePivotPointIndicator;
import org.ta4j.core.Decimal;
import org.ta4j.core.TimeSeries;

import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.stream.Collectors;

public class FibonacciSupportResistanceAnalyzer extends Analyzer {
    protected static String SOURCE;
    protected static String SOURCE_ID = FibonacciSupportResistanceAnalyzer.class.getSimpleName();
    private PivotPointIndicator pivotPointIndicator;
    private FibonacciSupportResistanceIndicator fibonacciIndicator;

    public FibonacciSupportResistanceAnalyzer(TimeSeries series) {
        super(series);
        this.pivotPointIndicator = new SimplePivotPointIndicator(series);
        this.fibonacciIndicator = new FibonacciSupportResistanceIndicator(pivotPointIndicator);
        this.SOURCE = "Fibonacci ";
        this.SOURCE_ID = this.getClass().getSimpleName();
    }

    public FibonacciSupportResistanceAnalyzer(PivotPointIndicator pivotPointIndicator) {
        super(pivotPointIndicator.getTimeSeries());
        this.pivotPointIndicator = pivotPointIndicator;
        this.fibonacciIndicator = new FibonacciSupportResistanceIndicator(pivotPointIndicator);
        this.SOURCE = "Fibonacci ";
        this.SOURCE_ID = this.getClass().getSimpleName();
    }

    public FibonacciSupportResistanceAnalyzer(PivotPointIndicator pivotPointIndicator, String nameSuffix) {
        this(pivotPointIndicator);
        this.SOURCE = "Fibonacci " + nameSuffix;
        this.SOURCE_ID = this.getClass().getSimpleName();
    }

    private Decimal getFirstLessThan(Decimal currentValue, List<Decimal> data) {
        for (Decimal value : data) {
            if (value.isLessThanOrEqual(currentValue)) return value;
        }
        return Decimal.ZERO;
    }

    private Decimal getFirstGreaterThan(Decimal currentValue, List<Decimal> data) {
        for (Decimal value : data) {
            if (value.isGreaterThanOrEqual(currentValue)) return value;
        }
        return Decimal.ZERO;
    }

    private String getPivotPointDescription(PivotPoint point) {
        return "[" + getTimeSeries().getTick(point.getIndex()).getEndTime().format(DateTimeFormatter.ISO_DATE) + " | " + point.getType() + " | " + point.getValue() + "]";
    }

    private String getDescription(int index, List<Decimal> data) {
        String pivotPointDescription = getPivotPointDescription(pivotPointIndicator.getValue(index));
        String dataString = data.stream().map(x -> String.format("%.2f", x.toDouble())).collect(Collectors.joining(", "));
        return dataString + " " + pivotPointDescription;
    }

    @Override
    public AnalysisResult getValue(int index) {
        List<Decimal> data = fibonacciIndicator.getValue(index);
        if (pivotPointIndicator.getValue(index).getType() == PivotPoint.PivotType.MAX) {
            if (getFirstLessThan(series.getTick(index).getClosePrice(), data).toDouble() == 0) {
                return new AnalysisResult(MarketState.UNKNOWN, Decimal.ZERO, "", SOURCE, SOURCE_ID);
            }
            return new AnalysisResult(MarketState.DOWN, getFirstLessThan(series.getTick(index).getClosePrice(), data), getDescription(index, data), SOURCE, SOURCE_ID);
        } else {
            if (getFirstGreaterThan(series.getTick(index).getClosePrice(), data).toDouble() == 0) {
                return new AnalysisResult(MarketState.UNKNOWN, Decimal.ZERO, "", SOURCE, SOURCE_ID);
            }
            return new AnalysisResult(MarketState.UP, getFirstGreaterThan(series.getTick(index).getClosePrice(), data), getDescription(index, data), SOURCE, SOURCE_ID);
        }
    }
}
