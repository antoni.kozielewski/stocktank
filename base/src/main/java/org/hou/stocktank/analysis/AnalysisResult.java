package org.hou.stocktank.analysis;

import org.ta4j.core.Decimal;

import java.util.HashMap;
import java.util.Map;

public class AnalysisResult {
    private MarketState marketState;
    private String description;
    private double value;
    private String source;
    private String sourceId;
    private Map<String, String> additionalData = new HashMap<>();

    public AnalysisResult(MarketState marketState, Decimal value, String description, String source, String sourceId) {
        this(marketState, value.toDouble(), description, source, sourceId);
    }

    public AnalysisResult(MarketState marketState, double value, String description, String source, String sourceId) {
        this.marketState = marketState;
        this.description = description;
        this.value = value;
        this.source = source;
        this.sourceId = sourceId;
    }

    public AnalysisResult(MarketState marketState, double value, String description, String source, String sourceId, Map<String, String> additionalData) {
        this(marketState, value, description, source, sourceId);
        this.additionalData = additionalData;
    }

    public MarketState getMarketState() {
        return marketState;
    }

    public String getDescription() {
        return description;
    }

    public double getValue() {
        return value;
    }

    public String getSource() {
        return source;
    }

    public String getSourceId() {
        return sourceId;
    }

    public Map<String, String> getAdditionalData() {
        return additionalData;
    }

    public String toString() {
        return source + " | " + marketState + " | value= " + value + " | " + description;
    }
}
