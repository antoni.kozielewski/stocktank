package org.hou.stocktank.analysis.analyzers.wrappers;

import org.hou.stocktank.analysis.analyzers.FibonacciSupportResistanceAnalyzer;
import org.hou.stocktank.indicators.pivotpoints.SimplePivotPointIndicator;
import org.ta4j.core.TimeSeries;


public class FibonacciShortRangeAnalyzerWrapper extends FibonacciSupportResistanceAnalyzer {

    public FibonacciShortRangeAnalyzerWrapper(TimeSeries series) {
        super(new SimplePivotPointIndicator(series, 10, 5, 0.06, 0.1), "Short Range");

    }

}
