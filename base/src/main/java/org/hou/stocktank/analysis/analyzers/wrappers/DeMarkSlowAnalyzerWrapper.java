package org.hou.stocktank.analysis.analyzers.wrappers;

import org.hou.stocktank.analysis.analyzers.demark.DeMarkReversalAnalyzer;
import org.hou.stocktank.indicators.pivotpoints.SimplePivotPointIndicator;
import org.ta4j.core.TimeSeries;

public class DeMarkSlowAnalyzerWrapper extends DeMarkReversalAnalyzer {

    public DeMarkSlowAnalyzerWrapper(TimeSeries series) {
        super(new SimplePivotPointIndicator(series, 20, 10, 0.12, 0.18));
    }

}
