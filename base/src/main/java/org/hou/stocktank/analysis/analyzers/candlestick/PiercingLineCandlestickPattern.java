package org.hou.stocktank.analysis.analyzers.candlestick;

import org.ta4j.core.TimeSeries;
import org.hou.stocktank.analysis.AnalysisResult;
import org.hou.stocktank.analysis.MarketState;

public class PiercingLineCandlestickPattern extends BaseCandlestickAnalyzer {

    public PiercingLineCandlestickPattern(TimeSeries series) {
        super(series);
    }

    private String getName() {
        return "/\\  - Piercing line";
    }

    private String getId() {
        return "PiercingLine";
    }

    @Override
    public AnalysisResult getValue(int i) {
        if (i > 3) {
            if (isFalling(i - 2)
                    && isFalling(i - 1)
                    && isRising(i)
                    && isValueInBody(getTick(i - 2), getTick(i - 1).getOpenPrice())
                    && isValueInBody(getTick(i - 1), getTick(i - 2).getClosePrice())
                    && isValueInBody(getTick(i), getTick(i - 1).getOpenPrice())
                    ) {
                return new AnalysisResult(MarketState.TURN_UP, 0, "", getName(), getId());
            }
        }
        return new AnalysisResult(MarketState.UNKNOWN, 0, "...", getName(), getId());
    }

}
