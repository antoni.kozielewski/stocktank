package org.hou.stocktank.analysis.analyzers;

import org.ta4j.core.Decimal;
import org.ta4j.core.TimeSeries;
import org.ta4j.core.indicators.CachedIndicator;
import org.ta4j.core.indicators.WilliamsRIndicator;
import org.hou.stocktank.analysis.AnalysisResult;
import org.hou.stocktank.analysis.MarketState;

public class WilliamsAnalyzerIndicator extends Analyzer {
    private WilliamsRIndicator indicator;
    private int timeFrame = 14;
    private Decimal overboughtLevel = Decimal.valueOf(-20);
    private Decimal oversoldLevel = Decimal.valueOf(-80);

    public WilliamsAnalyzerIndicator(TimeSeries series) {
        super(series);
        prepare();
    }

    public WilliamsAnalyzerIndicator(TimeSeries series, int timeFrame, int overboughtLevel, int oversoldLevel) {
        super(series);
        this.timeFrame = timeFrame;
        this.overboughtLevel = Decimal.valueOf(overboughtLevel);
        this.oversoldLevel = Decimal.valueOf(oversoldLevel);
        prepare();
    }

    private void prepare() {
        this.indicator = new WilliamsRIndicator(getTimeSeries(), timeFrame);
    }

    private String getName() {
        return "%R [" + timeFrame + "] {" + overboughtLevel + "," + oversoldLevel + "}";
    }

    private String getId() {
        return "%R" + timeFrame + "|" + overboughtLevel + "|" + oversoldLevel;
    }

    @Override
    public AnalysisResult getValue(int i) {
        if (indicator.getValue(i).isGreaterThan(overboughtLevel)) {
            return new AnalysisResult(MarketState.OVERBOUGHT, indicator.getValue(i).toDouble(), "", getName(), getId());
        } else if (indicator.getValue(i).isLessThan(oversoldLevel)) {
            return new AnalysisResult(MarketState.OVERSOLD, indicator.getValue(i).toDouble(), "", getName(), getId());
        }
        return new AnalysisResult(MarketState.UNKNOWN, indicator.getValue(i).toDouble(), "", getName(), getId());
    }
}
