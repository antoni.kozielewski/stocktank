package org.hou.stocktank.analysis.analyzers.candlestick;

import org.ta4j.core.TimeSeries;
import org.hou.stocktank.analysis.AnalysisResult;
import org.hou.stocktank.analysis.MarketState;

public class StarCandlestickPattern extends BaseCandlestickAnalyzer {

    public StarCandlestickPattern(TimeSeries series) {
        super(series);
    }

    private String getName() {
        return "Star pattern";
    }

    private String getId() {
        return "Star";
    }

    @Override
    public AnalysisResult getValue(int i) {
        if (i > 2) {
            if (isRising(i - 2)) {
                if (isCross(i - 1)
                        && isFalling(i)
                        && isValueHigherThanBody(getPrevious(i - 2), getTick(i - 1).getOpenPrice())
                        && isValueHigherThanBody(getTick(i), getTick(i - 1).getOpenPrice())
                        ) {
                    return new AnalysisResult(MarketState.TURN_DOWN, 0, "Evening doji star - finish rising trend", getName(), getId());
                }
                if (isRising(i - 1)
                        && isFalling(i)
                        && getTick(i).getOpenPrice().isEqual(getTick(i - 1).getOpenPrice())
                        && getTick(i).getOpenPrice().isEqual(getTick(i - 2).getClosePrice())
                        ) {
                    return new AnalysisResult(MarketState.TURN_DOWN, 0, "Evening star - finish rising trend", getName(), getId());
                }
            } else {
                if (isCross(i - 1)
                        && isRising(i)
                        && isValueLowerThanBody(getPrevious(i - 2), getTick(i - 1).getOpenPrice())
                        && isValueLowerThanBody(getTick(i), getTick(i - 1).getOpenPrice())
                        ) {
                    return new AnalysisResult(MarketState.TURN_UP, 0, "Morning doji star - finish falling trend", getName(), getId());
                }
                if (isFalling(i - 1)
                        && isRising(i)
                        && getTick(i).getOpenPrice().isEqual(getTick(i - 1).getOpenPrice())
                        && getTick(i).getOpenPrice().isEqual(getTick(i - 2).getClosePrice())
                        ) {
                    return new AnalysisResult(MarketState.TURN_UP, 0, "Morning star - finish falling trend", getName(), getId());
                }
            }
        }
        return new AnalysisResult(MarketState.UNKNOWN, 0, "...", getName(), getId());
    }
}
