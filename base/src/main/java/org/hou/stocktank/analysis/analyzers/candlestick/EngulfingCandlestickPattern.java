package org.hou.stocktank.analysis.analyzers.candlestick;

import org.ta4j.core.TimeSeries;
import org.hou.stocktank.analysis.AnalysisResult;
import org.hou.stocktank.analysis.MarketState;

public class EngulfingCandlestickPattern extends BaseCandlestickAnalyzer {

    public EngulfingCandlestickPattern(TimeSeries series) {
        super(series);
    }

    private String getName() {
        return "Engulfing pattern";
    }

    private String getId() {
        return "Engulfing";
    }

    @Override
    public AnalysisResult getValue(int i) {
        if (i > 1) {
            if (isRising(i - 1)) {
                if (isFalling(i)
                        && isValueInBody(getTick(i), getTick(i - 1).getMaxPrice())
                        && isValueInBody(getTick(i), getTick(i - 1).getMinPrice())) {
                    return new AnalysisResult(MarketState.TURN_DOWN, 0, "finish rising trend", getName(), getId());
                }
            } else if (isRising(i)
                    && isValueInBody(getTick(i), getTick(i - 1).getMaxPrice())
                    && isValueInBody(getTick(i), getTick(i - 1).getMinPrice())) {
                return new AnalysisResult(MarketState.TURN_UP, 0, "finish falling trend", getName(), getId());
            }
        }
        return new AnalysisResult(MarketState.UNKNOWN, 0, "...", getName(), getId());
    }
}
