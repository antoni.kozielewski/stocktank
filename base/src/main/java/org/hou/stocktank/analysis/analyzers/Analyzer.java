package org.hou.stocktank.analysis.analyzers;

import org.hou.stocktank.analysis.AnalysisResult;
import org.ta4j.core.Indicator;
import org.ta4j.core.TimeSeries;

public abstract class Analyzer implements Indicator<AnalysisResult> {
    protected TimeSeries series;

    public Analyzer(TimeSeries series) {
        this.series = series;
    }

    public TimeSeries getTimeSeries() {
        return series;
    }

}
