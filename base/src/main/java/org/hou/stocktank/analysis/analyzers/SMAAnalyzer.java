package org.hou.stocktank.analysis.analyzers;

import org.hou.stocktank.analysis.AnalysisResult;
import org.hou.stocktank.analysis.MarketState;
import org.ta4j.core.Decimal;
import org.ta4j.core.Indicator;
import org.ta4j.core.TimeSeries;
import org.ta4j.core.indicators.SMAIndicator;
import org.ta4j.core.indicators.helpers.ClosePriceIndicator;

public class SMAAnalyzer extends Analyzer {
    private int period;
    private Indicator<Decimal> sma;

    public SMAAnalyzer(TimeSeries series, int period) {
        super(series);
        this.period = period;
        this.sma = new SMAIndicator(new ClosePriceIndicator(series), period);

    }

    private String source() {
        return "SMA(" + period +") analyzer";
    }

    private String sourceId() {
        return this.getClass().getCanonicalName();
    }

    /**
     * percentage distance close price to the sma
     * @return
     */
    private Decimal getAnalysisValue(int i){
        return (getTimeSeries().getTick(i).getClosePrice().minus(sma.getValue(i))).dividedBy(getTimeSeries().getTick(i).getClosePrice()).multipliedBy(Decimal.valueOf(100));
    }

    @Override
    public AnalysisResult getValue(int i) {
        if (i == 0) {
            return new AnalysisResult(MarketState.UNKNOWN, getAnalysisValue(i), " percentage distance between close price and sma", source(), sourceId());
        }
        if ((getTimeSeries().getTick(i-1).getClosePrice().isGreaterThanOrEqual(sma.getValue(i-1)))
            && (getTimeSeries().getTick(i).getClosePrice().isLessThan(sma.getValue(i)))) {
            //crossed from above
            return new AnalysisResult(MarketState.TURN_DOWN, getAnalysisValue(i), " close price crossed DOWN SMA(" + period + ") !!!", source(), sourceId());
        } else if ((getTimeSeries().getTick(i-1).getClosePrice().isLessThanOrEqual(sma.getValue(i-1)))
                &&(getTimeSeries().getTick(i).getClosePrice().isGreaterThan(sma.getValue(i)))) {
            //crossed from below
            return new AnalysisResult(MarketState.TURN_UP, getAnalysisValue(i), " close price crossed UP SMA(" + period + ") !!!", source(), sourceId());
        }
        if (getTimeSeries().getTick(i).getClosePrice().isGreaterThan(sma.getValue(i))) {
            return new AnalysisResult(MarketState.UP, getAnalysisValue(i), " close price is above SMA(" + period + ")", source(), sourceId());
        } else if (getTimeSeries().getTick(i).getClosePrice().isLessThan(sma.getValue(i))){
            return new AnalysisResult(MarketState.DOWN, getAnalysisValue(i), " close price is below SMA(" + period + ")", source(), sourceId());
        }
        return new AnalysisResult(MarketState.UNKNOWN,0,"unknown", source(), sourceId());
    }
}
