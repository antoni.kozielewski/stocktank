package org.hou.stocktank.analysis.analyzers;


import org.hou.stocktank.analysis.AnalysisResult;
import org.hou.stocktank.analysis.MarketState;
import org.hou.stocktank.indicators.TimeShiftIndicator;
import org.ta4j.core.Decimal;
import org.ta4j.core.Indicator;
import org.ta4j.core.TimeSeries;
import org.ta4j.core.indicators.ichimoku.*;


public class IchimokuAnalyzerIndicator extends Analyzer {
    private static final int CLOUD_FORWARD_SHIFT = 26;
    private Indicator<Decimal> chikouSpan;
    private Indicator<Decimal> kijunSen;
    private Indicator<Decimal> tenkanSen;
    private Indicator<Decimal> senkouSpanA;
    private Indicator<Decimal> senkouSpanB;


    public IchimokuAnalyzerIndicator(TimeSeries series) {
        super(series);
    }

    private void prepareIndicators() {
        chikouSpan = new IchimokuChikouSpanIndicator(series, 26);
        kijunSen = new IchimokuKijunSenIndicator(series, 26);
        tenkanSen = new IchimokuTenkanSenIndicator(series, 9);
        senkouSpanA = new TimeShiftIndicator(series, new IchimokuSenkouSpanAIndicator(series), CLOUD_FORWARD_SHIFT);
        senkouSpanB = new TimeShiftIndicator(series, new IchimokuSenkouSpanBIndicator(series), CLOUD_FORWARD_SHIFT);
    }


    public Indicator<Decimal> getChikouSpan() {
        return chikouSpan;
    }

    public Indicator<Decimal> getKijunSen() {
        return kijunSen;
    }

    public Indicator<Decimal> getTenkanSen() {
        return tenkanSen;
    }

    public Indicator<Decimal> getSenkouSpanA() {
        return senkouSpanA;
    }

    public Indicator<Decimal> getSenkouSpanB() {
        return senkouSpanB;
    }

    private String getId() {
        return this.getClass().getName();
    }

    private String getName() {
        return "Ichimoku kinko hyo";
    }

    private Boolean isAbove(Indicator<Decimal> a, Indicator<Decimal> b, int index) {
        return a.getValue(index).isGreaterThan(b.getValue(index));
    }

    private Boolean isBelow(Indicator<Decimal> a, Indicator<Decimal> b, int index) {
        return isAbove(b, a, index);
    }

    private Boolean crossFromBelow(Indicator<Decimal> signal, Indicator<Decimal> line, int index) {
        return (signal.getValue(index - 1).isLessThanOrEqual(line.getValue(index - 1)))
                &&
                (signal.getValue(index).isGreaterThan(line.getValue(index)));
    }

    private Boolean crossFromAbove(Indicator<Decimal> signal, Indicator<Decimal> line, int index) {
        return (signal.getValue(index).isLessThan(line.getValue(index)))
                &&
                (signal.getValue(index - 1).isGreaterThanOrEqual(line.getValue(index - 1)));
    }

    private Boolean isUnderCloud(Indicator<Decimal> signal, int index) {
        return signal.getValue(index).isLessThan(senkouSpanA.getValue(index))
                &&
                signal.getValue(index).isLessThan(senkouSpanB.getValue(index));
    }

    private Boolean isAboveCloud(Indicator<Decimal> signal, int index) {
        return isAboveCloud(signal.getValue(index), index);
    }

    private Boolean isAboveCloud(Decimal value, int index) {
        return value.isGreaterThan(senkouSpanA.getValue(index))
                &&
                value.isGreaterThan(senkouSpanB.getValue(index));
    }

    private Boolean isInCloud(Indicator<Decimal> signal, int index) {
        return isInCloud(signal.getValue(index), index);
    }

    private Boolean isInCloud(Decimal value, int index) {
        return (senkouSpanA.getValue(index).isLessThan(senkouSpanB.getValue(index))
                && value.isGreaterThanOrEqual(senkouSpanA.getValue(index))
                && value.isLessThanOrEqual(senkouSpanB.getValue(index))
        )
                ||
                (senkouSpanB.getValue(index).isLessThan(senkouSpanA.getValue(index))
                        && value.isGreaterThanOrEqual(senkouSpanB.getValue(index))
                        && value.isLessThanOrEqual(senkouSpanA.getValue(index))
                );
    }

    private Boolean isBelowCloud(Indicator<Decimal> signal, int index) {
        return isBelowCloud(signal.getValue(index), index);
    }

    private Boolean isBelowCloud(Decimal value, int index) {
        return value.isLessThan(senkouSpanA.getValue(index))
                &&
                value.isLessThan(senkouSpanB.getValue(index));
    }

    private Decimal getCrossingPoint(Indicator<Decimal> a, Indicator<Decimal> b, int index) {
        return getCrossValue(a.getValue(index - 1), a.getValue(index), b.getValue(index - 1), b.getValue(index));
    }

    private Decimal getCrossValue(Decimal y1, Decimal y2, Decimal y3, Decimal y4) {
        Decimal d = (y3.minus(y4)).multipliedBy(Decimal.valueOf(-1)).minus((y1.minus(y2)).multipliedBy(Decimal.valueOf(-1)));
        if (d.isEqual(Decimal.ZERO)) return null;
        Decimal yi = ((y1.minus(y2)).multipliedBy(y3).minus((y3.minus(y4)).multipliedBy(y1))).dividedBy(d);
        return yi;
    }

    private Boolean isPriceInsideCloud(int index) {
        return ((isInCloud(series.getTick(index).getOpenPrice(), index)) && (isInCloud(series.getTick(index).getClosePrice(), index)));
    }

    @Override
    public AnalysisResult getValue(int i) {
        if (i > 50) {
            if ((tenkanSen == null) || (kijunSen == null) || (senkouSpanA == null) || (senkouSpanB == null)) {
                prepareIndicators();
            }

            // TekanSen & KijunSen
            // STRONG signals
            if (crossFromBelow(tenkanSen, kijunSen, i) && isAboveCloud(getCrossingPoint(tenkanSen, kijunSen, i), i)) {
                return new AnalysisResult(MarketState.STRONG_BUY, 10, "Strong buy signal - TekanSen cross kijunSen above the cloud !", getName(), getId());
            }
            if (crossFromAbove(tenkanSen, kijunSen, i) && isBelowCloud(getCrossingPoint(tenkanSen, kijunSen, i), i)) {
                return new AnalysisResult(MarketState.STRONG_SELL, 10, "Strong sell signal - TekanSen cross kijunSen above the cloud !", getName(), getId());
            }
            // NEUTRAL signals
            if (crossFromBelow(tenkanSen, kijunSen, i) && isInCloud(getCrossingPoint(tenkanSen, kijunSen, i), i)) {
                return new AnalysisResult(MarketState.NEUTRAL_BUY, 5, "Neutral buy signal - TekanSen cross kijunSen inside the cloud.", getName(), getId());
            }
            if (crossFromAbove(tenkanSen, kijunSen, i) && isInCloud(getCrossingPoint(tenkanSen, kijunSen, i), i)) {
                return new AnalysisResult(MarketState.NEUTRAL_SELL, 5, "Neutral sell signal - TekanSen cross kijunSen inside the cloud.", getName(), getId());
            }
            // WEAK signals
            if (crossFromBelow(tenkanSen, kijunSen, i) && isBelowCloud(getCrossingPoint(tenkanSen, kijunSen, i), i)) {
                return new AnalysisResult(MarketState.WEAK_BUY, 1, "Weak buy signal - TekanSen cross kijunSen below the cloud.", getName(), getId());
            }
            if (crossFromAbove(tenkanSen, kijunSen, i) && isAboveCloud(getCrossingPoint(tenkanSen, kijunSen, i), i)) {
                return new AnalysisResult(MarketState.WEAK_SELL, 1, "Weak sell signal - TekanSen cross kijunSen above the cloud.", getName(), getId());
            }


            // CLOUD CHANGES
            if (crossFromBelow(senkouSpanA, senkouSpanB, i)) {
                return new AnalysisResult(MarketState.BUY_CONFIRMATION, 1, "Change of  the cloud. - STRONG confirmation of previous buy signals.", getName(), getId());
            }
            if (crossFromAbove(senkouSpanA, senkouSpanB, i)) {
                return new AnalysisResult(MarketState.SELL_CONFIRMATION, 1, "Change of  the cloud. - STRONG confirmation of previous sell signals.", getName(), getId());
            }

            // THREE LINE SIGNALS
            if (isAbove(tenkanSen, kijunSen, i) && isAboveCloud(kijunSen, i) && isAbove(senkouSpanA, senkouSpanB, i)) {
                return new AnalysisResult(MarketState.NEUTRAL_BUY, 6, "BUY PERIOD - Three line signal - high probability of bullish trend.", getName(), getId());
            }
            if (isBelow(tenkanSen, kijunSen, i) && isBelowCloud(kijunSen, i) && isBelow(senkouSpanA, senkouSpanB, i)) {
                return new AnalysisResult(MarketState.NEUTRAL_SELL, 6, "SELL PERIOD - Three line signal - high probability of bearish trend.", getName(), getId());
            }

            // PRICE & CLOUD
            if (isPriceInsideCloud(i)) {
                return new AnalysisResult(MarketState.CONSOLIDATION, 0, "Consolidation and hihg probability of horizontal trend - price in the cloud.", getName(), getId());
            }
            if (isInCloud(series.getTick(i).getOpenPrice(), i) && isAboveCloud(series.getTick(i).getClosePrice(), i)) {
                return new AnalysisResult(MarketState.NEUTRAL_BUY, 6, "Cloud breakout. Prices go above the cloud.", getName(), getId());
            }
            if (isInCloud(series.getTick(i).getOpenPrice(), i) && isBelowCloud(series.getTick(i).getClosePrice(), i)) {
                return new AnalysisResult(MarketState.NEUTRAL_SELL, 6, "Cloud breakout. Prices go below the cloud.", getName(), getId());
            }
        }
        return new AnalysisResult(MarketState.UNKNOWN, 0, "no info", getName(), getId());
    }
}
