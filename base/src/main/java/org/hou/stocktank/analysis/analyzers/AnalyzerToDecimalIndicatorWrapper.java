package org.hou.stocktank.analysis.analyzers;

import org.ta4j.core.Decimal;
import org.ta4j.core.Indicator;
import org.ta4j.core.TimeSeries;

public class AnalyzerToDecimalIndicatorWrapper implements Indicator<Decimal> {
    private Analyzer analyzer;

    public AnalyzerToDecimalIndicatorWrapper(Analyzer src) {
        this.analyzer = src;
    }


    @Override
    public Decimal getValue(int i) {
        return Decimal.valueOf(analyzer.getValue(i).getValue());
    }

    @Override
    public TimeSeries getTimeSeries() {
        return analyzer.getTimeSeries();
    }

}
