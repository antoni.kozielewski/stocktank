package org.hou.stocktank.analysis.analyzers.candlestick;

import org.ta4j.core.TimeSeries;
import org.hou.stocktank.analysis.AnalysisResult;
import org.hou.stocktank.analysis.MarketState;

public class SoldierCandlestickPattern extends BaseCandlestickAnalyzer {

    public SoldierCandlestickPattern(TimeSeries series) {
        super(series);
    }

    private String getName() {
        return "Soldier pattern";
    }

    private String getId() {
        return "Soldier";
    }

    @Override
    public AnalysisResult getValue(int i) {
        if (i > 3) {
            if (getTick(i - 2).getClosePrice().isEqual(getTick(i - 1).getOpenPrice())
                    && getTick(i - 1).getClosePrice().isEqual(getTick(i).getOpenPrice())
                    && !isCross(i - 2)
                    && !isCross(i - 1)
                    && !isCross(i)
                    ) {
                if (isRising(i - 2) && isRising(i - 1) && isRising(i)) {
                    return new AnalysisResult(MarketState.TURN_DOWN, 0, "3 Bullish Soldier", getName(), getId());
                } else if (isFalling(i - 2) && isFalling(i - 1) && isFalling(i)) {
                    return new AnalysisResult(MarketState.TURN_UP, 0, "3 Bearish Soldier", getName(), getId());
                }
            }
        }
        return new AnalysisResult(MarketState.UNKNOWN, 0, "...", getName(), getId());
    }
}
