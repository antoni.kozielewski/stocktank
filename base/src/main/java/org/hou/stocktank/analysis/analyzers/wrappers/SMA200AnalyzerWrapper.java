package org.hou.stocktank.analysis.analyzers.wrappers;

import org.hou.stocktank.analysis.analyzers.SMAAnalyzer;
import org.ta4j.core.TimeSeries;

public class SMA200AnalyzerWrapper extends SMAAnalyzer {

    public SMA200AnalyzerWrapper(TimeSeries series) {
        super(series, 200);
    }
}
