package org.hou.stocktank.analysis.analyzers.candlestick;

import org.ta4j.core.TimeSeries;
import org.hou.stocktank.analysis.AnalysisResult;
import org.hou.stocktank.analysis.MarketState;

public class Bearish3CandlestickPattern extends BaseCandlestickAnalyzer {

    public Bearish3CandlestickPattern(TimeSeries series) {
        super(series);
    }

    private String getName() {
        return "\\/  - Bearish 3";
    }

    private String getId() {
        return "Bearish3";
    }

    @Override
    public AnalysisResult getValue(int i) {
        if (i > 3) {
            if (isFalling(i - 3)
                    && isRising(i - 2)
                    && isRising(i - 1)
                    && isRising(i)
                    && isUpstairs(i - 2)
                    && isUpstairs(i - 1)
                    && isUpstairs(i)
                    ) {
                return new AnalysisResult(MarketState.DOWN, 0, "only in falling trend - continue", getName(), getId());
            }
        }
        return new AnalysisResult(MarketState.UNKNOWN, 0, "...", getName(), getId());
    }

}
