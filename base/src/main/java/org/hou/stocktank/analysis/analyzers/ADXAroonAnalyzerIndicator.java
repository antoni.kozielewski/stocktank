package org.hou.stocktank.analysis.analyzers;

import org.ta4j.core.Decimal;
import org.ta4j.core.Indicator;
import org.ta4j.core.TimeSeries;

import org.hou.stocktank.analysis.AnalysisResult;
import org.hou.stocktank.analysis.MarketState;
import org.ta4j.core.indicators.AroonDownIndicator;
import org.ta4j.core.indicators.AroonUpIndicator;
import org.ta4j.core.indicators.adx.AverageDirectionalMovementIndicator;

public class ADXAroonAnalyzerIndicator extends Analyzer {
    private int timeFrame = 14;
    private Decimal adxTrendLevel = Decimal.valueOf(25);
    private Decimal AROON_HIGH_LEVEL = Decimal.valueOf(70);
    private Decimal AROON_LOW_LEVEL = Decimal.valueOf(30);
    private Indicator<Decimal> adx;
    private Indicator<Decimal> aroonUp;
    private Indicator<Decimal> aroonDown;

    public ADXAroonAnalyzerIndicator(TimeSeries series) {
        super(series);
        prepare();
    }

    private void prepare() {
        adx = new AverageDirectionalMovementIndicator(getTimeSeries(), timeFrame);
        aroonUp = new AroonUpIndicator(getTimeSeries(), timeFrame);
        aroonDown = new AroonDownIndicator(getTimeSeries(), timeFrame);
    }

    private String getName() {
        return "ADX + Aroon [timeFrame:" + timeFrame + ", adxLevel:" + adxTrendLevel + ", aroon: " + AROON_LOW_LEVEL + "/" + AROON_HIGH_LEVEL + "]";
    }

    private String getId() {
        return "ADX&Aroon" + timeFrame + "|" + adxTrendLevel + "|" + AROON_LOW_LEVEL + "|" + AROON_HIGH_LEVEL;
    }

    @Override
    public AnalysisResult getValue(int i) {
        if (adx.getValue(i).isGreaterThanOrEqual(adxTrendLevel)) {
            if (aroonUp.getValue(i).isGreaterThan(AROON_HIGH_LEVEL)
                    && aroonDown.getValue(i).isLessThan(AROON_LOW_LEVEL)) {
                return new AnalysisResult(MarketState.UP, adx.getValue(i), "", getName(), getId());
            } else if (aroonUp.getValue(i).isLessThan(AROON_LOW_LEVEL)
                    && aroonDown.getValue(i).isGreaterThan(AROON_HIGH_LEVEL)) {
                return new AnalysisResult(MarketState.DOWN, adx.getValue(i), "", getName(), getId());
            } else {
                return new AnalysisResult(MarketState.UNKNOWN, adx.getValue(i), "aroon said CANCELED", getName(), getId());
            }
        }
        return new AnalysisResult(MarketState.UNKNOWN, adx.getValue(i), "adx said CANCELED", getName(), getId());
    }

}
