package org.hou.stocktank.analysis.analyzers.wrappers;

import org.hou.stocktank.analysis.analyzers.FibonacciSupportResistanceAnalyzer;
import org.hou.stocktank.indicators.pivotpoints.SimplePivotPointIndicator;
import org.ta4j.core.TimeSeries;

public class FibonacciLongRangeAnalyzerWrapper extends FibonacciSupportResistanceAnalyzer {

    public FibonacciLongRangeAnalyzerWrapper(TimeSeries series) {
        super(new SimplePivotPointIndicator(series, 20, 10, 0.12, 0.18), "Long Range");
    }

}
