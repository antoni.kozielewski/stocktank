package org.hou.stocktank.analysis.analyzers;

import org.ta4j.core.Decimal;
import org.ta4j.core.Indicator;
import org.ta4j.core.TimeSeries;
import org.ta4j.core.indicators.helpers.ClosePriceIndicator;
import org.ta4j.core.indicators.RSIIndicator;
import org.hou.stocktank.analysis.AnalysisResult;
import org.hou.stocktank.analysis.MarketState;

public class RSIAnalyzerIndicator extends Analyzer {
    private Indicator<Decimal> rsi;
    private int rsiTimeframe = 15;
    private Decimal oversold = Decimal.valueOf(30);
    private Decimal overbought = Decimal.valueOf(70);

    public RSIAnalyzerIndicator(TimeSeries series) {
        super(series);
        this.rsi = new RSIIndicator(new ClosePriceIndicator(series), rsiTimeframe);
    }

    public RSIAnalyzerIndicator(TimeSeries series, int rsiTimeframe, int oversoldLevel, int overboughtLevel) {
        super(series);
        this.rsiTimeframe = rsiTimeframe;
        this.rsi = new RSIIndicator(new ClosePriceIndicator(series), rsiTimeframe);
        this.oversold = Decimal.valueOf(oversoldLevel);
        this.overbought = Decimal.valueOf(overboughtLevel);
    }

    private String getName() {
        return "RSI [" + oversold + "," + overbought + "]";
    }

    private String getId() {
        return "RSI" + oversold + "|" + overbought;
    }

    public AnalysisResult getValue(int i) {
        if (rsi.getValue(i).isGreaterThanOrEqual(overbought)) {
            return new AnalysisResult(MarketState.OVERBOUGHT, rsi.getValue(i).toDouble(), "RSI >= " + overbought, getName(), getId());
        } else if (rsi.getValue(i).isLessThanOrEqual(oversold)) {
            return new AnalysisResult(MarketState.OVERSOLD, rsi.getValue(i).toDouble(), "RSI <= " + oversold, getName(), getId());
        } else {
            return new AnalysisResult(MarketState.UNKNOWN, rsi.getValue(i).toDouble(), "", getName(), getId());
        }
    }

}
