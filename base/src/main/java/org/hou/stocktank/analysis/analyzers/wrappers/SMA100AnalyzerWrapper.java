package org.hou.stocktank.analysis.analyzers.wrappers;

import org.hou.stocktank.analysis.analyzers.SMAAnalyzer;
import org.ta4j.core.TimeSeries;

public class SMA100AnalyzerWrapper extends SMAAnalyzer {

    public SMA100AnalyzerWrapper(TimeSeries series) {
        super(series, 100);
    }
}
