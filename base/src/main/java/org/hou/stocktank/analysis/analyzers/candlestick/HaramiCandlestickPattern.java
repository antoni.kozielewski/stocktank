package org.hou.stocktank.analysis.analyzers.candlestick;

import org.ta4j.core.TimeSeries;
import org.ta4j.core.TimeSeries;
import org.hou.stocktank.analysis.AnalysisResult;
import org.hou.stocktank.analysis.MarketState;

public class HaramiCandlestickPattern extends BaseCandlestickAnalyzer {

    public HaramiCandlestickPattern(TimeSeries series) {
        super(series);
    }

    private String getName() {
        return "Harami pattern";
    }

    private String getId() {
        return "Harami";
    }

    @Override
    public AnalysisResult getValue(int i) {
        if (i > 1) {
            if (isRising(i - 1)) {
                if (isOpenInPreviousBody(i) && isCloseInPreviousBody(i) && isFalling(i)) {
                    return new AnalysisResult(MarketState.TURN_DOWN, 0, "Bearish Harami - only in rising trend", getName(), getId());
                }
            } else if (isFalling(i - 1)) {
                if (isOpenInPreviousBody(i) && isCloseInPreviousBody(i) && isRising(i)) {
                    return new AnalysisResult(MarketState.TURN_UP, 0, "Bullish Harami - only in falling trend", getName(), getId());
                }
            }
        }
        return new AnalysisResult(MarketState.UNKNOWN, 0, "...", getName(), getId());
    }
}
