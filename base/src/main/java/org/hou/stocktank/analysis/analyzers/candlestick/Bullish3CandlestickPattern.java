package org.hou.stocktank.analysis.analyzers.candlestick;

import org.hou.stocktank.analysis.AnalysisResult;
import org.hou.stocktank.analysis.MarketState;
import org.ta4j.core.TimeSeries;

public class Bullish3CandlestickPattern extends BaseCandlestickAnalyzer {

    public Bullish3CandlestickPattern(TimeSeries series) {
        super(series);
    }

    private String getName() {
        return "/\\  - Bullish 3";
    }

    private String getId() {
        return "Bullish3";
    }

    @Override
    public AnalysisResult getValue(int i) {
        if (i > 3) {
            if (isRising(i - 3)
                    && isFalling(i - 2)
                    && isFalling(i - 1)
                    && isFalling(i)
                    && isDownstairs(i - 2)
                    && isDownstairs(i - 1)
                    && isDownstairs(i)
                    ) {
                return new AnalysisResult(MarketState.UP, 0, "only in rising trend - continue", getName(), getId());
            }
        }
        return new AnalysisResult(MarketState.UNKNOWN, 0, "...", getName(), getId());
    }

}
