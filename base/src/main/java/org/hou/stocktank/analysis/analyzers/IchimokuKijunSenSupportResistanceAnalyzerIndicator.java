package org.hou.stocktank.analysis.analyzers;


import org.hou.stocktank.analysis.AnalysisResult;
import org.hou.stocktank.analysis.MarketState;
import org.hou.stocktank.indicators.TimeShiftIndicator;
import org.ta4j.core.Decimal;
import org.ta4j.core.Indicator;
import org.ta4j.core.TimeSeries;
import org.ta4j.core.indicators.ichimoku.*;


public class IchimokuKijunSenSupportResistanceAnalyzerIndicator extends Analyzer {
    private static final int CLOUD_FORWARD_SHIFT = 26;
    private Indicator<Decimal> kijunSen;

    public IchimokuKijunSenSupportResistanceAnalyzerIndicator(TimeSeries series) {
        super(series);
    }

    private void prepareIndicators() {
        kijunSen = new IchimokuKijunSenIndicator(series, 26);
    }


    public Indicator<Decimal> getKijunSen() {
        return kijunSen;
    }

    private String getId() {
        return this.getClass().getName();
    }

    private String getName() {
        return "Ichimoku Kijun Sen Support Resistance";
    }

    private Boolean isAbove(Indicator<Decimal> a, Indicator<Decimal> b, int index) {
        return a.getValue(index).isGreaterThan(b.getValue(index));
    }

    private Boolean isBelow(Indicator<Decimal> a, Indicator<Decimal> b, int index) {
        return isAbove(b, a, index);
    }

    private Boolean crossFromBelow(Indicator<Decimal> signal, Indicator<Decimal> line, int index) {
        return (signal.getValue(index - 1).isLessThanOrEqual(line.getValue(index - 1)))
                &&
                (signal.getValue(index).isGreaterThan(line.getValue(index)));
    }

    private Boolean crossFromAbove(Indicator<Decimal> signal, Indicator<Decimal> line, int index) {
        return (signal.getValue(index).isLessThan(line.getValue(index)))
                &&
                (signal.getValue(index - 1).isGreaterThanOrEqual(line.getValue(index - 1)));
    }


    @Override
    public AnalysisResult getValue(int i) {
        if (i > 50) {
            if (kijunSen == null) {
                prepareIndicators();
            }

            if (kijunSen.getValue(i).isLessThan(series.getTick(i).getClosePrice())) {
                return new AnalysisResult(MarketState.UP, kijunSen.getValue(i), "KijunSen resistance", getName(), getId());
            } else if (kijunSen.getValue(i).isGreaterThan(series.getTick(i).getClosePrice())) {
                return new AnalysisResult(MarketState.DOWN, kijunSen.getValue(i), "KijunSen support", getName(), getId());
            } else {
                return new AnalysisResult(MarketState.INFO, kijunSen.getValue(i), "Close Price touches KijunSen", getName(), getId());
            }
        }
        return new AnalysisResult(MarketState.UNKNOWN, 0, "no info", getName(), getId());
    }

}
