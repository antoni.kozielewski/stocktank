package org.hou.stocktank.analysis.analyzers.advanced;


import org.ta4j.core.Decimal;
import org.ta4j.core.Indicator;
import org.ta4j.core.TimeSeries;
import org.ta4j.core.indicators.StochasticOscillatorKIndicator;
import org.ta4j.core.indicators.EMAIndicator;
import org.ta4j.core.indicators.MACDIndicator;
import org.ta4j.core.indicators.RSIIndicator;
import org.ta4j.core.indicators.SMAIndicator;
import org.hou.stocktank.analysis.AnalysisResult;
import org.hou.stocktank.analysis.MarketState;
import org.hou.stocktank.analysis.analyzers.Analyzer;
import org.hou.stocktank.utils.fuzzy.FuzzyPattern;
import org.hou.stocktank.utils.fuzzy.TrapezoidPattern;
import org.hou.stocktank.utils.fuzzy.ValueRangeException;
import org.ta4j.core.indicators.helpers.ClosePriceIndicator;
import org.ta4j.core.indicators.helpers.MaxPriceIndicator;
import org.ta4j.core.indicators.helpers.MinPriceIndicator;

import java.util.HashMap;
import java.util.Map;

public class BPF01Analyzer extends Analyzer {
    private RSIIndicator rsi;
    private MACDIndicator macd;
    private Indicator<Decimal> macdSignal;
    private Indicator<Decimal> stochK;
    private Indicator<Decimal> stochD;

    private FuzzyPattern rsiBuyPattern;
    private FuzzyPattern rsiSellPattern;
    private FuzzyPattern stochBuyPattern;
    private FuzzyPattern stochSellPattern;
    private FuzzyPattern macdPattern;

    private double RSI_STOCH_STOCK_MARKET_OVERBOUGHT = 3.5;
    private double RSI_STOCH_STOCK_MARKET_OVERSOLD = -3.5;

    private int CROSS_SCANNING_PERIOD = 7;


    public BPF01Analyzer(TimeSeries series) {
        super(series);
        Indicator closePrice = new ClosePriceIndicator(series);
        rsi = new RSIIndicator(closePrice, 14);
        macd = new MACDIndicator(closePrice, 12, 26);
        macdSignal = new EMAIndicator(macd, 9);

        MaxPriceIndicator maxPriceIndicator = new MaxPriceIndicator(series);
        MinPriceIndicator minPriceIndicator = new MinPriceIndicator(series);
        StochasticOscillatorKIndicator kbase = new StochasticOscillatorKIndicator(new ClosePriceIndicator(series), 15, maxPriceIndicator, minPriceIndicator);
        stochK = new SMAIndicator(kbase,3);
        stochD = new SMAIndicator(stochK, 3);

        prepareFuzzyLogic();
    }

    private void prepareFuzzyLogic() {
        rsiBuyPattern = new TrapezoidPattern(43, 1, 56, 0);
        rsiSellPattern = new TrapezoidPattern(68, 0, 72, 1);
        stochBuyPattern = new TrapezoidPattern(45, 1, 58, 0);
        stochSellPattern = new TrapezoidPattern(70, 0, 85, 1);
        macdPattern = new TrapezoidPattern(0.1, 1, 0.3, 0);
    }

    /**
     * @param index
     * @param periodBefore
     * @return -2 if stochK crosses down, -1 if touches from up, 0 no cross, 1 touch from down, 2 cross from down
     */
    private int findLastCrossInPeriod(Indicator<Decimal> a, Indicator<Decimal> b, int index, int periodBefore) {
        int result = 0;
        int counter = 0;
        int startIndex = (index - periodBefore - 1 > 0) ? index - periodBefore - 1 : 1;
        int lastR = -666;
        for (int i = startIndex; i <= index; i++) {
            if (lastR == -666) { // first pass
                if (a.getValue(i).isGreaterThan(b.getValue(i))){
                    lastR = 1;
                } else if (a.getValue(i).equals(b.getValue(i))) {
                    lastR = 0;
                } else {
                    lastR = -1;
                }
            } else {
                // second and every next
                int currentR = -666;
                if (a.getValue(i).isGreaterThan(b.getValue(i))){
                    currentR = 1;
                } else if (a.getValue(i).equals(b.getValue(i))) {
                    currentR = 0;
                } else {
                    currentR = -1;
                }

                int thisResult = 0;
                if ((lastR == -1)&&(currentR==1)) {
                    thisResult = 2;
                } else if ((lastR == 1)&&(currentR==-1)) {
                    thisResult = -2;
                } else if ((lastR == 0)&&(currentR==1)) {
                    thisResult = 1;
                } else if ((lastR == 1)&&(currentR==0)) {
                    thisResult = -1;
                } else if ((lastR == -1)&&(currentR==0)) {
                    thisResult = 1;
                } else if ((lastR == 0)&&(currentR== -1)) {
                    thisResult = -1;
                }


                if((result == 1)&&(thisResult == 2)) { result = 2; }
                else if((result == -1)&&(thisResult ==-2)) { result = -2;}
                else if(thisResult != 0) { result = thisResult; }
                lastR = currentR;
            }
        }
        return result;
    }

    @Override
    public AnalysisResult getValue(int i) {
        double rsiBuy = 0;
        double rsiSell = 0;
        double stochBuy = 0;
        double stochSell = 0;
        double macdTester = 0;
        try {
            rsiBuy = rsiBuyPattern.getValue(rsi.getValue(i).toDouble());
            rsiSell = rsiSellPattern.getValue(rsi.getValue(i).toDouble());
            stochBuy = stochBuyPattern.getValue(stochK.getValue(i).toDouble());
            stochSell = stochSellPattern.getValue(stochK.getValue(i).toDouble());
            macdTester = macdPattern.getValue(macd.getValue(i).toDouble());
        } catch (ValueRangeException e) {
            e.printStackTrace();
        }

        int stochCross = findLastCrossInPeriod(stochK, stochD, i, CROSS_SCANNING_PERIOD);
        int macdCross = findLastCrossInPeriod(macd, macdSignal, i, CROSS_SCANNING_PERIOD);

        double buy = rsiBuy + stochBuy;
        double sell = rsiSell + stochSell;
        double value = (buy > sell) ? buy : -1 * sell;
        value = value + stochCross + macdCross;

        MarketState resultMarketState = MarketState.UNKNOWN;
        if (value <= RSI_STOCH_STOCK_MARKET_OVERSOLD) {
            resultMarketState = MarketState.OVERSOLD;
        } else if (value >= RSI_STOCH_STOCK_MARKET_OVERBOUGHT){
            resultMarketState = MarketState.OVERBOUGHT;
        }

        Map<String, String> data = new HashMap<>();
        data.put("RSI", String.valueOf(rsi.getValue(i).toDouble()));
        data.put("RSIBuy", String.valueOf(rsiBuy));
        data.put("RSISell", String.valueOf(rsiSell));
        data.put("stochK", String.valueOf(stochK.getValue(i).toDouble()));
        data.put("stochD", String.valueOf(stochD.getValue(i).toDouble()));
        data.put("stochBuy", String.valueOf(stochBuy));
        data.put("stochSell", String.valueOf(stochSell));
        data.put("stochCross", String.valueOf(stochCross));
        data.put("macdTester", String.valueOf(macdTester));
        data.put("macdCross", String.valueOf(macdCross));

        StringBuilder builder = new StringBuilder("Fuzzy Params: " + data.toString() + "\n");
        builder.append(String.format("   RSI buy = %.3g   :   %.5g RSI%n", rsiBuy, rsi.getValue(i).toDouble()));
        builder.append(String.format(" Stoch buy = %.3g   :   %.5g / %.5g K/D%n", stochBuy, stochK.getValue(i).toDouble(), stochD.getValue(i).toDouble()));
        builder.append(              "         Stoch [X]  =   " + stochCross + "\n");
        builder.append(              "         MACD  [X]  =   " + macdCross + "\n");
        builder.append(String.format("  RSI sell = %.3g   :   %.5g RSI%n", rsiSell, rsi.getValue(i).toDouble()));
        builder.append(String.format("Stoch sell = %.3g   :   %.5g / %.5g K/D%n", stochSell, stochK.getValue(i).toDouble(), stochD.getValue(i).toDouble()));
        builder.append(String.format("      MACD = %.3g   :   %.5g MACD%n", macdTester, macd.getValue(i).toDouble()));


        AnalysisResult result = new AnalysisResult(resultMarketState, value, builder.toString(), "BPF01Analyzer", "", data);

        return result;
    }

}
