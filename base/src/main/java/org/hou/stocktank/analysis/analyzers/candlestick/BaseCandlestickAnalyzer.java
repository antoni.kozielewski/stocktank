package org.hou.stocktank.analysis.analyzers.candlestick;

import org.hou.stocktank.analysis.AnalysisResult;
import org.ta4j.core.Decimal;
import org.ta4j.core.Indicator;
import org.ta4j.core.Tick;
import org.ta4j.core.TimeSeries;

public abstract class BaseCandlestickAnalyzer implements Indicator<AnalysisResult> {
    private TimeSeries series;
    protected Decimal flatBodyLimit = Decimal.valueOf(0);
    protected Decimal upperShadowLimit = Decimal.valueOf(0);
    protected Decimal lowerShadowLimit = Decimal.valueOf(0);

    public BaseCandlestickAnalyzer(TimeSeries series) {
        this.series = series;
    }

    public TimeSeries getTimeSeries() {
        return series;
    }

    /**
     * true if open < close
     *
     * @param index
     * @return
     */
    protected Boolean isRising(int index) {
        return getTimeSeries().getTick(index).getOpenPrice().plus(flatBodyLimit).minus(getTimeSeries().getTick(index).getClosePrice()).isLessThan(Decimal.ZERO);
    }

    /**
     * true if body > flatBodyLimit
     *
     * @param index
     * @return
     */
    protected Boolean isFlat(int index) {
        return getTimeSeries().getTick(index).getClosePrice().max(getTimeSeries().getTick(index).getOpenPrice())
                .minus
                        (getTimeSeries().getTick(index).getClosePrice().min(getTimeSeries().getTick(index).getOpenPrice())
                        ).isLessThanOrEqual(flatBodyLimit);
    }

    /**
     * true if open > close
     *
     * @param index
     * @return
     */
    protected Boolean isFalling(int index) {
        return getTimeSeries().getTick(index).getClosePrice().plus(flatBodyLimit).minus(getTimeSeries().getTick(index).getOpenPrice()).isLessThan(Decimal.ZERO);
    }

    protected Boolean hasUpperShadow(int index) {
        Tick tick = getTimeSeries().getTick(index);
        return tick.getMaxPrice().minus(tick.getOpenPrice().max(tick.getClosePrice())).isGreaterThan(upperShadowLimit);
    }

    protected Boolean hasLowerShadow(int index) {
        Tick tick = getTimeSeries().getTick(index);
        return tick.getOpenPrice().min(tick.getClosePrice()).minus(tick.getMinPrice()).isGreaterThan(lowerShadowLimit);
    }

    protected Tick getPrevious(int index) {
        if (index == 0) throw new RuntimeException("index == 0 - and you get previous tick from getTimeSeries()");
        return getTimeSeries().getTick(index - 1);
    }

    protected Boolean isCross(int index) {
        return getTimeSeries().getTick(index).getOpenPrice().isEqual(getTimeSeries().getTick(index).getClosePrice());
    }

    protected Tick getTick(int index) {
        return getTimeSeries().getTick(index);
    }

    protected Boolean isValueInBody(Tick tick, Decimal value) {
        return tick.getOpenPrice().min(tick.getClosePrice()).isLessThanOrEqual(value)
                &&
                tick.getOpenPrice().max(tick.getClosePrice()).isGreaterThanOrEqual(value);
    }

    protected Boolean isValueHigherThanBody(Tick tick, Decimal value) {
        return tick.getOpenPrice().max(tick.getClosePrice()).isLessThan(value);
    }

    protected Boolean isValueLowerThanBody(Tick tick, Decimal value) {
        return tick.getOpenPrice().min(tick.getClosePrice()).isGreaterThan(value);
    }

    protected Boolean isOpenInPreviousBody(int index) {
        return isValueInBody(getPrevious(index), getTimeSeries().getTick(index).getOpenPrice());
    }

    protected Boolean isCloseInPreviousBody(int index) {
        return isValueInBody(getPrevious(index), getTimeSeries().getTick(index).getClosePrice());
    }

    protected Boolean isUpstairs(int index) {
        return isOpenInPreviousBody(index)
                &&
                isValueHigherThanBody(getPrevious(index), getTimeSeries().getTick(index).getClosePrice());
    }

    protected Boolean isDownstairs(int index) {
        return
                isOpenInPreviousBody(index)
                        &&
                        isValueLowerThanBody(getPrevious(index), getTimeSeries().getTick(index).getClosePrice());
    }


}

