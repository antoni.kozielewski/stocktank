package org.hou.stocktank.analysis.analyzers;


import org.ta4j.core.Decimal;
import org.ta4j.core.Indicator;
import org.ta4j.core.TimeSeries;
import org.ta4j.core.indicators.EMAIndicator;
import org.ta4j.core.indicators.MACDIndicator;
import org.hou.stocktank.analysis.AnalysisResult;
import org.hou.stocktank.analysis.MarketState;
import org.ta4j.core.indicators.helpers.ClosePriceIndicator;

public class MACDAnalyzerIndicator extends Analyzer {
    private Indicator<Decimal> macd;
    private Indicator<Decimal> signal;
    private int shortMACDTimeFrame = 12;
    private int longMACDTimeFrame = 26;
    private int signalTimeFrame = 9;

    public MACDAnalyzerIndicator(TimeSeries series) {
        super(series);
        prepare();
    }

    public MACDAnalyzerIndicator(TimeSeries series, int shortTimeFrame, int longTimeFrame, int signalTimeFrame) {
        super(series);
        this.shortMACDTimeFrame = shortTimeFrame;
        this.longMACDTimeFrame = longTimeFrame;
        this.signalTimeFrame = signalTimeFrame;
        prepare();
    }

    private void prepare() {
        macd = new MACDIndicator(new ClosePriceIndicator(getTimeSeries()), shortMACDTimeFrame, longMACDTimeFrame);
        signal = new EMAIndicator(macd, signalTimeFrame);
    }

    private String getName() {
        return "MACD [" + shortMACDTimeFrame + "," + longMACDTimeFrame + "," + signalTimeFrame + "]";
    }

    private String getId() {
        return "MACD" + shortMACDTimeFrame + "|" + longMACDTimeFrame + "|" + signalTimeFrame;
    }

    @Override
    public AnalysisResult getValue(int i) {
        if (signal.getValue(i).isGreaterThanOrEqual(macd.getValue(i))) {
            return new AnalysisResult(MarketState.DOWN, signal.getValue(i).toDouble() - macd.getValue(i).toDouble(), "", getName(), getId());
        } else {
            return new AnalysisResult(MarketState.UP, signal.getValue(i).toDouble() - macd.getValue(i).toDouble(), "", getName(), getId());
        }
    }

}
