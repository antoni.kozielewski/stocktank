package org.hou.stocktank.analysis.analyzers;


import org.ta4j.core.Decimal;
import org.ta4j.core.Indicator;
import org.ta4j.core.TimeSeries;
import org.hou.stocktank.analysis.AnalysisResult;
import org.hou.stocktank.analysis.MarketState;
import org.hou.stocktank.indicators.HurstExponentIndicator;

public class HurstAnalyzerIndicator extends Analyzer{
    private Indicator<Decimal> hurstExponentIndicator;

    public HurstAnalyzerIndicator(TimeSeries series) {
        super(series);
        this.hurstExponentIndicator = new HurstExponentIndicator(series);
    }

    private String getId() {
        return "HurstExponent for daily returns and 1024 days period";
    }

    private String getName() {
        return "Hurst Exponent";
    }

    private String getDescription(Double value) {
        return (value < 0.5) ? " stream UNPREDICTABLE !!" : (value == 0.5) ? " stream RANDOM !!" : " stream partialy PREDICTABLE" ;
    }

    @Override
    public AnalysisResult getValue(int i) {
        Double value = hurstExponentIndicator.getValue(i).toDouble();
        return new AnalysisResult(MarketState.INFO, value, getDescription(value), getName(), getId());
    }
}
