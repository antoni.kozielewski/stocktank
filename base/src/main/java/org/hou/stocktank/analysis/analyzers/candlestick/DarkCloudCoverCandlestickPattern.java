package org.hou.stocktank.analysis.analyzers.candlestick;

import org.ta4j.core.TimeSeries;
import org.hou.stocktank.analysis.AnalysisResult;
import org.hou.stocktank.analysis.MarketState;

public class DarkCloudCoverCandlestickPattern extends BaseCandlestickAnalyzer {

    public DarkCloudCoverCandlestickPattern(TimeSeries series) {
        super(series);
    }

    private String getName() {
        return "\\/  - Dark cloud cover";
    }

    private String getId() {
        return "DarkCloudCover";
    }

    @Override
    public AnalysisResult getValue(int i) {
        if (i > 3) {
            if (isRising(i - 2)
                    && isRising(i - 1)
                    && isFalling(i)
                    && isValueInBody(getTick(i - 2), getTick(i - 1).getOpenPrice())
                    && isValueInBody(getTick(i - 1), getTick(i - 2).getClosePrice())
                    && isValueInBody(getTick(i), getTick(i - 1).getOpenPrice())
                    ) {
                return new AnalysisResult(MarketState.TURN_DOWN, 0, "", getName(), getId());
            }
        }
        return new AnalysisResult(MarketState.UNKNOWN, 0, "...", getName(), getId());
    }

}
