package org.hou.stocktank.analysis.analyzers.candlestick;

import org.ta4j.core.TimeSeries;
import org.hou.stocktank.analysis.AnalysisResult;
import org.hou.stocktank.analysis.MarketState;

public class ShootingStarCandlestickPattern extends BaseCandlestickAnalyzer {


    public ShootingStarCandlestickPattern(TimeSeries series) {
        super(series);
    }

    private String getName() {
        return "\\/ - Shooting Start pattern";
    }

    private String getId() {
        return "ShootingStar";
    }

    @Override
    public AnalysisResult getValue(int i) {
        if ((i > 3)
                && isUpstairs(i - 3) && isUpstairs(i - 2) && isUpstairs(i - 1)
                && isValueHigherThanBody(getPrevious(i), getTimeSeries().getTick(i).getOpenPrice())
                && hasUpperShadow(i)
                && isRising(i)
                && !hasLowerShadow(i)
                ) {
            return new AnalysisResult(MarketState.TURN_DOWN, 0, "...", getName(), getId());
        } else {
            return new AnalysisResult(MarketState.UNKNOWN, 0, "...", getName(), getId());
        }
    }
}
