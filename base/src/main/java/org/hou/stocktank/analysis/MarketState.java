package org.hou.stocktank.analysis;


public enum MarketState {
    OVERSOLD,
    OVERBOUGHT,
    UP,
    DOWN,
    UNKNOWN,
    TURN_DOWN,
    TURN_UP,
    INFO,
    STRONG_BUY,
    NEUTRAL_BUY,
    WEAK_BUY,
    STRONG_SELL,
    NEUTRAL_SELL,
    WEAK_SELL,
    BUY_CONFIRMATION,
    SELL_CONFIRMATION,
    CONSOLIDATION;
}
