package org.hou.stocktank.analysis.analyzers.candlestick;

import org.ta4j.core.TimeSeries;
import org.hou.stocktank.analysis.AnalysisResult;
import org.hou.stocktank.analysis.MarketState;

public class TweezerCandlestickPattern extends BaseCandlestickAnalyzer {

    public TweezerCandlestickPattern(TimeSeries series) {
        super(series);
    }

    private String getName() {
        return "Tweezer top/bottom pattern";
    }

    private String getId() {
        return "Tweezer";
    }

    @Override
    public AnalysisResult getValue(int i) {
        if (i > 1) {
            if (isRising(i - 1) && isFalling(i)) {
                if ((getTick(i - 1).getClosePrice().isEqual(getTick(i).getOpenPrice()))
                        || (getTick(i - 1).getMaxPrice().isEqual(getTick(i).getMaxPrice()))) {
                    return new AnalysisResult(MarketState.DOWN, 0, "Tweezer top", getName(), getId());
                }
            } else if (isFalling(i - 1) && isRising(i)) {
                if ((getTick(i - 1).getClosePrice().isEqual(getTick(i).getOpenPrice()))
                        || (getTick(i - 1).getMinPrice().isEqual(getTick(i).getMinPrice()))) {
                    return new AnalysisResult(MarketState.UP, 0, "Tweezer Bottom", getName(), getId());
                }
            }
        }
        return new AnalysisResult(MarketState.UNKNOWN, 0, "...", getName(), getId());
    }
}
