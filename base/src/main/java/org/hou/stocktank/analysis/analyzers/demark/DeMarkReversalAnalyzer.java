package org.hou.stocktank.analysis.analyzers.demark;

import org.hou.stocktank.analysis.AnalysisResult;
import org.hou.stocktank.analysis.analyzers.Analyzer;
import org.hou.stocktank.indicators.pivotpoints.PivotPoint;
import org.hou.stocktank.indicators.pivotpoints.PivotPointIndicator;
import org.ta4j.core.Decimal;

import java.util.function.BiFunction;

public class DeMarkReversalAnalyzer extends Analyzer{
    private PivotPointIndicator pivotPointIndicator;

    public DeMarkReversalAnalyzer(PivotPointIndicator pivotPointIndicator) {
        super(pivotPointIndicator.getTimeSeries());
        this.pivotPointIndicator = pivotPointIndicator;
    }

    public enum DeMarkSetupResult {
        PIVOT_TOO_CLOSE(-2),
        CANCELED(-1),
        OK (1),
        PERFECT(2);
        int value;
        private DeMarkSetupResult(int value) {
            this.value = value;
        }
        public int getValue() {
            return this.value;
        }
        public boolean isOKorBetter(){
            return isOKorBetter(this);
        }
        public boolean isOKorBetter(DeMarkSetupResult data) {
            return data.getValue() > 0;
        }
    }

    /**
     * check DeMark setup - looks up for 9 consecutive closes less/greater than n-4
     * @param index
     * @param func
     * @return
     */
    private DeMarkSetupResult setupScan(int index, BiFunction<Decimal,Decimal, Boolean> func) {
        if(index < 4) {
            return DeMarkSetupResult.CANCELED;
        }
        int startIndex = index - 9;
        for (int i=startIndex; i<startIndex + 9 && i< getTimeSeries().getEndIndex(); i++){
            if (!func.apply(getTimeSeries().getTick(i).getClosePrice(), getTimeSeries().getTick(i-4).getClosePrice())) {
                return DeMarkSetupResult.CANCELED;
            }
        }
        return DeMarkSetupResult.OK;
    }

    /**
     * checks perfection of DeMark setup - 8 and 9 lows should be less/greater than 6 and 7
     * @param index
     * @param func
     * @return
     */
    private boolean isPerfectSetup(int index, BiFunction<Decimal, Decimal, Boolean> func) {
        if(index < 4) {
            return false;
        }
        if (func.apply(getTimeSeries().getTick(index-1).getClosePrice(), getTimeSeries().getTick(index-3).getClosePrice())
            && func.apply(getTimeSeries().getTick(index).getClosePrice(), getTimeSeries().getTick(index-2).getClosePrice())) {
            return true;
        }
        return false;
    }

    /**
     * looks for nine consecutive closes higher/lower than the close four bars earlier.
     * @param index
     * @return
     */
    private DeMarkSetupResult testDeMarkSetup(int index) {
        PivotPoint pivotPoint = pivotPointIndicator.getValue(index);
        if (index -pivotPoint.getIndex() < 9) {
            return DeMarkSetupResult.PIVOT_TOO_CLOSE;
        }
        BiFunction<Decimal,Decimal,Boolean> comparationFunction = null;

        if (pivotPoint.getType() == PivotPoint.PivotType.MAX) {
            // bearish pivot
            comparationFunction = Decimal::isLessThan;
        } else {
            // bullish pivot
            comparationFunction = Decimal::isGreaterThan;
        }

        DeMarkSetupResult scanResult = setupScan(index,comparationFunction);
        if (scanResult.isOKorBetter()){
            if (isPerfectSetup(index, comparationFunction)) {
                return DeMarkSetupResult.PERFECT;
            }
            return DeMarkSetupResult.OK;
        } else {
            return scanResult;
        }
    }

    @Override
    public AnalysisResult getValue(int index) {
        DeMarkSetupResult setupResult = testDeMarkSetup(index);
        if (setupResult.isOKorBetter()) {

        }
        return null;
    }

}
