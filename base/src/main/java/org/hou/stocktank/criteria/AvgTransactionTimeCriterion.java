package org.hou.stocktank.criteria;

import org.ta4j.core.TimeSeries;
import org.ta4j.core.Trade;
import org.ta4j.core.TradingRecord;
import org.ta4j.core.analysis.criteria.AbstractAnalysisCriterion;

public class AvgTransactionTimeCriterion extends AbstractAnalysisCriterion {

    @Override
    public double calculate(TimeSeries timeSeries, Trade trade) {
        return trade.getExit().getIndex() - trade.getEntry().getIndex();
    }

    @Override
    public double calculate(TimeSeries timeSeries, TradingRecord tradingRecord) {
        if (tradingRecord.getTrades().size() > 0) {
            double sum = 0;
            for (Trade trade : tradingRecord.getTrades()) {
                sum = sum + calculate(timeSeries, trade);
            }
            return sum / tradingRecord.getTrades().size();
        }
        return 0;
    }

    @Override
    public boolean betterThan(double v, double v1) {
        return false;
    }

}
