package org.hou.stocktank.utils.fuzzy;

/**
 * /\ pattern - 3 points definition
 */
public class TriangleFuzzyPattern extends BaseFuzzyPattern {

    public TriangleFuzzyPattern(double a, double b, double c, double minValue, double maxValue) {
        this.addRange(-1 * Double.MAX_VALUE, a, true, false, (x) -> minValue);
        this.addRange(a, b, true, false, new FuzzyLineSlopeFunction(a, minValue, b, maxValue));
        this.addRange(b, b, true, true, (x) -> maxValue);
        this.addRange(b, c, false, true, new FuzzyLineSlopeFunction(b, maxValue, c, minValue));
        this.addRange(c, Double.MAX_VALUE, false, true, (x) -> minValue);
    }

}
