package org.hou.stocktank.utils.fuzzy;

import java.util.function.Function;

public interface FuzzySlopeFunction extends Function<Double, Double> {
}
