package org.hou.stocktank.utils.fuzzy;

import java.util.ArrayList;
import java.util.List;

public class BaseFuzzyPattern implements FuzzyPattern {
    private List<BaseFuzzyRange> ranges = new ArrayList<>();

    private Boolean isInConflict(BaseFuzzyRange range) {
        for (BaseFuzzyRange item : ranges) {
            if(range.isInConflict(item)) return true;
        }
        return false;
    }

    private Boolean isInConflict(double from, double to, boolean closeLeft, boolean closeRight) {
        return isInConflict(new BaseFuzzyRange(from, to, closeLeft, closeRight, (d) -> 1d));
    }

    public BaseFuzzyPattern addRange(double from, double to, boolean leftClose, boolean rightClose, FuzzySlopeFunction value) {
        if (isInConflict(from, to, leftClose, rightClose)) {
            throw new RuntimeException("conflict in ranges");
        }
        ranges.add(new BaseFuzzyRange(from, to, leftClose, rightClose, value));
        return this;
    }

    @Override
    public double getValue(double input) throws ValueRangeException {
        for (BaseFuzzyRange range : ranges) {
            if (range.isInRange(input)) return range.getValue(input);
        }
        return 0;
    }


}

