package org.hou.stocktank.utils.fuzzy;

public class ValueRangeException extends Exception {

    public ValueRangeException() {
    }

    public ValueRangeException(String message) {
        super(message);
    }

    public ValueRangeException(String message, Throwable cause) {
        super(message, cause);
    }

    public ValueRangeException(Throwable cause) {
        super(cause);
    }

    public ValueRangeException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
