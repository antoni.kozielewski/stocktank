package org.hou.stocktank.utils.fuzzy;

/**
 * defines function input -> fuzzy output
 */
public interface FuzzyPattern {

    /**
     * should return 0 .. 1 value
     * @param input
     * @return 0..1 value
     */
    double getValue(double input) throws ValueRangeException;
}
