package org.hou.stocktank.utils.fuzzy;

public class BaseFuzzyRange {
    private boolean closedLeft = false;
    private boolean closedRight = false;
    private double from;
    private double to;
    private FuzzySlopeFunction value;

    public BaseFuzzyRange(double from, double to, boolean closedLeft, boolean closedRight, FuzzySlopeFunction value) {
        this.closedLeft = closedLeft;
        this.closedRight = closedRight;
        this.from = from;
        this.to = to;
        this.value = value;
    }

    public double getFrom() {
        return from;
    }

    public double getTo() {
        return to;
    }

    public double getValue(double input) {
        return value.apply(input);
    }

    public boolean isClosedLeft() {
        return closedLeft;
    }

    public boolean isClosedRight() {
        return closedRight;
    }

    public Boolean isInRange(double value) {
        if (closedLeft) {
            if (closedRight) {
                return (from <= value) && (to >= value);
            } else {
                return (from <= value) && (to > value);
            }
        } else {
            if (closedRight) {
                return (from < value) && (to >= value);
            } else {
                return (from < value) && (to > value);
            }
        }
    }

    private Boolean isBetweenNoEdge(double value) {
        return value > from && value < to;
    }

    public Boolean isInConflict(BaseFuzzyRange test) {
        if (isInRange(test.from)) {
            if (test.closedLeft) {
                return true;
            } else if (isBetweenNoEdge(test.from)) {
                return true;
            }
        } else if (isInRange(test.to)) {
            if (test.closedRight) {
                return true;
            } else if (isBetweenNoEdge(test.to)) {
                return true;
            }
        }
        return false;
    }

    public String toString() {
        return from + "[" + ((closedLeft) ? "X]" : " ]") + " - " + to + "[" + ((closedRight) ? "X]" : " ]");
    }

}