package org.hou.stocktank.utils.fuzzy;


public class FuzzyLineSlopeFunction implements FuzzySlopeFunction {
    private double x1;
    private double y1;
    private double x2;
    private double y2;


    public FuzzyLineSlopeFunction(double x1, double y1, double x2, double y2) {
        this.x1 = x1;
        this.y1 = y1;
        this.x2 = x2;
        this.y2 = y2;
    }

    @Override
    public Double apply(Double aDouble) {
        if ((x1 > aDouble)||(x2<aDouble)) throw new RuntimeException("input not in range");
        return (y2-y1)/(x2-x1) * aDouble + (y1 - x1 *(y2-y1)/(x2-x1));
    }

}
