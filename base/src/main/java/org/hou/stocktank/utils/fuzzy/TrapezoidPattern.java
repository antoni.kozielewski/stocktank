package org.hou.stocktank.utils.fuzzy;


public class TrapezoidPattern extends BaseFuzzyPattern {

    /**
     * you need to define only 2 points of slope - rest is obvious
     */
    public TrapezoidPattern(double x1, double y1, double x2, double y2) {
        addRange(-1 * Double.MAX_VALUE, x1, true, false, (x) -> y1);
        addRange(x1,x2,true, true, new FuzzyLineSlopeFunction(x1,y1,x2,y2));
        addRange(x2, Double.MAX_VALUE, false, true, (x) -> y2);
    }

}
