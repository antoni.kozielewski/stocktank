package org.hou.stocktank.indicators;

import org.ta4j.core.Decimal;
import org.ta4j.core.Tick;
import org.ta4j.core.TimeSeries;
import org.ta4j.core.indicators.CachedIndicator;

import java.util.ArrayList;
import java.util.List;

public class ParabolicSARIndicator extends CachedIndicator<Decimal> {
    private Decimal accelerationStart = Decimal.valueOf(0.02); // default 0.02
    private Decimal acceleration = Decimal.valueOf(0.02);      // default 0.02
    private Decimal accelerationMax = Decimal.valueOf(0.20);   // default 0.20


    private List<PSARValue> psarValues = new ArrayList<>();

    public enum Direction {
        UP, DOWN;
    }

    public class PSARValue {
        private Decimal value;
        private Decimal ep; // extreem point
        private Decimal af; // acceleration factor
        private Direction direction;

        public PSARValue(Decimal value, Decimal ep, Decimal af, Direction direction) {
            this.value = value;
            this.ep = ep;
            this.af = af;
            this.direction = direction;
        }

        public Decimal getEp() {
            return ep;
        }

        public Decimal getAf() {
            return af;
        }

        public Decimal getValue() {
            return value;
        }

        public Direction getDirection() {
            return direction;
        }
    }

    public ParabolicSARIndicator(TimeSeries series) {
        super(series);
    }

    public ParabolicSARIndicator(TimeSeries series, double accelerationStart, double acceleration, double accelerationMax) {
        super(series);
        this.accelerationStart = Decimal.valueOf(accelerationStart);
        this.acceleration = Decimal.valueOf(acceleration);
        this.accelerationMax = Decimal.valueOf(accelerationMax);
    }

    private Decimal increaseAcceleration(Decimal currentAF) {
        if (currentAF.isLessThan(accelerationMax)) {
            return currentAF.plus(acceleration);
        }
        return accelerationMax;
    }

    private void recalculate() {
        Decimal lowValue = Decimal.valueOf(Double.MAX_VALUE); // najmniejsza wartosc z ostatniego okresu
        Decimal maxValue = Decimal.ZERO; // najwieksza wartosc z ostatniego okresu
        Decimal ep = Decimal.ZERO; // extreme point
        Direction direction = Direction.UP;
        Decimal af = accelerationStart; // acceleration factor
        Decimal lastPSAR = Decimal.ZERO;
        for (int index = 0; index < getTimeSeries().getTickCount(); index++) {
            Tick tick = getTimeSeries().getTick(index);
            if (index == 0) { // ----- PIERWSZY KROK zawsze do gory
                direction = Direction.UP;
                maxValue = tick.getOpenPrice().max(tick.getClosePrice());
                lowValue = tick.getOpenPrice().min(tick.getClosePrice());
                ep = maxValue;
                af = accelerationStart;
                psarValues.add(new PSARValue(lowValue, ep, af, direction));
                lastPSAR = lowValue;
            } else {
                if (direction == Direction.UP) { // ----- jesli jedziemy w gore to :
                    //Current SAR = Prior SAR + Prior AF(Prior EP - Prior SAR)
                    Decimal sar = lastPSAR.plus(af.multipliedBy(ep.minus(lastPSAR)));
                    lastPSAR = sar;
                    psarValues.add(new PSARValue(sar, ep, af, Direction.UP));
                    // sprawdzamy czy trzeba podniesc maxValue
                    if (maxValue.isLessThan(tick.getOpenPrice().max(tick.getClosePrice()))) {
                        maxValue = tick.getOpenPrice().max(tick.getClosePrice());
                    }
                    // sprawdzamy czy przesuwamy ep - poniewaz idziemy w gore to zliczamy podbicia szczytu - jak jest nowy szczyt to podbijamy acceleration
                    if (tick.getClosePrice().max(tick.getOpenPrice()).isGreaterThan(ep)) {
                        ep = tick.getClosePrice().max(tick.getOpenPrice());
                        af = increaseAcceleration(af);
                    } // jesli wartosc sar wpada w body albo wyzej niz open-close to zmieniamy kierunek
                    if (sar.isGreaterThanOrEqual(tick.getOpenPrice().min(tick.getClosePrice()))){
                        lowValue = Decimal.valueOf(Double.MAX_VALUE);
                        ep = maxValue;
                        af = accelerationStart;
                        lastPSAR = maxValue;
                        direction = Direction.DOWN;
                    }
                } else {
                    //Current SAR = Prior SAR - Prior AF(Prior SAR - Prior EP)
                    Decimal sar = lastPSAR.minus(af.multipliedBy(lastPSAR.minus(ep)));
                    lastPSAR = sar;
                    psarValues.add(new PSARValue(sar, ep, af, Direction.DOWN));
                    // collect maxValue
                    if (lowValue.isGreaterThan(tick.getOpenPrice().max(tick.getClosePrice()))){
                        lowValue = tick.getOpenPrice().max(tick.getClosePrice());
                    }
                    if (tick.getClosePrice().min(tick.getOpenPrice()).isLessThan(ep)) {
                        ep = tick.getClosePrice().min(tick.getOpenPrice());
                        af = increaseAcceleration(af);
                    }
                    if (sar.isLessThanOrEqual(tick.getClosePrice().max(tick.getOpenPrice()))){
                        maxValue = Decimal.ZERO;
                        ep = lowValue;
                        lastPSAR = lowValue;
                        af = accelerationStart;
                        direction = Direction.UP;
                    }
                }
            }
        }
    }

    @Override
    protected Decimal calculate(int i) {
        if (psarValues.size() == 0) {
            recalculate();
        }
        return psarValues.get(i).getValue();
    }

}
