package org.hou.stocktank.indicators;

import org.ta4j.core.Decimal;
import org.ta4j.core.Indicator;
import org.ta4j.core.indicators.CachedIndicator;
import org.ta4j.core.indicators.helpers.VolumeIndicator;


public class VolumeWeightedMovingAverage extends CachedIndicator<Decimal> {
    private final int timeFrame;
    private final Indicator<Decimal> baseIndicator;
    private final Indicator<Decimal> volumeIndicator;

    /**
     * Constructor.
     *
     * @param baseIndicator   base
     * @param volumeIndicator volume indicator
     * @param timeFrame       the time frame
     */
    public VolumeWeightedMovingAverage(Indicator<Decimal> baseIndicator, VolumeIndicator volumeIndicator, int timeFrame) {
        super(baseIndicator);
        this.timeFrame = timeFrame;
        this.baseIndicator = baseIndicator;
        this.volumeIndicator = volumeIndicator;
    }

    @Override
    protected Decimal calculate(int index) {
        int startIndex = Math.max(0, index - timeFrame + 1);
        Decimal cumulativeValue = Decimal.ZERO;
        Decimal cumulativeVolume = Decimal.ZERO;
        for (int i = startIndex; i <= index; i++) {
            Decimal currentVolume = volumeIndicator.getValue(i);
            cumulativeValue = cumulativeValue.plus(baseIndicator.getValue(i).multipliedBy(currentVolume));
            cumulativeVolume = cumulativeVolume.plus(currentVolume);
        }
        return cumulativeValue.dividedBy(cumulativeVolume);
    }
}
