package org.hou.stocktank.indicators.pivotpoints;

import org.ta4j.core.Decimal;
import org.ta4j.core.Indicator;
import org.ta4j.core.indicators.RecursiveCachedIndicator;

public class PivotPointToDecimalIndicatorWrapper extends RecursiveCachedIndicator<Decimal> {
    private Indicator<PivotPoint> indicator;

    public PivotPointToDecimalIndicatorWrapper(Indicator<PivotPoint> indicator) {
        super(indicator);
        this.indicator = indicator;
    }

    @Override
    protected Decimal calculate(int i) {
        return indicator.getValue(i).getValue();
    }
}
