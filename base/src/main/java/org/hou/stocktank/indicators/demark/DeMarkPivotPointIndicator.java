package org.hou.stocktank.indicators.demark;

import org.hou.stocktank.indicators.pivotpoints.PivotPoint;
import org.hou.stocktank.indicators.pivotpoints.PivotPointIndicator;
import org.ta4j.core.Decimal;
import org.ta4j.core.TimeSeries;

import java.util.function.BiFunction;

public class DeMarkPivotPointIndicator extends PivotPointIndicator {
    private int scanRange;

    public DeMarkPivotPointIndicator(TimeSeries series) {
        this(series, 4);
    }

    public DeMarkPivotPointIndicator(TimeSeries series, int scanRange) {
        super(series);
        this.scanRange = scanRange;
    }

    private Boolean isPivotPoint(int index, BiFunction<Decimal, Decimal, Boolean> compareFunction) {
        if ((index < scanRange) && (index > getTimeSeries().getEndIndex() - scanRange)) {
            return false;
        }
        for (int i = index - scanRange; i < index + scanRange; i++) {
            if (i != index) {
                if (!compareFunction.apply(getTimeSeries().getTick(index).getClosePrice(), getTimeSeries().getTick(i).getClosePrice())) {
                    return false;
                }
            }
        }
        return true;
    }

    @Override
    protected PivotPoint calculate(int index) {
        if (index == 0) {
            if (getTimeSeries().getTick(0).getClosePrice().isGreaterThan(getTimeSeries().getTick(1).getClosePrice())) {
                return new PivotPoint(0, 0, getTimeSeries().getTick(0).getClosePrice(), PivotPoint.PivotType.MAX);
            }
            return new PivotPoint(0, 0, getTimeSeries().getTick(0).getClosePrice(), PivotPoint.PivotType.MIN);
        } else {
            PivotPoint lastPivotPoint = getValue(index - 1);
            if (index < lastPivotPoint.getIndex() + scanRange) {
                return lastPivotPoint;
            }
            if (lastPivotPoint.getType() == PivotPoint.PivotType.MAX) {
                // look for MIN
                if (isPivotPoint(index, Decimal::isLessThan)) {
                    return new PivotPoint(index, lastPivotPoint.getIndex(), getTimeSeries().getTick(index).getClosePrice(), PivotPoint.PivotType.MIN);
                }
            } else {
                // look for MAX
                if (isPivotPoint(index, Decimal::isGreaterThan)) {
                    return new PivotPoint(index, lastPivotPoint.getIndex(), getTimeSeries().getTick(index).getClosePrice(), PivotPoint.PivotType.MAX);
                }
            }
            return lastPivotPoint;
        }
    }
}
