package org.hou.stocktank.indicators;


import org.ta4j.core.Decimal;
import org.ta4j.core.Indicator;
import org.ta4j.core.indicators.CachedIndicator;

public class LineAngleIndicator extends CachedIndicator<Decimal> {
    private int period = 1;
    private Indicator<Decimal> src;

    public LineAngleIndicator(Indicator<Decimal> indicator, int period){
        super(indicator);
        this.src = indicator;
        this.period = period;
    }

    @Override
    protected Decimal calculate(int i) {
        if (i > period +1) {
            double now = src.getValue(i).toDouble();
            double prev = src.getValue(i-period).toDouble();
            double value = 100 * (now - prev)/prev;
            return Decimal.valueOf(Math.toDegrees(Math.atan(value / period)));
        } else {
            return Decimal.ZERO;
        }
    }
}
