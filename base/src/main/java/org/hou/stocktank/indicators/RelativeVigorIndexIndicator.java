package org.hou.stocktank.indicators;


import org.ta4j.core.Decimal;
import org.ta4j.core.Tick;
import org.ta4j.core.TimeSeries;
import org.ta4j.core.indicators.CachedIndicator;

// RVI = (Close – Open) / (High – Low)
public class RelativeVigorIndexIndicator extends CachedIndicator<Decimal> {

    public RelativeVigorIndexIndicator(TimeSeries series) {
        super(series);
    }

    private Decimal getRVIValue(int index) {
        Tick tick = getTimeSeries().getTick(index);
        return (tick.getClosePrice().minus(tick.getOpenPrice())).dividedBy(tick.getMaxPrice().minus(tick.getMinPrice()));
    }

    @Override
    protected Decimal calculate(int index) {
        return getRVIValue(index);
    }

}
