package org.hou.stocktank.indicators.pivotpoints;

import org.ta4j.core.Decimal;
import org.ta4j.core.Tick;
import org.ta4j.core.TimeSeries;

import java.util.ArrayList;
import java.util.List;
import java.util.function.BiFunction;
import java.util.function.Function;

/**
 * returns pivot points
 *   scanRange - forward scan range for next min/max
 *   minDistanceCondition - min distance between min <-> max (evaluated with AND minChangeCondition)
 *   minChangeCondition - min change between min/max :: x +- x*change (evaluated with minDistanceCondtion)
 *   changeCondition - overrides other conditions x +- x * change
 */
public class SimplePivotPointIndicator extends PivotPointIndicator {
    private int scanRange = 10;
    private int minDistanceCondition = 5; // between min <-> max
    private double minChangeCondition = 0.06; // minimum change
    private double changeCondition = 0.1;  // overrides minDistance + minChange
    private Function<Tick, Decimal> minValueFunction = Tick::getMinPrice; // function that is base of calculations
    private Function<Tick, Decimal> maxValueFunction = Tick::getMaxPrice; // function that is base of calculations

    public SimplePivotPointIndicator(TimeSeries series) {
        super(series);
    }

    /**
     *
     * @param series
     * @param scanRange default value = 5
     * @param minDistanceCondition default value = 3
     * @param minChangeCondition default value = 0.03
     * @param changeCondition default value = 0.05
     */
    public SimplePivotPointIndicator(TimeSeries series, int scanRange, int minDistanceCondition, double minChangeCondition, double changeCondition) {
       this(series);
       this.scanRange = scanRange;
       this.minDistanceCondition = minDistanceCondition;
       this.minChangeCondition = minChangeCondition;
       this.changeCondition = changeCondition;
    }

    public List<PivotPoint> getPivotPoints() {
        List<PivotPoint> result = new ArrayList<>();
        for(int i=0; i<getTimeSeries().getEndIndex(); i++) {
            if (getValue(i).getIndex() == i) {
                result.add(getValue(i));
            }
        }
        return result;
    }

    private boolean isNewMax(int index, int lastPivotPointIndex) {
        Decimal value = maxValueFunction.apply(getTimeSeries().getTick(index));
        Decimal lastPivotPointValue = maxValueFunction.apply(getTimeSeries().getTick(lastPivotPointIndex));
        int maxFromScanRangeIndex = getMaxFromScanRange(index);
        if (maxFromScanRangeIndex == -1) return false;
        Decimal maxFromScanRangeValue = maxValueFunction.apply(getTimeSeries().getTick(maxFromScanRangeIndex));
        if (index == 0) {
            if (value.isGreaterThan(maxFromScanRangeValue)) return true;
            return false;
        }
        if (maxFromScanRangeValue.isGreaterThan(value)) {
            return false;
        }
        if((maxFromScanRangeValue.toDouble() > value.toDouble() - value.toDouble() * changeCondition)
          ||((index - lastPivotPointIndex> minDistanceCondition) && (lastPivotPointValue.toDouble() < value.toDouble() - value.toDouble() * minChangeCondition))){
                return true;
        }
        return false;
    }

    /**
     * by Karolina :)
     * @param index
     * @param lastPivotPointIndex
     * @return
     */
    private boolean isNewMin (int index, int lastPivotPointIndex){
        Decimal value = minValueFunction.apply(getTimeSeries().getTick(index));
        Decimal lastPivotPointValue = minValueFunction.apply(getTimeSeries().getTick(lastPivotPointIndex));
        int minFromScanRangeIndex = getMinFromScanRange(index);
        if (minFromScanRangeIndex == -1) return false;
        Decimal minFromScanRangeValue = minValueFunction.apply(getTimeSeries().getTick(minFromScanRangeIndex));
        if (index == 0) {
            if (value.isLessThan(minFromScanRangeValue)) return true;
            return false;
        }
        if (minFromScanRangeValue.isLessThan(value)) {
            return false;
        }
        if ((minFromScanRangeValue.toDouble() < value.toDouble() + value.toDouble() * changeCondition)
           ||((index - lastPivotPointIndex > minDistanceCondition) && (lastPivotPointValue.toDouble() > value.toDouble() + value.toDouble() * minChangeCondition))) {
                return true;
        }
        return false;
    }

    private int scan(int index, BiFunction<Decimal, Decimal, Boolean> function, Function<Tick, Decimal> vFunction) {
        Decimal value = null;
        int result = -1;
        for(int i=index; i < index+1+scanRange && i < getTimeSeries().getEndIndex(); i++){
            if (value == null) {
                value = vFunction.apply(getTimeSeries().getTick(i));
                result = i;
            } else if (function.apply(value, vFunction.apply(getTimeSeries().getTick(i)))) {
                value = vFunction.apply(getTimeSeries().getTick(i));
                result = i;
            }
        }
        return result;
    }

    private int getMaxFromScanRange(int index) {
        return scan(index, Decimal::isLessThan, maxValueFunction);
    }

    private int getMinFromScanRange(int index) {
        return scan(index, Decimal::isGreaterThan, minValueFunction);
    }

    @Override
    protected PivotPoint calculate(int index) {
        if (index > getTimeSeries().getEndIndex() - scanRange) {
            return getValue(index-1);
        }
        Tick tick = getTimeSeries().getTick(index);
        if (index == 0) {
            if (getMaxFromScanRange(0) == 0) {
                return new PivotPoint(index, -1, maxValueFunction.apply(tick), PivotPoint.PivotType.MAX);
            } else {
                return new PivotPoint(index, -1, minValueFunction.apply(tick), PivotPoint.PivotType.MIN);
            }

        }
        PivotPoint lastPivotPoint = getValue(index-1);
        if (PivotPoint.PivotType.MAX == lastPivotPoint.getType()) {
            // look for min
            if (isNewMin(index, lastPivotPoint.getIndex())) {
                return new PivotPoint(index, lastPivotPoint.getIndex(), minValueFunction.apply(tick), PivotPoint.PivotType.MIN);
            } else {
                return lastPivotPoint;
            }
        } else {
            // look for max
            if (isNewMax(index, lastPivotPoint.getIndex())) {
                return new PivotPoint(index, lastPivotPoint.getIndex(), maxValueFunction.apply(tick), PivotPoint.PivotType.MAX);
            } else {
                return lastPivotPoint;
            }
        }
    }

}
