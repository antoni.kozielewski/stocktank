package org.hou.stocktank.indicators;


import org.ta4j.core.Decimal;
import org.ta4j.core.Indicator;
import org.ta4j.core.TimeSeries;

public class TimeShiftIndicator implements Indicator<Decimal>{
    private TimeSeries series;
    private int shift;
    private Indicator<Decimal> indicator;

    /**
     *
     * @param series
     * @param indicator which values are shifting
     * @param shift (index - shift) - if shift is < 0 => move right
     */
    public TimeShiftIndicator(TimeSeries series, Indicator<Decimal> indicator, int shift) {
        this.series = series;
        this.indicator = indicator;
        this.shift = shift;
    }

    @Override
    public Decimal getValue(int i) {
        if (i > shift) {
            return indicator.getValue(i - shift);
        }
        else {
            return Decimal.NaN;
        }
    }

    @Override
    public TimeSeries getTimeSeries() {
        return series;
    }
}
