package org.hou.stocktank.indicators.demark;

import org.hou.stocktank.indicators.pivotpoints.PivotPoint;
import org.hou.stocktank.indicators.pivotpoints.PivotPointIndicator;
import org.ta4j.core.Decimal;
import org.ta4j.core.Tick;
import org.ta4j.core.indicators.RecursiveCachedIndicator;

import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.function.BiFunction;
import java.util.function.Function;

import static org.hou.stocktank.indicators.demark.DeMarkReversalIndicator.DeMarkSetupScanResult.*;


public class DeMarkReversalIndicator extends RecursiveCachedIndicator<DeMarkReversalIndicator.DeMarkReversalResult> {
    private PivotPointIndicator pivotPointIndicator;

    public DeMarkReversalIndicator(PivotPointIndicator pivotPointIndicator) {
        super(pivotPointIndicator.getTimeSeries());
        this.pivotPointIndicator = pivotPointIndicator;
    }

    public enum DeMarkSetupScanResult {
        CANCELED, YES, PERFECT;

        public boolean isOK() {
            return this == YES || this == PERFECT;
        }
    }

    public class DeMarkReversalResult {
        int lastPivotPointIndex;
        ZonedDateTime lastPivotPointDate;
        boolean isSetupFinished;
        boolean isSetupPerfection;
        int setupStartIndex;
        boolean isCountdownFinished;
        int countdownStartIndex;

        public DeMarkReversalResult(int lastPivotPointIndex, ZonedDateTime lastPivotPointDate, boolean isSetupFinished, boolean isSetupPerfection, int setupStartIndex, boolean isCountdownFinished, int countdownStartIndex) {
            this.lastPivotPointIndex = lastPivotPointIndex;
            this.isSetupFinished = isSetupFinished;
            this.lastPivotPointDate = lastPivotPointDate;
            this.isSetupPerfection = isSetupPerfection;
            this.setupStartIndex = setupStartIndex;
            this.isCountdownFinished = isCountdownFinished;
            this.countdownStartIndex = countdownStartIndex;
        }

        public int getLastPivotPointIndex() {
            return lastPivotPointIndex;
        }

        public boolean isSetupFinished() {
            return isSetupFinished;
        }

        public boolean isSetupPerfection() {
            return isSetupPerfection;
        }

        public int getSetupStartIndex() {
            return setupStartIndex;
        }

        public boolean isCountdownFinished() {
            return isCountdownFinished;
        }

        public int getCountdownStartIndex() {
            return countdownStartIndex;
        }

        public String toString() {
            return lastPivotPointIndex + ", " + lastPivotPointDate.format(DateTimeFormatter.ISO_DATE) + ", " + isSetupFinished + ", " + isSetupPerfection + ", " + setupStartIndex + ", " + isCountdownFinished + ", " + countdownStartIndex;
        }
    }

    private DeMarkSetupScanResult scan(int index, Function<Tick, Decimal> priceFunction, BiFunction<Decimal, Decimal, Boolean> compareFunction) {
        if (index > getTimeSeries().getEndIndex() - 9) {
            return CANCELED;
        }
        if (index < 4) {
            return CANCELED;
        }
        for (int i = index; i < index + 9; i++) {
            if (!compareFunction.apply(getTimeSeries().getTick(i).getClosePrice(), getTimeSeries().getTick(i - 4).getClosePrice())) {
                return CANCELED; // canceled
            }
        }
        if (
                (compareFunction.apply(priceFunction.apply(getTimeSeries().getTick(index + 8)), priceFunction.apply(getTimeSeries().getTick(index + 6)))
                        && compareFunction.apply(priceFunction.apply(getTimeSeries().getTick(index + 8)), priceFunction.apply(getTimeSeries().getTick(index + 7))))
                        ||
                        (compareFunction.apply(priceFunction.apply(getTimeSeries().getTick(index + 9)), priceFunction.apply(getTimeSeries().getTick(index + 6)))
                                && compareFunction.apply(priceFunction.apply(getTimeSeries().getTick(index + 9)), priceFunction.apply(getTimeSeries().getTick(index + 7))))
                ) {
            return PERFECT; // perfection of setup
        }
        return YES; // resistance retest suspected
    }

    private DeMarkSetupScanResult scanForFallingSetup(int index) {
        return scan(index, Tick::getMinPrice, Decimal::isLessThan);
    }

    private DeMarkSetupScanResult scanForRisingSetup(int index) {
        return scan(index, Tick::getMaxPrice, Decimal::isGreaterThan);
    }

    private boolean scanForFallingCountdown(int index, int scanRange) {
        return checkCountdown(index, scanRange, Tick::getMinPrice, Decimal::isLessThanOrEqual, this::countDownCancelationMinCheck);
    }

    private boolean scanForRisingCountdown(int index, int scanRange) {
        return checkCountdown(index, scanRange, Tick::getMaxPrice, Decimal::isGreaterThanOrEqual, this::countDownCancelationMaxCheck);
    }

    private Boolean countDownCancelationCheck(int index, BiFunction<Decimal, Decimal, Boolean> compareFunction) {
        if (index < 4) {
            return false;
        }
        if (index > getTimeSeries().getEndIndex() - 4) {
            return false;
        }
        for (int i = index - 4; i < index + 4; i++) {
            if (i != index) {
                if (!compareFunction.apply(getTimeSeries().getTick(i).getClosePrice(), getTimeSeries().getTick(index).getClosePrice())) {
                    return false;
                }
            }
        }
        return true;
    }

    private Boolean countDownCancelationMinCheck(Integer index) {
        return countDownCancelationCheck(index, Decimal::isGreaterThan);
    }

    private Boolean countDownCancelationMaxCheck(Integer index) {
        return countDownCancelationCheck(index, Decimal::isLessThan);
    }

    private boolean checkCountdown(int index, int scanRange, Function<Tick, Decimal> priceFunction, BiFunction<Decimal, Decimal, Boolean> compareFunction, Function<Integer, Boolean> cancelationFunction) {
        int counter = 0;
        if (index > getTimeSeries().getEndIndex() - 13) {
            return false;
        }
        for (int i = index; i < index + scanRange && i < getTimeSeries().getEndIndex(); i++) {
            if (compareFunction.apply(getTimeSeries().getTick(i).getClosePrice(), priceFunction.apply(getTimeSeries().getTick(i - 2)))) {
                counter++;
            }
            if (counter == 13) {
                return true;
            }
            if (cancelationFunction.apply(index)) {
                counter = 0; // cancelation - when there is new min/max ( compared close price four left and four right)
            }
        }
        return false;
    }

    @Override
    protected DeMarkReversalResult calculate(int index) {
        PivotPoint lastPivotPoint = pivotPointIndicator.getValue(index);
        for (int i = lastPivotPoint.getIndex(); i < index - 13; i++) {
            if (lastPivotPoint.getType() == PivotPoint.PivotType.MAX) {
                // look for MIN
                DeMarkSetupScanResult setupResult = scanForFallingSetup(i);
                if (setupResult.isOK()) {
                    for (int j = i; j < index - 13; j++) {
                        if (scanForFallingCountdown(j, index - j)) {
                            // jest spełnione
                            return new DeMarkReversalResult(lastPivotPoint.getIndex(), getTimeSeries().getTick(index).getEndTime(), true, setupResult == PERFECT, i, true, j);
                        }
                    }
                    // tylko setup
                    return new DeMarkReversalResult(lastPivotPoint.getIndex(), getTimeSeries().getTick(index).getEndTime(), true, setupResult == PERFECT, i, false, -1);
                }
            } else {
                // look for MAX
                DeMarkSetupScanResult setupResult = scanForRisingSetup(i);
                if (scanForRisingSetup(i).isOK()) {
                    for (int j = i; j < index - 13; j++) {
                        if (scanForRisingCountdown(j, index - j)) {
                            // jest spełnione
                            return new DeMarkReversalResult(lastPivotPoint.getIndex(), getTimeSeries().getTick(index).getEndTime(), true, setupResult == PERFECT, i, true, j);
                        }
                    }
                    // tylko setup
                    return new DeMarkReversalResult(lastPivotPoint.getIndex(), getTimeSeries().getTick(index).getEndTime(), true, setupResult == PERFECT, i, false, -1);
                }
            }
        }
        return new DeMarkReversalResult(lastPivotPoint.getIndex(), getTimeSeries().getTick(index).getEndTime(), false, false, 0, false, 0);
    }
}
