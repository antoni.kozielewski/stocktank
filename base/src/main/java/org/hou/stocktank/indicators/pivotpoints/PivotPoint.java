package org.hou.stocktank.indicators.pivotpoints;

import org.ta4j.core.Decimal;

public class PivotPoint {
    private int previousIndex;
    private int index;
    private Decimal value;
    private PivotType type;

    public enum PivotType {
        MAX, MIN;
    }

    public PivotPoint(int index, int previousIndex, Decimal value, PivotType type) {
        this.index = index;
        this.previousIndex = previousIndex;
        this.value = value;
        this.type = type;
    }

    public int getIndex() {
        return index;
    }

    public Decimal getValue() {
        return value;
    }

    public PivotType getType() {
        return type;
    }

    public int getPreviousIndex() {
        return previousIndex;
    }

    public String toString() {
        return "[" + index + "] = " + value.toDouble() + " : " + type;
    }
}
