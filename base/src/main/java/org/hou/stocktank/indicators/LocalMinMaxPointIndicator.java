package org.hou.stocktank.indicators;

import org.ta4j.core.Decimal;
import org.ta4j.core.Indicator;
import org.ta4j.core.TimeSeries;
import org.ta4j.core.indicators.CachedIndicator;


/**
 * returns -1 when indicator turns from up trend to down trend, and 1 when form falling to raising, 0 when no change of trend
 */
public class LocalMinMaxPointIndicator extends CachedIndicator<Decimal> {
    private Indicator<Decimal> baseIndicator;
    private int previousPeriod = 2;
    private int forwardPeriod = 2;

    public LocalMinMaxPointIndicator(TimeSeries series, Indicator<Decimal> baseIndicator, int previousPeriod, int forwardPeriod){
        super(series);
        this.baseIndicator = baseIndicator;
        this.previousPeriod = previousPeriod;
        this.forwardPeriod = forwardPeriod;
    }

    @Override
    protected Decimal calculate(int i) {
        if (i - previousPeriod - 1 - forwardPeriod>= 0){
            if (
                    baseIndicator.getValue(i-previousPeriod-1-forwardPeriod).isGreaterThan(baseIndicator.getValue(i-forwardPeriod-1)) &&
                    baseIndicator.getValue(i).isGreaterThan(baseIndicator.getValue(i-forwardPeriod-1))
                    ) {
                return Decimal.ONE;
            } else if (
                    baseIndicator.getValue(i-previousPeriod-1-forwardPeriod).isLessThan(baseIndicator.getValue(i-forwardPeriod-1)) &&
                    baseIndicator.getValue(i).isLessThan(baseIndicator.getValue(i-forwardPeriod-1))
                    ){
                return Decimal.valueOf(-1);
            } else {
                return Decimal.ZERO;
            }
        } else {
            return Decimal.ZERO;
        }
    }

}
