package org.hou.stocktank.indicators.pivotpoints;

import org.ta4j.core.Decimal;
import org.ta4j.core.indicators.RecursiveCachedIndicator;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class FibonacciSupportResistanceIndicator extends RecursiveCachedIndicator<List<Decimal>> {
    private PivotPointIndicator pivotPointIndicator;
    private List<Double> fibonacciSeries = Arrays.asList(0.236, 0.382, 0.5, 0.618, 0.786, 1.0, 1.272, 1.618, 2.618);

    public FibonacciSupportResistanceIndicator(PivotPointIndicator indicator) {
        super(indicator.getTimeSeries());
        pivotPointIndicator = indicator;
    }

    @Override
    protected List<Decimal> calculate(int index) {
        PivotPoint lastPivotPoint = pivotPointIndicator.getValue(index);
        if (lastPivotPoint.getPreviousIndex() < 0) {
            return new ArrayList<>();
        }
        PivotPoint previousPivotPoint = pivotPointIndicator.getValue(lastPivotPoint.getPreviousIndex());
        List<Decimal> result = new ArrayList<>();
        if (lastPivotPoint.getType() == PivotPoint.PivotType.MAX) {
            // bearish
            for(int i=0; i< fibonacciSeries.size(); i++) {
                result.add(Decimal.valueOf(lastPivotPoint.getValue().toDouble() - (lastPivotPoint.getValue().toDouble() - previousPivotPoint.getValue().toDouble()) * fibonacciSeries.get(i)));
            }
        } else {
            // bullish
            for(int i=0; i< fibonacciSeries.size(); i++) {
                result.add(Decimal.valueOf(lastPivotPoint.getValue().toDouble() + (previousPivotPoint.getValue().toDouble() - lastPivotPoint.getValue().toDouble()) * fibonacciSeries.get(i)));
            }
        }
        return result;
    }

}
