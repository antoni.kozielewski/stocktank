package org.hou.stocktank.indicators.pivotpoints;

import org.ta4j.core.TimeSeries;
import org.ta4j.core.indicators.RecursiveCachedIndicator;

public abstract class PivotPointIndicator extends RecursiveCachedIndicator<PivotPoint> {

    public PivotPointIndicator(TimeSeries series) {
        super(series);
    }

}
