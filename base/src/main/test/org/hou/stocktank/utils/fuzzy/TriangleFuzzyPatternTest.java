package org.hou.stocktank.utils.fuzzy;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class TriangleFuzzyPatternTest {
    private TriangleFuzzyPattern pattern;

    @Before
    public void prepare() {
        pattern = new TriangleFuzzyPattern(10, 20, 30, 0, 1);
    }


    @Test
    public void shapeTest() throws ValueRangeException {
        Assert.assertEquals(0, pattern.getValue(0), 0.001);
        Assert.assertEquals(0, pattern.getValue(5), 0.001);
        Assert.assertEquals(0.5, pattern.getValue(15), 0.001);
        Assert.assertEquals(1, pattern.getValue(20), 0.001);
        Assert.assertEquals(0.5, pattern.getValue(25), 0.001);
        Assert.assertEquals(0, pattern.getValue(30), 0.001);
        Assert.assertEquals(0, pattern.getValue(35), 0.001);
    }

}
