package org.hou.stocktank.utils.fuzzy;

import org.junit.Assert;
import org.junit.Test;

public class FuzzyLineSlopeFunctionTest {

    @Test
    public void risingTest(){
        FuzzySlopeFunction f = new FuzzyLineSlopeFunction(0,0,10,10);
        Assert.assertEquals(Double.valueOf(0), f.apply(Double.valueOf(0)));
        Assert.assertEquals(Double.valueOf(5), f.apply(Double.valueOf(5)));
        Assert.assertEquals(Double.valueOf(10), f.apply(Double.valueOf(10)));
    }

    @Test
    public void flatTest(){
        FuzzySlopeFunction f = new FuzzyLineSlopeFunction(10,10,20,10);
        Assert.assertEquals(Double.valueOf(10), f.apply(Double.valueOf(10)));
        Assert.assertEquals(Double.valueOf(10), f.apply(Double.valueOf(15)));
        Assert.assertEquals(Double.valueOf(10), f.apply(Double.valueOf(20)));
    }

    @Test
    public void fallingTest(){
        FuzzySlopeFunction f = new FuzzyLineSlopeFunction(0,10,10,0);
        Assert.assertEquals(Double.valueOf(10), f.apply(Double.valueOf(0)));
        Assert.assertEquals(Double.valueOf(5), f.apply(Double.valueOf(5)));
        Assert.assertEquals(Double.valueOf(0), f.apply(Double.valueOf(10)));
    }


}
