package org.hou.stocktank.utils.fuzzy;


import org.junit.Assert;
import org.junit.Test;

public class BaseFuzzyPatternTest {

    @Test
    public void conflictsTest(){
        double maxValue = 1;
        BaseFuzzyRange a0 = new BaseFuzzyRange(0,10,true, true, (x) -> maxValue);
        BaseFuzzyRange a1 = new BaseFuzzyRange(0,10,false, false, (x) -> maxValue);

        BaseFuzzyRange b0 = new BaseFuzzyRange(5,15,true, true, (x) -> maxValue);
        BaseFuzzyRange b1 = new BaseFuzzyRange(5,15,false, false, (x) -> maxValue);

        BaseFuzzyRange c0 = new BaseFuzzyRange(10,15,true, true, (x) -> maxValue);
        BaseFuzzyRange c1 = new BaseFuzzyRange(10,15,false, false, (x) -> maxValue);

        BaseFuzzyRange d0 = new BaseFuzzyRange(-1,0,true, true, (x) -> maxValue);
        BaseFuzzyRange d1 = new BaseFuzzyRange(-1,0,false, false, (x) -> maxValue);

        Assert.assertTrue(a0.isInConflict(b0));
        Assert.assertTrue(a0.isInConflict(b1));
        Assert.assertTrue(a0.isInConflict(c0));
        Assert.assertTrue(a0.isInConflict(d0));

        Assert.assertTrue(b0.isInConflict(c0));
        Assert.assertTrue(b0.isInConflict(c1));
        Assert.assertTrue(b1.isInConflict(c0));
        Assert.assertTrue(b1.isInConflict(c1));

        Assert.assertFalse(a0.isInConflict(c1));
        Assert.assertFalse(a0.isInConflict(d1));

        Assert.assertFalse(c0.isInConflict(d0));
        Assert.assertFalse(c0.isInConflict(d1));

        Assert.assertFalse(c1.isInConflict(d0));
        Assert.assertFalse(c1.isInConflict(d1));
    }
}
