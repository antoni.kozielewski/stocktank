package org.hou.stocktank.lab.tuning;

import java.util.concurrent.Executor;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.AsyncConfigurerSupport;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

@SpringBootApplication
@EnableAsync
@Configuration
@EnableConfigurationProperties
public class LabApplication extends AsyncConfigurerSupport {

    @Value("${threadExecutor.corePoolSize}")
    private int corePoolSize;

    @Value("${threadExecutor.maxPoolSize}")
    private int maxPoolSize;

    @Value("${threadExecutor.queueCapacity}")
    private int queueCapacity;

    @Value("${threadExecutor.threadNamePrefix}")
    private String threadNamePrefix;

    public static void main(String[] args) {
        SpringApplication.run(LabApplication.class, args);
    }

    @Override
    public Executor getAsyncExecutor() {
        ThreadPoolTaskExecutor executor = new ThreadPoolTaskExecutor();
        executor.setCorePoolSize(corePoolSize);
        executor.setMaxPoolSize(maxPoolSize);
        executor.setQueueCapacity(queueCapacity);
        executor.setThreadNamePrefix(threadNamePrefix);
        executor.initialize();
        return executor;
    }
}
