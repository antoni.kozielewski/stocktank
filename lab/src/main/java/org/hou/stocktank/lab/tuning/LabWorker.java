package org.hou.stocktank.lab.tuning;

import java.io.File;
import java.io.IOException;
import java.util.Date;
import java.util.Map;
import java.util.concurrent.Future;

import org.hou.stocktank.base.StrategyFactory;
import org.hou.stocktank.base.json.StrategyFactoryJsonizer;
import org.hou.stocktank.charts.MagicChartBuilder;
import org.hou.stocktank.genetics.Population;
import org.hou.stocktank.storage.PersistentResultStorage;
import org.hou.stocktank.storage.ResultStorage;
import org.hou.stocktank.utils.evaluation.StrategyTester;
import org.jfree.chart.ChartUtilities;
import org.jfree.chart.JFreeChart;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.AsyncResult;
import org.springframework.stereotype.Service;

import org.ta4j.core.TimeSeries;
import org.ta4j.core.TimeSeriesManager;
import org.ta4j.core.TradingRecord;

@Service
@ConfigurationProperties(prefix="worker")
public class LabWorker {
    private static final Logger logger = LoggerFactory.getLogger(LabWorker.class);

    private String storagePath;
    private String chartsPath;

    private int chartWidth;
    private int chartHeight;

    private String getStorageFilename(String stockName) {
        return stockName;
    }

    private String getChartsPath(String stockName, StrategyFactory strategyFactory) {
        return chartsPath + stockName + "-"+strategyFactory.getName()+ ".jpg";
    }

    private ResultStorage getStorage(String stockName){
        PersistentResultStorage res = new PersistentResultStorage(getStorageFilename(stockName), storagePath);
        return res;
    }

    private void generateChart(String stockName, TimeSeries series, StrategyFactory strategyFactory) {
        TimeSeriesManager manager = new TimeSeriesManager(series);
        TradingRecord tradingRecord = manager.run(strategyFactory.buildStrategy(series));
        MagicChartBuilder chartBuilder = new MagicChartBuilder(strategyFactory, series)
                .withTradingRecord(tradingRecord)
                .withBuySignals()
                .withSellSignals()
                .withPriceEMA()
                .withTitle(stockName + " - " + strategyFactory.getName())
                ;
        JFreeChart chart = chartBuilder.build().getChart();
        try {
            ChartUtilities.saveChartAsJPEG(new File(getChartsPath(stockName, strategyFactory)), chart, chartWidth, chartHeight);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void finishWork(String stockName, TimeSeries series, StrategyFactory strategyFactory) {
        logger.info("finish [{}] - {} ", stockName, strategyFactory.getName());
        String result = StrategyFactoryJsonizer.serialize(strategyFactory).toString();
        StrategyTester tester = new StrategyTester();
        Map<String, String> testResult = tester.testStrategy(stockName, series, 0, strategyFactory);
        getStorage(stockName).addResult(testResult);
        generateChart(stockName,series,strategyFactory);
    }

    @Async
    public Future<String> work(String stockName, TimeSeries timeSeries, StrategyFactory seed, int populationSize, int generations) {
        logger.info("init : {} - {} (population={}, generations={})", stockName, seed.getName(), populationSize, generations);
        Population population = new Population(populationSize, seed, timeSeries);
        Long avgTime = null;
        for (int i = 0; i < generations; i++) {
            long start = (new Date()).getTime();
            population.singleGeneration();
            long end = (new Date()).getTime();
            if (avgTime == null) avgTime = end - start;
            else avgTime = (avgTime + (end - start)) / 2;
            if (i % 10 == 0) {
                logger.trace("[avgTime=" + Math.round(avgTime) + "ms] " + population.getLastGenerationStats());
            }
        }
        finishWork(stockName, timeSeries, population.getBest());
        return new AsyncResult<>("Done");
    }

    public String getStoragePath() {
        return storagePath;
    }

    public void setStoragePath(String storagePath) {
        this.storagePath = storagePath;
    }

    public String getChartsPath() {
        return chartsPath;
    }

    public void setChartsPath(String chartsPath) {
        this.chartsPath = chartsPath;
    }

    public void setChartWidth(int chartWidth) {
        this.chartWidth = chartWidth;
    }

    public void setChartHeight(int chartHeight) {
        this.chartHeight = chartHeight;
    }

    public int getChartWidth() {
        return chartWidth;
    }

    public int getChartHeight() {
        return chartHeight;
    }
}


