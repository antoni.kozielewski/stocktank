package org.hou.stocktank.lab.tuning;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Future;

import org.hou.stocktank.base.StrategyFactory;
import org.hou.stocktank.utils.BossaPRNLoader;
import org.hou.stocktank.utils.TimeSeriesUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import org.ta4j.core.TimeSeries;

@Component
@ConfigurationProperties(prefix = "lab")
public class Lab implements ApplicationRunner {
    private static final Logger logger = LoggerFactory.getLogger(Lab.class);

    @Autowired
    LabWorker worker;
    private List<StrategyFactory> seeds = new ArrayList<>();

    // configuration
    private int populationSize;
    private int generations;
    private List<String> strategies = new ArrayList<>();
    private List<String> stocks = new ArrayList<>();
    private String stockDataPath;
    private String stockDataFileExtension;


    private StrategyFactory createNewInstance(String className) {
        logger.info("Loading strategy: {} ... ", className);
        Class<?> clazz = null;
        try {
            clazz = Class.forName(className);
            Constructor<?> ctor = clazz.getConstructor();
            StrategyFactory result = (StrategyFactory) ctor.newInstance();
            return result;
        } catch (ClassNotFoundException e) {
            throw new RuntimeException(e);
        } catch (NoSuchMethodException e) {
            throw new RuntimeException(e);
        } catch (InstantiationException e) {
            throw new RuntimeException(e);
        } catch (IllegalAccessException e) {
            throw new RuntimeException(e);
        } catch (InvocationTargetException e) {
            throw new RuntimeException(e);
        }
    }

    private List<StrategyFactory> getSeeds() {
        if (seeds.size() == 0) {
            for (String strategyClassName : strategies) {
                seeds.add(createNewInstance(strategyClassName));
            }
        }
        return seeds;
    }

    private boolean isDone(List<Future<String>> list) {
        for (Future future : list) {
            if (!future.isDone()) return false;
        }
        return true;
    }

    private TimeSeries getTimeSeries(String stockName) {
        TimeSeries tickSeries = BossaPRNLoader.load(stockName, stockDataPath + stockName + stockDataFileExtension);
        return TimeSeriesUtils.aggregateToDays(tickSeries);
    }

    @Override
    public void run(ApplicationArguments applicationArguments) throws Exception {
        logger.info("--START : ppopulation={}, generations={}--", populationSize, generations);

        List<Future<String>> results = new ArrayList<>();
        for (String stockName : stocks) {
            logger.info("training for stock: " + stockName);
            TimeSeries timeSeries = getTimeSeries(stockName);
            for (StrategyFactory seed : getSeeds()) {
                results.add(worker.work(stockName, timeSeries, seed, populationSize, generations));
            }
        }

        // Wait until they are all done
        while (isDone(results)) {
            Thread.sleep(1000);
            System.out.print(".");
        }

        System.out.println("------------------------------------------------------------------------------------------------------------------------");
        int fullResultCount = stocks.size() * seeds.size();
        int resultsCounter = 0;
        for (Future<String> result : results) {
            resultsCounter ++;
            System.out.println(result.get() + " " + ((int)(100*resultsCounter/fullResultCount)) + "%");
        }
    }


    public void setWorker(LabWorker worker) {
        this.worker = worker;
    }

    public void setPopulationSize(int populationSize) {
        this.populationSize = populationSize;
    }

    public void setGenerations(int generations) {
        this.generations = generations;
    }

    public void setStrategies(List<String> strategies) {
        this.strategies = strategies;
    }

    public void setStocks(List<String> stocks) {
        this.stocks = stocks;
    }

    public LabWorker getWorker() {
        return worker;
    }

    public int getPopulationSize() {
        return populationSize;
    }

    public int getGenerations() {
        return generations;
    }

    public List<String> getStrategies() {
        return strategies;
    }

    public List<String> getStocks() {
        return stocks;
    }

    public String getStockDataPath() {
        return stockDataPath;
    }

    public String getStockDataFileExtension() {
        return stockDataFileExtension;
    }

    public void setStockDataPath(String stockDataPath) {
        this.stockDataPath = stockDataPath;
    }

    public void setStockDataFileExtension(String stockDataFileExtension) {
        this.stockDataFileExtension = stockDataFileExtension;
    }
}
