package org.hou.stocktank.utils.analysis;


import org.hou.stocktank.utils.StocktankTXTUtil;
import org.ta4j.core.Decimal;

import java.io.File;
import java.util.List;
import java.util.function.BiFunction;
import java.util.function.Function;

public class MBankAnalysis {
    private static final Decimal MBANK_FEE = Decimal.valueOf(0.0039);
    private static final Decimal MBANK_MIN_FEE = Decimal.valueOf(3);
    private static final Decimal TAX_RATE = Decimal.valueOf(0.19);
    private TransactionsLogAnalyzer logAnalyzer;

    // fee calulation
    private final BiFunction<Integer, Decimal, Decimal> feeCalculation = (quantity, price) -> {
        if (price.multipliedBy(Decimal.valueOf(quantity)).multipliedBy(MBANK_FEE).isLessThan(MBANK_MIN_FEE)) {
            return MBANK_MIN_FEE;
        }
        return price.multipliedBy(Decimal.valueOf(quantity)).multipliedBy(MBANK_FEE);
    };

    // tax calculation
    private final Function<Decimal, Decimal> taxCalculation = (value) -> value.multipliedBy(TAX_RATE);



    private void prepareAnalysis(File file) {
        MBankTransactionLogParser parser = new MBankTransactionLogParser(feeCalculation);
        List<Operation> operations = parser.parse(file);
        logAnalyzer = new TransactionsLogAnalyzer(taxCalculation);
        AnalysisResult result = logAnalyzer.analyze(operations);
        logAnalyzer.printLogHistory(result);
        logAnalyzer.printWallet(result);
        logAnalyzer.prinIncomeFeesProfitTax(result);
        logAnalyzer.printCurrentYearAnalysis(result);
    }

    private void printInfo() {
        System.out.println(StocktankTXTUtil.createFrame(
                " ",
                "MBank Transaction Analyzer",
                " ",
                "How to:",
                "login mbank: ",
                "    -> inwestycje ",
                "       -> giełda ",
                "          -> historie offline",
                "             -> Historia transackji",
                "                    :: format csv ",
                " ",
                "save this file as: .........",
                "                                                                                               "));
        System.out.println("");
        System.out.println("");
        System.out.println("");
    }

    public static void main(String[] args) {
        MBankAnalysis analysis = new MBankAnalysis();
        analysis.printInfo();
        analysis.prepareAnalysis(new File("data/mbank_data_full.Csv"));
    }
}
