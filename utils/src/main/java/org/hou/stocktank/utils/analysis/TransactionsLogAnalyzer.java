package org.hou.stocktank.utils.analysis;


import org.hou.stocktank.utils.StocktankTXTUtil;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.ta4j.core.Decimal;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.DoubleStream;
import java.util.stream.IntStream;

public class TransactionsLogAnalyzer {
    private final Function<Decimal, Decimal> taxCalculation; // (value) -> tax

    public TransactionsLogAnalyzer(Function<Decimal, Decimal> taxCalculation) {
        this.taxCalculation = taxCalculation;
    }

    public AnalysisResult analyze(List<Operation> operations) {
        List<LogItem> result = new ArrayList<>();
        Map<String, List<Operation>> wallet = new HashMap<>();
        operations.sort((Operation o1, Operation o2) -> o1.compareTo(o2));
        for (Operation operationSrc : operations) {
            Operation operation = new Operation(operationSrc);
            if (operation.getType() == OperationType.BUY) {
                wallet.putIfAbsent(operation.getStock(), new ArrayList<>());
                wallet.get(operation.getStock()).add(operation);
            } else {
                while (operation.getQuantity() > 0) {
                    Operation buy = wallet.get(operation.getStock()).get(0);
                    if (buy.getQuantity() > operation.getQuantity()) {
                        Decimal buyFee = buy.getFeeForQuantity(operation.getQuantity());
                        Decimal sellFee = operation.getFee();
                        buy.recalculateForQuantity(buy.getQuantity() - operation.getQuantity());
                        wallet.get(operation.getStock()).set(0, buy);
                        result.add(new LogItem(buy.getDate(), operation.getDate(), operation.getStock(), operation.getQuantity(), buy.getPrice(), operation.getPrice(), buyFee, sellFee));
                        operation.setQuantity(0); // wszystko wykorzystane
                    } else if (buy.getQuantity() == operation.getQuantity()) {
                        result.add(new LogItem(buy.getDate(), operation.getDate(), operation.getStock(), operation.getQuantity(), buy.getPrice(), operation.getPrice(), buy.getFee(), operation.getFee()));
                        wallet.get(operation.getStock()).remove(0);
                        operation.setQuantity(0); // wszystko wykorzystane
                    } else if (buy.getQuantity() < operation.getQuantity()) {
                        Decimal buyFee = buy.getFee();
                        Decimal sellFee = operation.getFeeForQuantity(buy.getQuantity());
                        result.add(new LogItem(buy.getDate(), operation.getDate(), operation.getStock(), buy.getQuantity(), buy.getPrice(), operation.getPrice(), buyFee, sellFee));
                        operation.recalculateForQuantity(operation.getQuantity() - buy.getQuantity());
                        wallet.get(operation.getStock()).remove(0); // wykorzystane wszystko z buy
                    }
                }
            }
        }
        return new AnalysisResult(result, wallet, taxCalculation);
    }

    public void printWallet(AnalysisResult analysisResultResult) {
        List<String> strings = new ArrayList<>();
        strings.add(StocktankTXTUtil.fillRight("    WALLET : ", " ", 95));
        strings.add(
                StocktankTXTUtil.fillLeft("stock name", " ", 20) + " : " +
                        StocktankTXTUtil.fillCenter("quantity", " ", 10) + " | " +
                        StocktankTXTUtil.fillRight("avg price", " ", 20) + " | " +
                        StocktankTXTUtil.fillRight("    total", " ", 20)
        );
        strings.add(StocktankTXTUtil.fillRight("", "-", 70));
        double totalCash = 0;
        for (String stockName : analysisResultResult.getWallet().keySet()) {
            List<Operation> operations = analysisResultResult.getWallet().get(stockName);
            int totalQuantity = operations.stream().flatMapToInt(o -> IntStream.of((o.getType() == OperationType.BUY) ? o.getQuantity() : -1 * o.getQuantity())).sum();
            double avgPrice = operations.stream().flatMapToDouble(o -> DoubleStream.of((o.getType() == OperationType.BUY) ? o.getQuantity() * o.getPrice().toDouble() : -1 * o.getQuantity() * o.getPrice().toDouble())).sum();
            avgPrice = avgPrice / totalQuantity;
            if (avgPrice > 0) {
                totalCash = totalCash + avgPrice * totalQuantity;
            }
            if (totalQuantity > 0) {

                strings.add(
                        StocktankTXTUtil.fillLeft(stockName, " ", 20) + " : " +
                                StocktankTXTUtil.fillCenter(totalQuantity + "", " ", 10) + " | " +
                                StocktankTXTUtil.fillRight(StocktankTXTUtil.fillLeft(String.format("%1$,.2f", avgPrice) + "", " ", 9), " ", 20) + " | " +
                                StocktankTXTUtil.fillRight(StocktankTXTUtil.fillLeft(String.format("%1$,.2f", avgPrice * totalQuantity) + "", " ", 9), " ", 20)
                );
            }
        }
        strings.add(StocktankTXTUtil.fillRight("", "-", 70));
        strings.add(StocktankTXTUtil.fillLeft(String.format("%1$,.2f", totalCash) + "", " ", 20 + 10 + 20 + 10 + 8));
        System.out.println(StocktankTXTUtil.createFrame(strings.toArray(new String[]{})));
    }

    public void printLogHistory(AnalysisResult analysisResult) {
        System.out.println("");
        System.out.println(" Log history: ");
        System.out.println(new LogItem(null, null, "", 0, null, null, null, null).getPreatyHeader());
        analysisResult.getLogItems().stream().forEach(x -> System.out.println(x.toPreatyString()));
        System.out.println("");
        prinIncomeFeesProfitTax(analysisResult);
    }

    public void prinIncomeFeesProfitTax(AnalysisResult analysisResult) {
        System.out.println(StocktankTXTUtil.fillLeft(" total income : ", " ", 20) + StocktankTXTUtil.fillLeft(String.format("%1$,.2f", analysisResult.getTotalIncome().toDouble()), " ", 10));
        System.out.println(StocktankTXTUtil.fillLeft(" total fees : ", " ", 20) + StocktankTXTUtil.fillLeft(String.format("%1$,.2f", analysisResult.getTotalFees().toDouble()), " ", 10));
        System.out.println(StocktankTXTUtil.fillLeft(" total profit : ", " ", 20) + StocktankTXTUtil.fillLeft(String.format("%1$,.2f", analysisResult.getTotalProfit().toDouble()), " ", 10));
        System.out.println(StocktankTXTUtil.fillLeft(" total tax : ", " ", 20) + StocktankTXTUtil.fillLeft(String.format("%1$,.2f", analysisResult.getTax().toDouble()), " ", 10));
    }

    public void printPeriodAnalysis(AnalysisResult analysisResult, DateTime from, DateTime to) {
        analysisResult.filterLogItemsBySellDate(from, to);
        System.out.println(StocktankTXTUtil.fillCenter(" Analysis [" + from.toString(DateTimeFormat.forPattern("yyyy.MM.dd")) + " - " + to.toString(DateTimeFormat.forPattern("yyyy.MM.dd")) + "}", "-", 100));
        prinIncomeFeesProfitTax(analysisResult);
        System.out.println("");
        System.out.println("");
        printWallet(analysisResult);
    }

    public void printCurrentYearAnalysis(AnalysisResult analysisResult) {
        DateTime yearStart = new DateTime(DateTime.now().getYear(), 1, 1, 0, 0);
        printPeriodAnalysis(analysisResult, yearStart, DateTime.now());
    }

}

