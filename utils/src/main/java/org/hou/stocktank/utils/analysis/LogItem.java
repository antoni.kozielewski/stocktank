package org.hou.stocktank.utils.analysis;


import org.hou.stocktank.utils.StocktankTXTUtil;
import org.joda.time.DateTime;
import org.ta4j.core.Decimal;

class LogItem {
    public DateTime buyDate;
    public DateTime sellDate;
    public String stock;
    public int quantity;
    public Decimal buyPrice;
    public Decimal sellPrice;
    public Decimal buyFee;
    public Decimal sellFee;

    public LogItem(DateTime buyDate, DateTime sellDate, String stock, int quantity, Decimal buyPrice, Decimal sellPrice, Decimal buyFee, Decimal sellFee) {
        this.buyDate = buyDate;
        this.sellDate = sellDate;
        this.stock = stock;
        this.quantity = quantity;
        this.buyPrice = buyPrice;
        this.sellPrice = sellPrice;
        this.buyFee = buyFee;
        this.sellFee = sellFee;
    }

    public String toString() {
        return stock + " | " + buyDate + " | " + sellDate + " | Q " + quantity + " | " + buyPrice + " -> " + sellPrice + " || " + getIncome();
    }

    public Decimal getIncome() {
        return sellPrice.multipliedBy(Decimal.valueOf(quantity)).minus(buyPrice.multipliedBy(Decimal.valueOf(quantity)));
    }

    public Decimal getProfit() {
        return getIncome().minus(buyFee.plus(sellFee));
    }

    public String getPreatyHeader() {
        return
                StocktankTXTUtil.fillLeft("stock", " ", 20) + " | " +
                        StocktankTXTUtil.fillLeft("quantity", " ", 20) + " | " +
                        StocktankTXTUtil.fillLeft("buy price", " ", 10) + " | " +
                        StocktankTXTUtil.fillLeft("sell price", " ", 10) + " | " +
                        StocktankTXTUtil.fillLeft("buy fee", " ", 10) + " | " +
                        StocktankTXTUtil.fillLeft("sell fee", " ", 10) + " | " +
                        StocktankTXTUtil.fillLeft("income", " ", 10) + " | " +
                        StocktankTXTUtil.fillLeft("profit", " ", 10) + " | " +
                        StocktankTXTUtil.fillLeft("buy date", " ", 30) + " | " +
                        StocktankTXTUtil.fillLeft("sell date", " ", 30);
    }

    public String toPreatyString() {
        return
                StocktankTXTUtil.fillLeft(stock, " ", 20) + " | " +
                        StocktankTXTUtil.fillLeft(quantity + "", " ", 20) + " | " +
                        StocktankTXTUtil.fillLeft(buyPrice + "", " ", 10) + " | " +
                        StocktankTXTUtil.fillLeft(sellPrice + "", " ", 10) + " | " +
                        StocktankTXTUtil.fillLeft(String.format("%1$,.2f", buyFee.toDouble()) + "", " ", 10) + " | " +
                        StocktankTXTUtil.fillLeft(String.format("%1$,.2f", sellFee.toDouble()) + "", " ", 10) + " | " +
                        StocktankTXTUtil.fillLeft(String.format("%1$,.2f", getIncome().toDouble()) + "", " ", 10) + " | " +
                        StocktankTXTUtil.fillLeft(String.format("%1$,.2f", getProfit().toDouble()) + "", " ", 10) + " | " +
                        StocktankTXTUtil.fillLeft(buyDate + "", " ", 30) + " | " +
                        StocktankTXTUtil.fillLeft(sellDate + "", " ", 30);
    }
}