package org.hou.stocktank.utils;

public class StocktankTXTUtil {

    public static String fillRight(String s, String fill, int length) {
        String result = s;
        for (int i = result.length(); i < length; i++) {
            result = result + fill;
        }
        return result;
    }

    public static String fillLeft(String s, String fill, int length) {
        String result = s;
        for (int i = result.length(); i < length; i++) {
            result = fill + result;
        }
        return result;
    }

    public static String fillCenter(String s, String fill, int length) {
        return fillLeft(fillRight(s, fill, s.length() + (length - s.length()) / 2), fill, length);
    }

    static int calculateBar(long value, int maxInput, int maxOutput) {
        return maxOutput - (int) (Math.log10(maxInput - value + 1) * (maxOutput / Math.log10(maxInput)));
    }

    public static String getBarString(long value, int max, int length, String fill) {
        return fillLeft("" + (max - value), " ", 3) + " " + fillLeft("", fill, calculateBar(value, max, length));
    }

    public static String progessBar(int max, int percentValue) {
        return fillRight("\r|" + fillLeft("", "#", percentValue * max / 100), " ", max) + "|  " + percentValue + "%";
    }

    public static String createFrame(String ... content) {
        int width = 0;
        for(String s : content) {
            if (width < s.length()) {
                width = s.length();
            }
        }
        width = width + 4;
        String result = "|" + fillLeft("","-",width) + "|\n\r";
        for(String s : content) {
            result += "|" + fillRight(" " + s + " ", " ", width) + "|\n\r";
        }
        result += "|" + fillRight("","-",width) + "|\n\r";
        return result;
    }

}
