package org.hou.stocktank.utils.evaluation;

import java.util.HashMap;
import java.util.Map;

import org.hou.stocktank.base.StrategyFactory;
import org.hou.stocktank.base.json.deserialization.StrategyFactoryDeserializer;
import org.hou.stocktank.utils.MBankProfitCriterion;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.ta4j.core.*;
import org.ta4j.core.analysis.criteria.AverageProfitCriterion;
import org.ta4j.core.analysis.criteria.AverageProfitableTradesCriterion;
import org.ta4j.core.analysis.criteria.BuyAndHoldCriterion;
import org.ta4j.core.analysis.criteria.LinearTransactionCostCriterion;
import org.ta4j.core.analysis.criteria.MaximumDrawdownCriterion;
import org.ta4j.core.analysis.criteria.RewardRiskRatioCriterion;
import org.ta4j.core.analysis.criteria.TotalProfitCriterion;
import org.ta4j.core.analysis.criteria.VersusBuyAndHoldCriterion;

public class StrategyTester {
    public static final int MBANK_COST_AMOUNT = 1000; // ile inwestujemy
    public static final double MBANK_COST_PARAM = 0.0039; // procent od operacji

    public Map<String, String> testStrategy(String stockName, TimeSeries series, int unstablePeriod, StrategyFactory strategyFactory) {
        Map<String, String> result = new HashMap<>();
        result.put(StrategyTestingResultFields.STOCK_NAME.toString(), stockName);
        result.put(StrategyTestingResultFields.DATE_FROM_EPOCH.toString(), String.valueOf(series.getFirstTick().getBeginTime().toEpochSecond()));
        result.put(StrategyTestingResultFields.DATE_TO_EPOCH.toString(), String.valueOf(series.getLastTick().getBeginTime().toEpochSecond()));
        result.put(StrategyTestingResultFields.DATE_FROM.toString(), series.getFirstTick().getBeginTime().toString());
        result.put(StrategyTestingResultFields.DATE_TO.toString(), series.getLastTick().getBeginTime().toString());
        result.put(StrategyTestingResultFields.TICKS.toString(), String.valueOf(series.getTickCount()));
        result.put(StrategyTestingResultFields.UNSTABLE_PERIOD.toString(), String.valueOf(unstablePeriod));
        //--- StrategyFactory
        result.put(StrategyTestingResultFields.STRATEGY_FACTORY_NAME.toString(), strategyFactory.getName());
        result.put(StrategyTestingResultFields.STRATEGY_FACTORY_CLASS.toString(), strategyFactory.getClass().getName());
        GsonBuilder builder = new GsonBuilder();
        builder.registerTypeAdapter(StrategyFactory.class, new StrategyFactoryDeserializer());
        Gson gson = builder.create();
        String json = gson.toJson(strategyFactory);
        result.put(StrategyTestingResultFields.STRATEGY_FACTORY_JSON.toString(), json);
        //--- Trading
        Strategy strategy = strategyFactory.buildStrategy(series);
        strategy.setUnstablePeriod(unstablePeriod);
        TimeSeriesManager manager = new TimeSeriesManager(series);
        TradingRecord tradingRecord = manager.run(strategy);
        // --- results
        result.put(StrategyTestingResultFields.TRADES.toString(), String.valueOf(tradingRecord.getTrades().size()));
        result.put(StrategyTestingResultFields.AVERAGE_PROFIT.toString(), String.valueOf(new AverageProfitCriterion().calculate(series, tradingRecord)));
        result.put(StrategyTestingResultFields.PROFITABLE_TRADES_RATIO.toString(), String.valueOf(new AverageProfitableTradesCriterion().calculate(series, tradingRecord)));
        result.put(StrategyTestingResultFields.MAX_DRAWDOWN.toString(), String.valueOf(new MaximumDrawdownCriterion().calculate(series, tradingRecord)));
        result.put(StrategyTestingResultFields.REWARD_RISK_RATIO.toString(), String.valueOf(new RewardRiskRatioCriterion().calculate(series, tradingRecord)));
        result.put(StrategyTestingResultFields.MBANK_COSTS.toString(), String.valueOf(new LinearTransactionCostCriterion(MBANK_COST_AMOUNT, MBANK_COST_PARAM).calculate(series, tradingRecord)));
        result.put(StrategyTestingResultFields.BUY_HOLD_PROFIT.toString(), String.valueOf(new BuyAndHoldCriterion().calculate(series, tradingRecord)));
        result.put(StrategyTestingResultFields.TOTAL_PROFIT.toString(), String.valueOf(new TotalProfitCriterion().calculate(series, tradingRecord)));
        result.put(StrategyTestingResultFields.MBANK_PROFIT.toString(), String.valueOf(new MBankProfitCriterion().calculate(series, tradingRecord)));
        result.put(StrategyTestingResultFields.STRATEGY_VS_BUY_AND_HOLD.toString(), String.valueOf(new VersusBuyAndHoldCriterion(new TotalProfitCriterion()).calculate(series, tradingRecord)));
        return result;
    }

}
