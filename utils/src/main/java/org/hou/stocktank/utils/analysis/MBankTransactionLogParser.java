package org.hou.stocktank.utils.analysis;


import au.com.bytecode.opencsv.CSVReader;
import org.joda.time.DateTime;
import org.ta4j.core.Decimal;

import java.io.*;
import java.nio.charset.Charset;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.BiFunction;

public class MBankTransactionLogParser {
    public static final SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("dd.MM.yyyy kk:mm:ss");
    private static final Charset DATAFILE_CHARSET = Charset.forName("ISO-8859-1");
    private static final char DATAFILE_SEPARATOR = ';';
    private static final char DATAFILE_QUOTECHAT = '"';
    private static final Map<String, String> STOCK_NAME_REPLACE_MAP = new HashMap<>();
    private final BiFunction<Integer, Decimal, Decimal> feeCalculation;

    static {
        STOCK_NAME_REPLACE_MAP.put("NEWAG1", "NEWAG");
    }

    public MBankTransactionLogParser(BiFunction<Integer, Decimal, Decimal> feeCalculation) {
        this.feeCalculation = feeCalculation;
    }

    private Operation parseLine(String[] line) throws ParseException {
        // line format :: #Data transakcji;#Nazwa papieru;#Rodzaj transakcji;#Liczba;#Kurs;#Wartość transakcji;
        DateTime operationDate = new DateTime(DATE_FORMAT.parse(line[0]));
        String stockName = (STOCK_NAME_REPLACE_MAP.containsKey(line[1])) ? STOCK_NAME_REPLACE_MAP.get(line[1]) : line[1];
        OperationType operationType = (line[3].equals("K")) ? OperationType.BUY : OperationType.SELL;
        int quantity = Integer.valueOf(line[4].replace(" ", ""));
        Decimal price = Decimal.valueOf(line[5].replace(" ", "").replace(",", "."));
        return new Operation(operationDate, operationType, stockName, quantity, price, feeCalculation);
    }

    private InputStream load(String filename) throws IOException, ParseException {
        File file = new File(filename);
        if (!file.exists()) {
            throw new RuntimeException("file :" + filename + "  does not exist.");
        }
        return new FileInputStream(file);
    }

    private int findFirstLineStartWith(InputStream inputStream, String startWith) throws IOException {
        // find data start line
        BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream, Charset.forName("UTF-8")));
        int counter = 0;
        while (true) {
            String line = reader.readLine();
            if (line == null) {
                return -1;
            }
            if (line.startsWith(startWith)) {
                return counter;
            }
            counter++;
        }
    }


    public List<Operation> parse(File file) {
        try {
            InputStream inputStream = new FileInputStream(file);
            int startLine = findFirstLineStartWith(inputStream, "Czas transakcji;Walor;");
            CSVReader reader = new CSVReader(new InputStreamReader(new FileInputStream(file), DATAFILE_CHARSET), DATAFILE_SEPARATOR, DATAFILE_QUOTECHAT, startLine + 1);
            String[] line;
            List<Operation> result = new ArrayList<>();
            while ((line = reader.readNext()) != null) {
                result.add(parseLine(line));
            }
            return result;
        } catch (IOException | ParseException ioe) {
            throw new RuntimeException(ioe);
        }
    }

}
