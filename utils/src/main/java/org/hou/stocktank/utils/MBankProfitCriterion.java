package org.hou.stocktank.utils;

import org.ta4j.core.Decimal;
import org.ta4j.core.TimeSeries;
import org.ta4j.core.Trade;
import org.ta4j.core.TradingRecord;
import org.ta4j.core.analysis.criteria.AbstractAnalysisCriterion;

/**
 * Created by antoni.kozielewski on 2016-08-25.
 */
public class MBankProfitCriterion extends AbstractAnalysisCriterion {
    private Decimal percentFee = Decimal.valueOf(0.0039);

    public MBankProfitCriterion() {

    }

    public MBankProfitCriterion(Decimal percentFee) {
        this.percentFee = percentFee;
    }

    @Override
    public double calculate(TimeSeries series, TradingRecord tradingRecord) {
        double value = 1d;
        for (Trade trade : tradingRecord.getTrades()) {
            value *= calculateProfit(series, trade);
        }
        return value;
    }

    @Override
    public double calculate(TimeSeries series, Trade trade) {
        return calculateProfit(series, trade);
    }

    @Override
    public boolean betterThan(double criterionValue1, double criterionValue2) {
        return criterionValue1 > criterionValue2;
    }

    private double calculateProfit(TimeSeries series, Trade trade) {
        Decimal profit = Decimal.ONE;
        if (trade.isClosed()) {
            Decimal exitClosePrice = series.getTick(trade.getExit().getIndex()).getClosePrice();
            Decimal entryClosePrice = series.getTick(trade.getEntry().getIndex()).getClosePrice();

            if (trade.getEntry().isBuy()) {
                profit = exitClosePrice.dividedBy(entryClosePrice).minus(percentFee);
            } else {
                profit = entryClosePrice.dividedBy(exitClosePrice).minus(percentFee);
            }
        }
        return profit.toDouble();
    }


}
