package org.hou.stocktank.utils;

import au.com.bytecode.opencsv.CSVReader;
import org.ta4j.core.*;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.time.Duration;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.chrono.ChronoZonedDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class BossaPRNLoader {

    private static ZonedDateTime parseDateTime(String dateField, String timeField) {
        Integer year = Integer.valueOf(dateField.substring(0, 4));
        Integer month = Integer.valueOf(dateField.substring(4, 6));
        Integer day = Integer.valueOf(dateField.substring(6, 8));
        Integer hour = Integer.valueOf(timeField.substring(0, 2));
        Integer min = Integer.valueOf(timeField.substring(2, 4));
        Integer sec = Integer.valueOf(timeField.substring(4, 6));
        return ZonedDateTime.of(year, month, day,hour, min, sec,0, ZoneId.systemDefault());
    }

    private static Tick parseLine(String[] line, Tick lastTick) {
        ZonedDateTime lastTickDate;
        ZonedDateTime date = parseDateTime(line[2], line[3]).withHour(9).withMinute(0);
        if (lastTick == null) {
            lastTickDate = date.withHour(9).withMinute(0);
        } else {
            lastTickDate = lastTick.getEndTime();
        }
        Duration period = Duration.ofHours(8);
        Decimal open = Decimal.valueOf(line[4]);
        Decimal high = Decimal.valueOf(line[5]);
        Decimal low = Decimal.valueOf(line[6]);
        Decimal close = Decimal.valueOf(line[7]);
        Decimal volume = Decimal.valueOf(line[8]);
        return new BaseTick(period, date, open, high, low, close, volume);
    }

    public static TimeSeries load(String name, String filename, ChronoZonedDateTime from) {
        List<Tick> ticks = new ArrayList<Tick>();
        try {
            InputStream stream = new FileInputStream(filename);
            CSVReader csvReader = new CSVReader(new InputStreamReader(stream, Charset.forName("UTF-8")), ',', '"', 0);
            String[] line;
            Tick lastTick = null;
            while ((line = csvReader.readNext()) != null) {
                lastTick = parseLine(line, lastTick);
                if ((from != null)&&(lastTick.getBeginTime().isAfter(from))) {
                    ticks.add(lastTick);
                } else {
                    if (from == null) ticks.add(lastTick);
                }
            }
        } catch (IOException ioe) {
            Logger.getLogger(CSVReader.class.getName()).log(Level.SEVERE, "Unable to load ticks from CSV", ioe);
        } catch (NumberFormatException nfe) {
            Logger.getLogger(CSVReader.class.getName()).log(Level.SEVERE, "Error while parsing value", nfe);
        }
        return new BaseTimeSeries(name, ticks);
    }

    public static TimeSeries load(String name, String filename) {
        return load(name, filename, null);
    }

}
