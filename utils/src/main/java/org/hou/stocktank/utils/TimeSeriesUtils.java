package org.hou.stocktank.utils;

import org.ta4j.core.*;

import java.time.Duration;
import java.time.ZonedDateTime;
import java.time.chrono.ChronoZonedDateTime;
import java.util.ArrayList;
import java.util.List;


public class TimeSeriesUtils {

    public static TimeSeries getSubseries(ChronoZonedDateTime from, TimeSeries series) {
        return getSubseries(from, ZonedDateTime.now(), series);
    }

    public static TimeSeries getSubseries(ChronoZonedDateTime from, ChronoZonedDateTime to, TimeSeries timeSeries) {
        List<Tick> ticks = new ArrayList<>();
        for (int i = 0; i < timeSeries.getTickCount(); i++) {
            Tick tick = timeSeries.getTick(i);
            if (    (tick.getBeginTime().isAfter(from) || tick.getBeginTime().isEqual(from))
                    && (tick.getBeginTime().isBefore(to) || tick.getBeginTime().isEqual(to))
                    && (tick.getEndTime().isAfter(from) || tick.getEndTime().isEqual(from))
                    && (tick.getEndTime().isBefore(to)  || tick.getEndTime().isEqual(to))
                    ) {
                ticks.add(tick);
            }
        }
        return new BaseTimeSeries(timeSeries.getName(), ticks);
    }

    public static TimeSeries aggregateToDays(TimeSeries timeseries) {
        List<Tick> ticks = new ArrayList<Tick>();
        ZonedDateTime fromDate = timeseries.getFirstTick().getBeginTime().withNano(0);
        ZonedDateTime toDate = timeseries.getFirstTick().getEndTime().withNano(0);
        Decimal open = timeseries.getTick(0).getOpenPrice();
        Decimal high = Decimal.valueOf(0);
        Decimal low = Decimal.valueOf(Double.MAX_VALUE);
        Decimal close = Decimal.valueOf(0);
        Decimal volume = Decimal.valueOf(0);
        for (int i = 0; i < timeseries.getTickCount(); i++) {
            Tick tick = timeseries.getTick(i);
            if (tick.getBeginTime().withNano(0).isAfter(fromDate)) {
                ticks.add(new BaseTick(Duration.between(fromDate.withHour(9), fromDate.withHour(17)), fromDate.withHour(17), open, high, low, close, volume));
                fromDate = tick.getBeginTime().withNano(0);
                toDate = tick.getEndTime().withNano(0);
                open = tick.getOpenPrice();
                high = Decimal.valueOf(0);
                low = Decimal.valueOf(Double.MAX_VALUE);
                close = Decimal.valueOf(0);
                volume = Decimal.valueOf(0);
            }
            if (high.isLessThan(tick.getMaxPrice())) {
                high = tick.getMaxPrice();
            }
            if (low.isGreaterThan(tick.getMinPrice())) {
                low = tick.getMinPrice();
            }
            close = tick.getClosePrice();
            volume = volume.plus(tick.getVolume());
        }
        // last tick
        ticks.add(new BaseTick(Duration.between(fromDate.withHour(9), fromDate.withHour(17)), fromDate.withHour(17), open, high, low, close, volume));
        return new BaseTimeSeries(timeseries.getName(), ticks);
    }


    public static void print(TimeSeries timeseries) {
        System.out.println(" " + timeseries.getName());
        System.out.println("     " + timeseries.getTickCount() + "  ticks");
        System.out.println("---------------------------------------------------");
        System.out.println(" begin | end | open | high | low | close | volume |");
        for (int i = 0; i < timeseries.getTickCount(); i++) {
            Tick tick = timeseries.getTick(i);
            System.out.println(tick.getBeginTime()
                    + " | " + tick.getEndTime()
                    + " | " + tick.getOpenPrice()
                    + " | " + tick.getMaxPrice()
                    + " | " + tick.getMinPrice()
                    + " | " + tick.getClosePrice()
                    + " | " + tick.getVolume() + " |"
            );
        }
    }
}
