package org.hou.stocktank.utils.analysis;

import org.joda.time.DateTime;
import org.ta4j.core.Decimal;

import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

public class AnalysisResult {
    protected List<LogItem> logItems;
    protected Map<String, List<Operation>> wallet;
    protected Function<Decimal, Decimal> taxCalculation;

    public AnalysisResult(List<LogItem> logItems, Map<String, List<Operation>> wallet, Function<Decimal, Decimal> taxCalculation) {
        this.logItems = logItems;
        this.wallet = wallet;
        this.taxCalculation = taxCalculation;
    }

    public List<LogItem> getLogItems() {
        return logItems;
    }

    public Map<String, List<Operation>> getWallet() {
        return wallet;
    }

    public Decimal getTotalIncome() {
        return getLogItems().stream().map(x -> x.getIncome()).reduce(Decimal.ZERO, Decimal::plus);
    }

    public Decimal getTotalFees() {
        return getLogItems().stream().map(x -> x.buyFee.plus(x.sellFee)).reduce(Decimal.ZERO, Decimal::plus);
    }

    public Decimal getTotalProfit() {
        return getLogItems().stream().map(x -> x.getIncome().minus(x.buyFee.plus(x.sellFee))).reduce(Decimal.ZERO, Decimal::plus);
    }

    public Decimal getTax() {
        return taxCalculation.apply(getTotalProfit());
    }

    public void filterByStock(String stockName) {
        logItems = logItems.stream().filter(x -> x.stock == stockName).collect(Collectors.toList());
        for (String stock : wallet.keySet()) {
            if (stock != stockName) wallet.remove(stock);
        }
    }

    public void filterLogItemsBySellDate(DateTime dateFrom, DateTime dateTo) {
        logItems = logItems.stream().filter(x -> x.sellDate.isAfter(dateFrom.getMillis()) && x.sellDate.isBefore(dateTo)).collect(Collectors.toList());
    }
}