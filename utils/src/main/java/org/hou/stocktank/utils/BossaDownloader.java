package org.hou.stocktank.utils;


import org.slf4j.LoggerFactory;

import java.io.*;
import java.net.URL;
import java.nio.channels.Channels;
import java.nio.channels.FileChannel;
import java.nio.channels.ReadableByteChannel;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

public class BossaDownloader {
    private static final org.slf4j.Logger logger = LoggerFactory.getLogger(BossaDownloader.class);
    private String basePath = "http://bossa.pl/pub/intraday/mstock/cgl/";
    private String targetPath = "data/_IMPORT";
    private String fileSeparator = "/";
    private Boolean cleanTargetDirBeforeDownload = true;
    private int downloadTimeout = 60000;
    private int threadsNumber = 10;

    public BossaDownloader(){
        super();
    }

    public BossaDownloader(String basePath, String targetPath){
        this.basePath = basePath;
        this.targetPath = targetPath;
    }

    private class Downloader implements Runnable {
        private String url;
        private String target;
        byte[] buffer = new byte[1024];

        public Downloader(String url, String target) {
            this.url = url;
            this.target = target;
        }

        @Override
        public void run() {
            download();
        }

        private void download(){
            ReadableByteChannel in= null;
            try {
                logger.debug("starting : {}", url);
                in = Channels.newChannel(new URL(url).openStream());
                FileChannel out = new FileOutputStream(target).getChannel();
                out.transferFrom(in, 0, Long.MAX_VALUE);
                out.close();
                logger.debug("download finished : {}", url);
                unzip(target);
                logger.debug("unzip done : {}", url);
                removeFile(target);
                logger.info("done : {} " + url);
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        }

        private void removeFile(String file) {
            File f = new File(file);
            f.delete();
        }

        private void unzip(String file) throws IOException {
            ZipInputStream zip = new ZipInputStream(new FileInputStream(target));
            ZipEntry ze = zip.getNextEntry();
            while(ze!=null) {
                String filename = ze.getName();
                File newFile = new File(targetPath + fileSeparator + filename);
                new File(newFile.getParent()).mkdirs(); // dodajemy wszystkie katalogi
                FileOutputStream fos = new FileOutputStream(newFile);
                int len;
                while ((len = zip.read(buffer)) > 0) {
                    fos.write(buffer, 0, len);
                }
                fos.close();
                ze = zip.getNextEntry();
            }
            zip.closeEntry();
            zip.close();
        }

    }


    private File getTargetDir() {
        return new File(targetPath);
    }

    private String getDownloadURL(String stockName) {
        return basePath + stockName + ".zip";
    }

    private String getTargetWithPath(String stockName) {
        return targetPath + fileSeparator + stockName + ".zip";
    }

    private void cleanTargetDir() {
        for (File file : getTargetDir().listFiles())
            if (!file.isDirectory())
                file.delete();
    }

    public void downloadStocks(List<String> stockNames) {
        if (cleanTargetDirBeforeDownload) {
            cleanTargetDir();
        }
        try {
            ExecutorService pool = Executors.newFixedThreadPool(threadsNumber);
            for (String stockName : stockNames) {
                logger.info("downloading: {}", stockName);
                pool.submit(new Downloader(getDownloadURL(stockName), getTargetWithPath(stockName)));
            }
            pool.shutdown();
            pool.awaitTermination(downloadTimeout, TimeUnit.MILLISECONDS);
        } catch (InterruptedException e) {
            throw new RuntimeException("Download timeout - it takes to long");
        }
    }

    public void setBasePath(String basePath) {
        this.basePath = basePath;
    }

    public void setTargetPath(String targetPath) {
        this.targetPath = targetPath;
    }

    public void setFileSeparator(String fileSeparator) {
        this.fileSeparator = fileSeparator;
    }

    public void setCleanTargetDirBeforeDownload(Boolean cleanTargetDirBeforeDownload) {
        this.cleanTargetDirBeforeDownload = cleanTargetDirBeforeDownload;
    }

    public void setDownloadTimeout(int downloadTimeout) {
        this.downloadTimeout = downloadTimeout;
    }

    public void setThreadsNumber(int threadsNumber) {
        this.threadsNumber = threadsNumber;
    }
}
