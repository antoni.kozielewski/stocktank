package org.hou.stocktank.utils.analysis;

import org.joda.time.DateTime;
import org.ta4j.core.Decimal;

import java.util.function.BiFunction;

class Operation {
    private DateTime date;
    private OperationType type;
    private String stock;
    private int quantity;
    private Decimal price;
    private Decimal fee;
    private BiFunction<Integer, Decimal, Decimal> calculateFee;

    public Operation(Operation src) {
        this(src.date, src.type, src.stock, src.quantity, src.price, src.calculateFee);
    }

    public Operation(DateTime date, OperationType type, String stock, int quantity, Decimal price, BiFunction<Integer, Decimal, Decimal> calculateFee) {
        this.date = date;
        this.type = type;
        this.stock = stock;
        this.quantity = quantity;
        this.price = price;
        this.calculateFee = calculateFee;
        this.fee = this.calculateFee.apply(quantity, price);
    }

    public Decimal getFeeForQuantity(int newQuantity) {
        return (this.fee.multipliedBy(Decimal.valueOf(newQuantity))).dividedBy(Decimal.valueOf(quantity));
    }

    public void recalculateForQuantity(int newQuantity) {
        this.fee = getFeeForQuantity(newQuantity);
        this.quantity = newQuantity;
    }

    public int compareTo(Operation o) {
        return date.compareTo(o.date);
    }

//    private Decimal calculateFee() {
//        if (price.multipliedBy(Decimal.valueOf(quantity)).multipliedBy(MBANK_FEE).isLessThan(MBANK_MIN_FEE)) {
//            return MBANK_MIN_FEE;
//        }
//        return price.multipliedBy(Decimal.valueOf(quantity)).multipliedBy(MBANK_FEE);
//    }

    public DateTime getDate() {
        return date;
    }

    public OperationType getType() {
        return type;
    }

    public String getStock() {
        return stock;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public int getQuantity() {
        return quantity;
    }

    public Decimal getPrice() {
        return price;
    }

    public Decimal getFee() {
        return fee;
    }

    public String toString() {
        return date.toString() + " " + type.toString() + " " + stock + " " + quantity + " " + price + " :: [" + getFee() + "]";
    }
}