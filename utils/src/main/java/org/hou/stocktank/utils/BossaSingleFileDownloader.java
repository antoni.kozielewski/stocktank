package org.hou.stocktank.utils;


import org.slf4j.LoggerFactory;

import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URL;
import java.nio.channels.Channels;
import java.nio.channels.FileChannel;
import java.nio.channels.ReadableByteChannel;

public class BossaSingleFileDownloader {
    private static final org.slf4j.Logger logger = LoggerFactory.getLogger(BossaSingleFileDownloader.class);
    private static String url = "http://bossa.pl/pub/metastock/mstock/mstall.zip";
    private static String target = "data/mstall.zip";

    public BossaSingleFileDownloader(){
        super();
    }

    public BossaSingleFileDownloader(String url, String target){
        this.url = url;
        this.target = target;
    }

    public void download(){
        ReadableByteChannel in= null;
        try {
            logger.info("downloading : {}", url);
            in = Channels.newChannel(new URL(url).openStream());
            FileChannel out = new FileOutputStream(target).getChannel();
            out.transferFrom(in, 0, Long.MAX_VALUE);
            out.close();
            logger.info("download finished : {}", url);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public String getTarget() {
        return target;
    }

}
