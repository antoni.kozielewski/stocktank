package org.hou.stocktank.utils.evaluation;

import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

public enum StrategyTestingResultFields {
    STRATEGY_FACTORY_JSON("StrategyFactory json",15),
    STRATEGY_FACTORY_CLASS("StrategyFactory class",14),
    STRATEGY_FACTORY_NAME("StrategyFactory name",1),
    STOCK_NAME("Stock name",0.5),
    DATE_FROM_EPOCH("from epoch",100),
    DATE_TO_EPOCH("to epoch",101),
    DATE_FROM("Date from",1),
    DATE_TO("Date to",2),
    TICKS("Number of ticks",3),
    UNSTABLE_PERIOD("Unstable period",8),
    TRADES("Number of trades",9),
    AVERAGE_PROFIT("Average profit",10),
    PROFITABLE_TRADES_RATIO("Profitable trades ratio",11),
    MAX_DRAWDOWN("Maximum drawdown",12),
    REWARD_RISK_RATIO("Reward risk ratio",13),
    MBANK_COSTS("MBank costs",5.5),
    BUY_HOLD_PROFIT("Buy and Hold profit",6),
    TOTAL_PROFIT("Total profit",4),
    MBANK_PROFIT("MBank profit",5),
    STRATEGY_VS_BUY_AND_HOLD("Strategy vs BuyAndHold",7);

    private String name;
    private double defaultOrder;

    private StrategyTestingResultFields(String name, double order) {
        this.name = name;
        this.defaultOrder = order;
    }

    public String getName() {
        return name;
    }

    public double getDefaultOrder() {
        return defaultOrder;
    }

    public static final List<StrategyTestingResultFields> getOrderedValues(){
        List<StrategyTestingResultFields> result = Arrays.asList(StrategyTestingResultFields.values());
        result.sort(new Comparator<StrategyTestingResultFields>() {
            @Override
            public int compare(StrategyTestingResultFields o1, StrategyTestingResultFields o2) {
                return o1.defaultOrder < o2.defaultOrder ? -1 : o1.defaultOrder == o2.defaultOrder ? 0 : 1;
            }
        });
        return result;
    }
}
