package ichimoku;

import org.hou.stocktank.analysis.AnalysisResult;
import org.hou.stocktank.analysis.MarketState;
import org.hou.stocktank.analysis.analyzers.IchimokuAnalyzerIndicator;
import org.hou.stocktank.services.BossaSingleFileDataProviderService;
import org.ta4j.core.Indicator;
import org.ta4j.core.TimeSeries;

public class IchimokuAnalyzerTest {
    private BossaSingleFileDataProviderService dataProviderService;
    private static final String stockName = "wielton";

    private void test(String name) {
        System.out.println("TEST: " + name);
        dataProviderService = new BossaSingleFileDataProviderService();
        dataProviderService.setDataPath("data/mstall.zip");
        TimeSeries series = series = dataProviderService.load(stockName);
        Indicator<AnalysisResult> analyzer = new IchimokuAnalyzerIndicator(series);
        for (int i = 0; i < series.getTickCount(); i++) {
            AnalysisResult result = analyzer.getValue(i);
            if (result.getMarketState() != MarketState.UNKNOWN) {
                System.out.println(series.getTick(i).getBeginTime() + " | " + result.getMarketState() + " | " + result.getDescription());
            }
        }
        System.out.println("------------------------------------------------------------------------------");
    }

    public static void main(String[] args) {
        System.out.println("-------- START --------");
        IchimokuAnalyzerTest tester = new IchimokuAnalyzerTest();
        tester.test(stockName);
        System.out.println("------  FINISHED  -----");
    }
}
