package ichimoku;

import org.hou.stocktank.analysis.analyzers.IchimokuAnalyzerIndicator;
import org.hou.stocktank.charts.googlecharts.IchimokuHmtlChartGenerator;
import org.hou.stocktank.services.BossaSingleFileDataProviderService;
import org.hou.stocktank.utils.StocktankTXTUtil;
import org.hou.stocktank.utils.TimeSeriesUtils;
import org.ta4j.core.TimeSeries;

import java.io.IOException;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.Arrays;
import java.util.List;

public class IchimokuGenerator {
    private BossaSingleFileDataProviderService dataProviderService;
    private static List<String> stocks = Arrays.asList("ciech");
    private static final int size = 0;

    private void run(String stockName) throws IOException {
        dataProviderService = new BossaSingleFileDataProviderService();
        dataProviderService.setDataPath("data/mstall.zip");
        TimeSeries series = series = dataProviderService.load(stockName);
        series = TimeSeriesUtils.aggregateToDays(series);
        series = TimeSeriesUtils.getSubseries(ZonedDateTime.of(2013,01,01,0,0,0,0, ZoneId.systemDefault()),series);
        IchimokuAnalyzerIndicator indicator = new IchimokuAnalyzerIndicator(series);
        IchimokuHmtlChartGenerator generator = new IchimokuHmtlChartGenerator(indicator, series);
        if (size == 0) {
            generator.generateChart(stockName, series.getTickCount()-60);
        } else {
            if (series.getTickCount() > size) {
                generator.generateChart(stockName, size);
            } else {
                generator.generateChart(stockName, series.getTickCount() - 1);
            }
        }
    }

    private static void printFrame(String text) {
        System.out.println(StocktankTXTUtil.fillCenter("","-",100));
        System.out.println("|" + StocktankTXTUtil.fillCenter(text," ",98) + "|");
        System.out.println(StocktankTXTUtil.fillCenter("","-",100));
    }

    public static void main(String[] args) throws IOException {
        printFrame("Ichimoku chart generator");
        IchimokuGenerator instance = new IchimokuGenerator();
        for (String stock : stocks) {
            instance.run(stock);
            System.out.print(".");
        }
        System.out.println("");
        printFrame("DONE !!");
    }
}
