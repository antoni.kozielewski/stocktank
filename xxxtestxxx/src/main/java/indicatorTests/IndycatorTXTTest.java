package indicatorTests;

import org.hou.stocktank.indicators.HurstExponentIndicator;
import org.hou.stocktank.services.BossaSingleFileDataProviderService;
import org.hou.stocktank.utils.BossaDownloader;
import org.hou.stocktank.utils.BossaSingleFileDownloader;
import org.hou.stocktank.utils.TimeSeriesUtils;
import org.joda.time.DateTime;
import org.ta4j.core.Decimal;
import org.ta4j.core.Indicator;
import org.ta4j.core.TimeSeries;

import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.List;

public class IndycatorTXTTest {
    public static void main(String[] args) {
        System.out.println("-- -- START -- --");
        List<String> stocks = new ArrayList<>();
        stocks.add("CIECH");

        BossaDownloader bossaDownloader = new BossaDownloader();
        bossaDownloader.downloadStocks(stocks);

        BossaSingleFileDownloader bossaSingleFileDownloader = new BossaSingleFileDownloader();
        bossaSingleFileDownloader.download();
        BossaSingleFileDataProviderService dataProviderService = new BossaSingleFileDataProviderService();
        dataProviderService.setDataPath("data/mstall.zip");

        for (String stock : stocks) {
            System.out.println(" :: " + stock + " :: ");
            TimeSeries data = dataProviderService.load(stock);
            data = TimeSeriesUtils.getSubseries(ZonedDateTime.of(2010, 01, 01, 0, 0, 0, 0, ZoneId.systemDefault()), data);

            // -----------------------------------------------------------------------------------
            // ---- INDICATOR --------------------------------------------------------------------
            // -----------------------------------------------------------------------------------
            Indicator<Decimal> indicator = new HurstExponentIndicator(data);
            // -----------------------------------------------------------------------------------
            // -----------------------------------------------------------------------------------
            // -----------------------------------------------------------------------------------

            for (int i = data.getTickCount()-100; i < data.getTickCount(); i++) {
                System.out.println("[" + i + "]  " + data.getTick(i).getBeginTime() +"  -   " + indicator.getValue(i));
            }
        }
    }
}

