package indicatorTests;

import org.hou.stocktank.charts.MagicChart;
import org.hou.stocktank.charts.MagicDataPlot;
import org.hou.stocktank.charts.MagicLayer;
import org.hou.stocktank.charts.MagicRendererType;
import org.hou.stocktank.indicators.TimeShiftIndicator;
import org.hou.stocktank.utils.BossaPRNLoader;
import org.hou.stocktank.utils.TimeSeriesUtils;
import org.ta4j.core.Decimal;
import org.ta4j.core.Indicator;
import org.ta4j.core.TimeSeries;
import org.ta4j.core.indicators.helpers.ClosePriceIndicator;
import org.ta4j.core.indicators.helpers.OpenPriceIndicator;
import org.ta4j.core.indicators.ichimoku.*;

import java.awt.*;

public class IndicatorWithPricesTest {
    private static final String stockName = "pelion";

    public static void main(String[] args) {
        System.out.println("---------------START");
        TimeSeries series = BossaPRNLoader.load(stockName, "data/_IMPORT/" + stockName + ".prn");
        series = TimeSeriesUtils.aggregateToDays(series);
        //series = TimeSeriesUtils.getSubseries(new DateTime(2015,01,01,00,00),series);


        //Indicator indicator = new EMAIndicator(new StochasticOscillatorDIndicator(new EMAIndicator(new StochasticOscillatorKIndicator(series, 15),3)),3);
//        Indicator indicator = new RSIIndicator(new ClosePriceIndicator(series),20);
        //Indicator indicator = new ParabolicSARIndicator(series);
        //Indicator indicator = new HurstExponentIndicator(series);
// ichimoku
        Indicator<Decimal> chikouSpan = new IchimokuChikouSpanIndicator(series);
        Indicator<Decimal> kijunSen = new IchimokuKijunSenIndicator(series);
        Indicator<Decimal> tenkanSen = new IchimokuTenkanSenIndicator(series);
        Indicator<Decimal> senkouSpanA = new TimeShiftIndicator(series, new IchimokuSenkouSpanAIndicator(series), 26);
        Indicator<Decimal> senkouSpanB = new TimeShiftIndicator(series, new IchimokuSenkouSpanBIndicator(series), 26);

        //Indicator ema = new SMAIndicator(new ClosePriceIndicator(series), 14);
//        Indicator indicator = new StochasticOscillatorKIndicator(series, 15);
        for (int i = 0; i < series.getTickCount(); i++) {
            System.out.println(" " + series.getTick(i).getBeginTime() + " :: Price = O "
                    + series.getTick(i).getOpenPrice()
                    + ", H " + series.getTick(i).getMaxPrice()
                    + ", L " + series.getTick(i).getMinPrice()
                    + ", C " + series.getTick(i).getClosePrice()
            );
              //      + " :: " + indicator.getValue(i));
        }

        MagicDataPlot closePriceChart = new MagicDataPlot("close price", new ClosePriceIndicator(series), series, Color.BLACK, MagicRendererType.LINE);
        MagicDataPlot openPriceChart = new MagicDataPlot("begin price", new OpenPriceIndicator(series), series, Color.BLUE, MagicRendererType.LINE);

        MagicLayer layerPrice = new MagicLayer("Price");
        layerPrice.addDataPlot(new MagicDataPlot("price candles", series, Color.BLACK, MagicRendererType.CANDLESTICK));

        //layerPrice.addDataPlot(new MagicDataPlot("chikouSpan", chikouSpan, series, Color.BLUE, MagicRendererType.LINE));
        //layerPrice.addDataPlot(new MagicDataPlot("kijunSen", kijunSen, series, Color.RED, MagicRendererType.LINE));
        //layerPrice.addDataPlot(new MagicDataPlot("tenkanSen", tenkanSen, series, Color.PINK, MagicRendererType.LINE));
        layerPrice.addDataPlot(new MagicDataPlot("senkouSpanA", senkouSpanA, series, Color.BLUE, MagicRendererType.LINE));
        layerPrice.addDataPlot(new MagicDataPlot("senkouSpanB", senkouSpanB, series, Color.RED, MagicRendererType.LINE));




//        layerPrice.addDataPlot(new MagicDataPlot("indicator", indicator,series, Color.BLUE, MagicRendererType.LINE));
        //layerPrice.addDataPlot(new MagicDataPlot("ema", ema,series, Color.CYAN, MagicRendererType.LINE));

//        MagicLayer indicatorLayer = new MagicLayer("");
//        indicatorLayer.addDataPlot(new MagicDataPlot("indicator", indicator,series, Color.BLUE, MagicRendererType.LINE));
//        indicatorLayer.addHMarker(new MagicMarker(70, "sell", Color.black));
//        indicatorLayer.addHMarker(new MagicMarker(30, "buy", Color.black));

        MagicChart chart = new MagicChart();
        chart.withLayer(layerPrice);
        //chart.withLayer(indicatorLayer);
        chart.withWidht(1200);
        chart.showChart();

    }

}
