package indicatorTests;

import org.hou.stocktank.services.BossaSingleFileDataProviderService;
import org.hou.stocktank.utils.BossaPRNLoader;
import org.hou.stocktank.utils.BossaSingleFileDownloader;
import org.hou.stocktank.utils.TimeSeriesUtils;
import org.joda.time.DateTime;
import org.ta4j.core.Decimal;
import org.ta4j.core.Indicator;
import org.ta4j.core.TimeSeries;
import org.ta4j.core.indicators.MACDIndicator;
import org.ta4j.core.indicators.helpers.ClosePriceIndicator;

import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.List;

public class IndycatorComparator {
    public static void main(String[] args) {
        System.out.println("-- -- START -- --");
        List<String> stocks = new ArrayList<>();
        stocks.add("PROCHNIK");

//        BossaDownloader bossaDownloader = new BossaDownloader();
//        bossaDownloader.downloadStocks(stocks);

        BossaSingleFileDownloader bossaSingleFileDownloader = new BossaSingleFileDownloader();
        bossaSingleFileDownloader.download();
        BossaSingleFileDataProviderService dataProviderService = new BossaSingleFileDataProviderService();
        dataProviderService.setDataPath("data/mstall.zip");

        for (String stock : stocks) {
            System.out.println(" :: " + stock + " :: ");
            TimeSeries data1 = BossaPRNLoader.load(stock, "data/_IMPORT/" + stock + ".prn");
            data1 = TimeSeriesUtils.aggregateToDays(data1);
            data1 = TimeSeriesUtils.getSubseries(ZonedDateTime.of(2010, 01, 01, 0, 0, 0, 0, ZoneId.systemDefault()), data1);

            TimeSeries data2 = dataProviderService.load(stock);
            data2 = TimeSeriesUtils.getSubseries(ZonedDateTime.of(2010, 01, 01, 0, 0, 0, 0, ZoneId.systemDefault()), data2);

            Indicator<Decimal> cp1 = new ClosePriceIndicator(data1);
            Indicator<Decimal> cp2 = new ClosePriceIndicator(data2);
//            Indicator<Decimal> indicator1 = new RSIIndicator(cp1,15);
//            Indicator<Decimal> indicator2 = new RSIIndicator(cp2,15);
            Indicator<Decimal> indicator1 = new MACDIndicator(cp1, 10, 20);
            Indicator<Decimal> indicator2 = new MACDIndicator(cp2, 10, 20);

            for (int i = 0; i < data1.getTickCount(); i++) {
                System.out.println("[" + i + "]   -   " + indicator1.getValue(i) + " :: " + indicator2.getValue(i));
                System.out.println(" date1     " + data1.getTick(i).getBeginTime() + " --> " + data1.getTick(i).getEndTime());
                System.out.println(" date2     " + data2.getTick(i).getBeginTime() + " --> " + data2.getTick(i).getEndTime());
                System.out.println(" i        " + cp1.getValue(i) + " :: " + cp2.getValue(i));
                System.out.println(" i - 1    " + cp1.getValue(i - 1) + " :: " + cp2.getValue(i - 1));
                break;
            }
        }
    }
}

