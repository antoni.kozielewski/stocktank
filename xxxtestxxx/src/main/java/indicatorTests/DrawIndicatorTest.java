package indicatorTests;

import org.hou.stocktank.analysis.analyzers.AnalyzerToDecimalIndicatorWrapper;
import org.hou.stocktank.analysis.analyzers.advanced.BPF01Analyzer;
import org.hou.stocktank.base.IndicatorName;
import org.hou.stocktank.charts.MagicChart;
import org.hou.stocktank.charts.MagicDataPlot;
import org.hou.stocktank.charts.MagicLayer;
import org.hou.stocktank.charts.MagicRendererType;
import org.hou.stocktank.indicators.pivotpoints.PivotPointToDecimalIndicatorWrapper;
import org.hou.stocktank.indicators.pivotpoints.SimplePivotPointIndicator;
import org.hou.stocktank.services.BossaSingleFileDataProviderService;
import org.hou.stocktank.utils.TimeSeriesUtils;
import org.joda.time.DateTime;
import org.ta4j.core.Decimal;
import org.ta4j.core.Indicator;
import org.ta4j.core.TimeSeries;
import org.ta4j.core.indicators.SMAIndicator;
import org.ta4j.core.indicators.helpers.ClosePriceIndicator;
import org.ta4j.core.indicators.pivotpoints.*;

import java.awt.*;
import java.time.ZoneId;
import java.time.ZonedDateTime;

public class DrawIndicatorTest {
    private static final String stockName = "ciech";

    public static void main(String[] args) {
        System.out.println("---------------START");
        BossaSingleFileDataProviderService dataProviderService = new BossaSingleFileDataProviderService();
        dataProviderService.setDataPath("data/mstall.zip");
        TimeSeries series = dataProviderService.load(stockName);
        series = TimeSeriesUtils.getSubseries(ZonedDateTime.of(2010,10,01,0,0,0,0, ZoneId.systemDefault()),series);

//        Indicator indicator = new PivotPointToDecimalIndicatorWrapper(new SimplePivotPointIndicator(series));
        Indicator indicator = new SimplePivotPointIndicator(series);
        Indicator indicator2 = new SimplePivotPointIndicator(series,26,8,0.08,0.12);
//        Indicator indicator = new PivotPointToDecimalIndicatorWrapper(new SimplePivotPointIndicator(series));
//        Indicator indicator = new PivotPointIndicator(series, TimeLevel.WEEK);
//        Indicator indicator = new AnalyzerToDecimalIndicatorWrapper(new BPF01Analyzer(series));
//        Indicator indicator = new SMAIndicator(new ClosePriceIndicator(series),1);
//        Indicator indicator = new SMAIndicator(new ClosePriceIndicator(series),5);
//        Indicator angle = new LineAngleIndicator(ema16,1);


        MagicLayer indicatorLayer = new MagicLayer("");
        indicatorLayer.addDataPlot(new MagicDataPlot("indicator", indicator, series, Color.BLUE, MagicRendererType.LINE));
        indicatorLayer.addDataPlot(new MagicDataPlot("indicator", indicator2, series, Color.BLACK, MagicRendererType.LINE));
//        indicatorLayer.addDataPlot(new MagicDataPlot("ema4", ema4, series, Color.RED, MagicRendererType.LINE));
//        indicatorLayer.addDataPlot(new MagicDataPlot("ema16", ema16, series, Color.BLUE, MagicRendererType.LINE));
//        indicatorLayer.addDataPlot(new MagicDataPlot("angle", angle, series, Color.BLACK, MagicRendererType.LINE));

        MagicLayer priceLayer = new MagicLayer("Price");
        priceLayer.addDataPlot(new MagicDataPlot("price candles", series, Color.BLACK, MagicRendererType.CANDLESTICK));

        MagicChart chart = new MagicChart();
        chart.withLayer(priceLayer);
        chart.withLayer(indicatorLayer);
        chart.withWidht(1200);
        chart.showChart();

    }

}
