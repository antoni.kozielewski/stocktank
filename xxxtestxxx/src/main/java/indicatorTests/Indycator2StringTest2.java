package indicatorTests;

import org.hou.stocktank.indicators.demark.DeMarkPivotPointIndicator;
import org.hou.stocktank.indicators.demark.DeMarkReversalIndicator;
import org.hou.stocktank.indicators.pivotpoints.PivotPoint;
import org.hou.stocktank.indicators.pivotpoints.SimplePivotPointIndicator;
import org.hou.stocktank.services.BossaSingleFileDataProviderService;
import org.hou.stocktank.utils.TimeSeriesUtils;
import org.ta4j.core.Indicator;
import org.ta4j.core.TimeSeries;

import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;

public class Indycator2StringTest2 {
    public static void main(String[] args) {
        System.out.println("-- -- START -- --");
        String stock = "CIECH";

        BossaSingleFileDataProviderService dataProviderService = new BossaSingleFileDataProviderService();
        dataProviderService.setDataPath("data/mstall.zip");
        TimeSeries series = dataProviderService.load(stock);
        series = TimeSeriesUtils.getSubseries(ZonedDateTime.of(2013, 01, 01, 0, 0, 0, 0, ZoneId.systemDefault()), series);


        Indicator<PivotPoint> indicator = new SimplePivotPointIndicator(series, 10, 5, 0.06, 0.1);

        for (int i = 10; i < series.getEndIndex() - 4; i++) {
            if (i == indicator.getValue(i).getIndex()) {
                System.out.println(i + " :: " + indicator.getValue(i) + " --> " + series.getTick(i).getEndTime().format(DateTimeFormatter.ISO_DATE));
            }
        }


    }
}

