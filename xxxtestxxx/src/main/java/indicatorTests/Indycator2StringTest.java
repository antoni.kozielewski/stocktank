package indicatorTests;

import org.hou.stocktank.indicators.demark.DeMarkPivotPointIndicator;
import org.hou.stocktank.indicators.demark.DeMarkReversalIndicator;
import org.hou.stocktank.indicators.pivotpoints.SimplePivotPointIndicator;
import org.hou.stocktank.services.BossaSingleFileDataProviderService;
import org.hou.stocktank.utils.TimeSeriesUtils;
import org.ta4j.core.Indicator;
import org.ta4j.core.TimeSeries;

import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;

public class Indycator2StringTest {
    public static void main(String[] args) {
        System.out.println("-- -- START -- --");
        String stock = "CIECH";

        BossaSingleFileDataProviderService dataProviderService = new BossaSingleFileDataProviderService();
        dataProviderService.setDataPath("data/mstall.zip");
        TimeSeries series = dataProviderService.load(stock);
        series = TimeSeriesUtils.getSubseries(ZonedDateTime.of(2013, 01, 01, 0, 0, 0, 0, ZoneId.systemDefault()), series);

        Indicator indicator = new DeMarkReversalIndicator(new SimplePivotPointIndicator(series, 10, 5, 0.06, 0.1));
        //Indicator indicator = new DeMarkReversalIndicator(new DeMarkPivotPointIndicator(series, 4));

        for (int i = 10; i < series.getEndIndex() - 4; i++) {
            DeMarkReversalIndicator.DeMarkReversalResult result = (DeMarkReversalIndicator.DeMarkReversalResult) indicator.getValue(i);
            System.out.print(i + " [" + series.getTick(i).getEndTime().format(DateTimeFormatter.ISO_DATE) + "]  ");
            System.out.print(result.isSetupFinished() + ", " + result.isSetupPerfection() + ", " + result.isCountdownFinished());
            if (result.isCountdownFinished()) {
                System.out.print(series.getTick(result.getSetupStartIndex()).getEndTime().format(DateTimeFormatter.ISO_DATE) + " --> " +
                        series.getTick(result.getCountdownStartIndex()).getEndTime().format(DateTimeFormatter.ISO_DATE));
            }
            System.out.println();
        }


    }
}

