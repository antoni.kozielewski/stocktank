package indicatorTests;

import org.hou.stocktank.analysis.analyzers.AnalyzerToDecimalIndicatorWrapper;
import org.hou.stocktank.analysis.analyzers.advanced.BPF01Analyzer;
import org.hou.stocktank.charts.*;
import org.hou.stocktank.services.BossaSingleFileDataProviderService;
import org.ta4j.core.Indicator;
import org.ta4j.core.TimeSeries;

import java.awt.*;

public class IndicatorAndPricesTest {
    private static final String stockName = "TAURONPE";

    public static void main(String[] args) {
        System.out.println("---------------START");

        BossaSingleFileDataProviderService dataProviderService = new BossaSingleFileDataProviderService();
        dataProviderService.setDataPath("data/mstall.zip");
        TimeSeries series = dataProviderService.load(stockName);

        Indicator indicator = new AnalyzerToDecimalIndicatorWrapper(new BPF01Analyzer(series));

//        Indicator indicator = new ClosePriceIndicator(series);
//        Indicator indicator2 = new RSIIndicator(indicator, 10);
//        Indicator indicator3 = new RSIAnalyzerIndicator(series,10, 30, 70);
//
//        MagicDataPlot closePriceChart = new MagicDataPlot("close price", new ClosePriceIndicator(series), series, Color.BLACK, MagicRendererType.LINE);
//        MagicDataPlot openPriceChart = new MagicDataPlot("begin price", new OpenPriceIndicator(series), series, Color.BLUE, MagicRendererType.LINE);
//
//        MagicLayer layerPrice = new MagicLayer("Price");
//        layerPrice.addDataPlot(new MagicDataPlot("price candles", series, Color.BLACK, MagicRendererType.CANDLESTICK));

        MagicLayer indicatorLayer = new MagicLayer("");
        indicatorLayer.addDataPlot(new MagicDataPlot("indicator", indicator,series, Color.BLUE, MagicRendererType.LINE));
        indicatorLayer.addHMarker(new MagicMarker(70, "sell", Color.black));
        indicatorLayer.addHMarker(new MagicMarker(30, "buy", Color.black));

        MagicChart chart = new MagicChart();
        //chart.withLayer(layerPrice);
        chart.withLayer(indicatorLayer);
        chart.withWidht(1200);
        chart.showChart();

    }

}
