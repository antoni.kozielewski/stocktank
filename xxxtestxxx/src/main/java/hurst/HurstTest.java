package hurst;


import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

public class HurstTest {

    private List<Double> prepareMonotonicIncrement(int size) {
        List<Double> result = new ArrayList<>();
        for (int i = 0; i < size; i++) {
            result.add(Double.valueOf(result.size()));
        }
        return result;
    }

    private List<Double> prepareSinSet(int size, int periods) {
        List<Double> result = new ArrayList<>();
        for (int i = 0; i < size; i++) {
            result.add(Math.sin(periods * 2 * 3.14 * i / size));
        }
        return result;
    }


    private List<Double> prepareABSet(int size) {
        List<Double> result = new ArrayList<>();
        for (int i = 0; i < size; i++) {
            result.add((i % 2 == 0) ? -1d : 1d);
        }
        return result;
    }

    private List<Double> prepareRandomSet(int size) {
        Random random = new Random();
        List<Double> result = new ArrayList<>();
        for (int i = 0; i < size; i++) {
            result.add(random.nextDouble() - 0.5);
        }
        return result;
    }

    private List<Double> prepareOrderedSet(int size) {
        List<Double> x = prepareRandomSet(size);
        Collections.sort(x);
        return x;
    }

    private void printData(List<Double> data) {
        data.stream().forEach(System.out::println);
    }


    private void run() {
        int size = 1024;
        List<Double> sinData = prepareSinSet(size, 1);
        List<Double> monotonicData = prepareMonotonicIncrement(size);
        List<Double> randomData = prepareRandomSet(size);
        List<Double> orderedData = prepareOrderedSet(size);
        List<Double> abData = prepareABSet(size);

        HurstExponentEstimator estimator = new HurstExponentEstimator();
        System.out.println(" sin data       - expected 1   == " + estimator.estimate(sinData));
        System.out.println(" monotonic data - expected 1   == " + estimator.estimate(monotonicData));
        System.out.println(" random data    - expected 0.5 == " + estimator.estimate(randomData));
        System.out.println(" ordered data   - expected 1   == " + estimator.estimate(orderedData));
        System.out.println(" -1,1 data      - expected 0   == " + estimator.estimate(abData));
    }

    public static void main(String[] args) {
        System.out.println(" Hurst exponent estimation test:");
        HurstTest test = new HurstTest();
        test.run();
    }

}
