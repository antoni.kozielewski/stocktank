package hurst;


import org.apache.commons.math3.stat.regression.SimpleRegression;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class HurstExponentEstimator {

    private class XYPair {
        public Double x;
        public Double y;
        public XYPair(Double x, Double y) {
            this.x = x;
            this.y = y;
        }
    }

    private List<Double> z(List<Double> y) {
        List<Double> result = new ArrayList<>();
        for (int i = 0; i < y.size(); i++) {
            Double sum = 0d;
            for (int j = 0; j <= i; j++) {
                sum = sum + y.get(j);
            }
            result.add(sum);
        }
        return result;
    }

    private Double r(List<Double> z) {
        return z.stream().max(Double::compare).get() - z.stream().min(Double::compare).get();
    }

    private Double s(List<Double> region, Double mean) {
        Double sum = region.stream().map(x -> Math.pow((x - mean), 2)).mapToDouble(Double::doubleValue).sum();
        return Math.sqrt(sum / region.size());
    }

    private Double prepareRSforRegion(List<Double> region) {
        Double m = region.stream().mapToDouble(Double::doubleValue).sum() / region.size();
        List<Double> y = region.stream().map(x -> x - m).collect(Collectors.toList());
        Double r = r(z(y));
        Double s = s(region, m);
        return r / s;
    }

    private Double prepareRSForNumberOfRegions(List<Double> data, int numberOfRegions) {
        int singleRegionSize = data.size() / numberOfRegions;
        List<Double> rsList = new ArrayList<>();
        for (int i = 0; i < numberOfRegions; i++) {
            Double rs = prepareRSforRegion(data.subList(i*singleRegionSize, (i+1) * singleRegionSize));
            rsList.add(rs);
        }
        return rsList.stream().mapToDouble(Double::doubleValue).sum() /numberOfRegions;
    }

    private List<XYPair> prepareDataForRegresion(List<Double> data, int maxRegionizingLevel) {
        List<XYPair> result = new ArrayList<>();
        for(int i=0; i<=maxRegionizingLevel; i++) {
            int numberOfRegions = (int)Math.pow(2,i) ;
            int regionSize = data.size() / numberOfRegions;
            Double rs = prepareRSForNumberOfRegions(data, numberOfRegions);
            result.add(new XYPair(Math.log(regionSize), Math.log(rs)));
        }
        return result;
    }

    private Double doLinearRegression(List<XYPair> points){
        SimpleRegression simpleRegression = new SimpleRegression();
        points.stream().forEach(point -> simpleRegression.addData(point.x, point.y));
        return simpleRegression.getSlope();
    }

    private int estimateMaxRegionizingLevel(int size) {
        return ((int)(Math.log(size)/Math.log(2))) - 8; // tak zeby min 8 elementow zostalo
    }

    public Double estimate(List<Double> data) {
        return doLinearRegression( prepareDataForRegresion(data,estimateMaxRegionizingLevel(data.size())));
    }

}
