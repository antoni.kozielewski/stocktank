package strategy;

import org.hou.stocktank.analysis.analyzers.AnalyzerToDecimalIndicatorWrapper;
import org.hou.stocktank.analysis.analyzers.advanced.BPF01Analyzer;
import org.hou.stocktank.base.StrategyFactory;
import org.hou.stocktank.charts.MagicChartBuilder;
import org.hou.stocktank.charts.MagicDataPlot;
import org.hou.stocktank.charts.MagicLayer;
import org.hou.stocktank.charts.MagicRendererType;
import org.hou.stocktank.indicators.LineAngleIndicator;
import org.hou.stocktank.services.BossaSingleFileDataProviderService;
import org.hou.stocktank.strategies.advanced.AdvancedRSIStochasticStrategyFactory;
import org.hou.stocktank.utils.MBankProfitCriterion;
import org.hou.stocktank.utils.TimeSeriesUtils;
import org.joda.time.DateTime;
import org.ta4j.core.*;
import org.ta4j.core.analysis.criteria.*;
import org.ta4j.core.indicators.SMAIndicator;
import org.ta4j.core.indicators.StochasticOscillatorKIndicator;
import org.ta4j.core.indicators.helpers.ClosePriceIndicator;
import org.ta4j.core.indicators.helpers.MaxPriceIndicator;
import org.ta4j.core.indicators.helpers.MinPriceIndicator;

import java.awt.*;
import java.time.ZoneId;
import java.time.ZonedDateTime;

public class StrategyTest {
    private static final String stockName = "prochnik";

    public static void main(String[] args) {
        System.out.println("-------- START --------");
        BossaSingleFileDataProviderService dataProviderService = new BossaSingleFileDataProviderService();
        dataProviderService.setDataPath("data/mstall.zip");
        TimeSeries series = series = dataProviderService.load(stockName);
        series = TimeSeriesUtils.getSubseries(ZonedDateTime.of(2016,01,01,0,0,0,0, ZoneId.systemDefault()), series);

//          StrategyFactory strategyFactory = new BPF01StrategyFactory();
        StrategyFactory strategyFactory = new AdvancedRSIStochasticStrategyFactory();

//        StrategyFactory strategyFactory = new IchimokuStrategyFactory();
//        StrategyFactory strategyFactory = new MACDStrategyFactory();
//        StrategyFactory strategyFactory = new IchimokuStrategyFactory();
//        StrategyFactory strategyFactory = new SimpleLocalMinStrategyFactory();
//        StrategyFactory strategyFactory = new ParabolicStopAndReverseStrategyFactory();


        Strategy strategy = strategyFactory.buildStrategy(series);
        strategy.setUnstablePeriod(0);
        TimeSeriesManager manager = new TimeSeriesManager(series);
        TradingRecord tradingRecord = manager.run(strategy);

        System.out.println("--------------------------------");
        System.out.println("Trades:");

        for (Trade trade : tradingRecord.getTrades()) {
            System.out.println(" trade :" + trade.toString() + " :: " + series.getTick(trade.getEntry().getIndex()).getBeginTime() + " --> " + series.getTick(trade.getExit().getIndex()).getBeginTime() ) ;
        }
        System.out.println("--------------------------------");


        System.out.println(strategyFactory.toString());
        System.out.println(" Name: " + strategyFactory.getName());
        System.out.println(" Ticks: " + series.getTickCount());
        System.out.println(" From: " + series.getFirstTick().getBeginTime());
        System.out.println(" To:   " + series.getLastTick().getEndTime());

        MBankProfitCriterion mbankProfitCriterion = new MBankProfitCriterion();
        System.out.println("MBank profit: " + mbankProfitCriterion.calculate(series, tradingRecord));
        TotalProfitCriterion totalProfit = new TotalProfitCriterion();
        System.out.println("Total profit: " + totalProfit.calculate(series, tradingRecord));
        System.out.println("Number of ticks: " + new NumberOfTicksCriterion().calculate(series, tradingRecord));
        System.out.println("Average profit (per tick): " + new AverageProfitCriterion().calculate(series, tradingRecord));
        System.out.println("Number of trades: " + new NumberOfTradesCriterion().calculate(series, tradingRecord));
        System.out.println("Profitable trades ratio: " + new AverageProfitableTradesCriterion().calculate(series, tradingRecord));
        System.out.println("Maximum drawdown: " + new MaximumDrawdownCriterion().calculate(series, tradingRecord));
        System.out.println("Reward-risk ratio: " + new RewardRiskRatioCriterion().calculate(series, tradingRecord));
        System.out.println("Total transaction cost (from $1000): " + new LinearTransactionCostCriterion(1000, 0.0039).calculate(series, tradingRecord));
        System.out.println("Buy-and-hold: " + new BuyAndHoldCriterion().calculate(series, tradingRecord));
        System.out.println("Custom strategy profit vs buy-and-hold strategy profit: " + new VersusBuyAndHoldCriterion(totalProfit).calculate(series, tradingRecord));

        if (tradingRecord.getLastOrder() != null) {
            String lastOrder = tradingRecord.getLastOrder().isBuy() ? "BUY" : "SELL";
            int daysFromOrder = series.getTickCount() - tradingRecord.getLastOrder().getIndex();
            System.out.println(" Last order: " + lastOrder + " : " + daysFromOrder + " days ago.");
        }

        Indicator bpf01Indicator = new AnalyzerToDecimalIndicatorWrapper(new BPF01Analyzer(series));

        Indicator emaFast = new SMAIndicator(new ClosePriceIndicator(series),23);
        Indicator emaSlow = new SMAIndicator(emaFast,27);
        Indicator angle = new SMAIndicator(new LineAngleIndicator(emaFast,2), 5);




        // Chart builder
        MagicChartBuilder builder = new MagicChartBuilder(strategyFactory,series);
        builder
                .withTitle(stockName + " : " + strategyFactory.getName())
                .withTradingRecord(tradingRecord)
                //.withoutPriceCandles()
                .withoutPriceEMA()
                .withBuySignals()
                .withSellSignals()
                .withCashFlow()
                .withChartWidth(1200)
                .withChartHeight(900)
//                .withLastDays(180)
        ;
// macd layer
//        Indicator macd = new MACDIndicator(new ClosePriceIndicator(series), 30, 70);
//        Indicator macdSignal = new EMAIndicator(macd, 25);
//
//        MagicLayer macdLayer = new MagicLayer("macd");
//        MagicDataPlot macdChart = new MagicDataPlot("macd", macd, series, Color.darkGray, MagicRendererType.LINE);
//        macdLayer.addDataPlot(macdChart);
//        MagicDataPlot signalChart = new MagicDataPlot("macd signal", macdSignal, series, Color.RED, MagicRendererType.LINE);
//        macdLayer.addDataPlot(signalChart);
//
//        builder.withLayer("macd",macdLayer);

// 0 ... 100 indicators
        StochasticOscillatorKIndicator kbase = new StochasticOscillatorKIndicator(new ClosePriceIndicator(series), 14, new MaxPriceIndicator(series), new MinPriceIndicator(series));
        SMAIndicator stochK = new SMAIndicator(kbase,3);
        SMAIndicator stochD = new SMAIndicator(stochK, 3);

        MagicLayer normalizedLayer = new MagicLayer("normalized");
//        MagicDataPlot stochKChart = new MagicDataPlot("stochK", stochK, series, Color.orange, MagicRendererType.LINE);
//        normalizedLayer.addDataPlot(stochKChart);
//        MagicDataPlot stochDChart = new MagicDataPlot("stochD", stochD, series, Color.BLACK, MagicRendererType.LINE);
//        normalizedLayer.addDataPlot(stochDChart);

        builder.withLayer("normalized",normalizedLayer);

// angle layer
//        MagicLayer angleLayer = new MagicLayer("ema angle");
//        MagicDataPlot angleChart = new MagicDataPlot("ema angle", angle, series, Color.black, MagicRendererType.LINE);
//        angleLayer.addDataPlot(angleChart);
//        builder.withLayer("angle",angleLayer);

// ema layer
//        MagicLayer emaLayer = new MagicLayer("EMA");
//        MagicDataPlot emaFastChart = new MagicDataPlot("emaFast", emaFast, series, Color.red, MagicRendererType.LINE);
//        emaLayer.addDataPlot(emaFastChart);
//        MagicDataPlot emaSlowChart = new MagicDataPlot("emaSlow", emaSlow, series, Color.BLUE, MagicRendererType.LINE);
//        emaLayer.addDataPlot(emaSlowChart);

// bpf01 layer
        MagicLayer bpf01layer = new MagicLayer("BPF01");
        MagicDataPlot bpf01Chart = new MagicDataPlot("bpf01", bpf01Indicator, series, Color.black, MagicRendererType.LINE);
        bpf01layer.addDataPlot(bpf01Chart);
        builder.withLayer("bpf01",bpf01layer);



        builder.build().showChart();
        System.out.println("------  FINISHED  -----");
    }
}
