package strategy;


import org.hou.stocktank.base.StrategyFactory;
import org.hou.stocktank.services.BossaSingleFileDataProviderService;
import org.hou.stocktank.strategies.advanced.BPF01StrategyFactory;
import org.hou.stocktank.utils.MBankProfitCriterion;
import org.hou.stocktank.utils.StocktankTXTUtil;
import org.hou.stocktank.utils.TimeSeriesUtils;
import org.ta4j.core.Strategy;
import org.ta4j.core.TimeSeries;
import org.ta4j.core.TimeSeriesManager;
import org.ta4j.core.TradingRecord;

import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.Arrays;
import java.util.List;

public class MultistockStrategyTest {
    private List<String> stocks = Arrays.asList("pge", "tauronpe", "prochnik", "cdprojekt", "enea", "vistula", "livechat");
    private StrategyFactory strategyFactory = new BPF01StrategyFactory();

    private String singleStock(String stockName) {
        BossaSingleFileDataProviderService dataProviderService = new BossaSingleFileDataProviderService();
        dataProviderService.setDataPath("data/mstall.zip");
        TimeSeries series = series = dataProviderService.load(stockName);
        series = TimeSeriesUtils.getSubseries(ZonedDateTime.of(2014,01,01,0,0,0,0, ZoneId.systemDefault()),series);
        Strategy strategy = strategyFactory.buildStrategy(series);
        TimeSeriesManager manager = new TimeSeriesManager(series);
        TradingRecord tradingRecord = manager.run(strategy);
        MBankProfitCriterion mbankProfitCriterion = new MBankProfitCriterion();
        StringBuilder builder = new StringBuilder(StocktankTXTUtil.fillRight(stockName, " ", 10));
        builder.append("MBank profit: ");
        builder.append(StocktankTXTUtil.fillRight(String.valueOf(mbankProfitCriterion.calculate(series, tradingRecord)), " ", 15));
        builder.append(" / " + StocktankTXTUtil.fillRight(String.valueOf(tradingRecord.getTrades().size()), " ", 5));
        return builder.toString();
    }

    private void run() {
        System.out.println(" Multi stocks strategy test : " + strategyFactory.getName());
        for (String stockName : stocks) {
            System.out.println(singleStock(stockName));
        }
    }

    public static void main(String[] args) {
        MultistockStrategyTest xxx = new MultistockStrategyTest();
        xxx.run();
    }

}
