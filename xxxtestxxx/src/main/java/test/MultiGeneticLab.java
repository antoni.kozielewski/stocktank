package test;

import org.hou.stocktank.base.StrategyFactory;
import org.hou.stocktank.genetics.Population;
import org.hou.stocktank.storage.PersistentResultStorage;
import org.hou.stocktank.storage.ResultStorage;
import org.hou.stocktank.strategies.SimpleRVIStrategyFactory;
import org.hou.stocktank.utils.BossaPRNLoader;
import org.hou.stocktank.utils.TimeSeriesUtils;
import org.hou.stocktank.utils.evaluation.StrategyTester;
import org.hou.stocktank.utils.evaluation.StrategyTestingResultFields;
import org.ta4j.core.TimeSeries;

import java.util.Date;
import java.util.Map;

public class MultiGeneticLab {
    private String stockName;
    private String storagePath;
    private int populationSize = 100;
    private int generations = 100;
    private int times = 100;
    private StrategyFactory strategyFactory;
    private ResultStorage storage;

    public MultiGeneticLab(String stockName, StrategyFactory strategyFactory, String storagePath, int populationSize, int generations, int times) {
        this.stockName = stockName;
        this.storagePath = storagePath;
        this.populationSize = populationSize;
        this.generations = generations;
        this.times = times;
        this.strategyFactory = strategyFactory;
        storage = new PersistentResultStorage(stockName, storagePath);
    }

    public String preatyPrint(int columnWidth, String... values) {
        String result = "";
        for (String s : values) {
            if (s.length() < columnWidth) {
                while (s.length() < columnWidth) s = s + " ";
            }
            result = result + " " + s;
        }
        return result;
    }

    private void printResult(Map<String, String> result) {
        System.out.println("----------------------------------------------------------------------------");
        for (StrategyTestingResultFields key : StrategyTestingResultFields.getOrderedValues()) {
            if (result.containsKey(key.toString())) {
                System.out.println(preatyPrint(25, key.getName(), result.get(key.toString())));
            }
        }
    }

    private Map<String,String> singleWorld(int unstablePeriod, TimeSeries series) {
        strategyFactory.setUnstablePeriod(unstablePeriod);
        Population population = new Population(populationSize, strategyFactory, series);
        Double avgTime = null;
        for (int i = 0; i < generations; i++) {
            long start = (new Date()).getTime();
            population.singleGeneration();
            long end = (new Date()).getTime();
            if (avgTime == null) avgTime = Double.valueOf(end - start);
            else avgTime = (avgTime + (end - start)) / 2;
            System.out.print(".");
        }
        StrategyFactory sf = population.getBest();
        StrategyTester tester = new StrategyTester();
        Map<String, String> testResult = tester.testStrategy(stockName, series, 0, sf);
        return testResult;
    }


    public void run() {
        TimeSeries series = BossaPRNLoader.load(stockName, "data/_IMPORT/" + stockName + ".prn");
        series = TimeSeriesUtils.aggregateToDays(series);
        int ticks = series.getTickCount()/(times + 1);
        for (int t = 0; t < times; t++) {
            Map<String, String> result = singleWorld(100 + ticks * t, series);
            storage.addResult(result);
            printResult(result);
            System.out.println(":: " + t + " ::");
        }
    }

    public static void main(String[] args) {
        System.out.println("------------- START --------------");
        MultiGeneticLab lab = new MultiGeneticLab("cdprojekt", new SimpleRVIStrategyFactory(), "results", 200, 100, 10);
        lab.run();
        System.out.println("------------- FINISHED --------------");
    }
}
