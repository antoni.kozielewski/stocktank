package test;

import org.hou.stocktank.base.StrategyFactory;
import org.hou.stocktank.strategies.*;
import org.hou.stocktank.utils.BossaPRNLoader;
import org.hou.stocktank.utils.MBankProfitCriterion;
import org.hou.stocktank.utils.TimeSeriesUtils;
import org.ta4j.core.Strategy;
import org.ta4j.core.TimeSeries;
import org.ta4j.core.TimeSeriesManager;
import org.ta4j.core.TradingRecord;
import org.ta4j.core.analysis.criteria.*;


public class DefaultMultiStrategyTest {

    private static TradingRecord run(StrategyFactory sf, TimeSeries series) {
        Strategy strategy = sf.buildStrategy(series);
        TimeSeriesManager manager = new TimeSeriesManager(series);
        TradingRecord tradingRecord = manager.run(strategy);
        System.out.println(sf.toString());
        System.out.println(" Name: " + sf.getClass().getName());
        System.out.println(" Ticks: " + series.getTickCount());
        System.out.println(" From: " + series.getFirstTick().getBeginTime());
        System.out.println(" To:   " + series.getLastTick().getEndTime());
        // MBank Profit
        MBankProfitCriterion mbankProfitCriterion = new MBankProfitCriterion();
        System.out.println("MBank profit: " + mbankProfitCriterion.calculate(series, tradingRecord));

        // Total profit
        TotalProfitCriterion totalProfit = new TotalProfitCriterion();
        System.out.println("Total profit: " + totalProfit.calculate(series, tradingRecord));
        // Number of ticks
        System.out.println("Number of ticks: " + new NumberOfTicksCriterion().calculate(series, tradingRecord));
        // Average profit (per tick)
        System.out.println("Average profit (per tick): " + new AverageProfitCriterion().calculate(series, tradingRecord));
        // Number of trades
        System.out.println("Number of trades: " + new NumberOfTradesCriterion().calculate(series, tradingRecord));
        // Profitable trades ratio
        System.out.println("Profitable trades ratio: " + new AverageProfitableTradesCriterion().calculate(series, tradingRecord));
        // Maximum drawdown
        System.out.println("Maximum drawdown: " + new MaximumDrawdownCriterion().calculate(series, tradingRecord));
        // Reward-risk ratio
        System.out.println("Reward-risk ratio: " + new RewardRiskRatioCriterion().calculate(series, tradingRecord));
        // Total transaction cost
        System.out.println("Total transaction cost (from $1000): " + new LinearTransactionCostCriterion(1000, 0.0039).calculate(series, tradingRecord));
        // Buy-and-hold
        System.out.println("Buy-and-hold: " + new BuyAndHoldCriterion().calculate(series, tradingRecord));
        // Total profit vs buy-and-hold
        System.out.println("Custom strategy profit vs buy-and-hold strategy profit: " + new VersusBuyAndHoldCriterion(totalProfit).calculate(series, tradingRecord));

        if (tradingRecord.getLastOrder() != null) {
            String lastOrder = tradingRecord.getLastOrder().isBuy() ? "BUY" : "SELL";
            int daysFromOrder = series.getTickCount() - tradingRecord.getLastOrder().getIndex();
            System.out.println(" Last order: " + lastOrder + " : " + daysFromOrder + " days ago.");
        }
        System.out.println("------------------------------------------------------------------------------------------------------------------------------");
        return tradingRecord;
    }

    public static void main(String[] args) {
        System.out.println("--- START ---");
        String stockName = "farm51";
        TimeSeries series = BossaPRNLoader.load(stockName, "data/_IMPORT/" + stockName + ".prn");
        series = TimeSeriesUtils.aggregateToDays(series);

// MACD
        StrategyFactory macdStrategyFactory = new MACDStrategyFactory();
        TradingRecord macdTradingRecord = run(macdStrategyFactory, series);

// CCI
        StrategyFactory cciStrategyFactory = new CCIStrategyFactory();
        TradingRecord cciTradingRecord = run(cciStrategyFactory, series);

// RSI
        StrategyFactory rsiStrategyFactory = new RSIStrategyFactory();
        TradingRecord rsiTradingRecord = run(rsiStrategyFactory, series);

// STOCH_K
        //cdprojekt: [STOCH_K : (time frame=7) ], [STOCH_D : () ] Levels (buy level=43, sell level=92)  :: [born:199c]
        StrategyFactory stochasticStrategyFactory = new StochasticStrategyFactory();

        TradingRecord stochasticTradingRecord = run(stochasticStrategyFactory, series);

// MovingMomentum
        //avg=4.165390302292216,  max=5.105270407514112 : org.hou.stocktank.strategies.MovingMomentumStrategyFactory, [MACD : (short time frame=11, long time frame=135) ], [EMA_MACD : (time frame=52) ], [SHORT_EMA : (time frame=8) ], [LONG_EMA : (time frame=20) ], [STOCH_K : (time frame=7) ] Levels (buy level=39, sell level=85)  :: [born:142c]
        StrategyFactory movingMomentumStrategyFactory = new MovingMomentumStrategyFactory();
        TradingRecord movingMomentumTradingRecord = run(movingMomentumStrategyFactory, series);

// WILLIAMS
        StrategyFactory williamsRStrategyFactory = new WilliamsRStrategyFactory();

        TradingRecord williamsTradingRecord = run(williamsRStrategyFactory, series);

// AROON UP
        StrategyFactory aroonUPStrategyFactory = new AroonTrendStregthStrategyFactory();
        TradingRecord aroonUPTradingRecord = run(aroonUPStrategyFactory, series);


//        ChartBuilder chartUtils = new ChartBuilder(series)
//                .withTitle(stockName + " : MultiModel")
//                .withIndicator(new ClosePriceIndicator(series), "Price")
////                .withIndicator("CacheFlow", new CashFlow(series, tradingRecord), Color.black)
////                .withLevel("a", 30, Color.black)
////                .withLevel("a", 70, Color.black)
//                //.withStrategyFactory(stockName, strategyFactory, Color.yellow)
//                //.withTradingRecord(macdTradingRecord)
//                .withBuySignals(macdTradingRecord)
//                .withBuySignals(cciTradingRecord)
//                .withBuySignals(rsiTradingRecord)
//                .withBuySignals(stochasticTradingRecord)
//                .withBuySignals(movingMomentumTradingRecord)
//                .withBuySignals(williamsTradingRecord)
//                .withBuySignals(aroonUPTradingRecord)
//
//                .withSellSignals(macdTradingRecord)
//                .withSellSignals(cciTradingRecord)
//                .withSellSignals(rsiTradingRecord)
//                .withSellSignals(stochasticTradingRecord)
//                .withSellSignals(movingMomentumTradingRecord)
//                .withSellSignals(williamsTradingRecord)
//                .withSellSignals(aroonUPTradingRecord);
//
//
//        chartUtils.displayChart();

        System.out.println("--- END ---");
    }
}
