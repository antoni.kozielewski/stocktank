package test;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.plot.ValueMarker;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.StandardXYItemRenderer;
import org.jfree.data.time.Month;
import org.jfree.data.time.TimeSeries;
import org.jfree.data.time.TimeSeriesCollection;
import org.jfree.ui.ApplicationFrame;
import org.jfree.ui.Layer;
import org.jfree.ui.RefineryUtilities;

import java.awt.*;

public class FreeChart4jTest {

    public static void main(String[] args) {
        System.out.println("------------- START ---------------");

        TimeSeries s1 = new TimeSeries("L&G European Index Trust");
        s1.add(new Month(2, 2001), 181.8);
        s1.add(new Month(3, 2001), 167.3);
        s1.add(new Month(4, 2001), 153.8);
        s1.add(new Month(5, 2001), 167.6);
        s1.add(new Month(6, 2001), 158.8);
        s1.add(new Month(7, 2001), 148.3);
        s1.add(new Month(8, 2001), 153.9);
        s1.add(new Month(9, 2001), 142.7);
        s1.add(new Month(10, 2001), 123.2);
        s1.add(new Month(11, 2001), 131.8);
        s1.add(new Month(12, 2001), 139.6);
        s1.add(new Month(1, 2002), 142.9);
        s1.add(new Month(2, 2002), 138.7);
        s1.add(new Month(3, 2002), 137.3);
        s1.add(new Month(4, 2002), 143.9);
        s1.add(new Month(5, 2002), 139.8);
        s1.add(new Month(6, 2002), 137.0);
        s1.add(new Month(7, 2002), 132.8);

        TimeSeries s2 = new TimeSeries("L&G UK Index Trust");
        s2.add(new Month(2, 2001), 129600);
        s2.add(new Month(3, 2001), 123200);
        s2.add(new Month(4, 2001), 117200);
        s2.add(new Month(5, 2001), 124100);
        s2.add(new Month(6, 2001), 122600);
        s2.add(new Month(7, 2001), 119200);
        s2.add(new Month(8, 2001), 116500);
        s2.add(new Month(9, 2001), 112700);
        s2.add(new Month(10, 2001), 101500);
        s2.add(new Month(11, 2001), 101000);
        s2.add(new Month(12, 2001), 110300);
        s2.add(new Month(1, 2002), 111700);
        s2.add(new Month(2, 2002), 111000);
        s2.add(new Month(3, 2002), 109600);
        s2.add(new Month(4, 2002), 113200);
        s2.add(new Month(5, 2002), 111600);
        s2.add(new Month(6, 2002), 108800);
        s2.add(new Month(7, 2002), 101600);

        TimeSeriesCollection collection = new TimeSeriesCollection();
        collection.addSeries(s1);
        collection.addSeries(s2);
        TimeSeriesCollection collection2 = new TimeSeriesCollection();
        collection2.addSeries(s2);

        JFreeChart chart = ChartFactory.createTimeSeriesChart(
                "Test", // title
                "X", // x-axis label
                "Y", // y-axis label
                collection, // data
                true, // create legend
                true, // generate tooltips?
                false // generate URLs?
        );

        XYPlot plot = (XYPlot) chart.getPlot();

        // AXIS
        final NumberAxis axis = new NumberAxis("big");
        plot.setRangeAxis(1, axis);
        plot.setDataset(1, collection2);
        plot.mapDatasetToRangeAxis(0, 1);
        final StandardXYItemRenderer renderer = new StandardXYItemRenderer();
        renderer.setSeriesPaint(1, Color.BLUE);
        plot.setRenderer(1, renderer);

        ValueMarker marker = new ValueMarker(75);
        plot.addRangeMarker(0, marker, Layer.FOREGROUND);

        ValueMarker marker2 = new ValueMarker(120000);
        ValueMarker marker3 = new ValueMarker(121000);
        ValueMarker marker4 = new ValueMarker(122000);
        ValueMarker marker5 = new ValueMarker(123000);
        plot.addRangeMarker(1, marker2, Layer.FOREGROUND);
        plot.addRangeMarker(1, marker3, Layer.FOREGROUND);
        plot.addRangeMarker(1, marker4, Layer.FOREGROUND);
        plot.addRangeMarker(1, marker5, Layer.FOREGROUND);


        // MagicDataPlot panel
        ChartPanel panel = new ChartPanel(chart);
        panel.setFillZoomRectangle(true);
        panel.setMouseWheelEnabled(true);
        panel.setPreferredSize(new Dimension(1024, 400));
        // Application frame
        ApplicationFrame frame = new ApplicationFrame("TEST");
        frame.setContentPane(panel);
        frame.pack();
        RefineryUtilities.centerFrameOnScreen(frame);
        frame.setVisible(true);

        System.out.println("------------- FINISHED ---------------");
    }
}
