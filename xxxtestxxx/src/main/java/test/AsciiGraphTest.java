package test;

import com.mitchtalmadge.asciigraph.ASCIIGraph;
import org.hou.stocktank.services.BossaSingleFileDataProviderService;
import org.hou.stocktank.utils.TimeSeriesUtils;
import org.ta4j.core.Decimal;
import org.ta4j.core.TimeSeries;

import java.math.BigDecimal;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by Ant on 2018-05-22.
 */
public class AsciiGraphTest {

    public static void main(String[] args) {

        BossaSingleFileDataProviderService dataProviderService = new BossaSingleFileDataProviderService();
        dataProviderService.setDataPath("data/mstall.zip");
        TimeSeries series = series = dataProviderService.load("apator");

        double[] x = TimeSeriesUtils.getSubseries(ZonedDateTime.of(2018, 1,1,0,0,0,0, ZoneId.systemDefault()),series).getTickData().stream().map(t -> t.getClosePrice().toDouble()).mapToDouble(Double::doubleValue).toArray();
        System.out.println("----------------------------------------------------------------------------------------------------------------------------------------");
        System.out.println(ASCIIGraph.fromSeries(x).withNumRows(25).plot());
        System.out.println("----------------------------------------------------------------------------------------------------------------------------------------");
    }
}
