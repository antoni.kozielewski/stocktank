package test;

import org.hou.stocktank.analysis.AnalysisResult;
import org.hou.stocktank.analysis.analyzers.advanced.BPF01Analyzer;
import org.hou.stocktank.services.BossaSingleFileDataProviderService;
import org.ta4j.core.Indicator;
import org.ta4j.core.TimeSeries;

import java.io.File;

public class AnalyzerTest {
    private static final String stockName = "livechat";

    private void test(String fileName) {
        System.out.println("-------- START --------");
        BossaSingleFileDataProviderService dataProviderService = new BossaSingleFileDataProviderService();
        dataProviderService.setDataPath("data/mstall.zip");
        TimeSeries series = series = dataProviderService.load(stockName);

        //Indicator<AnalysisResult> analyzer = new ShootingStarCandlestickPattern(series);
        Indicator<AnalysisResult> analyzer = new BPF01Analyzer(series);

        AnalysisResult result = analyzer.getValue(series.getTickCount()-1);
        System.out.println(result);

//        for (int i = 0; i < series.getTickCount(); i++) {
//            AnalysisResult result = analyzer.getValue(i);
//            if ((result.getMarketState() != MarketState.UNKNOWN)&&(result.getValue() >= 4 || result.getValue() <= -4)) {
//                System.out.println(series.getTick(i).getBeginTime());
//                System.out.println(result);
//            }
//        }
        System.out.println("------------------------------------------------------------------------------");
    }

    public static void main(String[] args) {
        System.out.println("-------- START --------");
        AnalyzerTest tester = new AnalyzerTest();
        File dir = new File("data/_IMPORT/");
        for (File f : dir.listFiles()) {
            tester.test(f.getName());
        }
        System.out.println("------  FINISHED  -----");
    }
}
