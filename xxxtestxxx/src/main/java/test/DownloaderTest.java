package test;


import org.hou.stocktank.utils.BossaDownloader;

import java.util.ArrayList;
import java.util.List;

public class DownloaderTest {
    public static void main(String[] args) {
        System.out.println("---START---");
        List<String> stocks = new ArrayList<>();
        stocks.add("CDPROJEKT");
        stocks.add("PROCHNIK");
        stocks.add("KGHM");
        stocks.add("SANOK");
        stocks.add("TAURONPE");
        BossaDownloader downloader = new BossaDownloader();
        downloader.downloadStocks(stocks);
        System.out.println("---FINISHED---");
    }
}
