package test;

import org.hou.stocktank.services.BossaSingleFileDataProviderService;
import org.ta4j.core.Tick;
import org.ta4j.core.TimeSeries;

import java.time.ZoneId;
import java.time.ZonedDateTime;

public class TimeSeriesPrinter {

    public static void main(String[] args) {
        BossaSingleFileDataProviderService dataProviderService = new BossaSingleFileDataProviderService();
        dataProviderService.setDataPath("data/mstall.zip");
        TimeSeries series = series = dataProviderService.load("cdprojekt");
        for(int i = 0; i < series.getTickCount(); i++) {
            if (series.getTick(i).getBeginTime().isAfter(ZonedDateTime.of(2017,06,19,0,0,0,0, ZoneId.systemDefault()))) {
                Tick tick = series.getTick(i);
                System.out.println(tick.getBeginTime() + " : " + tick.getMinPrice() + " / " + tick.getMaxPrice() + " | " + tick.getOpenPrice() + " / " + tick.getClosePrice());
            }
        }

    }
}
