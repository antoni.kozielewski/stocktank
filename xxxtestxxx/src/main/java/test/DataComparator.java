package test;

import org.hou.stocktank.services.BossaSingleFileDataProviderService;
import org.hou.stocktank.utils.BossaDownloader;
import org.hou.stocktank.utils.BossaPRNLoader;
import org.hou.stocktank.utils.BossaSingleFileDownloader;
import org.hou.stocktank.utils.TimeSeriesUtils;
import org.ta4j.core.TimeSeries;

import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.List;

public class DataComparator {
    public static void main(String[] args) {
        System.out.println("---- START ----");
        List<String> stocks = new ArrayList<>();
        stocks.add("06MAGNA");

        BossaDownloader bossaDownloader = new BossaDownloader();
        bossaDownloader.downloadStocks(stocks);


        BossaSingleFileDownloader bossaSingleFileDownloader = new BossaSingleFileDownloader();
        bossaSingleFileDownloader.download();
        BossaSingleFileDataProviderService dataProviderService = new BossaSingleFileDataProviderService();
        dataProviderService.setDataPath("data/mstall.zip");

        for(String stock : stocks) {
            System.out.println(" :: " + stock + " :: ");
            TimeSeries data1 = BossaPRNLoader.load(stock, "data/_IMPORT/" + stock + ".prn");
            data1 = TimeSeriesUtils.aggregateToDays(data1);
            TimeSeries data2 = dataProviderService.load(stock);
            data2 = TimeSeriesUtils.getSubseries(ZonedDateTime.of(2003,01,19,0,0,0,0, ZoneId.systemDefault()), data2);
            System.out.println(" data1 count " + data1.getTickCount() + " : " + data1.getFirstTick().getBeginTime());
            System.out.println(" data2 count " + data2.getTickCount() + " : " + data2.getFirstTick().getBeginTime());
            for(int i = 0; i < data1.getTickCount(); i++){
                System.out.println(data1.getTick(i) + " :: " + data2.getTick(i) + "               [" + i + "]");
            }
        }
    }
}
