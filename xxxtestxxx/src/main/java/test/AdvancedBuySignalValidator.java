package test;

import org.hou.stocktank.base.StrategyFactory;
import org.hou.stocktank.services.BossaSingleFileDataProviderService;
import org.hou.stocktank.strategies.ParabolicStopAndReverseStrategyFactory;
import org.ta4j.core.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class AdvancedBuySignalValidator {
    private BossaSingleFileDataProviderService dataProviderService;
    private List<String> stocks = Arrays.asList(
            "KRUK",
            "CIECH",
            "PELION",
            "ALIOR",
            "KGHM",
            "ABPL",
            "BPC",
            "BIOMAXIMA",
            "INFOSYS",
            "PIK",
            "INVESTEKO",
            "KOPEX",
            "SIMPLE",
            "11BIT",
            "BUMECH",
            "ENEA",
            "HELIO",
            "JUJUBEE",
            "PROCHNIK",
            "SELVITA",
            "SANOK",
            "LIVECHAT"
    );
    private StrategyFactory strategyFactory = new ParabolicStopAndReverseStrategyFactory();
    //private StrategyFactory strategyFactory = new IchimokuStrategyFactory();

    private List<String> getStocks() {
        return stocks;
    }

    private List<Double> validate(String stock, int checkChangeAfterNumberOfSessions) {
        List<Double> result = new ArrayList<>();
        TimeSeries series = dataProviderService.load(stock);
        Strategy strategy = strategyFactory.buildStrategy(series);
        TimeSeriesManager manager = new TimeSeriesManager(series);
        TradingRecord tradingRecord = manager.run(strategy);
        for(Trade trade : tradingRecord.getTrades()){
            if(trade.getEntry().getIndex() < series.getTickCount() - checkChangeAfterNumberOfSessions) {
                int startIndex = trade.getEntry().getIndex();
                int checkIndex = startIndex + checkChangeAfterNumberOfSessions;
                double startValue = series.getTick(startIndex).getClosePrice().toDouble();
                double checkValue = series.getTick(checkIndex).getClosePrice().toDouble();
                double change = 100 * (checkValue - startValue)/startValue;
                result.add(change);
            }
        }
        return result;
    }

    private void validate() {
        dataProviderService = new BossaSingleFileDataProviderService();
        dataProviderService.setDataPath("data/mstall.zip");
        for(int days = 3; days < 600; days++) {
            double totalAVG = 0;
            for (String stock : getStocks()) {
                List<Double> result = validate(stock, days);
                double min = Math.round(100 * result.stream().min(Double::compare).get()) / 100d;
                double max = Math.round(100 * result.stream().max(Double::compare).get()) / 100d;
                double avg = Math.round(100 * result.stream().mapToDouble(Double::doubleValue).sum() / result.size()) / 100d;
                long minus = result.stream().filter(x -> x < 0).count();
                long plus = result.stream().filter(x -> x > 0).count();
                totalAVG = (totalAVG + avg) / 2;
//                System.out.println(" " +
//                        StocktankTXTUtil.fillLeft(stock, " ", 20) + " | " +
//                        " min: " + StocktankTXTUtil.fillLeft(String.valueOf(min), " ", 8) +
//                        " avg: " + StocktankTXTUtil.fillLeft(String.valueOf(avg), " ", 8) +
//                        " max: " + StocktankTXTUtil.fillLeft(String.valueOf(max), " ", 8) +
//                        " ::: " +
//                        " - " + StocktankTXTUtil.fillLeft(String.valueOf(minus), " ", 4) +
//                        " | " +
//                        " + " + StocktankTXTUtil.fillLeft(String.valueOf(plus), " ", 4)
//                );
            }
            System.out.println(" [" + days + "] avg change: " + totalAVG);
        }
    }

    public static void main(String[] args) {
        System.out.println("----- START ----");
        AdvancedBuySignalValidator validator = new AdvancedBuySignalValidator();
        validator.validate();
        System.out.println("----- END ----");
    }

}
