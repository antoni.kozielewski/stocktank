package test;

import org.hou.stocktank.base.StrategyFactory;
import org.hou.stocktank.services.BossaSingleFileDataProviderService;
import org.hou.stocktank.strategies.IchimokuStrategyFactory;
import org.hou.stocktank.utils.evaluation.StrategyTester;
import org.hou.stocktank.utils.evaluation.StrategyTestingResultFields;
import org.ta4j.core.TimeSeries;

import java.util.Map;

public class NewStrategyTest {

    public static String preatyPrint(int columnWidth, String... values) {
        String result = "";
        for (String s : values) {
            if (s.length() < columnWidth) {
                while (s.length() < columnWidth) s = s + " ";
            }
            result = result + " " + s;
        }
        return result;
    }

    private void testStock(String stockName) {
        BossaSingleFileDataProviderService dataProviderService = new BossaSingleFileDataProviderService();
        dataProviderService.setDataPath("data/mstall.zip");
        TimeSeries series = series = dataProviderService.load(stockName);
        StrategyFactory strategyFactory = new IchimokuStrategyFactory();
        StrategyTester tester = new StrategyTester();
        Map<String, String> result = tester.testStrategy(stockName, series, 0, strategyFactory);
    }

    public static void main(String[] args) {
        System.out.println("---------- START ----------");
        String stockName = "prairie";
        BossaSingleFileDataProviderService dataProviderService = new BossaSingleFileDataProviderService();
        dataProviderService.setDataPath("data/mstall.zip");
        TimeSeries series = series = dataProviderService.load(stockName);

        StrategyFactory strategyFactory = new IchimokuStrategyFactory();

        StrategyTester tester = new StrategyTester();
        Map<String, String> result = tester.testStrategy(stockName, series, 0, strategyFactory);
        for (StrategyTestingResultFields key : StrategyTestingResultFields.getOrderedValues()) {
            if (result.containsKey(key.toString())) {
                System.out.println(preatyPrint(25, key.getName(), result.get(key.toString())));
            }
        }
        System.out.println("---------- END ----------");
    }
}
