package test;

import org.hou.stocktank.charts.MagicCandlesticRenderer;
import org.hou.stocktank.utils.BossaPRNLoader;
import org.hou.stocktank.utils.TimeSeriesUtils;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.DateAxis;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.plot.XYPlot;
import org.jfree.data.time.TimeSeries;
import org.jfree.data.xy.AbstractXYDataset;
import org.jfree.data.xy.DefaultOHLCDataset;
import org.jfree.data.xy.OHLCDataItem;
import org.jfree.data.xy.XYDataset;
import org.jfree.ui.ApplicationFrame;
import org.jfree.ui.RefineryUtilities;
import org.ta4j.core.Tick;

import java.awt.*;
import java.sql.Date;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class FreeChart4jTest2 {

    private OHLCDataItem[] buildChartTimeSeries(String name, org.ta4j.core.TimeSeries series) {
        List<OHLCDataItem> dataItems = new ArrayList<>();
        TimeSeries chartTimeSeries = new TimeSeries(name);
        for (int i = 0; i < series.getTickCount(); i++) {
            Tick tick = series.getTick(i);
            OHLCDataItem item = new OHLCDataItem(
                    Date.from(tick.getBeginTime().toInstant()),
                    tick.getOpenPrice().toDouble(),
                    tick.getMaxPrice().toDouble(),
                    tick.getMinPrice().toDouble(),
                    tick.getClosePrice().toDouble(),
                    tick.getVolume().toDouble());
            dataItems.add(item);
        }
        Collections.reverse(dataItems);
        OHLCDataItem[] result = dataItems.toArray(new OHLCDataItem[dataItems.size()]);
        return result;
    }

    private AbstractXYDataset getDataSet(String name, org.ta4j.core.TimeSeries series) {
        return new DefaultOHLCDataset(name, buildChartTimeSeries(name, series));
    }

    private void printSeries(org.ta4j.core.TimeSeries series) {
        for(int i = 0; i < series.getTickCount(); i++){
            Tick tick = series.getTick(i);
            System.out.println(i + " :: " + tick.getBeginTime() + " - " + tick.getEndTime() + " : " + tick.getOpenPrice() + " | " + tick.getMaxPrice() + " | " + tick.getMinPrice() + " | " + tick.getClosePrice() + " --> " + tick.getVolume());
        }
    }

    private void run() {
        String stockName = "stalprod";
        org.ta4j.core.TimeSeries series = BossaPRNLoader.load(stockName, "data/_IMPORT/" + stockName + ".prn");
        series = TimeSeriesUtils.aggregateToDays(series);

        MagicCandlesticRenderer renderer = new MagicCandlesticRenderer();
        DateAxis domainAxis = new DateAxis("Date");
        NumberAxis rangeAxis = new NumberAxis("Price");
        XYDataset dataset = getDataSet("Price", series);

        XYPlot mainPlot = new XYPlot(dataset, domainAxis, rangeAxis, renderer);
        renderer.setSeriesPaint(0, Color.BLUE);
        renderer.setDrawVolume(false);
        rangeAxis.setAutoRangeIncludesZero(false);

        JFreeChart chart = new JFreeChart(
                "Test",
                null,
                mainPlot,
                false
        );

        ChartPanel panel = new ChartPanel(chart);
        panel.setFillZoomRectangle(true);
        panel.setMouseWheelEnabled(true);
        panel.setPreferredSize(new Dimension(1024, 400));
        // Application frame
        ApplicationFrame frame = new ApplicationFrame("TEST");
        frame.setContentPane(panel);
        frame.pack();
        RefineryUtilities.centerFrameOnScreen(frame);
        frame.setVisible(true);

    }

    public static void main(String[] args) {
        FreeChart4jTest2 ttt = new FreeChart4jTest2();
        ttt.run();
    }
}
