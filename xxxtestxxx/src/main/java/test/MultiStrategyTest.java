package test;

import org.hou.stocktank.base.IndicatorName;
import org.hou.stocktank.base.Level;
import org.hou.stocktank.base.Param;
import org.hou.stocktank.base.StrategyFactory;
import org.hou.stocktank.strategies.*;
import org.hou.stocktank.utils.BossaPRNLoader;
import org.hou.stocktank.utils.MBankProfitCriterion;
import org.hou.stocktank.utils.TimeSeriesUtils;
import org.ta4j.core.Strategy;
import org.ta4j.core.TimeSeries;
import org.ta4j.core.TimeSeriesManager;
import org.ta4j.core.TradingRecord;
import org.ta4j.core.analysis.criteria.*;


public class MultiStrategyTest {

    private static TradingRecord run(StrategyFactory sf, TimeSeries series) {
        Strategy strategy = sf.buildStrategy(series);
        TimeSeriesManager manager = new TimeSeriesManager(series);
        TradingRecord tradingRecord = manager.run(strategy);
        System.out.println(sf.toString());
        System.out.println(" Name: " + sf.getClass().getName());
        System.out.println(" Ticks: " + series.getTickCount());
        System.out.println(" From: " + series.getFirstTick().getBeginTime());
        System.out.println(" To:   " + series.getLastTick().getEndTime());
        // MBank Profit
        MBankProfitCriterion mbankProfitCriterion = new MBankProfitCriterion();
        System.out.println("MBank profit: " + mbankProfitCriterion.calculate(series, tradingRecord));

        // Total profit
        TotalProfitCriterion totalProfit = new TotalProfitCriterion();
        System.out.println("Total profit: " + totalProfit.calculate(series, tradingRecord));
        // Number of ticks
        System.out.println("Number of ticks: " + new NumberOfTicksCriterion().calculate(series, tradingRecord));
        // Average profit (per tick)
        System.out.println("Average profit (per tick): " + new AverageProfitCriterion().calculate(series, tradingRecord));
        // Number of trades
        System.out.println("Number of trades: " + new NumberOfTradesCriterion().calculate(series, tradingRecord));
        // Profitable trades ratio
        System.out.println("Profitable trades ratio: " + new AverageProfitableTradesCriterion().calculate(series, tradingRecord));
        // Maximum drawdown
        System.out.println("Maximum drawdown: " + new MaximumDrawdownCriterion().calculate(series, tradingRecord));
        // Reward-risk ratio
        System.out.println("Reward-risk ratio: " + new RewardRiskRatioCriterion().calculate(series, tradingRecord));
        // Total transaction cost
        System.out.println("Total transaction cost (from $1000): " + new LinearTransactionCostCriterion(1000, 0.0039).calculate(series, tradingRecord));
        // Buy-and-hold
        System.out.println("Buy-and-hold: " + new BuyAndHoldCriterion().calculate(series, tradingRecord));
        // Total profit vs buy-and-hold
        System.out.println("Custom strategy profit vs buy-and-hold strategy profit: " + new VersusBuyAndHoldCriterion(totalProfit).calculate(series, tradingRecord));

        if (tradingRecord.getLastOrder() != null) {
            String lastOrder = tradingRecord.getLastOrder().isBuy() ? "BUY" : "SELL";
            int daysFromOrder = series.getTickCount() - tradingRecord.getLastOrder().getIndex();
            System.out.println(" Last order: " + lastOrder + " : " + daysFromOrder + " days ago.");
        }
        System.out.println("------------------------------------------------------------------------------------------------------------------------------");
        return tradingRecord;
    }

    public static void main(String[] args) {
        System.out.println("--- START ---");
        String stockName = "cdprojekt";
        TimeSeries series = BossaPRNLoader.load(stockName, "data/_IMPORT/" + stockName + ".prn");
        series = TimeSeriesUtils.aggregateToDays(series);

// MACD
        StrategyFactory macdStrategyFactory = new MACDStrategyFactory()
                .setParam(IndicatorName.MACD, Param.SHORT_TIME_FRAME, 77)
                .setParam(IndicatorName.MACD, Param.LONG_TIME_FRAME, 163)
                .setParam(IndicatorName.EMA, Param.TIME_FRAME, 43);

        TradingRecord macdTradingRecord = run(macdStrategyFactory, series);

// CCI
        StrategyFactory cciStrategyFactory = new CCIStrategyFactory()
                .withParam(IndicatorName.CCI, Param.SHORT_TIME_FRAME, 27)
                .withParam(IndicatorName.CCI, Param.LONG_TIME_FRAME, 36)
                .withLevel(IndicatorName.CCI, Level.BUY_LEVEL, 118)
                .withLevel(IndicatorName.CCI, Level.SELL_LEVEL, 111);

        TradingRecord cciTradingRecord = run(cciStrategyFactory, series);

// RSI
        StrategyFactory rsiStrategyFactory = new RSIStrategyFactory()
                .setParam(IndicatorName.RSI, Param.TIME_FRAME, 14)
                .withLevel(IndicatorName.RSI, Level.BUY_LEVEL, 39)
                .withLevel(IndicatorName.RSI, Level.SELL_LEVEL, 88);

        TradingRecord rsiTradingRecord = run(rsiStrategyFactory, series);

// STOCH_K
        //cdprojekt: [STOCH_K : (time frame=7) ], [STOCH_D : () ] Levels (buy level=43, sell level=92)  :: [born:199c]
        StrategyFactory stochasticStrategyFactory = new StochasticStrategyFactory()
                .setParam(IndicatorName.STOCH_K, Param.TIME_FRAME, 7)
                .withLevel(IndicatorName.STOCH_K, Level.BUY_LEVEL, 43)
                .withLevel(IndicatorName.STOCH_K, Level.SELL_LEVEL, 92);

        TradingRecord stochasticTradingRecord = run(stochasticStrategyFactory, series);

// MovingMomentum
        //avg=4.165390302292216,  max=5.105270407514112 : org.hou.stocktank.strategies.MovingMomentumStrategyFactory, [MACD : (short time frame=11, long time frame=135) ], [EMA_MACD : (time frame=52) ], [SHORT_EMA : (time frame=8) ], [LONG_EMA : (time frame=20) ], [STOCH_K : (time frame=7) ] Levels (buy level=39, sell level=85)  :: [born:142c]
        StrategyFactory movingMomentumStrategyFactory = new MovingMomentumStrategyFactory()
                .setParam(IndicatorName.EMA_MACD, Param.TIME_FRAME, 52)
                .setParam(IndicatorName.MACD, Param.SHORT_TIME_FRAME, 13)
                .setParam(IndicatorName.MACD, Param.LONG_TIME_FRAME, 111)
                .setParam(IndicatorName.SHORT_EMA, Param.TIME_FRAME, 5)
                .setParam(IndicatorName.LONG_EMA, Param.TIME_FRAME, 22)
                .setParam(IndicatorName.STOCH_K, Param.TIME_FRAME, 10)
                .withLevel(IndicatorName.STOCH_K, Level.BUY_LEVEL, 51)
                .withLevel(IndicatorName.STOCH_K, Level.SELL_LEVEL, 80);

        TradingRecord movingMomentumTradingRecord = run(movingMomentumStrategyFactory, series);

// WILLIAMS
        StrategyFactory williamsRStrategyFactory = new WilliamsRStrategyFactory()
                .withParam(IndicatorName.WILLIAMS_R, Param.TIME_FRAME, 8)
                .withLevel(IndicatorName.WILLIAMS_R, Level.BUY_LEVEL, 56)
                .withLevel(IndicatorName.WILLIAMS_R, Level.SELL_LEVEL, 21);

        TradingRecord williamsTradingRecord = run(williamsRStrategyFactory, series);

// AROON UP
        StrategyFactory aroonUPStrategyFactory = new AroonTrendStregthStrategyFactory()
                .withParam(IndicatorName.AROON_UP, Param.TIME_FRAME, 8)
                .withLevel(IndicatorName.AROON_UP, Level.TRIGGER_LEVEL, 22);

        TradingRecord aroonUPTradingRecord = run(aroonUPStrategyFactory, series);

// CMO
        StrategyFactory cmoStrategyFactory = new CMOStrategyFactory()
                .setParam(IndicatorName.CMO, Param.TIME_FRAME, 18)
                .withLevel(IndicatorName.CMO, Level.BUY_LEVEL, 38)
                .withLevel(IndicatorName.CMO, Level.SELL_LEVEL, 51);

        TradingRecord cmoTradingRecord = run(rsiStrategyFactory, series);


//        ChartBuilder chartUtils = new ChartBuilder(series)
//                .withTitle(stockName + " : MultiModel")
//                .withIndicator(new ClosePriceIndicator(series), "Price")
//                .withIndicator("CacheFlow", new CashFlow(series, tradingRecord), Color.black)
//                .withLevel("a", 30, Color.black)
//                .withLevel("a", 70, Color.black)
                //.withStrategyFactory(stockName, strategyFactory, Color.yellow)
                //.withTradingRecord(macdTradingRecord)
//                .withBuySignals(macdTradingRecord)
//                .withBuySignals(cciTradingRecord)
//                .withBuySignals(rsiTradingRecord)
//                .withBuySignals(stochasticTradingRecord)
//                .withBuySignals(movingMomentumTradingRecord)
//                .withBuySignals(williamsTradingRecord)
//                .withBuySignals(aroonUPTradingRecord)
//                .withBuySignals(cmoTradingRecord)

//                .withSellSignals(macdTradingRecord)
//                .withSellSignals(cciTradingRecord)
//                .withSellSignals(rsiTradingRecord)
//                .withSellSignals(stochasticTradingRecord)
//                .withSellSignals(movingMomentumTradingRecord)
//                .withSellSignals(williamsTradingRecord)
//                .withSellSignals(aroonUPTradingRecord)
//                .withSellSignals(cmoTradingRecord);
//
//
//        chartUtils.displayChart();

        System.out.println("--- END ---");
    }
}
