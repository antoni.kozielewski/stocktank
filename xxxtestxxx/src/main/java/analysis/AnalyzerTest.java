package analysis;

import org.hou.stocktank.analysis.AnalysisResult;
import org.hou.stocktank.analysis.analyzers.Analyzer;
import org.hou.stocktank.analysis.analyzers.wrappers.FibonacciLongRangeAnalyzerWrapper;
import org.hou.stocktank.services.BossaSingleFileDataProviderService;
import org.hou.stocktank.utils.TimeSeriesUtils;
import org.ta4j.core.TimeSeries;
import java.time.ZoneId;
import java.time.ZonedDateTime;


public class AnalyzerTest {

    public void run() {
        System.out.println("-- -- START -- --");
        String stock = "CIECH";

        BossaSingleFileDataProviderService dataProviderService = new BossaSingleFileDataProviderService();
        dataProviderService.setDataPath("data/mstall.zip");

        TimeSeries series = dataProviderService.load(stock);
        series = TimeSeriesUtils.getSubseries(ZonedDateTime.of(2013, 01, 01, 0, 0, 0, 0, ZoneId.systemDefault()), series);

//        FibonacciSupportResistanceAnalyzer analyzer = new FibonacciSupportResistanceAnalyzer(new SimplePivotPointIndicator(series, 10, 5, 0.06, 0.1));
        Analyzer analyzer = new FibonacciLongRangeAnalyzerWrapper(series);

        AnalysisResult result = analyzer.getValue(series.getEndIndex());
        System.out.println("Stock : " + stock);
        System.out.println(" :: " + result.getDescription());
        System.out.println(" :: " + result.getSource());
        System.out.println("Close price   :: " + series.getTick(series.getEndIndex()).getClosePrice());
        System.out.println("Market state  :: " + result.getMarketState());
        System.out.println("Value         :: " + result.getValue());
        System.out.println("Description   :: " + result.getDescription());

        for(int i=series.getEndIndex() -100; i<series.getEndIndex(); i++){
            result = analyzer.getValue(i);
            System.out.println(series.getTick(i).getEndTime() + " | " + result.getMarketState() + " :: " + result.getDescription());
        }


    }

    public static void main(String[] args) {
        AnalyzerTest inst = new AnalyzerTest();
        inst.run();
    }
}
