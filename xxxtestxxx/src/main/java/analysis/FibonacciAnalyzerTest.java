package analysis;

import org.hou.stocktank.indicators.pivotpoints.FibonacciSupportResistanceIndicator;
import org.hou.stocktank.indicators.pivotpoints.SimplePivotPointIndicator;
import org.hou.stocktank.services.BossaSingleFileDataProviderService;
import org.hou.stocktank.utils.TimeSeriesUtils;
import org.ta4j.core.Decimal;
import org.ta4j.core.TimeSeries;

import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;


public class FibonacciAnalyzerTest {

    public void run() {
        System.out.println("-- -- START -- --");
        String stock = "ATAL";

        BossaSingleFileDataProviderService dataProviderService = new BossaSingleFileDataProviderService();
        dataProviderService.setDataPath("data/mstall.zip");

        TimeSeries series = dataProviderService.load(stock);
        series = TimeSeriesUtils.getSubseries(ZonedDateTime.of(2013, 01, 01, 0, 0, 0, 0, ZoneId.systemDefault()), series);
        // long range
        //SimplePivotPointIndicator pivotPointIndicator = new SimplePivotPointIndicator(series, 20, 10, 0.12, 0.18);

        // short range
        SimplePivotPointIndicator pivotPointIndicator = new SimplePivotPointIndicator(series, 10, 5, 0.06, 0.1);


        FibonacciSupportResistanceIndicator indicator = new FibonacciSupportResistanceIndicator(pivotPointIndicator);

        System.out.println("Stock : " + stock);
        System.out.println("");

        for(int i=0; i<series.getEndIndex(); i++){
            if (pivotPointIndicator.getValue(i).getIndex() == i) {
                Decimal value = series.getTick(i).getClosePrice();
                List<Decimal> result = indicator.getValue(i-1);

                //System.out.println(" data " + result.stream().map(x -> String.format("%2f",x.toDouble())).collect(Collectors.joining(", ")));
                List<Decimal> diversion = result.stream().map(x -> x.minus(value)).collect(Collectors.toList());
                //System.out.println(" min  " + diversion.stream().map(x -> String.format("%2f",x.toDouble())).collect(Collectors.joining(", ")));
                List<Decimal> percent = diversion.stream().map(x -> Decimal.HUNDRED.multipliedBy(x.dividedBy(value))).collect(Collectors.toList());
                //System.out.println(" %    " + percent.stream().map(x -> String.format("%2f",x.toDouble())).collect(Collectors.joining(", ")));
                Optional<Decimal> minimum = percent.stream().map(x -> x.abs()).min(Decimal::compareTo);
                Double res = (minimum.isPresent()) ? minimum.get().toDouble() : 100;
                if (res < 100) {
                    System.out.println(series.getTick(i).getEndTime() + " | close = " + series.getTick(i).getClosePrice() + " ||| " + " delta[%] : " + String.format("%.2f",res));
                } else {
                    System.out.println(" unknown ");
                }
            }
        }


    }

    public static void main(String[] args) {
        FibonacciAnalyzerTest inst = new FibonacciAnalyzerTest();
        inst.run();
    }
}
