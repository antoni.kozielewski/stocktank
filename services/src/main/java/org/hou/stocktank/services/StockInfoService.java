package org.hou.stocktank.services;

import org.ta4j.core.TimeSeries;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

@Service
public class StockInfoService {
    private final static String CHART_URL = "chart";
    private final static String CLOSE_PRICE               = "close price  ";
    private final static String FIVE_SESSIONS_AVG_VOLUMEN = "avg vol (5 sessions)  ";
    private final static String FIVE_SESSIONS_CHANGE =      "change  (5 sessions)  ";
    private final static String TEN_SESSIONS_CHANGE =       "change  (10 sessions) ";
    private final static String TWENTY_SESSIONS_CHANGE =    "change  (20 sessions) ";

    @Autowired
    BossaSingleFileDataProviderService dataProvider;

    public Map<String, String> getInfoMap(String stockName) {
        Map<String, String> result = new LinkedHashMap<>();
        result.put(CHART_URL, getChartUrl(stockName));
        TimeSeries series = dataProvider.load(stockName);
        result.put(CLOSE_PRICE, String.valueOf(series.getTick(series.getEndIndex()).getClosePrice().toDouble()));
        result.put(FIVE_SESSIONS_AVG_VOLUMEN, getNSessionsAvgVolumen(5, series));
        result.put(FIVE_SESSIONS_CHANGE, getNSessionsChange(5, series));
        result.put(TEN_SESSIONS_CHANGE, getNSessionsChange(10, series));
        result.put(TWENTY_SESSIONS_CHANGE, getNSessionsChange(20, series));
        return result;
    }

    private String getChartUrl(String stockName) {
        return "http://www.bankier.pl/inwestowanie/profile/quote.html?symbol="+stockName.toUpperCase();
    }

    private String getNSessionsAvgVolumen(int n, TimeSeries series){
        double value = 0d;
        double volumen = 0;
        for(int i= series.getTickCount() - n -1; i < series.getTickCount(); i++) {
            volumen = volumen + series.getTick(i).getVolume().toDouble();
            value = value + series.getTick(i).getClosePrice().toDouble();
        }
        return String.format("%,d",(Math.round(100*volumen * value / n))/100) + " PLN";
    }

    private String getNSessionsChange(int n, TimeSeries series){
        double start = series.getTick(series.getTickCount()-n-1).getClosePrice().toDouble();
        double end = series.getLastTick().getClosePrice().toDouble();
        return String.valueOf( String.format("%.2f",(Math.round(10000*(end - start)/start))/100d)) + " %";
    }







}
