package org.hou.stocktank.services.storage;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface AnalysisRecordRepository extends CrudRepository<AnalysisRecord, String> {

    List<AnalysisRecord> findByStock(String stock);
}
