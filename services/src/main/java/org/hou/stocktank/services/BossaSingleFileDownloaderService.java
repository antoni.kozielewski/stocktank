package org.hou.stocktank.services;

import org.hou.stocktank.utils.BossaSingleFileDownloader;
import org.joda.time.DateTime;
import org.slf4j.LoggerFactory;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.IOException;
import java.util.Enumeration;
import java.util.zip.ZipEntry;
import java.util.zip.ZipException;
import java.util.zip.ZipFile;

@Service
@ConfigurationProperties(prefix = "stocktank-configuration")
public class BossaSingleFileDownloaderService {
    private static final org.slf4j.Logger logger = LoggerFactory.getLogger(BossaSingleFileDownloaderService.class);
    private String dataURL;
    private String dataPath;

    public void run() throws IOException {
        if(shouldDownload()) {
            BossaSingleFileDownloader downloader = new BossaSingleFileDownloader();
            downloader.download();
        }
    }

    private Boolean shouldDownload() throws IOException {
        File f = new File(dataPath);
        if (!f.exists()) return true;
        ZipFile zipFile = null;
        try {
            zipFile = new ZipFile(f);
        } catch (ZipException e) {
            logger.info(" file corrupted - will be redownloaded.");
            f.delete();
            return true;
        }
        Enumeration<? extends ZipEntry> entries = zipFile.entries();
        while(entries.hasMoreElements()){
            ZipEntry entry = entries.nextElement();
            if (!entry.isDirectory()){
                DateTime now = new DateTime();
                DateTime fileTime = new DateTime(entry.getTime());
                if (now.minusDays(1).isAfter(fileTime)) {
                    logger.info(" Data file is too old {} - need to download new one;", fileTime);
                    return true;
                }
                logger.info(" Data file is ok: {} ", fileTime);
                return false;
            }
        }
        logger.info(" There is no data file or some other shit happend. ");
        return true;
    }

    public String getDataURL() {
        return dataURL;
    }

    public String getDataPath() {
        return dataPath;
    }

    public void setDataURL(String url) {
        this.dataURL = url;
    }

    public void setDataPath(String dataPath) {
        this.dataPath = dataPath;
    }
}
