package org.hou.stocktank.services;

import org.ta4j.core.Tick;
import org.ta4j.core.TimeSeries;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@Service
@ConfigurationProperties(prefix = "stocktank-configuration")
public class StocksFilter {

    @Autowired
    private BossaSingleFileDataProviderService dataProviderService;

    private int stockFilterSessions;
    private int stockFilterMinSessionsVolumePerc80;

    public Boolean isInteresting(String stock) {
        TimeSeries series = dataProviderService.load(stock);
        List<Double> list = new ArrayList<>();
        if (series.getTickCount() == 0) return false;
        for (int i = series.getTickCount() - 1 - stockFilterSessions; i < series.getTickCount(); i++) {
            Tick tick = series.getTick(i);
            list.add(tick.getVolume().multipliedBy(tick.getClosePrice()).toDouble());
        }
        Collections.sort(list);
        if (list.get(list.size() * 4 / 5) > stockFilterMinSessionsVolumePerc80) return true;
        return false;
    }

    public int getStockFilterSessions() {
        return stockFilterSessions;
    }

    public int getStockFilterMinSessionsVolumePerc80() {
        return stockFilterMinSessionsVolumePerc80;
    }

    public void setStockFilterSessions(int stockFilterSessions) {
        this.stockFilterSessions = stockFilterSessions;
    }

    public void setStockFilterMinSessionsVolumePerc80(int stockFilterMinSessionsVolumePerc80) {
        this.stockFilterMinSessionsVolumePerc80 = stockFilterMinSessionsVolumePerc80;
    }
}
