package org.hou.stocktank.services.transport;


import org.ta4j.core.Tick;

import java.time.ZonedDateTime;

public class TickRecord {
    private String stock;
    private ZonedDateTime beginDateTime;
    private ZonedDateTime endDateTime;
    private double open;
    private double high;
    private double low;
    private double close;
    private double volume;

    public TickRecord(String stock, Tick tick) {
        this.stock = stock;
        this.beginDateTime = tick.getBeginTime();
        this.endDateTime = tick.getBeginTime();
        this.open = tick.getOpenPrice().toDouble();
        this.high = tick.getMaxPrice().toDouble();
        this.low = tick.getMinPrice().toDouble();
        this.close = tick.getClosePrice().toDouble();
        this.volume = tick.getVolume().toDouble();
    }

    public double getOpen() {
        return open;
    }

    public double getHigh() {
        return high;
    }

    public double getLow() {
        return low;
    }

    public double getClose() {
        return close;
    }

    public String getStock() {
        return stock;
    }

    public ZonedDateTime getBeginDateTime() {
        return beginDateTime;
    }

    public ZonedDateTime getEndDateTime() {
        return endDateTime;
    }

    public double getVolume() {
        return volume;
    }
}
