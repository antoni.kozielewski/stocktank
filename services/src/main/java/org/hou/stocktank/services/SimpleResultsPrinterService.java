package org.hou.stocktank.services;

import com.mitchtalmadge.asciigraph.ASCIIGraph;
import org.hou.stocktank.analysis.MarketState;
import org.hou.stocktank.services.storage.AnalysisRecord;
import org.hou.stocktank.services.storage.SignalRecord;
import org.hou.stocktank.services.storage.SignalRecordTXTUtil;
import org.hou.stocktank.utils.StocktankTXTUtil;
import org.hou.stocktank.utils.TimeSeriesUtils;
import org.springframework.stereotype.Service;
import org.ta4j.core.TimeSeries;

import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
public class SimpleResultsPrinterService {

    private void printSeparator() {
        System.out.println("");
        System.out.println("");
        System.out.println("");
        System.out.println("");
    }

    public void printHeader(String title) {
        printSeparator();
        System.out.println(StocktankTXTUtil.fillCenter("","#",167));
        System.out.println(StocktankTXTUtil.fillCenter("","#",167));
        System.out.println(StocktankTXTUtil.fillCenter(""," ",167));
        System.out.println(StocktankTXTUtil.fillCenter( StocktankTXTUtil.fillCenter(title," ", 80) ,"#",167));
        System.out.println(StocktankTXTUtil.fillCenter(""," ",167));
        System.out.println(StocktankTXTUtil.fillCenter("","#",167));
        System.out.println(StocktankTXTUtil.fillCenter("","#",167));
    }

    private void printStockInfo(String stockName, Map<String, String> stockInfoData) {
        System.out.println(StocktankTXTUtil.fillCenter("","-",167));
        System.out.println(StocktankTXTUtil.fillRight("|  " + stockName, " ",166) + "|");
        for(String key : stockInfoData.keySet()){
            System.out.println("|" + StocktankTXTUtil.fillLeft( key, " ",63) + ": " + StocktankTXTUtil.fillRight( stockInfoData.get(key), " ",100) + "|");
        }
        System.out.println(StocktankTXTUtil.fillCenter("","-",167));
    }

    private void printSignals(List<SignalRecord> signals, int maxScanningDays) {
        System.out.println("");
        List<SignalRecord> buys = signals.stream().filter(x -> x.getSignal().equals("BUY")).collect(Collectors.toList());
        List<SignalRecord> sells = signals.stream().filter(x -> x.getSignal().equals("SELL")).collect(Collectors.toList());
        Collections.sort(buys, Comparator.comparing(SignalRecord::getDate).reversed());
        Collections.sort(sells, Comparator.comparing(SignalRecord::getDate));
        buys.stream().forEach(b -> {
            b.setStrategyClassName(""); // remove StrategyClassName
            System.out.println(SignalRecordTXTUtil.prettyString(b, maxScanningDays));
        }) ;
        System.out.println("");
        System.out.println("- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -  |          |          |          |          |          |");
        System.out.println("");
        sells.stream().forEach(s -> {
            s.setStrategyClassName(""); // remove StrategyClassName
            System.out.println(SignalRecordTXTUtil.prettyString(s, maxScanningDays));
        }) ;
        System.out.println("");
    }

    private void printAnalysis(List<AnalysisRecord> analysisRecords) {
        System.out.println("");
        analysisRecords.stream()
                .filter(x -> x != null)
                .filter(x -> !x.getMarketState().equals(MarketState.UNKNOWN.name()))
                .forEach(x -> System.out.println(SignalRecordTXTUtil.prettyString(x)));
        System.out.println("");
    }

    private void printChart(String stockName) { // FIXME przerobic tak zeby nie bylo nic zahardcodowane
        BossaSingleFileDataProviderService dataProviderService = new BossaSingleFileDataProviderService();
        dataProviderService.setDataPath("data/mstall.zip");
        TimeSeries series = dataProviderService.load(stockName);
        // ucina do poczatku 2018
        double[] x = TimeSeriesUtils.getSubseries(ZonedDateTime.of(2018, 1,1,0,0,0,0, ZoneId.systemDefault()),series).getTickData().stream().map(t -> t.getClosePrice().toDouble()).mapToDouble(Double::doubleValue).toArray();
        System.out.println(ASCIIGraph.fromSeries(x).withNumRows(25).plot());
    }

    public void printStockRaport(String stockName, List<SignalRecord> signals, List<AnalysisRecord> analysisRecords, Map<String, String> stockInfoData, int maxScanningDays) {
        printSeparator();
        printStockInfo(stockName, stockInfoData);
        printChart(stockName);
        printSignals(signals, maxScanningDays);
        printAnalysis(analysisRecords);
    }

}
