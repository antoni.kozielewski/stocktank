package org.hou.stocktank.services;

import org.hou.stocktank.utils.StocktankTXTUtil;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;


@ConfigurationProperties(prefix = "stocktank-configuration")
public class ProcessController implements ApplicationRunner {
    private List<String> stocks = new ArrayList<>();
    private List<String> walletStocks = new ArrayList<>();
    private List<String> strategies = new ArrayList<>();
    private List<String> analyzers = new ArrayList<>();
    private Boolean dataReady = false;
    private String version;
    private int stockFilterMinSessionsVolumePerc80;
    private int maxScanningDays;
    private int stockFilterSessions;

    @Autowired
    private StockScannerService scannerService;

    @Autowired
    private ResultsService resultsService;

    @Autowired
    private BossaSingleFileDownloaderService bossaSingleFileDownloaderService;

    @Autowired
    private StocksFilter stockFilter;

    @Override
    public void run(ApplicationArguments applicationArguments) throws Exception {
        printInfoHeader();
        dataReady = false;
        bossaSingleFileDownloaderService.run();
        // analyze
        int counter = 0;
        System.out.print(StocktankTXTUtil.progessBar(100, 0));
        for (String stockName : stocks) {
            if (stockFilter.isInteresting(stockName) || walletStocks.contains(stockName)) {
                scannerService.scan(stockName);
            }
            counter++;
            System.out.print(StocktankTXTUtil.progessBar(100, 100 * counter/stocks.size()));
        }
        // give results
        resultsService.showResults();
        dataReady = true;
    }

    private void printInfoHeader() {
        System.out.println(StocktankTXTUtil.fillCenter("","-",167));
        System.out.println(StocktankTXTUtil.fillRight("| Stock Tank Scanner v. " + version," ",166) + "|");
        System.out.println(StocktankTXTUtil.fillRight("| "," ",166) + "|");
        System.out.println(StocktankTXTUtil.fillRight("| " + DateTime.now().toString(DateTimeFormat.forPattern("YYYY-MM-d"))," ",166) + "|");
        System.out.println(StocktankTXTUtil.fillRight("| "," ",166) + "|");
        System.out.println(StocktankTXTUtil.fillRight("| max scanning sessions     : " + maxScanningDays," ",166) + "|");
        System.out.println(StocktankTXTUtil.fillRight("| stock volume filter days  : " + stockFilterSessions," ",166) + "|");
        System.out.println(StocktankTXTUtil.fillRight("| stock volume filter perc80: " + stockFilterMinSessionsVolumePerc80," ",166) + "|");
        System.out.println(StocktankTXTUtil.fillRight("| "," ",166) + "|");
        System.out.println(StocktankTXTUtil.fillRight("| Wallet stocks:"," ",166) + "|");
        for(String stock : walletStocks) {
            System.out.println(StocktankTXTUtil.fillRight("|    - " + stock , " ", 166) + "|");
        }
        System.out.println(StocktankTXTUtil.fillCenter("","-",167));
        System.out.println("");
        System.out.println("");
    }

    public List<String> getStrategies() {
        return strategies;
    }

    public List<String> getStocks() {
        return stocks;
    }

    public void setStocks(List<String> stocks) {
        this.stocks = stocks;
    }

    public void setStrategies(List<String> strategies) {
        this.strategies = strategies;
    }

    public List<String> getAnalyzers() {
        return analyzers;
    }

    public void setAnalyzers(List<String> analyzers) {
        this.analyzers = analyzers;
    }

    public int getMaxScanningDays() {
        return maxScanningDays;
    }

    public int getStockFilterMinSessionsVolumePerc80() {
        return stockFilterMinSessionsVolumePerc80;
    }

    public int getStockFilterSessions() {
        return stockFilterSessions;
    }

    public void setStockFilterMinSessionsVolumePerc80(int stockFilterMinSessionsVolumePerc80) {
        this.stockFilterMinSessionsVolumePerc80 = stockFilterMinSessionsVolumePerc80;
    }

    public void setMaxScanningDays(int maxScanningDays) {
        this.maxScanningDays = maxScanningDays;
    }

    public void setStockFilterSessions(int stockFilterSessions) {
        this.stockFilterSessions = stockFilterSessions;
    }

    public List<String> getWalletStocks() {
        return walletStocks;
    }

    public void setWalletStocks(List<String> walletStocks) {
        this.walletStocks = walletStocks;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String getVersion() {
        return version;
    }
}
