package org.hou.stocktank.services;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import java.util.Locale;

@SpringBootApplication
@Configuration
@EnableConfigurationProperties
public class StockTankApplication {

	public static void main(String[] args) {
		Locale.setDefault(new Locale("us", "US"));
		SpringApplication.run(StockTankApplication.class, args);
	}
}
