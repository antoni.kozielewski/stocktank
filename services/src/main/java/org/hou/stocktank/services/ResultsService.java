package org.hou.stocktank.services;

import org.hou.stocktank.services.storage.AnalysisRecord;
import org.hou.stocktank.services.storage.AnalysisRecordRepository;
import org.hou.stocktank.services.storage.SignalRecord;
import org.hou.stocktank.services.storage.SignalRecordRepository;
import org.hou.stocktank.strategies.IchimokuStrategyFactory;
import org.hou.stocktank.strategies.ParabolicStopAndReverseStrategyFactory;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Service
@ConfigurationProperties(prefix = "stocktank-configuration")
public class ResultsService {
    private List<String> walletStocks = new ArrayList<>();
    private int maxScanningDays;

    @Autowired
    SimpleResultsPrinterService printerService;

    @Autowired
    SignalRecordRepository signalRepository;

    @Autowired
    AnalysisRecordRepository analysisRepository;

    @Autowired
    StockInfoService stockInfoService;

    Long minSignalsForBestBuys;

    public void showResults() {
        showScannins();
        showWallet();
    }

    private Long getFromisEpoch(int days) {
        return DateTime.now().minusDays(days).getMillis() / 1000;
    }

    private void showStocks(List<String> stocks) {
        for (String stockName : stocks) {
            List<SignalRecord> signals = signalRepository.findLastSignalsByStock(stockName);
            List<AnalysisRecord> analysis = analysisRepository.findByStock(stockName);
            Map<String, String> info = stockInfoService.getInfoMap(stockName);
            printerService.printStockRaport(stockName, signals, analysis, info, maxScanningDays);
        }
    }

    private void showScannins() {
        int daysBack = 1;

        if (DateTime.now().getDayOfWeek() == 6) {
            daysBack = 2;
        } // saturday
        if (DateTime.now().getDayOfWeek() == 7) {
            daysBack = 3;
        } // sunday
        if (DateTime.now().getDayOfWeek() == 1) {
            daysBack = 4;
        } // monday morning

        // best SAR & ichimoku
        List<String> sarBuys = signalRepository.findStocksWithStrategySignalAfterTimestamp(ParabolicStopAndReverseStrategyFactory.class.getName(), "BUY", getFromisEpoch(daysBack));
        List<String> ichimokuBuys = signalRepository.findStocksWithStrategySignalAfterTimestamp(IchimokuStrategyFactory.class.getName(), getFromisEpoch(daysBack));

        printerService.printHeader("SAR RECOMMENDATIONS [last " + daysBack + " days]");
        showStocks(sarBuys);

        printerService.printHeader("ICHIMOKU RECOMMENDATIONS [last " + daysBack + " days]");
        showStocks(ichimokuBuys);

        List<String> bestBuys = signalRepository.findStocksWithNSignalsAfterTimestamp(minSignalsForBestBuys, "BUY", getFromisEpoch(4 + daysBack));
        printerService.printHeader("BUY RECOMMENDATIONS ");
        showStocks(bestBuys);
    }

    private void showWallet() {
        printerService.printHeader(" WALLET STOCKS ");
        showStocks(walletStocks);
    }

    public List<String> getWalletStocks() {
        return walletStocks;
    }

    public void setWalletStocks(List<String> walletStocks) {
        this.walletStocks = walletStocks;
    }

    public int getMaxScanningDays() {
        return maxScanningDays;
    }

    public void setMaxScanningDays(int maxScanningDays) {
        this.maxScanningDays = maxScanningDays;
    }

    public Long getMinSignalsForBestBuys() {
        return minSignalsForBestBuys;
    }

    public void setMinSignalsForBestBuys(Long minSignalsForBestBuys) {
        this.minSignalsForBestBuys = minSignalsForBestBuys;
    }
}
