package org.hou.stocktank.charts;

public enum MagicRendererType {
    LINE,
    SPLINE,
    DEFAULT,
    CANDLESTICK;
}
