package org.hou.stocktank.charts;

import java.awt.*;

public class MagicMarker {
    private double value = 0;
    private String label = "";
    private Paint paint = null;

    public MagicMarker(double value, String label, Paint paint) {
        this.value = value;
        this.label = label;
        this.paint = paint;
    }

    public double getValue() {
        return value;
    }

    public String getLabel() {
        return label;
    }

    public Paint getPaint() {
        return paint;
    }

    public void setValue(double value) {
        this.value = value;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public void setPaint(Paint paint) {
        this.paint = paint;
    }
}

