package org.hou.stocktank.charts;

import java.util.ArrayList;
import java.util.List;

public class MagicLayer {
    private String name;
    private List<MagicDataPlot> dataPlots = new ArrayList<>();
    private List<MagicMarker> vMarkers = new ArrayList<>();
    private List<MagicMarker> hMarkers = new ArrayList<>();

    public MagicLayer(String name) {
        this.name = name;
    }

    public MagicLayer addDataPlot(MagicDataPlot chart) {
        dataPlots.add(chart);
        return this;
    }

    public List<MagicDataPlot> getDataPlots() {
        return dataPlots;
    }

    public void addVMarker(MagicMarker marker) {
        vMarkers.add(marker);
    }

    public void addHMarker(MagicMarker marker) {
        hMarkers.add(marker);
    }

    public List<MagicMarker> getVMarkers() {
        return vMarkers;
    }

    public List<MagicMarker> getHMarkers() {
        return hMarkers;
    }

    public String getName() {
        return name;
    }

}
