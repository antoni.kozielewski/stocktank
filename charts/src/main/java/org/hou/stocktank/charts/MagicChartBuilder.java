package org.hou.stocktank.charts;

import java.awt.*;
import java.util.*;
import java.util.List;

import org.hou.stocktank.base.*;
import org.hou.stocktank.utils.TimeSeriesUtils;
import org.jfree.chart.plot.Plot;
import org.jfree.data.time.Minute;
import org.joda.time.DateTime;

import org.ta4j.core.Indicator;
import org.ta4j.core.Order;
import org.ta4j.core.TimeSeries;
import org.ta4j.core.Trade;
import org.ta4j.core.TradingRecord;
import org.ta4j.core.analysis.CashFlow;
import org.ta4j.core.indicators.helpers.ClosePriceIndicator;
import org.ta4j.core.indicators.EMAIndicator;

public class MagicChartBuilder {
    private StrategyFactory strategyFactory;
    private TimeSeries timeSeries;
    private TradingRecord tradingRecord;

    private String title = "title";
    private boolean showPriceCandles = true;
    private boolean showPriceEma = true;
    private boolean showBuySignals = false;
    private boolean showSellSignals = false;
    private boolean showCashFlow = false;
    private boolean showLevels = false;
    private boolean showIndicators = true;
    private int chartWidth = 800;
    private int chartHeight = 600;
    private DateTime fromDate;
    private DateTime toDate;
    private Map<String,Indicator> additionalIndicators = new HashMap<>();
    private Map<String,MagicLayer> additionalLayers = new HashMap<>();

    private List<Color> indicatorColors = Arrays.asList(Color.GRAY, Color.RED, Color.GREEN, Color.MAGENTA, Color.BLACK);

    public MagicChartBuilder(StrategyFactory strategyFactory, TimeSeries series) {
        this.strategyFactory = strategyFactory;
        this.timeSeries = series;
    }

    public MagicChartBuilder(StrategyFactory strategyFactory, TimeSeries series, TradingRecord tradingRecord) {
        this.strategyFactory = strategyFactory;
        this.timeSeries = series;
        this.tradingRecord = tradingRecord;
    }

    private double indexToDate(int index) {
        return new Minute(Date.from(timeSeries.getTick(index).getBeginTime().toInstant())).getFirstMillisecond();
    }


    public MagicChart build() {
        // PERIOD OF TIME
        if(fromDate == null) fromDate = new DateTime(Date.from(timeSeries.getFirstTick().getBeginTime().toInstant()));
        if(toDate == null) toDate = new DateTime(Date.from(timeSeries.getLastTick().getEndTime().toInstant()));
        TimeSeries series = TimeSeriesUtils.getSubseries(timeSeries.getFirstTick().getBeginTime(), timeSeries.getLastTick().getEndTime(), timeSeries);
        // GENERAL
        MagicChart magicChart = new MagicChart();
        magicChart
                .withTitle(title)
                .withHeight(chartHeight)
                .withWidht(chartWidth);

        // INDICATORS
        if (showIndicators) {
            MagicLayer indicatorsLayer = new MagicLayer("indicators");
            IndicatorsFactory indicatorsFactory = new IndicatorsFactory();
            Map<IndicatorName, Indicator> indicators = indicatorsFactory.buildIndicators(strategyFactory.getIndicatorsDefinitions(), series);
            int counter = 0;
            for (IndicatorName indicatorName : strategyFactory.getIndicatorsDefinitions().keySet()) {
                Color color = indicatorColors.get(counter % indicatorColors.size());
                MagicDataPlot indicatorChart = new MagicDataPlot(indicatorName.name(), indicators.get(indicatorName), series, color, MagicRendererType.LINE);
                indicatorsLayer.addDataPlot(indicatorChart);
                counter++;
                magicChart.withLayer(indicatorsLayer);
            }
        }

        // PRICE
        if (showPriceCandles || showPriceEma) {
            MagicLayer priceLayer = new MagicLayer("Price");
            if (showPriceCandles) {
                priceLayer.addDataPlot(new MagicDataPlot("price candles", series, Color.BLACK, MagicRendererType.CANDLESTICK));
            }
            if (showPriceEma) {
                priceLayer.addDataPlot(new MagicDataPlot("ema", new EMAIndicator(new ClosePriceIndicator(series), 40), series, Color.RED, MagicRendererType.LINE));
            }
            magicChart.withLayer(priceLayer);
        }

        // ADDITIONAL INDICATORS
        if (additionalIndicators.size() > 0){
            MagicLayer additionalIndicatorsLayer = new MagicLayer("additional indicators");
            int counter = 0;
            for (String key : additionalIndicators.keySet()) {
                Indicator indicator = additionalIndicators.get(key);
                Color color = indicatorColors.get(counter % indicatorColors.size());
                MagicDataPlot indicatorChart = new MagicDataPlot(key, indicator, series, color, MagicRendererType.LINE);
                additionalIndicatorsLayer.addDataPlot(indicatorChart);
                counter++;
                magicChart.withLayer(additionalIndicatorsLayer);
            }
        }

        // ADDITIONAL LAYERS
        for(String layerName : additionalLayers.keySet()) {
            magicChart.withLayer(additionalLayers.get(layerName));
        }

        // BUY SIGNALS
        MagicLayer signalsLayer = new MagicLayer("signals");
        if (showBuySignals || showSellSignals) {
            if (tradingRecord == null) throw new RuntimeException("MagicChartBuilder : no tradingRecord !");
            for (Trade trade : tradingRecord.getTrades()) {
                if (showBuySignals) signalsLayer.addVMarker(new MagicMarker(indexToDate(trade.getEntry().getIndex()), "B", Color.BLUE));
                if (showSellSignals) signalsLayer.addVMarker(new MagicMarker(indexToDate(trade.getExit().getIndex()), "S", Color.RED));
            }
            Order lastOrder = tradingRecord.getLastOrder();
            if (lastOrder != null) {
                if (showBuySignals && lastOrder.isBuy()) signalsLayer.addVMarker(new MagicMarker(indexToDate(lastOrder.getIndex()), "B", Color.BLUE));
                if (showSellSignals && lastOrder.isSell()) signalsLayer.addVMarker(new MagicMarker(indexToDate(lastOrder.getIndex()), "S", Color.RED));
            }
        }
        magicChart.withLayer(signalsLayer);

        // LEVELS
        MagicLayer levelsLayer = new MagicLayer("levels");
        if (showLevels) {
            for (IndicatorName indicatorName : strategyFactory.getIndicatorsDefinitions().keySet()) {
                IndicatorDefinition indicatorDefinition = strategyFactory.getIndicatorsDefinitions().get(indicatorName);
                Configurable levels = indicatorDefinition.getLevels();
                for (String level : levels.getParams().keySet()) {
                    levelsLayer.addHMarker(new MagicMarker(levels.getParam(level), level, Color.BLACK));
                }
            }
            magicChart.withLayer(levelsLayer);
        }

        // CASH FLOW
        if (showCashFlow) {
            if (tradingRecord == null) throw new RuntimeException("MagicChartBuilder : no tradingRecord !");
            MagicLayer cashFlowLayer = new MagicLayer("CashFlow");
            Indicator cashFlowIndicator = new CashFlow(series, tradingRecord);
            MagicDataPlot cashFlowPlot = new MagicDataPlot("CashFlow", cashFlowIndicator, series, Color.BLUE, MagicRendererType.LINE);
            cashFlowLayer.addDataPlot(cashFlowPlot);
            magicChart.withLayer(cashFlowLayer);
        }

        return magicChart;
    }

    public MagicChartBuilder withPriceCandles() {
        showPriceCandles = true;
        return this;
    }

    public MagicChartBuilder withoutPriceCandles() {
        showPriceCandles = false;
        return this;
    }

    public MagicChartBuilder withPriceEMA() {
        showPriceEma = true;
        return this;
    }

    public MagicChartBuilder withLevels() {
        this.showLevels = true;
        return this;
    }

    public MagicChartBuilder withoutPriceEMA() {
        showPriceEma = false;
        return this;
    }

    public MagicChartBuilder withTradingRecord(TradingRecord tradingRecord) {
        this.tradingRecord = tradingRecord;
        return this;
    }

    public MagicChartBuilder withBuySignals() {
        this.showBuySignals = true;
        return this;
    }

    public MagicChartBuilder withSellSignals() {
        this.showSellSignals = true;
        return this;
    }

    public MagicChartBuilder withCashFlow() {
        this.showCashFlow = true;
        return this;
    }

    public MagicChartBuilder withTitle(String title) {
        this.title = title;
        return this;
    }

    public MagicChartBuilder withChartWidth(int width) {
        this.chartWidth = width;
        return this;
    }

    public MagicChartBuilder withChartHeight(int height) {
        this.chartHeight = height;
        return this;
    }

    public MagicChartBuilder withFromDate(DateTime date) {
        this.fromDate = date;
        return this;
    }

    public MagicChartBuilder withToDate(DateTime date) {
        this.toDate = date;
        return this;
    }

    public MagicChartBuilder withLastDays(int numberOfDays) {
        this.fromDate = new DateTime().minusDays(numberOfDays);
        this.toDate = null;
        return this;
    }

    public MagicChartBuilder withoutIndicators(){
        showIndicators = false;
        return this;
    }

    public MagicChartBuilder withAdditionalIndicators(Map<String, Indicator> additionalIndicators){
        this.additionalIndicators = additionalIndicators;
        return this;
    }

    public MagicChartBuilder withLayer(String name, MagicLayer layer) {
        this.additionalLayers.put(name, layer);
        return this;
    }

}
