package org.hou.stocktank.charts.googlecharts;


import org.hou.stocktank.analysis.analyzers.IchimokuAnalyzerIndicator;
import org.ta4j.core.TimeSeries;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.ZonedDateTime;

public class IchimokuHmtlChartGenerator {
    private final String ROW_PATTERN = "[new Date(\"%TICK_DATE%\"), %TICK_LOW%, %TICK_OPEN%, %TICK_CLOSE%, %TICK_HIGH%, %TICK_SENKOUA%, %TICK_SENKOUB%, %TICK_TENKANSEN%, %TICK_KIJUNSEN%, %TICK_CHIKOUSPAN%],";
    private IchimokuAnalyzerIndicator ichimokuAnalyzer;
    private TimeSeries timeSeries;
    private String TEMPLATE_PATH = "charts/src/main/resources/templates/basic_google_chart_template.html";
    private String TARGET_DIR = "results/";
    private double CHART_VAXIS_MARGIN_PERCENT = 15; //ile procent calego obszaru to margines z dolu i z gory

    public IchimokuHmtlChartGenerator(IchimokuAnalyzerIndicator analyzerIndicator, TimeSeries timeSeries) {
        this.ichimokuAnalyzer = analyzerIndicator;
        this.timeSeries = timeSeries;
    }

    private String getDateString(ZonedDateTime dateTime) {
        return dateTime.getYear() + "-"
                + dateTime.getMonth() + "-"
                + dateTime.getDayOfMonth() + " "
                + dateTime.getHour() + ":" + dateTime.getMinute() + ":" + dateTime.getSecond();
    }

    private String prepareRow(int i) {
        ichimokuAnalyzer.getValue(i);
        String row = ROW_PATTERN;
        row = row.replace("%TICK_DATE%", getDateString(timeSeries.getTick(i).getBeginTime())); // 2010-01-01 01:00:00 format
        row = row.replace("%TICK_LOW%", (timeSeries.getTick(i) != null) ? timeSeries.getTick(i).getMinPrice().toString() : "");
        row = row.replace("%TICK_HIGH%", (timeSeries.getTick(i) != null) ? timeSeries.getTick(i).getMaxPrice().toString() : "");
        row = row.replace("%TICK_OPEN%", (timeSeries.getTick(i) != null) ? timeSeries.getTick(i).getOpenPrice().toString() : "");
        row = row.replace("%TICK_CLOSE%", (timeSeries.getTick(i) != null) ? timeSeries.getTick(i).getClosePrice().toString() : "");

        row = row.replace("%TICK_SENKOUA%", (ichimokuAnalyzer.getSenkouSpanA().getValue(i) != null) ? ichimokuAnalyzer.getSenkouSpanA().getValue(i).toString() : "");
        row = row.replace("%TICK_SENKOUB%", (ichimokuAnalyzer.getSenkouSpanB().getValue(i) != null) ? ichimokuAnalyzer.getSenkouSpanB().getValue(i).toString() : "");

        row = row.replace("%TICK_TENKANSEN%", (ichimokuAnalyzer.getTenkanSen().getValue(i) != null) ? ichimokuAnalyzer.getTenkanSen().getValue(i).toString() : "");
        row = row.replace("%TICK_KIJUNSEN%", (ichimokuAnalyzer.getKijunSen().getValue(i) != null) ? ichimokuAnalyzer.getKijunSen().getValue(i).toString() : "");
        row = row.replace("%TICK_CHIKOUSPAN%", (ichimokuAnalyzer.getChikouSpan().getValue(i) != null) ? ichimokuAnalyzer.getChikouSpan().getValue(i).toString() : "");
        return row;
    }

    private double getChartVaxisMargin(int size) {
        double max = getMaxPriceInPeriod(size);
        double min = getMinPriceInPeriod(size);
        return (max - min) * CHART_VAXIS_MARGIN_PERCENT / 100;
    }

    private double getMaxPriceInPeriod(int size) {
        double max = 0;
        for (int i = timeSeries.getTickCount() - size - 1; i < timeSeries.getTickCount(); i++) {
            if (max < timeSeries.getTick(i).getMaxPrice().toDouble()) max = timeSeries.getTick(i).getMaxPrice().toDouble();
        }
        return max;
    }

    private double getMinPriceInPeriod(int size) {
        double min = Double.MAX_VALUE;
        for (int i = timeSeries.getTickCount() - size - 1; i < timeSeries.getTickCount(); i++) {
            if (min > timeSeries.getTick(i).getMaxPrice().toDouble()) min = timeSeries.getTick(i).getMaxPrice().toDouble();
        }
        return min;
    }

    private String loadFile(String filePath) throws IOException {
        Path path = Paths.get(filePath);
        Charset charset = StandardCharsets.UTF_8;

        return new String(Files.readAllBytes(path), charset);
    }

    private void saveFile(String content, String filePath) throws IOException {
        Path path = Paths.get(filePath);
        Charset charset = StandardCharsets.UTF_8;
        if (Files.exists(path)) {
            Files.delete(path);
        }
        Files.createFile(path);
        Files.write(path, content.getBytes(charset));
    }

    private String generateData(int size, String template) throws IOException {
        StringBuilder builder = new StringBuilder();
        for (int i = timeSeries.getTickCount() - size; i < timeSeries.getTickCount(); i++) {
            builder.append(prepareRow(i));
        }
        template = template.replaceAll("%%DATA%%", builder.toString());
        return template;
    }

    private String getTargetFileName(String stockName) {
        return TARGET_DIR + "/" + stockName + ".html";
    }

    public void generateChart(String stockName, int size) throws IOException {
        String content = loadFile(TEMPLATE_PATH);
        content = generateData(size, content);
        content = content.replaceAll("%%STOCKNAME%%", stockName);
        content = content.replaceAll("%%VAXIS_MAX%%", String.valueOf(getMaxPriceInPeriod(size) + getChartVaxisMargin(size)));
        content = content.replaceAll("%%VAXIS_MIN%%", String.valueOf(getMinPriceInPeriod(size) - getChartVaxisMargin(size)));
        saveFile(content, getTargetFileName(stockName));
    }

}
