package org.hou.stocktank.charts;

import java.awt.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import org.hou.stocktank.indicators.pivotpoints.PivotPoint;
import org.hou.stocktank.indicators.pivotpoints.SimplePivotPointIndicator;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.DateAxis;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.plot.Marker;
import org.jfree.chart.plot.ValueMarker;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.XYItemRenderer;
import org.jfree.chart.renderer.xy.XYLineAndShapeRenderer;
import org.jfree.data.time.Minute;
import org.jfree.data.time.RegularTimePeriod;
import org.jfree.data.time.TimeSeriesCollection;
import org.jfree.data.xy.AbstractXYDataset;
import org.jfree.data.xy.DefaultOHLCDataset;
import org.jfree.data.xy.OHLCDataItem;
import org.jfree.data.xy.XYDataset;
import org.jfree.ui.ApplicationFrame;
import org.jfree.ui.RectangleAnchor;
import org.jfree.ui.RefineryUtilities;
import org.joda.time.DateTime;

import org.ta4j.core.Decimal;
import org.ta4j.core.Indicator;
import org.ta4j.core.Tick;
import org.ta4j.core.TimeSeries;

public class MagicChart {
    private int windowWidth = 1000;
    private int windowHeight = 500;
    private String title = "Title";
    private XYPlot mainPlot = null;
    private JFreeChart chart;
    private List<MagicLayer> layerList = new ArrayList<>();

    public MagicChart withLayer(MagicLayer layer) {
        layerList.add(layer);
        return this;
    }

    public MagicChart withTitle(String title) {
        this.title = title;
        return this;
    }

    public MagicChart withWidht(int width) {
        this.windowWidth = width;
        return this;
    }

    public MagicChart withHeight(int height) {
        this.windowHeight = height;
        return this;
    }

    private RegularTimePeriod getMiddleTime(DateTime beginTime, DateTime endTime) {
        return new Minute(new DateTime(beginTime.getMillis() + (endTime.getMillis() - beginTime.getMillis()) / 2).toDate());
    }

    private org.jfree.data.time.TimeSeries buildLineChartTimeSeries(String name, TimeSeries tickSeries, Indicator<Decimal> indicator) {
        org.jfree.data.time.TimeSeries chartTimeSeries = new org.jfree.data.time.TimeSeries(name);
        for (int i = 0; i < tickSeries.getTickCount(); i++) {
            Tick tick = tickSeries.getTick(i);
            chartTimeSeries.add(new Minute(Date.from(tick.getEndTime().toInstant())), indicator.getValue(i).toDouble());
        }
        return chartTimeSeries;
    }

    private org.jfree.data.time.TimeSeries buildLineChartTimeSeriesFromPivotPointIndicator(String name, TimeSeries timeSeries, Indicator<PivotPoint> indicator) {
        org.jfree.data.time.TimeSeries chartTimeSeries = new org.jfree.data.time.TimeSeries(name);
        for (int i = 0; i < timeSeries.getTickCount(); i++) {
            if (indicator.getValue(i).getIndex() == i) {
                Tick tick = timeSeries.getTick(i);
                chartTimeSeries.add(new Minute(Date.from(tick.getEndTime().toInstant())), indicator.getValue(i).getValue().toDouble());
            }
        }
        return chartTimeSeries;
    }

    private OHLCDataItem[] buildCandlestickChartTimeSeries(String name, TimeSeries series) {
        List<OHLCDataItem> dataItems = new ArrayList<>();
        org.jfree.data.time.TimeSeries chartTimeSeries = new org.jfree.data.time.TimeSeries(name);
        for (int i = 0; i < series.getTickCount(); i++) {
            Tick tick = series.getTick(i);
            OHLCDataItem item = new OHLCDataItem(
                    Date.from(tick.getEndTime().toInstant()),
                    tick.getOpenPrice().toDouble(),
                    tick.getMaxPrice().toDouble(),
                    tick.getMinPrice().toDouble(),
                    tick.getClosePrice().toDouble(),
                    tick.getVolume().toDouble());
            dataItems.add(item);
        }
        Collections.reverse(dataItems);
        OHLCDataItem[] result = dataItems.toArray(new OHLCDataItem[dataItems.size()]);
        return result;
    }

    private AbstractXYDataset getOHLCDataSet(String name, TimeSeries series) {
        return new DefaultOHLCDataset(name, buildCandlestickChartTimeSeries(name, series));
    }

    private void prepareChart() {
        int axisCounter = 0;
        int chartIndex = 0;
        DateAxis domainAxis = new DateAxis("Date");
        final NumberAxis rangeAxis = new NumberAxis("xxx");
        for (MagicLayer layer : layerList) {
            for (MagicDataPlot chart : layer.getDataPlots()) {
                XYDataset chartDataset = null;
                XYItemRenderer chartRenderer = null;
                switch (chart.getRenderer()) {
                    case CANDLESTICK:
                        chartDataset = getOHLCDataSet(chart.getName(), chart.getSeries());
                        chartRenderer = new MagicCandlesticRenderer();
                        break;
                    case LINE:
                    case DEFAULT:
                        if (chart.getIndicator() instanceof SimplePivotPointIndicator) {
                            chartDataset = new TimeSeriesCollection(buildLineChartTimeSeriesFromPivotPointIndicator("pivot points", chart.getSeries(), chart.getIndicator()));
                            chartRenderer = new XYLineAndShapeRenderer(true, false);
                            chartRenderer.setSeriesPaint(0, chart.getPaint());
                            break;
                        }
                        chartDataset = new TimeSeriesCollection(buildLineChartTimeSeries(chart.getName(), chart.getSeries(), chart.getIndicator()));
                        chartRenderer = new XYLineAndShapeRenderer(true, false);
                        chartRenderer.setSeriesPaint(0, chart.getPaint());
                        break;
                }
                if (mainPlot == null) {
                    mainPlot = new XYPlot(chartDataset, domainAxis, rangeAxis, chartRenderer);
                } else {
                    mainPlot.setDataset(chartIndex, chartDataset);
                    mainPlot.setRenderer(chartIndex, chartRenderer);
                    mainPlot.setRangeAxis(axisCounter, rangeAxis);
                    mainPlot.mapDatasetToRangeAxis(chartIndex,axisCounter);
                }
                chartIndex++;
            }
            for(MagicMarker marker : layer.getVMarkers()) {
                Marker markerX = new ValueMarker(marker.getValue());
                markerX.setPaint(marker.getPaint());
                markerX.setLabel(marker.getLabel());
                markerX.setLabelAnchor(RectangleAnchor.BOTTOM_RIGHT);
                mainPlot.addDomainMarker(axisCounter, markerX, org.jfree.ui.Layer.FOREGROUND);
            }

            axisCounter ++;
        }
        chart = new JFreeChart(title, null, mainPlot, false);
    }

    private void renderChart() {
        // MagicDataPlot panel
        ChartPanel panel = new ChartPanel(chart);
        //panel.setFillZoomRectangle(true);
        panel.setRangeZoomable(true);
        panel.setDomainZoomable(true);
        panel.setMouseWheelEnabled(true);
        panel.setPreferredSize(new Dimension(windowWidth, windowHeight));
        // Application frame
        ApplicationFrame frame = new ApplicationFrame(title);
        frame.setContentPane(panel);
        frame.pack();
        RefineryUtilities.centerFrameOnScreen(frame);
        frame.setVisible(true);
    }

    public void showChart() {
        prepareChart();
        renderChart();
    }

    public JFreeChart getChart() {
        prepareChart();
        return chart;
    }


}
