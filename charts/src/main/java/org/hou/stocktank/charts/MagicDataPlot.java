package org.hou.stocktank.charts;

import java.awt.*;

import org.ta4j.core.Indicator;
import org.ta4j.core.TimeSeries;

public class MagicDataPlot {
    private String name;
    private TimeSeries series;
    private Indicator indicator;
    private Paint paint;
    private MagicRendererType renderer;

    public MagicDataPlot(String name, TimeSeries series, Paint paint, MagicRendererType renderer) {
        this.name = name;
        this.series = series;
        this.paint = paint;
        this.renderer = renderer;
    }

    public MagicDataPlot(String name, Indicator indicator, TimeSeries series, Paint paint, MagicRendererType renderer) {
        this.name = name;
        this.indicator = indicator;
        this.paint = paint;
        this.renderer = renderer;
        this.series = series;
    }

    public String getName() {
        return name;
    }

    public TimeSeries getSeries() {
        return series;
    }

    public Paint getPaint() {
        return paint;
    }

    public MagicRendererType getRenderer() {
        return renderer;
    }

    public Indicator getIndicator() {
        return indicator;
    }
}
