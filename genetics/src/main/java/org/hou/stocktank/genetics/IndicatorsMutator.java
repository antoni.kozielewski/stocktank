package org.hou.stocktank.genetics;

import org.hou.stocktank.base.IndicatorDefinition;

public interface IndicatorsMutator {
    IndicatorDefinition mutate(IndicatorDefinition source);
}
