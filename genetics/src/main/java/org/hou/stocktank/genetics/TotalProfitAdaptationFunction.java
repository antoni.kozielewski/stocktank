package org.hou.stocktank.genetics;

import org.hou.stocktank.base.StrategyFactory;

import org.ta4j.core.TimeSeries;
import org.ta4j.core.TimeSeriesManager;
import org.ta4j.core.TradingRecord;
import org.ta4j.core.analysis.criteria.TotalProfitCriterion;

public class TotalProfitAdaptationFunction implements AdaptationFunction {
    private TimeSeries series;

    public TotalProfitAdaptationFunction(TimeSeries series) {
        this.series = series;
    }

    @Override
    public double checkAdaptation(StrategyFactory st) {
        TimeSeriesManager manager = new TimeSeriesManager(series);
        TradingRecord tradingRecord = manager.run(st.buildStrategy(series));
        TotalProfitCriterion totalProfit = new TotalProfitCriterion();
        double result = totalProfit.calculate(series, tradingRecord);
        System.out.println(">> " + tradingRecord.getTrades().size() + " :: " + st.toString());
        return result;
    }
}
