package org.hou.stocktank.genetics;

import org.hou.stocktank.base.IndicatorName;
import org.hou.stocktank.base.StrategyFactory;

public class BasicStrategyFactoryChildMaker implements StrategyFactoryChildMaker {
    private IndicatorChildMaker indicatorChildMaker;

    public BasicStrategyFactoryChildMaker(IndicatorChildMaker indicatorChildMaker) {
        this.indicatorChildMaker = indicatorChildMaker;
    }

    private StrategyFactory randomParent(StrategyFactory parentA, StrategyFactory parentB) {
        if (Math.random() < 0.5) return parentA;
        else return parentB;
    }

    @Override
    public StrategyFactory makeChild(StrategyFactory parentA, StrategyFactory parentB) {
        StrategyFactory child = randomParent(parentA, parentB);
        for (IndicatorName name : child.getIndicatorsDefinitions().keySet()) {
            child.setIndicatorDefinition(name, indicatorChildMaker.makeChild(parentA.getIndicatorsDefinitions().get(name), parentB.getIndicatorsDefinitions().get(name)));
        }
        return child;
    }
}
