package org.hou.stocktank.genetics;

import org.hou.stocktank.base.StrategyFactory;

public interface StrategyFactoryChildMaker {
    StrategyFactory makeChild(StrategyFactory parentA, StrategyFactory parentB);
}
