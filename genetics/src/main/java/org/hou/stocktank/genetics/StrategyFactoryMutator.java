package org.hou.stocktank.genetics;

import org.hou.stocktank.base.StrategyFactory;

public interface StrategyFactoryMutator {
    StrategyFactory mutate(StrategyFactory source);
}
