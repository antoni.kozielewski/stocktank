package org.hou.stocktank.genetics;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;
import java.util.UUID;

import org.hou.stocktank.base.IndicatorDefinition;
import org.hou.stocktank.base.IndicatorName;
import org.hou.stocktank.base.StrategyFactory;

import org.ta4j.core.*;
import org.ta4j.core.analysis.criteria.TotalProfitCriterion;

public class Population {
    private Map<String, StrategyFactory> population = new HashMap<>();
    private List<AdaptationDescriptor> adaptation = new ArrayList<>();
    private GenerationDescriptor generationDescriptor;
    private List<GenerationDescriptor> generationHistory = new ArrayList<>();
    private TimeSeries series;
    private int maxPopulationSize = 500; // population size
    private Double killRate = 0.3d; // how many die in every generation
    private Double mutationRate = 0.3d; // how many mutate
    private Double notMutated = 0.1d; // how big is best adaptated subjects not mutate
    private int generation = 0; // generation counter

    private Double child2random = 0.5; // children/randoms - how many children is born to how many totaly new subjects

    private Double indicatorMutationRate = 0.3d; // probability of change
    private Integer indicatorMutatorMaxChange = 2; // max change in one mutation

    private AdaptationFunction adaptationFunction;
    private IndicatorsMutator indicatorsMutator = new BasicIndicatorMutator(indicatorMutationRate, indicatorMutatorMaxChange);
    private StrategyFactoryMutator mutator = new BasicStrategyFactoryMutator(indicatorsMutator);

    private StrategyFactory seed; // the first strategyFactory that produces all others

    public Population(int maxPopulationSize, StrategyFactory seed, TimeSeries series) {
        this.maxPopulationSize = maxPopulationSize;
        this.seed = seed;
        this.series = series;
        this.adaptationFunction = new TotalProfitAdaptationFunction(series);
    }

    public void prepareNewRandom() {
        generationDescriptor.randoms = maxPopulationSize - population.size();
        for (int i = population.size(); i < maxPopulationSize; i++) {
            population.put(UUID.randomUUID().toString(), seed.createRandom());
        }
    }

    public void validateAlive() {
        int counter = 0;
        Map<String, StrategyFactory> newGeneration = new HashMap<>();
        for (String key : population.keySet()) {
            if (population.get(key).validateConfiguration()) {
                newGeneration.put(key, population.get(key));
            } else {
                counter++;
            }
        }
        population = newGeneration;
        generationDescriptor.accidents = counter;
    }

    public void calculateAdaptation() {
        adaptation.clear();
        generationDescriptor.avgAdaptation = null;
        generationDescriptor.maxAdaptation = Double.MIN_VALUE;
        generationDescriptor.bestSubject = null;
        for (String key : population.keySet()) {
            StrategyFactory sf = population.get(key);
            Strategy strategy = sf.buildStrategy(series);
            TimeSeriesManager manager = new TimeSeriesManager(series);
            TradingRecord tradingRecord = manager.run(strategy);

            TotalProfitCriterion totalProfit = new TotalProfitCriterion();
            Double adaptationValue = totalProfit.calculate(series, tradingRecord);

//            Double adaptationValue = adaptationFunction.checkAdaptation(population.get(key));
            adaptation.add(new AdaptationDescriptor(key, adaptationValue));
            if (generationDescriptor.maxAdaptation < adaptationValue) {
                generationDescriptor.maxAdaptation = adaptationValue;
                generationDescriptor.bestSubject = sf;
            }
            if (generationDescriptor.avgAdaptation == null) {
                generationDescriptor.avgAdaptation = adaptationValue;
            } else {
                generationDescriptor.avgAdaptation = (generationDescriptor.avgAdaptation + adaptationValue) / 2;
            }
            // adaptation musi byc posortowane
            adaptation.sort(new Comparator<AdaptationDescriptor>() {
                @Override
                public int compare(AdaptationDescriptor o1, AdaptationDescriptor o2) {
                    if (o1.value < o2.value) return -1;
                    else if (o1.value > o2.value) return 1;
                    return 0;
                }
            });
        }
    }

    public void kill() {
        Set<String> subjects = population.keySet();
        List<AdaptationDescriptor> newAdaptation = adaptation;
        for (int i = 0; i < population.size() * killRate; i++) {
            subjects.remove(adaptation.get(i).key);
            adaptation.remove(adaptation.get(i));
            generationDescriptor.killed++;
        }
        adaptation = newAdaptation;
        Map<String, StrategyFactory> newGeneration = new HashMap<>();
        for (String key : population.keySet()) {
            if (subjects.contains(key)) newGeneration.put(key, population.get(key));
        }
        this.population = newGeneration;
    }

    public void mutate() {
        int numberOfMutations = (int) (adaptation.size() * mutationRate);
        for (int i = 0; i < numberOfMutations; i++) {
            int randomId = (int) (Math.random() * (adaptation.size() - adaptation.size() * notMutated));
            String mutationKey = adaptation.get(randomId).key;
            if (population.get(mutationKey) != null) {
                StrategyFactory sf = mutator.mutate(population.get(mutationKey));
                sf.setLifeDescription(sf.getDescription() + "m");
                population.put(mutationKey, sf);
                generationDescriptor.mutated++;
            } else {
                System.out.println(" null mutated key : " + mutationKey);
            }
        }
    }

    private StrategyFactory randomParent() {
        Random generator = new Random();
        List<StrategyFactory> values = new ArrayList(population.values());
        return values.get(generator.nextInt(values.size()));
    }

    public void generateChildrens() {
        int howMany = (int) (child2random * (maxPopulationSize - population.size()));
        for (int i = 0; i < howMany; i++) {
            StrategyFactory parentA = randomParent();
            StrategyFactory parentB = randomParent();
            StrategyFactory child = parentA.createClone();
            for (IndicatorName indicatorName : child.getIndicatorsDefinitions().keySet()) {
                IndicatorDefinition newIndicatorDefinition = new IndicatorDefinition(indicatorName, parentA.getIndicatorsDefinitions().get(indicatorName).getBaseIndicator());
                for (String param : parentA.getIndicatorsDefinitions().get(indicatorName).getParams().keySet()) {
                    Integer value = (Math.random() < 0.5) ? parentA.getIndicatorsDefinitions().get(indicatorName).getParam(param) : parentB.getIndicatorsDefinitions().get(indicatorName).getParam(param);
                    child.setParam(indicatorName, param, value);
                }
                for (String level : parentA.getIndicatorsDefinitions().get(indicatorName).getLevels().getParams().keySet()) {
                    Integer value = (Math.random() < 0.5) ? parentA.getIndicatorsDefinitions().get(indicatorName).getLevel(level) : parentB.getIndicatorsDefinitions().get(indicatorName).getLevel(level);
                    child.withLevel(indicatorName, level, value);
                }
            }
            String uuid = UUID.randomUUID().toString();
            child.setLifeDescription("[born:" + generation + "c]");
            population.put(uuid, child);
            generationDescriptor.children++;
        }
    }

    public void singleGeneration() {
        generation++;
        generationDescriptor = new GenerationDescriptor(generation);
        prepareNewRandom();
        validateAlive();
        if (population.size() == 0) {
            throw new RuntimeException("!!! NOBODY SURVIVED !!!");
        }
        calculateAdaptation();
        kill();
        mutate();
        generateChildrens();
        generationHistory.add(generationDescriptor);
    }

    public void printGenerationsHistory() {
        for (GenerationDescriptor desc : generationHistory) {
            System.out.println("---------------------------------------------------------");
            System.out.println(" generation : " + desc.generation);
            System.out.println(" killed   : " + desc.killed);
            System.out.println(" mutated  : " + desc.mutated);
            System.out.println(" accidents: " + desc.accidents);
            System.out.println(" children : " + desc.children);
            System.out.println(" randoms  : " + desc.randoms);
            System.out.println(" -------");
            System.out.println(" avg=" + desc.avgAdaptation + ", max=" + desc.maxAdaptation + " : " + desc.bestSubject);

        }
    }

    public String getLastGenerationStats() {
        GenerationDescriptor desc = generationHistory.get(generationHistory.size() - 1);
        String result = "[" + desc.generation + "] ";
        result += " (avg=" + desc.avgAdaptation + ", max=" + desc.maxAdaptation + ") ";
        result += " k:" + desc.killed;
        result += ", m:" + desc.mutated;
        result += ", a:" + desc.accidents;
        result += ", c:" + desc.children;
        result += ", r:" + desc.randoms;
        result += "\n" + desc.bestSubject.toString();
        return result;
    }

    public StrategyFactory getBest() {
        return population.get(adaptation.get(adaptation.size()-1).key);
    }

    public void printPopulation() {
        for (AdaptationDescriptor ad : adaptation) System.out.println(ad.key + " :: " + population.get(ad.key) + " | " + ad.value);
    }

    public class AdaptationDescriptor {
        protected String key;
        protected Double value;
        public AdaptationDescriptor(String key, Double value) {
            this.key = key;
            this.value = value;
        }
    }

    public class GenerationDescriptor {
        protected int generation = -1;
        protected int killed = 0;
        protected int mutated = 0;
        protected int children = 0;
        protected int randoms = 0;   // new randoms
        protected int accidents = 0;  // validated configuration = false
        protected Double avgAdaptation = null;
        protected Double maxAdaptation = Double.MIN_VALUE;
        protected StrategyFactory bestSubject = null;

        public GenerationDescriptor(int generation) {
            this.generation = generation;
        }
    }


}
