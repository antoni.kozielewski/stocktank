package org.hou.stocktank.genetics;

import org.hou.stocktank.base.IndicatorDefinition;

public class BasicIndicatorMutator implements IndicatorsMutator {
    private Double mutationRate = 0.1;
    private Integer maxChange = 3;

    public BasicIndicatorMutator(Double muationRate, Integer maxChange) {
        this.maxChange = maxChange;
        this.mutationRate = muationRate;
    }

    @Override
    public IndicatorDefinition mutate(IndicatorDefinition source) {
        for (String name : source.getParams().keySet()) {
            if (Math.random() <= mutationRate) {
                int randomValue = -maxChange + (int) (Math.random() * 2 * maxChange);
                source.setParam(name, Math.abs(source.getParam(name) + randomValue) + 1);
            }
        }
        for (String name : source.getLevels().getParams().keySet()) {
            if (Math.random() <= mutationRate) {
                int randomValue = -maxChange + (int) (Math.random() * 2 * maxChange);
                source.setLevel(name, Math.abs(source.getLevel(name) + randomValue) + 1);
            }
        }
        return source;
    }
}
