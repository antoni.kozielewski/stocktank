package org.hou.stocktank.genetics;

import org.hou.stocktank.base.StrategyFactory;

public interface AdaptationFunction {
    /**
     * sprawdza przystosowanie do srodowiska pojedynczego osobnika - im wiecej tym lepiej przystosowany | te co mają mniej niż 1 powinny ginac z automatu
     *
     * @param st
     * @return
     */
    double checkAdaptation(StrategyFactory st);
}
