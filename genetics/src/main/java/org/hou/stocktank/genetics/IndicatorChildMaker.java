package org.hou.stocktank.genetics;

import org.hou.stocktank.base.IndicatorDefinition;

public interface IndicatorChildMaker {

    IndicatorDefinition makeChild(IndicatorDefinition parentA, IndicatorDefinition parentB);
}
