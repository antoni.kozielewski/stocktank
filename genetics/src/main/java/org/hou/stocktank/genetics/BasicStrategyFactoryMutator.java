package org.hou.stocktank.genetics;

import org.hou.stocktank.base.Configurable;
import org.hou.stocktank.base.IndicatorName;
import org.hou.stocktank.base.StrategyFactory;

public class BasicStrategyFactoryMutator implements StrategyFactoryMutator {
    private IndicatorsMutator indicatorsMutator;
    private Double levelsMutationRate = 0.1;
    private Integer levelMaxMutation = 3;

    public BasicStrategyFactoryMutator(IndicatorsMutator indicatorsMutator) {
        this.indicatorsMutator = indicatorsMutator;
    }


    public BasicStrategyFactoryMutator withLevelsMutationRate(Double rate) {
        this.levelsMutationRate = rate;
        return this;
    }

    public BasicStrategyFactoryMutator withMaxLevelMutation(Integer maxMutation) {
        this.levelMaxMutation = maxMutation;
        return this;
    }

    private Configurable mutateLevels(Configurable levels) {
        for (String name : levels.getParams().keySet()) {
            if (Math.random() < levelsMutationRate) {
                int randomValue = -levelMaxMutation + (int) (Math.random() * 2 * levelMaxMutation);
                levels.setParam(name, Math.abs(levels.getParam(name) + randomValue) + 1);
            }
        }
        return levels;
    }

    @Override
    public StrategyFactory mutate(StrategyFactory source) {
        for (IndicatorName indicatorName : source.getIndicatorsDefinitions().keySet()) {
            source.setIndicatorDefinition(indicatorName, indicatorsMutator.mutate(source.getIndicatorsDefinitions().get(indicatorName)));
        }
        return source;
    }
}
