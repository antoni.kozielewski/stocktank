package org.hou.stocktank.genetics;

import org.hou.stocktank.base.IndicatorDefinition;

public class BasicIndicatorChildMaker implements IndicatorChildMaker {

    private IndicatorDefinition randomParent(IndicatorDefinition parentA, IndicatorDefinition parentB) {
        if (Math.random() < 0.5) return parentA;
        else return parentB;
    }

    @Override
    public IndicatorDefinition makeChild(IndicatorDefinition parentA, IndicatorDefinition parentB) {
        IndicatorDefinition child = new IndicatorDefinition(parentA.getIndicatorName(), randomParent(parentA, parentB).getBaseIndicator());
        for (String param : parentA.getParams().keySet()) {
            child.setParam(param, randomParent(parentA, parentB).getParam(param));
        }
        for (String level : parentA.getLevels().getParams().keySet()) {
            child.setLevel(level, randomParent(parentA, parentB).getLevel(level));
        }
        return child;
    }
}
