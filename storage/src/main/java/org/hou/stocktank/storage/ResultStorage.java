package org.hou.stocktank.storage;

import java.util.Map;

public interface ResultStorage {

    void addResult(Map<String, String> data);

    Iterable<Map<String, String>> getIterator();
}
