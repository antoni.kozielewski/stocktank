package org.hou.stocktank.storage;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class PersistentResultStorage implements ResultStorage {
    private static final Logger logger = LoggerFactory.getLogger(PersistentResultStorage.class);
    private static final String LINE_SEPARATOR = "\n";
    private String storageName = "default";
    private String path = "";


    public class BufferedReaderIterator implements Iterable<Map<String, String>> {
        GsonBuilder builder = new GsonBuilder();
        private BufferedReader r;

        public BufferedReaderIterator(BufferedReader r) {
            this.r = r;
        }

        @Override
        public Iterator<Map<String, String>> iterator() {
            return new Iterator<Map<String, String>>() {

                @Override
                public boolean hasNext() {
                    try {
                        r.mark(1);
                        if (r.read() < 0) {
                            return false;
                        }
                        r.reset();
                        return true;
                    } catch (IOException e) {
                        return false;
                    }
                }

                private Map<String, String> deserializeResult(String input) {
                    Gson gson = builder.create();
                    return gson.fromJson(input, HashMap.class);
                }

                @Override
                public Map<String, String> next() {
                    try {
                        return deserializeResult(r.readLine());
                    } catch (IOException e) {
                        return null;
                    }
                }

                @Override
                public void remove() {
                    throw new UnsupportedOperationException();
                }
            };
        }

    }


    public PersistentResultStorage(String storageName, String path) {
        this.storageName = storageName;
        this.path = path;
    }

    private String json(Map<String, String> data) {
        GsonBuilder builder = new GsonBuilder();
        Gson gson = builder.create();
        return gson.toJson(data);
    }

    private String getFilename() {
        return path + "/" + storageName + "_storage.data";
    }

    private void appendFile(String data) {
        try {
            if (!new File(getFilename()).exists()) {
                new File(getFilename()).createNewFile();
                logger.debug("new file created : " + getFilename());
            }
            Files.write(Paths.get(getFilename()), (LINE_SEPARATOR + data).getBytes(), StandardOpenOption.APPEND);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void addResult(Map<String, String> data) {
        appendFile(json(data));
    }

    @Override
    public Iterable<Map<String, String>> getIterator() {
        BufferedReader bufferedReader = null;
        try {
            bufferedReader = new BufferedReader(new FileReader(getFilename()));
        } catch (FileNotFoundException e) {
            throw new RuntimeException(e);
        }
        return new BufferedReaderIterator(bufferedReader);
    }
}
